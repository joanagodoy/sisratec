/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import dominio.Usuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
public class AcessoFilter implements Filter {

    private static final boolean debug = false;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public AcessoFilter() {
    }

//    private HashMap<String, ArrayList<String>> permissoes() {
//
//        HashMap<String, ArrayList<String>> hashPermissoes = new HashMap<String, ArrayList<String>>();
//        ArrayList<String> permissoesAdmin = new ArrayList<String>();
//        ArrayList<String> permissoesAnalista = new ArrayList<String>();
//        permissoesAnalista.add("BuscaGraficoBateria");
//        permissoesAnalista.add("gerarRelatorioPerformance");
//        permissoesAnalista.add("gerarRelatorioAcompanhamentoDiario");
//        permissoesAnalista.add("listarVisitas");
//        permissoesAnalista.add("RelatorioPerformance");
//        permissoesAnalista.add("AlteraSenha");
//        permissoesAnalista.add("AlterarSenha");
//        permissoesAnalista.add("Monitoramento");
//        permissoesAnalista.add("LocalizacaoCliente");
//        permissoesAnalista.add("BuscarVendedoresRota");
//        permissoesAnalista.add("BuscarMapaNovo");
//        permissoesAnalista.add("LocalizarCliente");
//        permissoesAnalista.add("MapaLocalizacao");
//        permissoesAnalista.add("ObterCoordenadas");
//        permissoesAnalista.add("ObterTodasCoordenadas");
//        permissoesAnalista.add("ObterQtdLocalizacoes");
//        permissoesAnalista.add("atualizarLatitudeLongitude");
//        permissoesAnalista.add("Logout");
//
//        permissoesAdmin.addAll(permissoesAnalista);
//        permissoesAdmin.add("Usuarios");
//        permissoesAdmin.add("BuscarUsuarios");
//        permissoesAdmin.add("AlteraUsuarios");
//        permissoesAdmin.add("AlterarUsuarios");
//        permissoesAdmin.add("AlteraConfiguracao");
//        permissoesAdmin.add("AlterarConfiguracao");
//        permissoesAdmin.add("Configuracao");
//        permissoesAdmin.add("legenda_mapa_novo.jsp");
//        permissoesAdmin.add("legenda2_mapa_novo.jsp");
//        hashPermissoes.put("admin", permissoesAdmin);
//        hashPermissoes.put("analista", permissoesAnalista);
//        return hashPermissoes;
//
//    }

    private boolean possuiAcesso(int tipo, String pagina) {
        return true;/*
         if (pagina.equals("Home")) {
         return true;
         }
         if (permissoes().get(tipo).contains(pagina)) {
         return true;
         }
         //        System.out.println("Sem acesso a URL " + pagina + ".");
         return false;*/

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        boolean logado = false;
        String pagina = req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);
        Usuario usuario;
        try {
            usuario = (Usuario) req.getSession().getAttribute("usuario");
        } catch (Exception e) {
            usuario = null;
        }
        if (usuario != null) {
            logado = true;
        }
        if (req.getRequestURI().contains("/apk") || req.getRequestURI().contains("buscarNotificacoes") || req.getRequestURI().contains("/apk") || req.getRequestURI().contains("/EXT.") || req.getRequestURI().contains("/Android.") || req.getRequestURI().endsWith("/EfetuarLogin") || req.getRequestURI().contains("/css/") || req.getRequestURI().contains("/js/") || req.getRequestURI().contains("/img/") || req.getRequestURI().contains("/js/libs") || req.getRequestURI().contains("/assets/") || req.getRequestURI().contains("/dist/")) {
            chain.doFilter(request, response);
            return;
        }
        if (!logado && !pagina.isEmpty()) {
            res.sendRedirect("");
        }
        if (pagina.isEmpty() || pagina.endsWith(".jsp") || req.getRequestURI().endsWith("/EfetuarLogin")) {
            if (logado) {
                res.sendRedirect("Home");
                return;
            }
        }
        if (logado && !possuiAcesso(usuario.getTipo().getId(), pagina)) {
            res.sendRedirect("Home");
            return;
        }
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
