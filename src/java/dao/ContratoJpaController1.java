/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.StatusContrato;
import dominio.Usuario;
import dominio.Cliente;
import dominio.Contrato;
import dominio.ContratoFoto;
import java.util.ArrayList;
import java.util.List;
import dominio.Visita;
import dominio.Imovel;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class ContratoJpaController1 implements Serializable {

    public ContratoJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Contrato contrato) throws PreexistingEntityException, Exception {
        if (contrato.getContratoFotoList() == null) {
            contrato.setContratoFotoList(new ArrayList<ContratoFoto>());
        }
        if (contrato.getVisitaList() == null) {
            contrato.setVisitaList(new ArrayList<Visita>());
        }
        if (contrato.getImovelList() == null) {
            contrato.setImovelList(new ArrayList<Imovel>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusContrato status = contrato.getStatus();
            if (status != null) {
                status = em.getReference(status.getClass(), status.getId());
                contrato.setStatus(status);
            }
//            Usuario vendedor = contrato.getVendedor();
//            if (vendedor != null) {
//                vendedor = em.getReference(vendedor.getClass(), vendedor.getId());
//                contrato.setVendedor(vendedor);
//            }
            Cliente cliente = contrato.getCliente();
            if (cliente != null) {
                cliente = em.getReference(cliente.getClass(), cliente.getId());
                contrato.setCliente(cliente);
            }
            Usuario usuario = contrato.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                contrato.setUsuario(usuario);
            }
            List<ContratoFoto> attachedContratoFotoList = new ArrayList<ContratoFoto>();
            for (ContratoFoto contratoFotoListContratoFotoToAttach : contrato.getContratoFotoList()) {
                contratoFotoListContratoFotoToAttach = em.getReference(contratoFotoListContratoFotoToAttach.getClass(), contratoFotoListContratoFotoToAttach.getId());
                attachedContratoFotoList.add(contratoFotoListContratoFotoToAttach);
            }
            contrato.setContratoFotoList(attachedContratoFotoList);
            List<Visita> attachedVisitaList = new ArrayList<Visita>();
            for (Visita visitaListVisitaToAttach : contrato.getVisitaList()) {
                visitaListVisitaToAttach = em.getReference(visitaListVisitaToAttach.getClass(), visitaListVisitaToAttach.getId());
                attachedVisitaList.add(visitaListVisitaToAttach);
            }
            contrato.setVisitaList(attachedVisitaList);
            List<Imovel> attachedImovelList = new ArrayList<Imovel>();
            for (Imovel imovelListImovelToAttach : contrato.getImovelList()) {
                imovelListImovelToAttach = em.getReference(imovelListImovelToAttach.getClass(), imovelListImovelToAttach.getId());
                attachedImovelList.add(imovelListImovelToAttach);
            }
            contrato.setImovelList(attachedImovelList);
            em.persist(contrato);
            if (status != null) {
                status.getContratoList().add(contrato);
                status = em.merge(status);
            }
//            if (vendedor != null) {
//                vendedor.getContratoList().add(contrato);
//                vendedor = em.merge(vendedor);
//            }
//            if (cliente != null) {
//                cliente.getContratoList().add(contrato);
//                cliente = em.merge(cliente);
//            }
//            if (usuario != null) {
//                usuario.getContratoList().add(contrato);
//                usuario = em.merge(usuario);
//            }
            for (ContratoFoto contratoFotoListContratoFoto : contrato.getContratoFotoList()) {
                Contrato oldContratoOfContratoFotoListContratoFoto = contratoFotoListContratoFoto.getContrato();
                contratoFotoListContratoFoto.setContrato(contrato);
                contratoFotoListContratoFoto = em.merge(contratoFotoListContratoFoto);
                if (oldContratoOfContratoFotoListContratoFoto != null) {
                    oldContratoOfContratoFotoListContratoFoto.getContratoFotoList().remove(contratoFotoListContratoFoto);
                    oldContratoOfContratoFotoListContratoFoto = em.merge(oldContratoOfContratoFotoListContratoFoto);
                }
            }
            for (Visita visitaListVisita : contrato.getVisitaList()) {
                Contrato oldNrContratoOfVisitaListVisita = visitaListVisita.getNrContrato();
                visitaListVisita.setNrContrato(contrato);
                visitaListVisita = em.merge(visitaListVisita);
                if (oldNrContratoOfVisitaListVisita != null) {
                    oldNrContratoOfVisitaListVisita.getVisitaList().remove(visitaListVisita);
                    oldNrContratoOfVisitaListVisita = em.merge(oldNrContratoOfVisitaListVisita);
                }
            }
            for (Imovel imovelListImovel : contrato.getImovelList()) {
                Contrato oldNrContratoOfImovelListImovel = imovelListImovel.getNrContrato();
                imovelListImovel.setNrContrato(contrato);
                imovelListImovel = em.merge(imovelListImovel);
                if (oldNrContratoOfImovelListImovel != null) {
                    oldNrContratoOfImovelListImovel.getImovelList().remove(imovelListImovel);
                    oldNrContratoOfImovelListImovel = em.merge(oldNrContratoOfImovelListImovel);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findContrato(contrato.getNrContrato()) != null) {
                throw new PreexistingEntityException("Contrato " + contrato + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Contrato contrato) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Contrato persistentContrato = em.find(Contrato.class, contrato.getNrContrato());
            StatusContrato statusOld = persistentContrato.getStatus();
            StatusContrato statusNew = contrato.getStatus();
//            Usuario vendedorOld = persistentContrato.getVendedor();
//            Usuario vendedorNew = contrato.getVendedor();
            Cliente clienteOld = persistentContrato.getCliente();
            Cliente clienteNew = contrato.getCliente();
            Usuario usuarioOld = persistentContrato.getUsuario();
            Usuario usuarioNew = contrato.getUsuario();
            List<ContratoFoto> contratoFotoListOld = persistentContrato.getContratoFotoList();
            List<ContratoFoto> contratoFotoListNew = contrato.getContratoFotoList();
            List<Visita> visitaListOld = persistentContrato.getVisitaList();
            List<Visita> visitaListNew = contrato.getVisitaList();
            List<Imovel> imovelListOld = persistentContrato.getImovelList();
            List<Imovel> imovelListNew = contrato.getImovelList();
            List<String> illegalOrphanMessages = null;
            for (ContratoFoto contratoFotoListOldContratoFoto : contratoFotoListOld) {
                if (!contratoFotoListNew.contains(contratoFotoListOldContratoFoto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ContratoFoto " + contratoFotoListOldContratoFoto + " since its contrato field is not nullable.");
                }
            }
            for (Visita visitaListOldVisita : visitaListOld) {
                if (!visitaListNew.contains(visitaListOldVisita)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Visita " + visitaListOldVisita + " since its nrContrato field is not nullable.");
                }
            }
            for (Imovel imovelListOldImovel : imovelListOld) {
                if (!imovelListNew.contains(imovelListOldImovel)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Imovel " + imovelListOldImovel + " since its nrContrato field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (statusNew != null) {
                statusNew = em.getReference(statusNew.getClass(), statusNew.getId());
                contrato.setStatus(statusNew);
            }
//            if (vendedorNew != null) {
//                vendedorNew = em.getReference(vendedorNew.getClass(), vendedorNew.getId());
//                contrato.setVendedor(vendedorNew);
//            }
            if (clienteNew != null) {
                clienteNew = em.getReference(clienteNew.getClass(), clienteNew.getId());
                contrato.setCliente(clienteNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                contrato.setUsuario(usuarioNew);
            }
            List<ContratoFoto> attachedContratoFotoListNew = new ArrayList<ContratoFoto>();
            for (ContratoFoto contratoFotoListNewContratoFotoToAttach : contratoFotoListNew) {
                contratoFotoListNewContratoFotoToAttach = em.getReference(contratoFotoListNewContratoFotoToAttach.getClass(), contratoFotoListNewContratoFotoToAttach.getId());
                attachedContratoFotoListNew.add(contratoFotoListNewContratoFotoToAttach);
            }
            contratoFotoListNew = attachedContratoFotoListNew;
            contrato.setContratoFotoList(contratoFotoListNew);
            List<Visita> attachedVisitaListNew = new ArrayList<Visita>();
            for (Visita visitaListNewVisitaToAttach : visitaListNew) {
                visitaListNewVisitaToAttach = em.getReference(visitaListNewVisitaToAttach.getClass(), visitaListNewVisitaToAttach.getId());
                attachedVisitaListNew.add(visitaListNewVisitaToAttach);
            }
            visitaListNew = attachedVisitaListNew;
            contrato.setVisitaList(visitaListNew);
            List<Imovel> attachedImovelListNew = new ArrayList<Imovel>();
            for (Imovel imovelListNewImovelToAttach : imovelListNew) {
                imovelListNewImovelToAttach = em.getReference(imovelListNewImovelToAttach.getClass(), imovelListNewImovelToAttach.getId());
                attachedImovelListNew.add(imovelListNewImovelToAttach);
            }
            imovelListNew = attachedImovelListNew;
            contrato.setImovelList(imovelListNew);
            contrato = em.merge(contrato);
            if (statusOld != null && !statusOld.equals(statusNew)) {
                statusOld.getContratoList().remove(contrato);
                statusOld = em.merge(statusOld);
            }
            if (statusNew != null && !statusNew.equals(statusOld)) {
                statusNew.getContratoList().add(contrato);
                statusNew = em.merge(statusNew);
            }
//            if (vendedorOld != null && !vendedorOld.equals(vendedorNew)) {
//                vendedorOld.getContratoList().remove(contrato);
//                vendedorOld = em.merge(vendedorOld);
//            }
//            if (vendedorNew != null && !vendedorNew.equals(vendedorOld)) {
//                vendedorNew.getContratoList().add(contrato);
//                vendedorNew = em.merge(vendedorNew);
//            }
            if (clienteOld != null && !clienteOld.equals(clienteNew)) {
                clienteOld.getContratoList().remove(contrato);
                clienteOld = em.merge(clienteOld);
            }
            if (clienteNew != null && !clienteNew.equals(clienteOld)) {
                clienteNew.getContratoList().add(contrato);
                clienteNew = em.merge(clienteNew);
            }
//            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
//                usuarioOld.getContratoList().remove(contrato);
//                usuarioOld = em.merge(usuarioOld);
//            }
//            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
//                usuarioNew.getContratoList().add(contrato);
//                usuarioNew = em.merge(usuarioNew);
//            }
            for (ContratoFoto contratoFotoListNewContratoFoto : contratoFotoListNew) {
                if (!contratoFotoListOld.contains(contratoFotoListNewContratoFoto)) {
                    Contrato oldContratoOfContratoFotoListNewContratoFoto = contratoFotoListNewContratoFoto.getContrato();
                    contratoFotoListNewContratoFoto.setContrato(contrato);
                    contratoFotoListNewContratoFoto = em.merge(contratoFotoListNewContratoFoto);
                    if (oldContratoOfContratoFotoListNewContratoFoto != null && !oldContratoOfContratoFotoListNewContratoFoto.equals(contrato)) {
                        oldContratoOfContratoFotoListNewContratoFoto.getContratoFotoList().remove(contratoFotoListNewContratoFoto);
                        oldContratoOfContratoFotoListNewContratoFoto = em.merge(oldContratoOfContratoFotoListNewContratoFoto);
                    }
                }
            }
            for (Visita visitaListNewVisita : visitaListNew) {
                if (!visitaListOld.contains(visitaListNewVisita)) {
                    Contrato oldNrContratoOfVisitaListNewVisita = visitaListNewVisita.getNrContrato();
                    visitaListNewVisita.setNrContrato(contrato);
                    visitaListNewVisita = em.merge(visitaListNewVisita);
                    if (oldNrContratoOfVisitaListNewVisita != null && !oldNrContratoOfVisitaListNewVisita.equals(contrato)) {
                        oldNrContratoOfVisitaListNewVisita.getVisitaList().remove(visitaListNewVisita);
                        oldNrContratoOfVisitaListNewVisita = em.merge(oldNrContratoOfVisitaListNewVisita);
                    }
                }
            }
            for (Imovel imovelListNewImovel : imovelListNew) {
                if (!imovelListOld.contains(imovelListNewImovel)) {
                    Contrato oldNrContratoOfImovelListNewImovel = imovelListNewImovel.getNrContrato();
                    imovelListNewImovel.setNrContrato(contrato);
                    imovelListNewImovel = em.merge(imovelListNewImovel);
                    if (oldNrContratoOfImovelListNewImovel != null && !oldNrContratoOfImovelListNewImovel.equals(contrato)) {
                        oldNrContratoOfImovelListNewImovel.getImovelList().remove(imovelListNewImovel);
                        oldNrContratoOfImovelListNewImovel = em.merge(oldNrContratoOfImovelListNewImovel);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = contrato.getNrContrato();
                if (findContrato(id) == null) {
                    throw new NonexistentEntityException("The contrato with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Contrato contrato;
            try {
                contrato = em.getReference(Contrato.class, id);
                contrato.getNrContrato();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The contrato with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ContratoFoto> contratoFotoListOrphanCheck = contrato.getContratoFotoList();
            for (ContratoFoto contratoFotoListOrphanCheckContratoFoto : contratoFotoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Contrato (" + contrato + ") cannot be destroyed since the ContratoFoto " + contratoFotoListOrphanCheckContratoFoto + " in its contratoFotoList field has a non-nullable contrato field.");
            }
            List<Visita> visitaListOrphanCheck = contrato.getVisitaList();
            for (Visita visitaListOrphanCheckVisita : visitaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Contrato (" + contrato + ") cannot be destroyed since the Visita " + visitaListOrphanCheckVisita + " in its visitaList field has a non-nullable nrContrato field.");
            }
            List<Imovel> imovelListOrphanCheck = contrato.getImovelList();
            for (Imovel imovelListOrphanCheckImovel : imovelListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Contrato (" + contrato + ") cannot be destroyed since the Imovel " + imovelListOrphanCheckImovel + " in its imovelList field has a non-nullable nrContrato field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            StatusContrato status = contrato.getStatus();
            if (status != null) {
                status.getContratoList().remove(contrato);
                status = em.merge(status);
            }
//            Usuario vendedor = contrato.getVendedor();
//            if (vendedor != null) {
//                vendedor.getContratoList().remove(contrato);
//                vendedor = em.merge(vendedor);
//            }
            Cliente cliente = contrato.getCliente();
            if (cliente != null) {
                cliente.getContratoList().remove(contrato);
                cliente = em.merge(cliente);
            }
//            Usuario usuario = contrato.getUsuario();
//            if (usuario != null) {
//                usuario.getContratoList().remove(contrato);
//                usuario = em.merge(usuario);
//            }
            em.remove(contrato);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Contrato> findContratoEntities() {
        return findContratoEntities(true, -1, -1);
    }

    public List<Contrato> findContratoEntities(int maxResults, int firstResult) {
        return findContratoEntities(false, maxResults, firstResult);
    }

    private List<Contrato> findContratoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Contrato.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Contrato findContrato(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Contrato.class, id);
        } finally {
            em.close();
        }
    }

    public int getContratoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Contrato> rt = cq.from(Contrato.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
