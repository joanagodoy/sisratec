/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import dominio.PlanoVindi;
import dominio.PlanoVindiPK;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Usuario
 */
public class PlanoVindiJpaController implements Serializable {

    public PlanoVindiJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PlanoVindi planoVindi) throws PreexistingEntityException, Exception {
        if (planoVindi.getPlanoVindiPK() == null) {
            planoVindi.setPlanoVindiPK(new PlanoVindiPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(planoVindi);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPlanoVindi(planoVindi.getPlanoVindiPK()) != null) {
                throw new PreexistingEntityException("PlanoVindi " + planoVindi + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PlanoVindi planoVindi) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            planoVindi = em.merge(planoVindi);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PlanoVindiPK id = planoVindi.getPlanoVindiPK();
                if (findPlanoVindi(id) == null) {
                    throw new NonexistentEntityException("The planoVindi with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PlanoVindiPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PlanoVindi planoVindi;
            try {
                planoVindi = em.getReference(PlanoVindi.class, id);
                planoVindi.getPlanoVindiPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The planoVindi with id " + id + " no longer exists.", enfe);
            }
            em.remove(planoVindi);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PlanoVindi> findPlanoVindiEntities() {
        return findPlanoVindiEntities(true, -1, -1);
    }

    public List<PlanoVindi> findPlanoVindiEntities(int maxResults, int firstResult) {
        return findPlanoVindiEntities(false, maxResults, firstResult);
    }

    private List<PlanoVindi> findPlanoVindiEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PlanoVindi.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PlanoVindi findPlanoVindi(PlanoVindiPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PlanoVindi.class, id);
        } finally {
            em.close();
        }
    }
    
    public PlanoVindi findPlanoVindiByItem(String item, boolean imobiliaria) {
        EntityManager em = getEntityManager();
        try {
            return (PlanoVindi) em.createNamedQuery("PlanoVindi.findByItem").setParameter("item", item).setParameter("imobiliaria", imobiliaria).getSingleResult();
        }catch(Exception e){
            return null;
        } finally {
            em.close();
        }
    }

    public int getPlanoVindiCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PlanoVindi> rt = cq.from(PlanoVindi.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
