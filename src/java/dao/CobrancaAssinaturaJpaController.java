/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Cliente;
import dominio.CobrancaAssinatura;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author julia
 */
public class CobrancaAssinaturaJpaController implements Serializable {

    public CobrancaAssinaturaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaAssinatura cobrancaAssinatura) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente cliente = cobrancaAssinatura.getCliente();
            if (cliente != null) {
                cliente = em.getReference(cliente.getClass(), cliente.getId());
                cobrancaAssinatura.setCliente(cliente);
            }
            em.persist(cobrancaAssinatura);
            if (cliente != null) {
                cliente.getCobrancaAssinaturaList().add(cobrancaAssinatura);
                cliente = em.merge(cliente);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaAssinatura cobrancaAssinatura) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaAssinatura persistentCobrancaAssinatura = em.find(CobrancaAssinatura.class, cobrancaAssinatura.getId());
            Cliente clienteOld = persistentCobrancaAssinatura.getCliente();
            Cliente clienteNew = cobrancaAssinatura.getCliente();
            if (clienteNew != null) {
                clienteNew = em.getReference(clienteNew.getClass(), clienteNew.getId());
                cobrancaAssinatura.setCliente(clienteNew);
            }
            cobrancaAssinatura = em.merge(cobrancaAssinatura);
            if (clienteOld != null && !clienteOld.equals(clienteNew)) {
                clienteOld.getCobrancaAssinaturaList().remove(cobrancaAssinatura);
                clienteOld = em.merge(clienteOld);
            }
            if (clienteNew != null && !clienteNew.equals(clienteOld)) {
                clienteNew.getCobrancaAssinaturaList().add(cobrancaAssinatura);
                clienteNew = em.merge(clienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaAssinatura.getId();
                if (findCobrancaAssinatura(id) == null) {
                    throw new NonexistentEntityException("The cobrancaAssinatura with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaAssinatura cobrancaAssinatura;
            try {
                cobrancaAssinatura = em.getReference(CobrancaAssinatura.class, id);
                cobrancaAssinatura.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaAssinatura with id " + id + " no longer exists.", enfe);
            }
            Cliente cliente = cobrancaAssinatura.getCliente();
            if (cliente != null) {
                cliente.getCobrancaAssinaturaList().remove(cobrancaAssinatura);
                cliente = em.merge(cliente);
            }
            em.remove(cobrancaAssinatura);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaAssinatura> findCobrancaAssinaturaEntities() {
        return findCobrancaAssinaturaEntities(true, -1, -1);
    }

    public List<CobrancaAssinatura> findCobrancaAssinaturaEntities(int maxResults, int firstResult) {
        return findCobrancaAssinaturaEntities(false, maxResults, firstResult);
    }

    private List<CobrancaAssinatura> findCobrancaAssinaturaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaAssinatura.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaAssinatura findCobrancaAssinatura(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaAssinatura.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaAssinaturaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaAssinatura> rt = cq.from(CobrancaAssinatura.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
     public List<CobrancaAssinatura> findCobrancaAssinaturaByStatus(boolean ativo){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByStatus").setParameter("ativo", ativo).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
    public List<CobrancaAssinatura> findCobrancaAssinaturaByPeriodicidade(int periodicidade){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByPeriodicidade").setParameter("periodicidade", periodicidade).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
    public List<CobrancaAssinatura> findCobrancaAssinaturaByAdimplencia(int adimplencia){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByAdimplencia").setParameter("adimplencia", adimplencia).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
    public List<CobrancaAssinatura> findCobrancaAssinaturaByStatusPeriodicade(boolean ativo, int periodicidade){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByStatusPeriodicidade").setParameter("ativo", ativo).setParameter("periodicidade", periodicidade).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
    public List<CobrancaAssinatura> findCobrancaAssinaturaByStatusAdimplencia(boolean ativo, int adimplencia){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByStatusAdimplencia").setParameter("ativo", ativo).setParameter("adimplencia", adimplencia).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
    public List<CobrancaAssinatura> findCobrancaAssinaturaByPeriodicidadeAdimplencia(int periodicidade, int adimplencia){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByPeriodicidadeAdimplencia").setParameter("periodicidade", periodicidade).setParameter("adimplencia", adimplencia).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
    public List<CobrancaAssinatura> findCobrancaAssinaturaByStatusPeriodicidadeAdimplencia(boolean ativo, int periodicidade, int adimplencia){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("CobrancaAssinatura.findByStatusPeriodicidadeAdimplencia").setParameter("ativo", ativo).setParameter("periodicidade", periodicidade).setParameter("adimplencia", adimplencia).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
	
	public CobrancaAssinatura findCobrancaAssinaturaByCliente(Cliente cliente){
        EntityManager em = getEntityManager();
        try{
            return (CobrancaAssinatura) em.createNamedQuery("CobrancaAssinatura.findByCliente").setParameter("cliente", cliente).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }  
    
}
