/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.CobrancaAssinatura;

import dominio.CobrancaRegistro;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Joana
 */
public class CobrancaRegistroJpaController implements Serializable {

    public CobrancaRegistroJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaRegistro cobrancaRegistro) {
        
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaAssinatura assinatura = cobrancaRegistro.getAssinatura();
            if (assinatura != null) {
                assinatura = em.getReference(assinatura.getClass(), assinatura.getId());
                cobrancaRegistro.setAssinatura(assinatura);
            }
            
            if (assinatura != null) {
                assinatura.getCobrancaRegistroList().add(cobrancaRegistro);
                assinatura = em.merge(assinatura);
            }            
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaRegistro cobrancaRegistro) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaRegistro persistentCobrancaRegistro = em.find(CobrancaRegistro.class, cobrancaRegistro.getId());
            CobrancaAssinatura assinaturaOld = persistentCobrancaRegistro.getAssinatura();
            CobrancaAssinatura assinaturaNew = cobrancaRegistro.getAssinatura();
            
            
            if (assinaturaNew != null) {
                assinaturaNew = em.getReference(assinaturaNew.getClass(), assinaturaNew.getId());
                cobrancaRegistro.setAssinatura(assinaturaNew);
            }
            
            cobrancaRegistro = em.merge(cobrancaRegistro);
            if (assinaturaOld != null && !assinaturaOld.equals(assinaturaNew)) {
                assinaturaOld.getCobrancaRegistroList().remove(cobrancaRegistro);
                assinaturaOld = em.merge(assinaturaOld);
            }
            if (assinaturaNew != null && !assinaturaNew.equals(assinaturaOld)) {
                assinaturaNew.getCobrancaRegistroList().add(cobrancaRegistro);
                assinaturaNew = em.merge(assinaturaNew);
            }
            
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaRegistro.getId();
                if (findCobrancaRegistro(id) == null) {
                    throw new NonexistentEntityException("The cobrancaRegistro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaRegistro cobrancaRegistro;
            try {
                cobrancaRegistro = em.getReference(CobrancaRegistro.class, id);
                cobrancaRegistro.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaRegistro with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
         
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            CobrancaAssinatura assinatura = cobrancaRegistro.getAssinatura();
            if (assinatura != null) {
                assinatura.getCobrancaRegistroList().remove(cobrancaRegistro);
                assinatura = em.merge(assinatura);
            }
            em.remove(cobrancaRegistro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaRegistro> findCobrancaRegistroEntities() {
        return findCobrancaRegistroEntities(true, -1, -1);
    }

    public List<CobrancaRegistro> findCobrancaRegistroEntities(int maxResults, int firstResult) {
        return findCobrancaRegistroEntities(false, maxResults, firstResult);
    }

    private List<CobrancaRegistro> findCobrancaRegistroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaRegistro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaRegistro findCobrancaRegistro(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaRegistro.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaRegistroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaRegistro> rt = cq.from(CobrancaRegistro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
