/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.CobrancaRegistroReferencia;
import dominio.CobrancaFaturaStatus;
import dominio.Cliente;
import dominio.CobrancaFatura;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Joana
 */
public class CobrancaFaturaJpaController2 implements Serializable {

    public CobrancaFaturaJpaController2(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaFatura cobrancaFatura) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaRegistroReferencia registro = cobrancaFatura.getRegistro();
            if (registro != null) {
                registro = em.getReference(registro.getClass(), registro.getId());
                cobrancaFatura.setRegistro(registro);
            }
            CobrancaFaturaStatus status = cobrancaFatura.getStatus();
            if (status != null) {
                status = em.getReference(status.getClass(), status.getId());
                cobrancaFatura.setStatus(status);
            }
            Cliente cliente = cobrancaFatura.getCliente();
            if (cliente != null) {
                cliente = em.getReference(cliente.getClass(), cliente.getId());
                cobrancaFatura.setCliente(cliente);
            }
            em.persist(cobrancaFatura);
            if (registro != null) {
                registro.getCobrancaFaturaList().add(cobrancaFatura);
                registro = em.merge(registro);
            }
            if (status != null) {
                status.getCobrancaFaturaList().add(cobrancaFatura);
                status = em.merge(status);
            }
            if (cliente != null) {
                cliente.getCobrancaFaturaList().add(cobrancaFatura);
                cliente = em.merge(cliente);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaFatura cobrancaFatura) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaFatura persistentCobrancaFatura = em.find(CobrancaFatura.class, cobrancaFatura.getId());
            CobrancaRegistroReferencia registroOld = persistentCobrancaFatura.getRegistro();
            CobrancaRegistroReferencia registroNew = cobrancaFatura.getRegistro();
            CobrancaFaturaStatus statusOld = persistentCobrancaFatura.getStatus();
            CobrancaFaturaStatus statusNew = cobrancaFatura.getStatus();
            Cliente clienteOld = persistentCobrancaFatura.getCliente();
            Cliente clienteNew = cobrancaFatura.getCliente();
            if (registroNew != null) {
                registroNew = em.getReference(registroNew.getClass(), registroNew.getId());
                cobrancaFatura.setRegistro(registroNew);
            }
            if (statusNew != null) {
                statusNew = em.getReference(statusNew.getClass(), statusNew.getId());
                cobrancaFatura.setStatus(statusNew);
            }
            if (clienteNew != null) {
                clienteNew = em.getReference(clienteNew.getClass(), clienteNew.getId());
                cobrancaFatura.setCliente(clienteNew);
            }
            cobrancaFatura = em.merge(cobrancaFatura);
            if (registroOld != null && !registroOld.equals(registroNew)) {
                registroOld.getCobrancaFaturaList().remove(cobrancaFatura);
                registroOld = em.merge(registroOld);
            }
            if (registroNew != null && !registroNew.equals(registroOld)) {
                registroNew.getCobrancaFaturaList().add(cobrancaFatura);
                registroNew = em.merge(registroNew);
            }
            if (statusOld != null && !statusOld.equals(statusNew)) {
                statusOld.getCobrancaFaturaList().remove(cobrancaFatura);
                statusOld = em.merge(statusOld);
            }
            if (statusNew != null && !statusNew.equals(statusOld)) {
                statusNew.getCobrancaFaturaList().add(cobrancaFatura);
                statusNew = em.merge(statusNew);
            }
            if (clienteOld != null && !clienteOld.equals(clienteNew)) {
                clienteOld.getCobrancaFaturaList().remove(cobrancaFatura);
                clienteOld = em.merge(clienteOld);
            }
            if (clienteNew != null && !clienteNew.equals(clienteOld)) {
                clienteNew.getCobrancaFaturaList().add(cobrancaFatura);
                clienteNew = em.merge(clienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaFatura.getId();
                if (findCobrancaFatura(id) == null) {
                    throw new NonexistentEntityException("The cobrancaFatura with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaFatura cobrancaFatura;
            try {
                cobrancaFatura = em.getReference(CobrancaFatura.class, id);
                cobrancaFatura.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaFatura with id " + id + " no longer exists.", enfe);
            }
            CobrancaRegistroReferencia registro = cobrancaFatura.getRegistro();
            if (registro != null) {
                registro.getCobrancaFaturaList().remove(cobrancaFatura);
                registro = em.merge(registro);
            }
            CobrancaFaturaStatus status = cobrancaFatura.getStatus();
            if (status != null) {
                status.getCobrancaFaturaList().remove(cobrancaFatura);
                status = em.merge(status);
            }
            Cliente cliente = cobrancaFatura.getCliente();
            if (cliente != null) {
                cliente.getCobrancaFaturaList().remove(cobrancaFatura);
                cliente = em.merge(cliente);
            }
            em.remove(cobrancaFatura);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaFatura> findCobrancaFaturaEntities() {
        return findCobrancaFaturaEntities(true, -1, -1);
    }

    public List<CobrancaFatura> findCobrancaFaturaEntities(int maxResults, int firstResult) {
        return findCobrancaFaturaEntities(false, maxResults, firstResult);
    }

    private List<CobrancaFatura> findCobrancaFaturaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaFatura.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaFatura findCobrancaFatura(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaFatura.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaFaturaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaFatura> rt = cq.from(CobrancaFatura.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
