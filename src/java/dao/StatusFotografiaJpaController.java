/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Fotografia;
import dominio.StatusFotografia;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class StatusFotografiaJpaController implements Serializable {

    public StatusFotografiaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StatusFotografia statusFotografia) {
        if (statusFotografia.getFotografiaList() == null) {
            statusFotografia.setFotografiaList(new ArrayList<Fotografia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Fotografia> attachedFotografiaList = new ArrayList<Fotografia>();
            for (Fotografia fotografiaListFotografiaToAttach : statusFotografia.getFotografiaList()) {
                fotografiaListFotografiaToAttach = em.getReference(fotografiaListFotografiaToAttach.getClass(), fotografiaListFotografiaToAttach.getId());
                attachedFotografiaList.add(fotografiaListFotografiaToAttach);
            }
            statusFotografia.setFotografiaList(attachedFotografiaList);
            em.persist(statusFotografia);
            for (Fotografia fotografiaListFotografia : statusFotografia.getFotografiaList()) {
                StatusFotografia oldStatusOfFotografiaListFotografia = fotografiaListFotografia.getStatus();
                fotografiaListFotografia.setStatus(statusFotografia);
                fotografiaListFotografia = em.merge(fotografiaListFotografia);
                if (oldStatusOfFotografiaListFotografia != null) {
                    oldStatusOfFotografiaListFotografia.getFotografiaList().remove(fotografiaListFotografia);
                    oldStatusOfFotografiaListFotografia = em.merge(oldStatusOfFotografiaListFotografia);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StatusFotografia statusFotografia) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusFotografia persistentStatusFotografia = em.find(StatusFotografia.class, statusFotografia.getId());
            List<Fotografia> fotografiaListOld = persistentStatusFotografia.getFotografiaList();
            List<Fotografia> fotografiaListNew = statusFotografia.getFotografiaList();
            List<String> illegalOrphanMessages = null;
            for (Fotografia fotografiaListOldFotografia : fotografiaListOld) {
                if (!fotografiaListNew.contains(fotografiaListOldFotografia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Fotografia " + fotografiaListOldFotografia + " since its status field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Fotografia> attachedFotografiaListNew = new ArrayList<Fotografia>();
            for (Fotografia fotografiaListNewFotografiaToAttach : fotografiaListNew) {
                fotografiaListNewFotografiaToAttach = em.getReference(fotografiaListNewFotografiaToAttach.getClass(), fotografiaListNewFotografiaToAttach.getId());
                attachedFotografiaListNew.add(fotografiaListNewFotografiaToAttach);
            }
            fotografiaListNew = attachedFotografiaListNew;
            statusFotografia.setFotografiaList(fotografiaListNew);
            statusFotografia = em.merge(statusFotografia);
            for (Fotografia fotografiaListNewFotografia : fotografiaListNew) {
                if (!fotografiaListOld.contains(fotografiaListNewFotografia)) {
                    StatusFotografia oldStatusOfFotografiaListNewFotografia = fotografiaListNewFotografia.getStatus();
                    fotografiaListNewFotografia.setStatus(statusFotografia);
                    fotografiaListNewFotografia = em.merge(fotografiaListNewFotografia);
                    if (oldStatusOfFotografiaListNewFotografia != null && !oldStatusOfFotografiaListNewFotografia.equals(statusFotografia)) {
                        oldStatusOfFotografiaListNewFotografia.getFotografiaList().remove(fotografiaListNewFotografia);
                        oldStatusOfFotografiaListNewFotografia = em.merge(oldStatusOfFotografiaListNewFotografia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = statusFotografia.getId();
                if (findStatusFotografia(id) == null) {
                    throw new NonexistentEntityException("The statusFotografia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusFotografia statusFotografia;
            try {
                statusFotografia = em.getReference(StatusFotografia.class, id);
                statusFotografia.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The statusFotografia with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Fotografia> fotografiaListOrphanCheck = statusFotografia.getFotografiaList();
            for (Fotografia fotografiaListOrphanCheckFotografia : fotografiaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StatusFotografia (" + statusFotografia + ") cannot be destroyed since the Fotografia " + fotografiaListOrphanCheckFotografia + " in its fotografiaList field has a non-nullable status field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(statusFotografia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StatusFotografia> findStatusFotografiaEntities() {
        return findStatusFotografiaEntities(true, -1, -1);
    }

    public List<StatusFotografia> findStatusFotografiaEntities(int maxResults, int firstResult) {
        return findStatusFotografiaEntities(false, maxResults, firstResult);
    }

    private List<StatusFotografia> findStatusFotografiaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StatusFotografia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StatusFotografia findStatusFotografia(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StatusFotografia.class, id);
        } finally {
            em.close();
        }
    }

    public int getStatusFotografiaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StatusFotografia> rt = cq.from(StatusFotografia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
