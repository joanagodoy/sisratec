/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import dominio.RegiaoCidade;
import dominio.RegiaoCidadePK;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author joana
 */
public class RegiaoCidadeJpaController implements Serializable {

    public RegiaoCidadeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RegiaoCidade regiaoCidade) throws PreexistingEntityException, Exception {
        if (regiaoCidade.getRegiaoCidadePK() == null) {
            regiaoCidade.setRegiaoCidadePK(new RegiaoCidadePK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(regiaoCidade);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegiaoCidade(regiaoCidade.getRegiaoCidadePK()) != null) {
                throw new PreexistingEntityException("RegiaoCidade " + regiaoCidade + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RegiaoCidade regiaoCidade) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            regiaoCidade = em.merge(regiaoCidade);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                RegiaoCidadePK id = regiaoCidade.getRegiaoCidadePK();
                if (findRegiaoCidade(id) == null) {
                    throw new NonexistentEntityException("The regiaoCidade with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(RegiaoCidadePK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RegiaoCidade regiaoCidade;
            try {
                regiaoCidade = em.getReference(RegiaoCidade.class, id);
                regiaoCidade.getRegiaoCidadePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The regiaoCidade with id " + id + " no longer exists.", enfe);
            }
            em.remove(regiaoCidade);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RegiaoCidade> findRegiaoCidadeEntities() {
        return findRegiaoCidadeEntities(true, -1, -1);
    }

    public List<RegiaoCidade> findRegiaoCidadeEntities(int maxResults, int firstResult) {
        return findRegiaoCidadeEntities(false, maxResults, firstResult);
    }

    private List<RegiaoCidade> findRegiaoCidadeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RegiaoCidade.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RegiaoCidade findRegiaoCidade(RegiaoCidadePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RegiaoCidade.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegiaoCidadeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RegiaoCidade> rt = cq.from(RegiaoCidade.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Object[]> findCidadesDisponiveisByEstado(String estado){
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT c.id, c.nome FROM cidade c WHERE estado = ? AND id not in (SELECT cidade FROM regiao_cidade)";
            return em.createNativeQuery(sql).setParameter(1, estado).getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
        
    }
    
    public List<Object[]> findCidadesAssociadasByRegiao(String regiao){
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT id, nome FROM cidade WHERE id IN (SELECT cidade FROM regiao_cidade WHERE regiao = ?)";
            return em.createNativeQuery(sql).setParameter(1, regiao).getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
        
    }
    
    
    
}
