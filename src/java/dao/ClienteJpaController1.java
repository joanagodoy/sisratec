/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dominio.Cliente;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Usuario;
import dominio.Endereco;
import dominio.Fotografia;
import java.util.ArrayList;
import java.util.List;
import dominio.Contrato;
import dominio.Cobranca;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class ClienteJpaController1 implements Serializable {

    public ClienteJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cliente cliente) {
        if (cliente.getFotografiaList() == null) {
            cliente.setFotografiaList(new ArrayList<Fotografia>());
        }
        if (cliente.getContratoList() == null) {
            cliente.setContratoList(new ArrayList<Contrato>());
        }
        if (cliente.getCobrancaList() == null) {
            cliente.setCobrancaList(new ArrayList<Cobranca>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario = cliente.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                cliente.setUsuario(usuario);
            }
            Endereco endereco = cliente.getEndereco();
            if (endereco != null) {
                endereco = em.getReference(endereco.getClass(), endereco.getId());
                cliente.setEndereco(endereco);
            }
            Usuario vendedor = cliente.getVendedor();
            if (vendedor != null) {
                vendedor = em.getReference(vendedor.getClass(), vendedor.getId());
                cliente.setVendedor(vendedor);
            }
            List<Fotografia> attachedFotografiaList = new ArrayList<Fotografia>();
            for (Fotografia fotografiaListFotografiaToAttach : cliente.getFotografiaList()) {
                fotografiaListFotografiaToAttach = em.getReference(fotografiaListFotografiaToAttach.getClass(), fotografiaListFotografiaToAttach.getId());
                attachedFotografiaList.add(fotografiaListFotografiaToAttach);
            }
            cliente.setFotografiaList(attachedFotografiaList);
            List<Contrato> attachedContratoList = new ArrayList<Contrato>();
            for (Contrato contratoListContratoToAttach : cliente.getContratoList()) {
                contratoListContratoToAttach = em.getReference(contratoListContratoToAttach.getClass(), contratoListContratoToAttach.getNrContrato());
                attachedContratoList.add(contratoListContratoToAttach);
            }
            cliente.setContratoList(attachedContratoList);
            List<Cobranca> attachedCobrancaList = new ArrayList<Cobranca>();
            for (Cobranca cobrancaListCobrancaToAttach : cliente.getCobrancaList()) {
                cobrancaListCobrancaToAttach = em.getReference(cobrancaListCobrancaToAttach.getClass(), cobrancaListCobrancaToAttach.getId());
                attachedCobrancaList.add(cobrancaListCobrancaToAttach);
            }
            cliente.setCobrancaList(attachedCobrancaList);
            em.persist(cliente);
            if (usuario != null) {
                usuario.getClienteList().add(cliente);
                usuario = em.merge(usuario);
            }
            if (endereco != null) {
                endereco.getClienteList().add(cliente);
                endereco = em.merge(endereco);
            }
            if (vendedor != null) {
                vendedor.getClienteList().add(cliente);
                vendedor = em.merge(vendedor);
            }
            for (Fotografia fotografiaListFotografia : cliente.getFotografiaList()) {
                Cliente oldIdClienteOfFotografiaListFotografia = fotografiaListFotografia.getIdCliente();
                fotografiaListFotografia.setIdCliente(cliente);
                fotografiaListFotografia = em.merge(fotografiaListFotografia);
                if (oldIdClienteOfFotografiaListFotografia != null) {
                    oldIdClienteOfFotografiaListFotografia.getFotografiaList().remove(fotografiaListFotografia);
                    oldIdClienteOfFotografiaListFotografia = em.merge(oldIdClienteOfFotografiaListFotografia);
                }
            }
            for (Contrato contratoListContrato : cliente.getContratoList()) {
                Cliente oldClienteOfContratoListContrato = contratoListContrato.getCliente();
                contratoListContrato.setCliente(cliente);
                contratoListContrato = em.merge(contratoListContrato);
                if (oldClienteOfContratoListContrato != null) {
                    oldClienteOfContratoListContrato.getContratoList().remove(contratoListContrato);
                    oldClienteOfContratoListContrato = em.merge(oldClienteOfContratoListContrato);
                }
            }
            for (Cobranca cobrancaListCobranca : cliente.getCobrancaList()) {
                Cliente oldClienteOfCobrancaListCobranca = cobrancaListCobranca.getCliente();
                cobrancaListCobranca.setCliente(cliente);
                cobrancaListCobranca = em.merge(cobrancaListCobranca);
                if (oldClienteOfCobrancaListCobranca != null) {
                    oldClienteOfCobrancaListCobranca.getCobrancaList().remove(cobrancaListCobranca);
                    oldClienteOfCobrancaListCobranca = em.merge(oldClienteOfCobrancaListCobranca);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cliente cliente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente persistentCliente = em.find(Cliente.class, cliente.getId());
            Usuario usuarioOld = persistentCliente.getUsuario();
            Usuario usuarioNew = cliente.getUsuario();
            Endereco enderecoOld = persistentCliente.getEndereco();
            Endereco enderecoNew = cliente.getEndereco();
            Usuario vendedorOld = persistentCliente.getVendedor();
            Usuario vendedorNew = cliente.getVendedor();
            List<Fotografia> fotografiaListOld = persistentCliente.getFotografiaList();
            List<Fotografia> fotografiaListNew = cliente.getFotografiaList();
            List<Contrato> contratoListOld = persistentCliente.getContratoList();
            List<Contrato> contratoListNew = cliente.getContratoList();
            List<Cobranca> cobrancaListOld = persistentCliente.getCobrancaList();
            List<Cobranca> cobrancaListNew = cliente.getCobrancaList();
            List<String> illegalOrphanMessages = null;
            for (Fotografia fotografiaListOldFotografia : fotografiaListOld) {
                if (!fotografiaListNew.contains(fotografiaListOldFotografia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Fotografia " + fotografiaListOldFotografia + " since its idCliente field is not nullable.");
                }
            }
            for (Contrato contratoListOldContrato : contratoListOld) {
                if (!contratoListNew.contains(contratoListOldContrato)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Contrato " + contratoListOldContrato + " since its cliente field is not nullable.");
                }
            }
            for (Cobranca cobrancaListOldCobranca : cobrancaListOld) {
                if (!cobrancaListNew.contains(cobrancaListOldCobranca)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cobranca " + cobrancaListOldCobranca + " since its cliente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                cliente.setUsuario(usuarioNew);
            }
            if (enderecoNew != null) {
                enderecoNew = em.getReference(enderecoNew.getClass(), enderecoNew.getId());
                cliente.setEndereco(enderecoNew);
            }
            if (vendedorNew != null) {
                vendedorNew = em.getReference(vendedorNew.getClass(), vendedorNew.getId());
                cliente.setVendedor(vendedorNew);
            }
            List<Fotografia> attachedFotografiaListNew = new ArrayList<Fotografia>();
            for (Fotografia fotografiaListNewFotografiaToAttach : fotografiaListNew) {
                fotografiaListNewFotografiaToAttach = em.getReference(fotografiaListNewFotografiaToAttach.getClass(), fotografiaListNewFotografiaToAttach.getId());
                attachedFotografiaListNew.add(fotografiaListNewFotografiaToAttach);
            }
            fotografiaListNew = attachedFotografiaListNew;
            cliente.setFotografiaList(fotografiaListNew);
            List<Contrato> attachedContratoListNew = new ArrayList<Contrato>();
            for (Contrato contratoListNewContratoToAttach : contratoListNew) {
                contratoListNewContratoToAttach = em.getReference(contratoListNewContratoToAttach.getClass(), contratoListNewContratoToAttach.getNrContrato());
                attachedContratoListNew.add(contratoListNewContratoToAttach);
            }
            contratoListNew = attachedContratoListNew;
            cliente.setContratoList(contratoListNew);
            List<Cobranca> attachedCobrancaListNew = new ArrayList<Cobranca>();
            for (Cobranca cobrancaListNewCobrancaToAttach : cobrancaListNew) {
                cobrancaListNewCobrancaToAttach = em.getReference(cobrancaListNewCobrancaToAttach.getClass(), cobrancaListNewCobrancaToAttach.getId());
                attachedCobrancaListNew.add(cobrancaListNewCobrancaToAttach);
            }
            cobrancaListNew = attachedCobrancaListNew;
            cliente.setCobrancaList(cobrancaListNew);
            cliente = em.merge(cliente);
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getClienteList().remove(cliente);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getClienteList().add(cliente);
                usuarioNew = em.merge(usuarioNew);
            }
            if (enderecoOld != null && !enderecoOld.equals(enderecoNew)) {
                enderecoOld.getClienteList().remove(cliente);
                enderecoOld = em.merge(enderecoOld);
            }
            if (enderecoNew != null && !enderecoNew.equals(enderecoOld)) {
                enderecoNew.getClienteList().add(cliente);
                enderecoNew = em.merge(enderecoNew);
            }
            if (vendedorOld != null && !vendedorOld.equals(vendedorNew)) {
                vendedorOld.getClienteList().remove(cliente);
                vendedorOld = em.merge(vendedorOld);
            }
            if (vendedorNew != null && !vendedorNew.equals(vendedorOld)) {
                vendedorNew.getClienteList().add(cliente);
                vendedorNew = em.merge(vendedorNew);
            }
            for (Fotografia fotografiaListNewFotografia : fotografiaListNew) {
                if (!fotografiaListOld.contains(fotografiaListNewFotografia)) {
                    Cliente oldIdClienteOfFotografiaListNewFotografia = fotografiaListNewFotografia.getIdCliente();
                    fotografiaListNewFotografia.setIdCliente(cliente);
                    fotografiaListNewFotografia = em.merge(fotografiaListNewFotografia);
                    if (oldIdClienteOfFotografiaListNewFotografia != null && !oldIdClienteOfFotografiaListNewFotografia.equals(cliente)) {
                        oldIdClienteOfFotografiaListNewFotografia.getFotografiaList().remove(fotografiaListNewFotografia);
                        oldIdClienteOfFotografiaListNewFotografia = em.merge(oldIdClienteOfFotografiaListNewFotografia);
                    }
                }
            }
            for (Contrato contratoListNewContrato : contratoListNew) {
                if (!contratoListOld.contains(contratoListNewContrato)) {
                    Cliente oldClienteOfContratoListNewContrato = contratoListNewContrato.getCliente();
                    contratoListNewContrato.setCliente(cliente);
                    contratoListNewContrato = em.merge(contratoListNewContrato);
                    if (oldClienteOfContratoListNewContrato != null && !oldClienteOfContratoListNewContrato.equals(cliente)) {
                        oldClienteOfContratoListNewContrato.getContratoList().remove(contratoListNewContrato);
                        oldClienteOfContratoListNewContrato = em.merge(oldClienteOfContratoListNewContrato);
                    }
                }
            }
            for (Cobranca cobrancaListNewCobranca : cobrancaListNew) {
                if (!cobrancaListOld.contains(cobrancaListNewCobranca)) {
                    Cliente oldClienteOfCobrancaListNewCobranca = cobrancaListNewCobranca.getCliente();
                    cobrancaListNewCobranca.setCliente(cliente);
                    cobrancaListNewCobranca = em.merge(cobrancaListNewCobranca);
                    if (oldClienteOfCobrancaListNewCobranca != null && !oldClienteOfCobrancaListNewCobranca.equals(cliente)) {
                        oldClienteOfCobrancaListNewCobranca.getCobrancaList().remove(cobrancaListNewCobranca);
                        oldClienteOfCobrancaListNewCobranca = em.merge(oldClienteOfCobrancaListNewCobranca);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cliente.getId();
                if (findCliente(id) == null) {
                    throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente cliente;
            try {
                cliente = em.getReference(Cliente.class, id);
                cliente.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Fotografia> fotografiaListOrphanCheck = cliente.getFotografiaList();
            for (Fotografia fotografiaListOrphanCheckFotografia : fotografiaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Fotografia " + fotografiaListOrphanCheckFotografia + " in its fotografiaList field has a non-nullable idCliente field.");
            }
            List<Contrato> contratoListOrphanCheck = cliente.getContratoList();
            for (Contrato contratoListOrphanCheckContrato : contratoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Contrato " + contratoListOrphanCheckContrato + " in its contratoList field has a non-nullable cliente field.");
            }
            List<Cobranca> cobrancaListOrphanCheck = cliente.getCobrancaList();
            for (Cobranca cobrancaListOrphanCheckCobranca : cobrancaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Cobranca " + cobrancaListOrphanCheckCobranca + " in its cobrancaList field has a non-nullable cliente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Usuario usuario = cliente.getUsuario();
            if (usuario != null) {
                usuario.getClienteList().remove(cliente);
                usuario = em.merge(usuario);
            }
            Endereco endereco = cliente.getEndereco();
            if (endereco != null) {
                endereco.getClienteList().remove(cliente);
                endereco = em.merge(endereco);
            }
            Usuario vendedor = cliente.getVendedor();
            if (vendedor != null) {
                vendedor.getClienteList().remove(cliente);
                vendedor = em.merge(vendedor);
            }
            em.remove(cliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cliente> findClienteEntities() {
        return findClienteEntities(true, -1, -1);
    }

    public List<Cliente> findClienteEntities(int maxResults, int firstResult) {
        return findClienteEntities(false, maxResults, firstResult);
    }

    private List<Cliente> findClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cliente findCliente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cliente> rt = cq.from(Cliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
