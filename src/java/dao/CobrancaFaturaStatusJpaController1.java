/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.CobrancaFatura;
import dominio.CobrancaFaturaStatus;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Joana
 */
public class CobrancaFaturaStatusJpaController1 implements Serializable {

    public CobrancaFaturaStatusJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaFaturaStatus cobrancaFaturaStatus) {
        if (cobrancaFaturaStatus.getCobrancaFaturaList() == null) {
            cobrancaFaturaStatus.setCobrancaFaturaList(new ArrayList<CobrancaFatura>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<CobrancaFatura> attachedCobrancaFaturaList = new ArrayList<CobrancaFatura>();
            for (CobrancaFatura cobrancaFaturaListCobrancaFaturaToAttach : cobrancaFaturaStatus.getCobrancaFaturaList()) {
                cobrancaFaturaListCobrancaFaturaToAttach = em.getReference(cobrancaFaturaListCobrancaFaturaToAttach.getClass(), cobrancaFaturaListCobrancaFaturaToAttach.getId());
                attachedCobrancaFaturaList.add(cobrancaFaturaListCobrancaFaturaToAttach);
            }
            cobrancaFaturaStatus.setCobrancaFaturaList(attachedCobrancaFaturaList);
            em.persist(cobrancaFaturaStatus);
            for (CobrancaFatura cobrancaFaturaListCobrancaFatura : cobrancaFaturaStatus.getCobrancaFaturaList()) {
                CobrancaFaturaStatus oldStatusOfCobrancaFaturaListCobrancaFatura = cobrancaFaturaListCobrancaFatura.getStatus();
                cobrancaFaturaListCobrancaFatura.setStatus(cobrancaFaturaStatus);
                cobrancaFaturaListCobrancaFatura = em.merge(cobrancaFaturaListCobrancaFatura);
                if (oldStatusOfCobrancaFaturaListCobrancaFatura != null) {
                    oldStatusOfCobrancaFaturaListCobrancaFatura.getCobrancaFaturaList().remove(cobrancaFaturaListCobrancaFatura);
                    oldStatusOfCobrancaFaturaListCobrancaFatura = em.merge(oldStatusOfCobrancaFaturaListCobrancaFatura);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaFaturaStatus cobrancaFaturaStatus) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaFaturaStatus persistentCobrancaFaturaStatus = em.find(CobrancaFaturaStatus.class, cobrancaFaturaStatus.getId());
            List<CobrancaFatura> cobrancaFaturaListOld = persistentCobrancaFaturaStatus.getCobrancaFaturaList();
            List<CobrancaFatura> cobrancaFaturaListNew = cobrancaFaturaStatus.getCobrancaFaturaList();
            List<String> illegalOrphanMessages = null;
            for (CobrancaFatura cobrancaFaturaListOldCobrancaFatura : cobrancaFaturaListOld) {
                if (!cobrancaFaturaListNew.contains(cobrancaFaturaListOldCobrancaFatura)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CobrancaFatura " + cobrancaFaturaListOldCobrancaFatura + " since its status field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<CobrancaFatura> attachedCobrancaFaturaListNew = new ArrayList<CobrancaFatura>();
            for (CobrancaFatura cobrancaFaturaListNewCobrancaFaturaToAttach : cobrancaFaturaListNew) {
                cobrancaFaturaListNewCobrancaFaturaToAttach = em.getReference(cobrancaFaturaListNewCobrancaFaturaToAttach.getClass(), cobrancaFaturaListNewCobrancaFaturaToAttach.getId());
                attachedCobrancaFaturaListNew.add(cobrancaFaturaListNewCobrancaFaturaToAttach);
            }
            cobrancaFaturaListNew = attachedCobrancaFaturaListNew;
            cobrancaFaturaStatus.setCobrancaFaturaList(cobrancaFaturaListNew);
            cobrancaFaturaStatus = em.merge(cobrancaFaturaStatus);
            for (CobrancaFatura cobrancaFaturaListNewCobrancaFatura : cobrancaFaturaListNew) {
                if (!cobrancaFaturaListOld.contains(cobrancaFaturaListNewCobrancaFatura)) {
                    CobrancaFaturaStatus oldStatusOfCobrancaFaturaListNewCobrancaFatura = cobrancaFaturaListNewCobrancaFatura.getStatus();
                    cobrancaFaturaListNewCobrancaFatura.setStatus(cobrancaFaturaStatus);
                    cobrancaFaturaListNewCobrancaFatura = em.merge(cobrancaFaturaListNewCobrancaFatura);
                    if (oldStatusOfCobrancaFaturaListNewCobrancaFatura != null && !oldStatusOfCobrancaFaturaListNewCobrancaFatura.equals(cobrancaFaturaStatus)) {
                        oldStatusOfCobrancaFaturaListNewCobrancaFatura.getCobrancaFaturaList().remove(cobrancaFaturaListNewCobrancaFatura);
                        oldStatusOfCobrancaFaturaListNewCobrancaFatura = em.merge(oldStatusOfCobrancaFaturaListNewCobrancaFatura);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaFaturaStatus.getId();
                if (findCobrancaFaturaStatus(id) == null) {
                    throw new NonexistentEntityException("The cobrancaFaturaStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaFaturaStatus cobrancaFaturaStatus;
            try {
                cobrancaFaturaStatus = em.getReference(CobrancaFaturaStatus.class, id);
                cobrancaFaturaStatus.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaFaturaStatus with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CobrancaFatura> cobrancaFaturaListOrphanCheck = cobrancaFaturaStatus.getCobrancaFaturaList();
            for (CobrancaFatura cobrancaFaturaListOrphanCheckCobrancaFatura : cobrancaFaturaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CobrancaFaturaStatus (" + cobrancaFaturaStatus + ") cannot be destroyed since the CobrancaFatura " + cobrancaFaturaListOrphanCheckCobrancaFatura + " in its cobrancaFaturaList field has a non-nullable status field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(cobrancaFaturaStatus);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaFaturaStatus> findCobrancaFaturaStatusEntities() {
        return findCobrancaFaturaStatusEntities(true, -1, -1);
    }

    public List<CobrancaFaturaStatus> findCobrancaFaturaStatusEntities(int maxResults, int firstResult) {
        return findCobrancaFaturaStatusEntities(false, maxResults, firstResult);
    }

    private List<CobrancaFaturaStatus> findCobrancaFaturaStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaFaturaStatus.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaFaturaStatus findCobrancaFaturaStatus(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaFaturaStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaFaturaStatusCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaFaturaStatus> rt = cq.from(CobrancaFaturaStatus.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
