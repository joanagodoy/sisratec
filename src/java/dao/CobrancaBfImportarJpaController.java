/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import dominio.CobrancaBfImportar;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Joana
 */
public class CobrancaBfImportarJpaController implements Serializable {

    public CobrancaBfImportarJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaBfImportar cobrancaBfImportar) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cobrancaBfImportar);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCobrancaBfImportar(cobrancaBfImportar.getCode()) != null) {
                throw new PreexistingEntityException("CobrancaBfImportar " + cobrancaBfImportar + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaBfImportar cobrancaBfImportar) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cobrancaBfImportar = em.merge(cobrancaBfImportar);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaBfImportar.getCode();
                if (findCobrancaBfImportar(id) == null) {
                    throw new NonexistentEntityException("The cobrancaBfImportar with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaBfImportar cobrancaBfImportar;
            try {
                cobrancaBfImportar = em.getReference(CobrancaBfImportar.class, id);
                cobrancaBfImportar.getCode();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaBfImportar with id " + id + " no longer exists.", enfe);
            }
            em.remove(cobrancaBfImportar);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaBfImportar> findCobrancaBfImportarEntities() {
        return findCobrancaBfImportarEntities(true, -1, -1);
    }

    public List<CobrancaBfImportar> findCobrancaBfImportarEntities(int maxResults, int firstResult) {
        return findCobrancaBfImportarEntities(false, maxResults, firstResult);
    }

    private List<CobrancaBfImportar> findCobrancaBfImportarEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaBfImportar.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaBfImportar findCobrancaBfImportar(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaBfImportar.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaBfImportarCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaBfImportar> rt = cq.from(CobrancaBfImportar.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
