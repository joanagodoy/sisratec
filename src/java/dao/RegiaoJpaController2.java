/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dominio.Regiao;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Usuario;
import java.util.ArrayList;
import java.util.List;
import dominio.Visita;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author julia
 */
public class RegiaoJpaController2 implements Serializable {

    public RegiaoJpaController2(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Regiao regiao) {
        if (regiao.getUsuarioList() == null) {
            regiao.setUsuarioList(new ArrayList<Usuario>());
        }
        if (regiao.getVisitaList() == null) {
            regiao.setVisitaList(new ArrayList<Visita>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Usuario> attachedUsuarioList = new ArrayList<Usuario>();
            for (Usuario usuarioListUsuarioToAttach : regiao.getUsuarioList()) {
                usuarioListUsuarioToAttach = em.getReference(usuarioListUsuarioToAttach.getClass(), usuarioListUsuarioToAttach.getId());
                attachedUsuarioList.add(usuarioListUsuarioToAttach);
            }
            regiao.setUsuarioList(attachedUsuarioList);
            List<Visita> attachedVisitaList = new ArrayList<Visita>();
            for (Visita visitaListVisitaToAttach : regiao.getVisitaList()) {
                visitaListVisitaToAttach = em.getReference(visitaListVisitaToAttach.getClass(), visitaListVisitaToAttach.getId());
                attachedVisitaList.add(visitaListVisitaToAttach);
            }
            regiao.setVisitaList(attachedVisitaList);
            em.persist(regiao);
            for (Usuario usuarioListUsuario : regiao.getUsuarioList()) {
                Regiao oldRegiaoOfUsuarioListUsuario = usuarioListUsuario.getRegiao();
                usuarioListUsuario.setRegiao(regiao);
                usuarioListUsuario = em.merge(usuarioListUsuario);
                if (oldRegiaoOfUsuarioListUsuario != null) {
                    oldRegiaoOfUsuarioListUsuario.getUsuarioList().remove(usuarioListUsuario);
                    oldRegiaoOfUsuarioListUsuario = em.merge(oldRegiaoOfUsuarioListUsuario);
                }
            }
            for (Visita visitaListVisita : regiao.getVisitaList()) {
                Regiao oldRegiaoOfVisitaListVisita = visitaListVisita.getRegiao();
                visitaListVisita.setRegiao(regiao);
                visitaListVisita = em.merge(visitaListVisita);
                if (oldRegiaoOfVisitaListVisita != null) {
                    oldRegiaoOfVisitaListVisita.getVisitaList().remove(visitaListVisita);
                    oldRegiaoOfVisitaListVisita = em.merge(oldRegiaoOfVisitaListVisita);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Regiao regiao) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Regiao persistentRegiao = em.find(Regiao.class, regiao.getId());
            List<Usuario> usuarioListOld = persistentRegiao.getUsuarioList();
            List<Usuario> usuarioListNew = regiao.getUsuarioList();
            List<Visita> visitaListOld = persistentRegiao.getVisitaList();
            List<Visita> visitaListNew = regiao.getVisitaList();
            List<String> illegalOrphanMessages = null;
            for (Usuario usuarioListOldUsuario : usuarioListOld) {
                if (!usuarioListNew.contains(usuarioListOldUsuario)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Usuario " + usuarioListOldUsuario + " since its regiao field is not nullable.");
                }
            }
            for (Visita visitaListOldVisita : visitaListOld) {
                if (!visitaListNew.contains(visitaListOldVisita)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Visita " + visitaListOldVisita + " since its regiao field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Usuario> attachedUsuarioListNew = new ArrayList<Usuario>();
            for (Usuario usuarioListNewUsuarioToAttach : usuarioListNew) {
                usuarioListNewUsuarioToAttach = em.getReference(usuarioListNewUsuarioToAttach.getClass(), usuarioListNewUsuarioToAttach.getId());
                attachedUsuarioListNew.add(usuarioListNewUsuarioToAttach);
            }
            usuarioListNew = attachedUsuarioListNew;
            regiao.setUsuarioList(usuarioListNew);
            List<Visita> attachedVisitaListNew = new ArrayList<Visita>();
            for (Visita visitaListNewVisitaToAttach : visitaListNew) {
                visitaListNewVisitaToAttach = em.getReference(visitaListNewVisitaToAttach.getClass(), visitaListNewVisitaToAttach.getId());
                attachedVisitaListNew.add(visitaListNewVisitaToAttach);
            }
            visitaListNew = attachedVisitaListNew;
            regiao.setVisitaList(visitaListNew);
            regiao = em.merge(regiao);
            for (Usuario usuarioListNewUsuario : usuarioListNew) {
                if (!usuarioListOld.contains(usuarioListNewUsuario)) {
                    Regiao oldRegiaoOfUsuarioListNewUsuario = usuarioListNewUsuario.getRegiao();
                    usuarioListNewUsuario.setRegiao(regiao);
                    usuarioListNewUsuario = em.merge(usuarioListNewUsuario);
                    if (oldRegiaoOfUsuarioListNewUsuario != null && !oldRegiaoOfUsuarioListNewUsuario.equals(regiao)) {
                        oldRegiaoOfUsuarioListNewUsuario.getUsuarioList().remove(usuarioListNewUsuario);
                        oldRegiaoOfUsuarioListNewUsuario = em.merge(oldRegiaoOfUsuarioListNewUsuario);
                    }
                }
            }
            for (Visita visitaListNewVisita : visitaListNew) {
                if (!visitaListOld.contains(visitaListNewVisita)) {
                    Regiao oldRegiaoOfVisitaListNewVisita = visitaListNewVisita.getRegiao();
                    visitaListNewVisita.setRegiao(regiao);
                    visitaListNewVisita = em.merge(visitaListNewVisita);
                    if (oldRegiaoOfVisitaListNewVisita != null && !oldRegiaoOfVisitaListNewVisita.equals(regiao)) {
                        oldRegiaoOfVisitaListNewVisita.getVisitaList().remove(visitaListNewVisita);
                        oldRegiaoOfVisitaListNewVisita = em.merge(oldRegiaoOfVisitaListNewVisita);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = regiao.getId();
                if (findRegiao(id) == null) {
                    throw new NonexistentEntityException("The regiao with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Regiao regiao;
            try {
                regiao = em.getReference(Regiao.class, id);
                regiao.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The regiao with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Usuario> usuarioListOrphanCheck = regiao.getUsuarioList();
            for (Usuario usuarioListOrphanCheckUsuario : usuarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Regiao (" + regiao + ") cannot be destroyed since the Usuario " + usuarioListOrphanCheckUsuario + " in its usuarioList field has a non-nullable regiao field.");
            }
            List<Visita> visitaListOrphanCheck = regiao.getVisitaList();
            for (Visita visitaListOrphanCheckVisita : visitaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Regiao (" + regiao + ") cannot be destroyed since the Visita " + visitaListOrphanCheckVisita + " in its visitaList field has a non-nullable regiao field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(regiao);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Regiao> findRegiaoEntities() {
        return findRegiaoEntities(true, -1, -1);
    }

    public List<Regiao> findRegiaoEntities(int maxResults, int firstResult) {
        return findRegiaoEntities(false, maxResults, firstResult);
    }

    private List<Regiao> findRegiaoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Regiao.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Regiao findRegiao(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Regiao.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegiaoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Regiao> rt = cq.from(Regiao.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
