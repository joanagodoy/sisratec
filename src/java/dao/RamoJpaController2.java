/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Cliente;
import dominio.Ramo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Joana
 */
public class RamoJpaController2 implements Serializable {

    public RamoJpaController2(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ramo ramo) throws PreexistingEntityException, Exception {
        if (ramo.getClienteList() == null) {
            ramo.setClienteList(new ArrayList<Cliente>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : ramo.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getId());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            ramo.setClienteList(attachedClienteList);
            em.persist(ramo);
            for (Cliente clienteListCliente : ramo.getClienteList()) {
                Ramo oldRamoOfClienteListCliente = clienteListCliente.getRamo();
                clienteListCliente.setRamo(ramo);
                clienteListCliente = em.merge(clienteListCliente);
                if (oldRamoOfClienteListCliente != null) {
                    oldRamoOfClienteListCliente.getClienteList().remove(clienteListCliente);
                    oldRamoOfClienteListCliente = em.merge(oldRamoOfClienteListCliente);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRamo(ramo.getId()) != null) {
                throw new PreexistingEntityException("Ramo " + ramo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ramo ramo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ramo persistentRamo = em.find(Ramo.class, ramo.getId());
            List<Cliente> clienteListOld = persistentRamo.getClienteList();
            List<Cliente> clienteListNew = ramo.getClienteList();
            List<String> illegalOrphanMessages = null;
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cliente " + clienteListOldCliente + " since its ramo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getId());
                attachedClienteListNew.add(clienteListNewClienteToAttach);
            }
            clienteListNew = attachedClienteListNew;
            ramo.setClienteList(clienteListNew);
            ramo = em.merge(ramo);
            for (Cliente clienteListNewCliente : clienteListNew) {
                if (!clienteListOld.contains(clienteListNewCliente)) {
                    Ramo oldRamoOfClienteListNewCliente = clienteListNewCliente.getRamo();
                    clienteListNewCliente.setRamo(ramo);
                    clienteListNewCliente = em.merge(clienteListNewCliente);
                    if (oldRamoOfClienteListNewCliente != null && !oldRamoOfClienteListNewCliente.equals(ramo)) {
                        oldRamoOfClienteListNewCliente.getClienteList().remove(clienteListNewCliente);
                        oldRamoOfClienteListNewCliente = em.merge(oldRamoOfClienteListNewCliente);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ramo.getId();
                if (findRamo(id) == null) {
                    throw new NonexistentEntityException("The ramo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ramo ramo;
            try {
                ramo = em.getReference(Ramo.class, id);
                ramo.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ramo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Cliente> clienteListOrphanCheck = ramo.getClienteList();
            for (Cliente clienteListOrphanCheckCliente : clienteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ramo (" + ramo + ") cannot be destroyed since the Cliente " + clienteListOrphanCheckCliente + " in its clienteList field has a non-nullable ramo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(ramo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ramo> findRamoEntities() {
        return findRamoEntities(true, -1, -1);
    }

    public List<Ramo> findRamoEntities(int maxResults, int firstResult) {
        return findRamoEntities(false, maxResults, firstResult);
    }

    private List<Ramo> findRamoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ramo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ramo findRamo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ramo.class, id);
        } finally {
            em.close();
        }
    }

    public int getRamoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ramo> rt = cq.from(Ramo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
