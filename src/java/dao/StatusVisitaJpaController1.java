/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dominio.StatusVisita;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Visita;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class StatusVisitaJpaController1 implements Serializable {

    public StatusVisitaJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StatusVisita statusVisita) {
        if (statusVisita.getVisitaList() == null) {
            statusVisita.setVisitaList(new ArrayList<Visita>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Visita> attachedVisitaList = new ArrayList<Visita>();
            for (Visita visitaListVisitaToAttach : statusVisita.getVisitaList()) {
                visitaListVisitaToAttach = em.getReference(visitaListVisitaToAttach.getClass(), visitaListVisitaToAttach.getId());
                attachedVisitaList.add(visitaListVisitaToAttach);
            }
            statusVisita.setVisitaList(attachedVisitaList);
            em.persist(statusVisita);
            for (Visita visitaListVisita : statusVisita.getVisitaList()) {
                StatusVisita oldStatusOfVisitaListVisita = visitaListVisita.getStatus();
                visitaListVisita.setStatus(statusVisita);
                visitaListVisita = em.merge(visitaListVisita);
                if (oldStatusOfVisitaListVisita != null) {
                    oldStatusOfVisitaListVisita.getVisitaList().remove(visitaListVisita);
                    oldStatusOfVisitaListVisita = em.merge(oldStatusOfVisitaListVisita);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StatusVisita statusVisita) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusVisita persistentStatusVisita = em.find(StatusVisita.class, statusVisita.getId());
            List<Visita> visitaListOld = persistentStatusVisita.getVisitaList();
            List<Visita> visitaListNew = statusVisita.getVisitaList();
            List<String> illegalOrphanMessages = null;
            for (Visita visitaListOldVisita : visitaListOld) {
                if (!visitaListNew.contains(visitaListOldVisita)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Visita " + visitaListOldVisita + " since its status field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Visita> attachedVisitaListNew = new ArrayList<Visita>();
            for (Visita visitaListNewVisitaToAttach : visitaListNew) {
                visitaListNewVisitaToAttach = em.getReference(visitaListNewVisitaToAttach.getClass(), visitaListNewVisitaToAttach.getId());
                attachedVisitaListNew.add(visitaListNewVisitaToAttach);
            }
            visitaListNew = attachedVisitaListNew;
            statusVisita.setVisitaList(visitaListNew);
            statusVisita = em.merge(statusVisita);
            for (Visita visitaListNewVisita : visitaListNew) {
                if (!visitaListOld.contains(visitaListNewVisita)) {
                    StatusVisita oldStatusOfVisitaListNewVisita = visitaListNewVisita.getStatus();
                    visitaListNewVisita.setStatus(statusVisita);
                    visitaListNewVisita = em.merge(visitaListNewVisita);
                    if (oldStatusOfVisitaListNewVisita != null && !oldStatusOfVisitaListNewVisita.equals(statusVisita)) {
                        oldStatusOfVisitaListNewVisita.getVisitaList().remove(visitaListNewVisita);
                        oldStatusOfVisitaListNewVisita = em.merge(oldStatusOfVisitaListNewVisita);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = statusVisita.getId();
                if (findStatusVisita(id) == null) {
                    throw new NonexistentEntityException("The statusVisita with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusVisita statusVisita;
            try {
                statusVisita = em.getReference(StatusVisita.class, id);
                statusVisita.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The statusVisita with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Visita> visitaListOrphanCheck = statusVisita.getVisitaList();
            for (Visita visitaListOrphanCheckVisita : visitaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StatusVisita (" + statusVisita + ") cannot be destroyed since the Visita " + visitaListOrphanCheckVisita + " in its visitaList field has a non-nullable status field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(statusVisita);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StatusVisita> findStatusVisitaEntities() {
        return findStatusVisitaEntities(true, -1, -1);
    }

    public List<StatusVisita> findStatusVisitaEntities(int maxResults, int firstResult) {
        return findStatusVisitaEntities(false, maxResults, firstResult);
    }

    private List<StatusVisita> findStatusVisitaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StatusVisita.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StatusVisita findStatusVisita(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StatusVisita.class, id);
        } finally {
            em.close();
        }
    }

    public int getStatusVisitaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StatusVisita> rt = cq.from(StatusVisita.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
