/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import dominio.Cobranca;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author joana
 */
public class CobrancaJpaController implements Serializable {

    public CobrancaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cobranca cobranca) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cobranca);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCobranca(cobranca.getId()) != null) {
                throw new PreexistingEntityException("Cobranca " + cobranca + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cobranca cobranca) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cobranca = em.merge(cobranca);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobranca.getId();
                if (findCobranca(id) == null) {
                    throw new NonexistentEntityException("The cobranca with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cobranca cobranca;
            try {
                cobranca = em.getReference(Cobranca.class, id);
                cobranca.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobranca with id " + id + " no longer exists.", enfe);
            }
            em.remove(cobranca);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cobranca> findCobrancaEntities() {
        return findCobrancaEntities(true, -1, -1);
    }

    public List<Cobranca> findCobrancaEntities(int maxResults, int firstResult) {
        return findCobrancaEntities(false, maxResults, firstResult);
    }

    private List<Cobranca> findCobrancaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cobranca.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cobranca findCobranca(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cobranca.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cobranca> rt = cq.from(Cobranca.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Cobranca findByCodigo(String codigo){
        EntityManager em = getEntityManager();
        try{
            return (Cobranca) em.createNamedQuery("Cobranca.findByCodigo").setParameter("codigo", codigo).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    
    public List<Cobranca> findVencidas(){
        EntityManager em = getEntityManager();
        try{
//            .setParameter("data", new Date()).setParameter("null", null)
            return em.createNamedQuery("Cobranca.findVencidas").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
}
