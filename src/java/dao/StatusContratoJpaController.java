/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Contrato;
import dominio.StatusContrato;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class StatusContratoJpaController implements Serializable {

    public StatusContratoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StatusContrato statusContrato) {
        if (statusContrato.getContratoList() == null) {
            statusContrato.setContratoList(new ArrayList<Contrato>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Contrato> attachedContratoList = new ArrayList<Contrato>();
            for (Contrato contratoListContratoToAttach : statusContrato.getContratoList()) {
                contratoListContratoToAttach = em.getReference(contratoListContratoToAttach.getClass(), contratoListContratoToAttach.getNrContrato());
                attachedContratoList.add(contratoListContratoToAttach);
            }
            statusContrato.setContratoList(attachedContratoList);
            em.persist(statusContrato);
            for (Contrato contratoListContrato : statusContrato.getContratoList()) {
                StatusContrato oldStatusOfContratoListContrato = contratoListContrato.getStatus();
                contratoListContrato.setStatus(statusContrato);
                contratoListContrato = em.merge(contratoListContrato);
                if (oldStatusOfContratoListContrato != null) {
                    oldStatusOfContratoListContrato.getContratoList().remove(contratoListContrato);
                    oldStatusOfContratoListContrato = em.merge(oldStatusOfContratoListContrato);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StatusContrato statusContrato) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusContrato persistentStatusContrato = em.find(StatusContrato.class, statusContrato.getId());
            List<Contrato> contratoListOld = persistentStatusContrato.getContratoList();
            List<Contrato> contratoListNew = statusContrato.getContratoList();
            List<String> illegalOrphanMessages = null;
            for (Contrato contratoListOldContrato : contratoListOld) {
                if (!contratoListNew.contains(contratoListOldContrato)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Contrato " + contratoListOldContrato + " since its status field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Contrato> attachedContratoListNew = new ArrayList<Contrato>();
            for (Contrato contratoListNewContratoToAttach : contratoListNew) {
                contratoListNewContratoToAttach = em.getReference(contratoListNewContratoToAttach.getClass(), contratoListNewContratoToAttach.getNrContrato());
                attachedContratoListNew.add(contratoListNewContratoToAttach);
            }
            contratoListNew = attachedContratoListNew;
            statusContrato.setContratoList(contratoListNew);
            statusContrato = em.merge(statusContrato);
            for (Contrato contratoListNewContrato : contratoListNew) {
                if (!contratoListOld.contains(contratoListNewContrato)) {
                    StatusContrato oldStatusOfContratoListNewContrato = contratoListNewContrato.getStatus();
                    contratoListNewContrato.setStatus(statusContrato);
                    contratoListNewContrato = em.merge(contratoListNewContrato);
                    if (oldStatusOfContratoListNewContrato != null && !oldStatusOfContratoListNewContrato.equals(statusContrato)) {
                        oldStatusOfContratoListNewContrato.getContratoList().remove(contratoListNewContrato);
                        oldStatusOfContratoListNewContrato = em.merge(oldStatusOfContratoListNewContrato);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = statusContrato.getId();
                if (findStatusContrato(id) == null) {
                    throw new NonexistentEntityException("The statusContrato with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusContrato statusContrato;
            try {
                statusContrato = em.getReference(StatusContrato.class, id);
                statusContrato.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The statusContrato with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Contrato> contratoListOrphanCheck = statusContrato.getContratoList();
            for (Contrato contratoListOrphanCheckContrato : contratoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StatusContrato (" + statusContrato + ") cannot be destroyed since the Contrato " + contratoListOrphanCheckContrato + " in its contratoList field has a non-nullable status field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(statusContrato);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StatusContrato> findStatusContratoEntities() {
        return findStatusContratoEntities(true, -1, -1);
    }

    public List<StatusContrato> findStatusContratoEntities(int maxResults, int firstResult) {
        return findStatusContratoEntities(false, maxResults, firstResult);
    }

    private List<StatusContrato> findStatusContratoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StatusContrato.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StatusContrato findStatusContrato(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StatusContrato.class, id);
        } finally {
            em.close();
        }
    }

    public int getStatusContratoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StatusContrato> rt = cq.from(StatusContrato.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
