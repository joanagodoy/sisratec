/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dominio.Cliente;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.StatusVisita;
import dominio.Contrato;
import dominio.Imovel;
import dominio.Regiao;
import dominio.Usuario;
import dominio.Visita;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TemporalType;

/**
 *
 * @author Usuario
 */
public class VisitaJpaController implements Serializable {

    public VisitaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Visita visita) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusVisita status = visita.getStatus();
            if (status != null) {
                status = em.getReference(status.getClass(), status.getId());
                visita.setStatus(status);
            }
            Contrato nrContrato = visita.getNrContrato();
            if (nrContrato != null) {
                nrContrato = em.getReference(nrContrato.getClass(), nrContrato.getNrContrato());
                visita.setNrContrato(nrContrato);
            }
            Usuario usuario = visita.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                visita.setUsuario(usuario);
            }
            Usuario fotografo = visita.getFotografo();
            if (fotografo != null) {
                fotografo = em.getReference(fotografo.getClass(), fotografo.getId());
                visita.setFotografo(fotografo);
            }
            em.persist(visita);
            if (status != null) {
                status.getVisitaList().add(visita);
                status = em.merge(status);
            }
            if (nrContrato != null) {
                nrContrato.getVisitaList().add(visita);
                nrContrato = em.merge(nrContrato);
            }
            if (usuario != null) {
                usuario.getVisitaList().add(visita);
                usuario = em.merge(usuario);
            }
            if (fotografo != null) {
                fotografo.getVisitaList().add(visita);
                fotografo = em.merge(fotografo);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Visita visita) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Visita persistentVisita = em.find(Visita.class, visita.getId());
            StatusVisita statusOld = persistentVisita.getStatus();
            StatusVisita statusNew = visita.getStatus();
            Contrato nrContratoOld = persistentVisita.getNrContrato();
            Contrato nrContratoNew = visita.getNrContrato();
            Usuario usuarioOld = persistentVisita.getUsuario();
            Usuario usuarioNew = visita.getUsuario();
            Usuario fotografoOld = persistentVisita.getFotografo();
            Usuario fotografoNew = visita.getFotografo();
            if (statusNew != null) {
                statusNew = em.getReference(statusNew.getClass(), statusNew.getId());
                visita.setStatus(statusNew);
            }
            if (nrContratoNew != null) {
                nrContratoNew = em.getReference(nrContratoNew.getClass(), nrContratoNew.getNrContrato());
                visita.setNrContrato(nrContratoNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                visita.setUsuario(usuarioNew);
            }
            if (fotografoNew != null) {
                fotografoNew = em.getReference(fotografoNew.getClass(), fotografoNew.getId());
                visita.setFotografo(fotografoNew);
            }
            visita = em.merge(visita);
            if (statusOld != null && !statusOld.equals(statusNew)) {
                statusOld.getVisitaList().remove(visita);
                statusOld = em.merge(statusOld);
            }
            if (statusNew != null && !statusNew.equals(statusOld)) {
                statusNew.getVisitaList().add(visita);
                statusNew = em.merge(statusNew);
            }
            if (nrContratoOld != null && !nrContratoOld.equals(nrContratoNew)) {
                nrContratoOld.getVisitaList().remove(visita);
                nrContratoOld = em.merge(nrContratoOld);
            }
            if (nrContratoNew != null && !nrContratoNew.equals(nrContratoOld)) {
                nrContratoNew.getVisitaList().add(visita);
                nrContratoNew = em.merge(nrContratoNew);
            }
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getVisitaList().remove(visita);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getVisitaList().add(visita);
                usuarioNew = em.merge(usuarioNew);
            }
            if (fotografoOld != null && !fotografoOld.equals(fotografoNew)) {
                fotografoOld.getVisitaList().remove(visita);
                fotografoOld = em.merge(fotografoOld);
            }
            if (fotografoNew != null && !fotografoNew.equals(fotografoOld)) {
                fotografoNew.getVisitaList().add(visita);
                fotografoNew = em.merge(fotografoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = visita.getId();
                if (findVisita(id) == null) {
                    throw new NonexistentEntityException("The visita with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Visita visita;
            try {
                visita = em.getReference(Visita.class, id);
                visita.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The visita with id " + id + " no longer exists.", enfe);
            }
            StatusVisita status = visita.getStatus();
            if (status != null) {
                status.getVisitaList().remove(visita);
                status = em.merge(status);
            }
            Contrato nrContrato = visita.getNrContrato();
            if (nrContrato != null) {
                nrContrato.getVisitaList().remove(visita);
                nrContrato = em.merge(nrContrato);
            }
            Usuario usuario = visita.getUsuario();
            if (usuario != null) {
                usuario.getVisitaList().remove(visita);
                usuario = em.merge(usuario);
            }
            Usuario fotografo = visita.getFotografo();
            if (fotografo != null) {
                fotografo.getVisitaList().remove(visita);
                fotografo = em.merge(fotografo);
            }
            em.remove(visita);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Visita> findVisitaEntities() {
        return findVisitaEntities(true, -1, -1);
    }

    public List<Visita> findVisitaEntities(int maxResults, int firstResult) {
        return findVisitaEntities(false, maxResults, firstResult);
    }

    private List<Visita> findVisitaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Visita.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Visita findVisita(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Visita.class, id);
        } finally {
            em.close();
        }
    }

    public int getVisitaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Visita> rt = cq.from(Visita.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    
    public List findVisitas(Date dI, Date dF, Regiao idRegiao) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Visita.findByDtRegiaoVisita").setParameter("dataInicial", dI, TemporalType.DATE).setParameter("dataFinal", dF, TemporalType.DATE).setParameter("regiao", idRegiao).getResultList();
        } finally {
            em.close();
        }
    }
    
    public List findVisitas(Date dI, Date dF, Usuario vendedor) {
        EntityManager em = getEntityManager();
        try {
            if(vendedor == null){
                return em.createNamedQuery("Visita.findByDtVisita").setParameter("dataInicial", dI, TemporalType.DATE).setParameter("dataFinal", dF, TemporalType.DATE).getResultList();
            }else{
                return em.createNamedQuery("Visita.findByDtVendedorVisita").setParameter("dataInicial", dI, TemporalType.DATE).setParameter("dataFinal", dF, TemporalType.DATE).setParameter("vendedor", vendedor).getResultList();
            }
        } finally {
            em.close();
        }
    }
    
    public List findVisitasByFotografo(Date dI, Date dF, Usuario fotografo) {
        EntityManager em = getEntityManager();
        try {
            if(fotografo == null){
                return em.createNamedQuery("Visita.findByDtVisita").setParameter("dataInicial", dI, TemporalType.DATE).setParameter("dataFinal", dF, TemporalType.DATE).getResultList();
            }else{
                return em.createNamedQuery("Visita.findByDtFotografoVisita").setParameter("dataInicial", dI, TemporalType.DATE).setParameter("dataFinal", dF, TemporalType.DATE).setParameter("fotografo", fotografo).getResultList();
            }
        } finally {
            em.close();
        }
    }
    
//    public Object findPtsByCliente(Integer cliente) {
//        EntityManager em = getEntityManager();
//        try {
//            String sql = "SELECT IFNULL(0, pts_vista_realizado) FROM visita v INNER JOIN contrato c ON (v.nr_contrato = c.nr_contrato) WHERE c.cliente = ?";
//            return em.createNativeQuery(sql).setParameter(1, cliente).getSingleResult();
//        }catch(Exception e){
//            e.printStackTrace();
//            return 0;
//        }finally {
//            em.close();
//        }
//    }
    
    public List<Visita> findByCliente(Cliente cliente) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Visita.findByCliente").setParameter("cliente", cliente).getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }
    
    
    public Visita findByClienteImovelNull(Cliente cliente) {
        EntityManager em = getEntityManager();
        try {
            return (Visita) em.createNamedQuery("Visita.findByClienteImovelNull").setParameter("cliente", cliente).getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }
    
    public Visita findByClienteImovel(Cliente cliente, Imovel imovel) {
        EntityManager em = getEntityManager();
        try {
            return (Visita) em.createNamedQuery("Visita.findByClienteImovel").setParameter("cliente", cliente).setParameter("imovel", imovel).getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }
    
    public Visita findByImovel(Imovel imovel) {
        EntityManager em = getEntityManager();
        try {
            return (Visita) em.createNamedQuery("Visita.findByImovel").setParameter("imovel", imovel).getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            em.close();
        }
    }
    
    public List<Time> getHorariosDisponiveis(Date data, String idVisita, Integer idRegiaoVisita) {
        EntityManager em = getEntityManager();
        try {
//SELECT visita_horarios.horario
//FROM visita_horarios 
//LEFT JOIN (
//  SELECT horario
//  FROM visita_horarios AS d
//  JOIN visita AS b
//    ON (b.hr_visita_inicial BETWEEN d.horario AND DATE_ADD(d.horario, INTERVAL 3530 SECOND)
//    OR DATE_SUB(b.hr_visita_final, INTERVAL 900 SECOND) BETWEEN d.horario AND DATE_ADD(d.horario, INTERVAL 3530 SECOND))
//    AND b.dt_visita = '2018-02-21' and b.id != "" AND b.status = 1 AND b.regiao = 2
//) AS not_available
//ON visita_horarios.horario = not_available.horario
//WHERE not_available.horario is null
            String sql = "SELECT distinct visita_horarios.horario\n" +
                        "FROM visita_horarios \n" +
                        "LEFT JOIN (\n" +
                        "  SELECT horario\n" +
                        "  FROM visita_horarios AS d\n" +
                        "  JOIN visita AS b\n" +
                        "    ON (b.hr_visita_inicial BETWEEN d.horario AND DATE_ADD(d.horario, INTERVAL 3530 SECOND)\n" +
                        "    OR DATE_SUB(b.hr_visita_final, INTERVAL 900 SECOND) BETWEEN d.horario AND DATE_ADD(d.horario, INTERVAL 3530 SECOND))\n" +
                        "    AND b.dt_visita = ? AND b.id != ? AND b.status = 1 AND b.regiao = ?\n" +
                        ") AS not_available\n" +
                        "ON visita_horarios.horario = not_available.horario\n" +
                        "WHERE not_available.horario is null" ;
//            System.out.println(sql);
            return em.createNativeQuery(sql).setParameter(1, data).setParameter(2, idVisita).setParameter(3, idRegiaoVisita).getResultList();
        } finally {
            em.close();
        }
    }
    
    public Object[] findInformacoes(Usuario user, String periodo) {
        EntityManager em = getEntityManager();
        try {
            String filter = "";
            String sqlData = "''";
            switch (periodo) {
                case "semana":
//                    filter = " AND (WEEKOFYEAR(now()) = WEEKOFYEAR(r.data) and YEAR(now()) = YEAR(r.data)) ";
                    filter = " AND r.dt_visita BETWEEN ADDDATE(NOW(), INTERVAL 1-DAYOFWEEK(NOW()) DAY) AND ADDDATE(NOW(), INTERVAL 7-DAYOFWEEK(NOW()) DAY)";
                    sqlData = "DATE_FORMAT(ADDDATE(NOW(), INTERVAL 1-DAYOFWEEK(NOW()) DAY),'%Y-%m-%d'), DATE_FORMAT(ADDDATE(NOW(), INTERVAL 7-DAYOFWEEK(NOW()) DAY),'%Y-%m-%d')";
                    break;
                case "mes":
//                    filter = " AND (MONTH(now()) = MONTH(r.data) and YEAR(now()) = YEAR(r.data)) ";
                    filter = " AND r.dt_visita BETWEEN DATE_FORMAT(NOW(),'%Y-%m-01') AND DATE_FORMAT(LAST_DAY(NOW()),'%Y-%m-%d')";
                    sqlData = "DATE_FORMAT(NOW(),'%Y-%m-01'), DATE_FORMAT(LAST_DAY(NOW()),'%Y-%m-%d')";
                    break;
                case "ano":
//                    filter = " AND YEAR(now()) = YEAR(r.data) ";
                    filter = " AND r.dt_visita BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND DATE_FORMAT(NOW(),'%Y-12-31')";
                    sqlData = "DATE_FORMAT(NOW(),'%Y-01-01'), DATE_FORMAT(NOW(),'%Y-12-31')";
                    break;
            }
//            if (user.getTipo().getId() == 3) {//fotografo
//                filter += " and r.regiao = '" + user.getRegiao() + "' ";
//            }
            if (user.getTipo().getId() == 4) {
                filter += " and cliente.vendedor = " + user.getId();
            }
            String sql = "";
            sql = "SELECT " + sqlData + " data, SUM(IF(r.status = 1,1,0)) pendentes, SUM(IF(r.status = 3,1,0)) cancelados, SUM(IF(r.status = 2 || r.status = 4,1,0)) realizados, SUM(IF(r.status = 4,1,0)) pagos FROM `visita` r INNER JOIN cliente ON cliente.id = r.cliente where 1 " + filter;
            System.out.println(sql);
            return (Object[]) em.createNativeQuery(sql).getSingleResult();
        } finally {
            em.close();
        }
    }
    
    public List<Visita> findVisitas(Usuario user, String status, Date i, Date f) throws ParseException {
        EntityManager em = getEntityManager();
        try {
            String filtroData = "";
//            if (!cancelados || status.equalsIgnoreCase("todos")) {
//                filtroData += " and r.status != 2 ";
//            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (i != null) {
                filtroData += " and r.dt_visita >= '" + sdf.format(i) + "' ";
            }
            if (f != null) {
                filtroData += " and r.dt_visita <= '" + sdf.format(f) + "' ";
            }
            String filtroStatus = "";
            if (status != null && !status.equalsIgnoreCase("todos")) {
                if (status.equalsIgnoreCase("pendente")) {
                    filtroStatus = " and r.status = 1 ";
                } else if (status.equalsIgnoreCase("realizado")) {
                    if (user.getTipo().getId() == 1 || user.getTipo().getId() == 2){
                        filtroStatus = " and r.status = 2 ";
                    }else{
                        filtroStatus = " and (r.status = 2 or r.status = 4) ";
                    }
                } else if (status.equalsIgnoreCase("cancelado")) {
                    filtroStatus = " and r.status = 3 ";
                } else if (status.equalsIgnoreCase("pago")) {
                    filtroStatus = " and r.status = 4 ";
                }
            }
            String sql = "";
            
            if (user.getTipo().getId() == 1 || user.getTipo().getId() == 2 || user.getTipo().getId() == 5) {
                sql = "SELECT * FROM visita r WHERE 1 " + filtroData + filtroStatus;
            } else if (user.getTipo().getId() == 3) {//fotografo
//                and r.regiao = " + user.getRegiao().getId()
                sql = "SELECT * FROM visita r WHERE 1 " + filtroData + filtroStatus;
            } else if (user.getTipo().getId() == 4) {//vendedor
                sql = "SELECT * FROM visita r INNER JOIN cliente c ON r.cliente = c.id WHERE 1 and c.vendedor = " + user.getId() + filtroData + filtroStatus;
            }
            System.out.println(sql);
            return em.createNativeQuery(sql, Visita.class).getResultList();
        } finally {
            em.close();
        }
    }
    
    public List findVisitasByCliente(Cliente cliente) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Visita.findByCliente").setParameter("cliente", cliente).getResultList();
        } finally {
            em.close();
        }
    }
    
    public List findVisitasPendentesByData(Date dtVisita) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Visita.findPendenteByDtVisita").setParameter("dtVisita", dtVisita).getResultList();
        } finally {
            em.close();
        }
    }
}
