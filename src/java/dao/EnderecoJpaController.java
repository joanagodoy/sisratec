/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Cidade;
import dominio.Cliente;
import java.util.ArrayList;
import java.util.List;
import dominio.Imovel;
import dominio.Endereco;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class EnderecoJpaController implements Serializable {

    public EnderecoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Endereco endereco) {
        if (endereco.getClienteList() == null) {
            endereco.setClienteList(new ArrayList<Cliente>());
        }
        if (endereco.getImovelList() == null) {
            endereco.setImovelList(new ArrayList<Imovel>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cidade cidade = endereco.getCidade();
            if (cidade != null) {
                cidade = em.getReference(cidade.getClass(), cidade.getId());
                endereco.setCidade(cidade);
            }
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : endereco.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getId());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            endereco.setClienteList(attachedClienteList);
            List<Imovel> attachedImovelList = new ArrayList<Imovel>();
            for (Imovel apartamentoListImovelToAttach : endereco.getImovelList()) {
                apartamentoListImovelToAttach = em.getReference(apartamentoListImovelToAttach.getClass(), apartamentoListImovelToAttach.getId());
                attachedImovelList.add(apartamentoListImovelToAttach);
            }
            endereco.setImovelList(attachedImovelList);
            em.persist(endereco);
            if (cidade != null) {
                cidade.getEnderecoList().add(endereco);
                cidade = em.merge(cidade);
            }
            for (Cliente clienteListCliente : endereco.getClienteList()) {
                Endereco oldEnderecoOfClienteListCliente = clienteListCliente.getEndereco();
                clienteListCliente.setEndereco(endereco);
                clienteListCliente = em.merge(clienteListCliente);
                if (oldEnderecoOfClienteListCliente != null) {
                    oldEnderecoOfClienteListCliente.getClienteList().remove(clienteListCliente);
                    oldEnderecoOfClienteListCliente = em.merge(oldEnderecoOfClienteListCliente);
                }
            }
            for (Imovel apartamentoListImovel : endereco.getImovelList()) {
                Endereco oldEnderecoOfImovelListImovel = apartamentoListImovel.getEndereco();
                apartamentoListImovel.setEndereco(endereco);
                apartamentoListImovel = em.merge(apartamentoListImovel);
                if (oldEnderecoOfImovelListImovel != null) {
                    oldEnderecoOfImovelListImovel.getImovelList().remove(apartamentoListImovel);
                    oldEnderecoOfImovelListImovel = em.merge(oldEnderecoOfImovelListImovel);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Endereco endereco) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Endereco persistentEndereco = em.find(Endereco.class, endereco.getId());
            Cidade cidadeOld = persistentEndereco.getCidade();
            Cidade cidadeNew = endereco.getCidade();
            List<Cliente> clienteListOld = persistentEndereco.getClienteList();
            List<Cliente> clienteListNew = endereco.getClienteList();
            List<Imovel> apartamentoListOld = persistentEndereco.getImovelList();
            List<Imovel> apartamentoListNew = endereco.getImovelList();
            List<String> illegalOrphanMessages = null;
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cliente " + clienteListOldCliente + " since its endereco field is not nullable.");
                }
            }
//            for (Imovel apartamentoListOldImovel : apartamentoListOld) {
//                if (!apartamentoListNew.contains(apartamentoListOldImovel)) {
//                    if (illegalOrphanMessages == null) {
//                        illegalOrphanMessages = new ArrayList<String>();
//                    }
//                    illegalOrphanMessages.add("You must retain Imovel " + apartamentoListOldImovel + " since its endereco field is not nullable.");
//                }
//            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cidadeNew != null) {
                cidadeNew = em.getReference(cidadeNew.getClass(), cidadeNew.getId());
                endereco.setCidade(cidadeNew);
            }
            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
//            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
//                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getId());
//                attachedClienteListNew.add(clienteListNewClienteToAttach);
//            }
            clienteListNew = attachedClienteListNew;
            endereco.setClienteList(clienteListNew);
            List<Imovel> attachedImovelListNew = new ArrayList<Imovel>();
//            for (Imovel apartamentoListNewImovelToAttach : apartamentoListNew) {
//                apartamentoListNewImovelToAttach = em.getReference(apartamentoListNewImovelToAttach.getClass(), apartamentoListNewImovelToAttach.getId());
//                attachedImovelListNew.add(apartamentoListNewImovelToAttach);
//            }
            apartamentoListNew = attachedImovelListNew;
            endereco.setImovelList(apartamentoListNew);
            endereco = em.merge(endereco);
            if (cidadeOld != null && !cidadeOld.equals(cidadeNew)) {
                cidadeOld.getEnderecoList().remove(endereco);
                cidadeOld = em.merge(cidadeOld);
            }
            if (cidadeNew != null && !cidadeNew.equals(cidadeOld)) {
                cidadeNew.getEnderecoList().add(endereco);
                cidadeNew = em.merge(cidadeNew);
            }
//            for (Cliente clienteListNewCliente : clienteListNew) {
//                if (!clienteListOld.contains(clienteListNewCliente)) {
//                    Endereco oldEnderecoOfClienteListNewCliente = clienteListNewCliente.getEndereco();
//                    clienteListNewCliente.setEndereco(endereco);
//                    clienteListNewCliente = em.merge(clienteListNewCliente);
//                    if (oldEnderecoOfClienteListNewCliente != null && !oldEnderecoOfClienteListNewCliente.equals(endereco)) {
//                        oldEnderecoOfClienteListNewCliente.getClienteList().remove(clienteListNewCliente);
//                        oldEnderecoOfClienteListNewCliente = em.merge(oldEnderecoOfClienteListNewCliente);
//                    }
//                }
//            }
//            for (Imovel apartamentoListNewImovel : apartamentoListNew) {
//                if (!apartamentoListOld.contains(apartamentoListNewImovel)) {
//                    Endereco oldEnderecoOfImovelListNewImovel = apartamentoListNewImovel.getEndereco();
//                    apartamentoListNewImovel.setEndereco(endereco);
//                    apartamentoListNewImovel = em.merge(apartamentoListNewImovel);
//                    if (oldEnderecoOfImovelListNewImovel != null && !oldEnderecoOfImovelListNewImovel.equals(endereco)) {
//                        oldEnderecoOfImovelListNewImovel.getImovelList().remove(apartamentoListNewImovel);
//                        oldEnderecoOfImovelListNewImovel = em.merge(oldEnderecoOfImovelListNewImovel);
//                    }
//                }
//            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = endereco.getId();
                if (findEndereco(id) == null) {
                    throw new NonexistentEntityException("The endereco with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Endereco endereco;
            try {
                endereco = em.getReference(Endereco.class, id);
                endereco.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The endereco with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Cliente> clienteListOrphanCheck = endereco.getClienteList();
            for (Cliente clienteListOrphanCheckCliente : clienteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Endereco (" + endereco + ") cannot be destroyed since the Cliente " + clienteListOrphanCheckCliente + " in its clienteList field has a non-nullable endereco field.");
            }
            List<Imovel> apartamentoListOrphanCheck = endereco.getImovelList();
            for (Imovel apartamentoListOrphanCheckImovel : apartamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Endereco (" + endereco + ") cannot be destroyed since the Imovel " + apartamentoListOrphanCheckImovel + " in its apartamentoList field has a non-nullable endereco field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cidade cidade = endereco.getCidade();
            if (cidade != null) {
                cidade.getEnderecoList().remove(endereco);
                cidade = em.merge(cidade);
            }
            em.remove(endereco);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Endereco> findEnderecoEntities() {
        return findEnderecoEntities(true, -1, -1);
    }

    public List<Endereco> findEnderecoEntities(int maxResults, int firstResult) {
        return findEnderecoEntities(false, maxResults, firstResult);
    }

    private List<Endereco> findEnderecoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Endereco.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Endereco findEndereco(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Endereco.class, id);
        } finally {
            em.close();
        }
    }

    public int getEnderecoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Endereco> rt = cq.from(Endereco.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
