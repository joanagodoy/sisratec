/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.StatusFotografia;
import dominio.Cliente;
import dominio.Fotografia;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class FotografiaJpaController implements Serializable {

    public FotografiaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Fotografia fotografia) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatusFotografia status = fotografia.getStatus();
            if (status != null) {
                status = em.getReference(status.getClass(), status.getId());
                fotografia.setStatus(status);
            }
            Cliente idCliente = fotografia.getIdCliente();
            if (idCliente != null) {
                idCliente = em.getReference(idCliente.getClass(), idCliente.getId());
                fotografia.setIdCliente(idCliente);
            }
            em.persist(fotografia);
            if (status != null) {
                status.getFotografiaList().add(fotografia);
                status = em.merge(status);
            }
            if (idCliente != null) {
                idCliente.getFotografiaList().add(fotografia);
                idCliente = em.merge(idCliente);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Fotografia fotografia) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fotografia persistentFotografia = em.find(Fotografia.class, fotografia.getId());
            StatusFotografia statusOld = persistentFotografia.getStatus();
            StatusFotografia statusNew = fotografia.getStatus();
            Cliente idClienteOld = persistentFotografia.getIdCliente();
            Cliente idClienteNew = fotografia.getIdCliente();
            if (statusNew != null) {
                statusNew = em.getReference(statusNew.getClass(), statusNew.getId());
                fotografia.setStatus(statusNew);
            }
            if (idClienteNew != null) {
                idClienteNew = em.getReference(idClienteNew.getClass(), idClienteNew.getId());
                fotografia.setIdCliente(idClienteNew);
            }
            fotografia = em.merge(fotografia);
            if (statusOld != null && !statusOld.equals(statusNew)) {
                statusOld.getFotografiaList().remove(fotografia);
                statusOld = em.merge(statusOld);
            }
            if (statusNew != null && !statusNew.equals(statusOld)) {
                statusNew.getFotografiaList().add(fotografia);
                statusNew = em.merge(statusNew);
            }
            if (idClienteOld != null && !idClienteOld.equals(idClienteNew)) {
                idClienteOld.getFotografiaList().remove(fotografia);
                idClienteOld = em.merge(idClienteOld);
            }
            if (idClienteNew != null && !idClienteNew.equals(idClienteOld)) {
                idClienteNew.getFotografiaList().add(fotografia);
                idClienteNew = em.merge(idClienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = fotografia.getId();
                if (findFotografia(id) == null) {
                    throw new NonexistentEntityException("The fotografia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fotografia fotografia;
            try {
                fotografia = em.getReference(Fotografia.class, id);
                fotografia.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fotografia with id " + id + " no longer exists.", enfe);
            }
            StatusFotografia status = fotografia.getStatus();
            if (status != null) {
                status.getFotografiaList().remove(fotografia);
                status = em.merge(status);
            }
            Cliente idCliente = fotografia.getIdCliente();
            if (idCliente != null) {
                idCliente.getFotografiaList().remove(fotografia);
                idCliente = em.merge(idCliente);
            }
            em.remove(fotografia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Fotografia> findFotografiaEntities() {
        return findFotografiaEntities(true, -1, -1);
    }

    public List<Fotografia> findFotografiaEntities(int maxResults, int firstResult) {
        return findFotografiaEntities(false, maxResults, firstResult);
    }

    private List<Fotografia> findFotografiaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Fotografia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Fotografia findFotografia(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Fotografia.class, id);
        } finally {
            em.close();
        }
    }

    public int getFotografiaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Fotografia> rt = cq.from(Fotografia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Fotografia> findFotografiasFotografo() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Fotografia.findFotografo").getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Fotografia> findFotografiasEditor() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Fotografia.findEditor").getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Fotografia> findFotografiasByStatus(Integer status) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Fotografia.findByStatus").setParameter("status", status).getResultList();
        } finally {
            em.close();
        }
    }
    
    public Fotografia findFotografiaByCliente(Cliente cliente) {
        EntityManager em = getEntityManager();
        try {
            return (Fotografia) em.createNamedQuery("Fotografia.findByCliente").setParameter("cliente", cliente).getSingleResult();
        }catch(Exception e){ 
            return null;
        }finally {
            em.close();
        }
    }
    
    public List<Fotografia> findFotografiasByCliente(Cliente cliente) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Fotografia.findByCliente").setParameter("cliente", cliente).getResultList();
        }catch(Exception e){ 
            return null;
        }finally {
            em.close();
        }
    }
    
}
