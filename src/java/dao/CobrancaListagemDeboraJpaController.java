/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import dominio.CobrancaListagemDebora;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Joana
 */
public class CobrancaListagemDeboraJpaController implements Serializable {

    public CobrancaListagemDeboraJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaListagemDebora cobrancaListagemDebora) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cobrancaListagemDebora);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCobrancaListagemDebora(cobrancaListagemDebora.getCliente()) != null) {
                throw new PreexistingEntityException("CobrancaListagemDebora " + cobrancaListagemDebora + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaListagemDebora cobrancaListagemDebora) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cobrancaListagemDebora = em.merge(cobrancaListagemDebora);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaListagemDebora.getCliente();
                if (findCobrancaListagemDebora(id) == null) {
                    throw new NonexistentEntityException("The cobrancaListagemDebora with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaListagemDebora cobrancaListagemDebora;
            try {
                cobrancaListagemDebora = em.getReference(CobrancaListagemDebora.class, id);
                cobrancaListagemDebora.getCliente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaListagemDebora with id " + id + " no longer exists.", enfe);
            }
            em.remove(cobrancaListagemDebora);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaListagemDebora> findCobrancaListagemDeboraEntities() {
        return findCobrancaListagemDeboraEntities(true, -1, -1);
    }

    public List<CobrancaListagemDebora> findCobrancaListagemDeboraEntities(int maxResults, int firstResult) {
        return findCobrancaListagemDeboraEntities(false, maxResults, firstResult);
    }

    private List<CobrancaListagemDebora> findCobrancaListagemDeboraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaListagemDebora.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaListagemDebora findCobrancaListagemDebora(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaListagemDebora.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaListagemDeboraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaListagemDebora> rt = cq.from(CobrancaListagemDebora.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
