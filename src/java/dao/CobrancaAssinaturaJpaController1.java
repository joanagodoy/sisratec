/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Cliente;
import dominio.CobrancaAssinatura;
import dominio.CobrancaRegistro;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Joana
 */
public class CobrancaAssinaturaJpaController1 implements Serializable {

    public CobrancaAssinaturaJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaAssinatura cobrancaAssinatura) {
        if (cobrancaAssinatura.getCobrancaRegistroList() == null) {
            cobrancaAssinatura.setCobrancaRegistroList(new ArrayList<CobrancaRegistro>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente cliente = cobrancaAssinatura.getCliente();
            if (cliente != null) {
                cliente = em.getReference(cliente.getClass(), cliente.getId());
                cobrancaAssinatura.setCliente(cliente);
            }
            List<CobrancaRegistro> attachedCobrancaRegistroList = new ArrayList<CobrancaRegistro>();
            for (CobrancaRegistro cobrancaRegistroListCobrancaRegistroToAttach : cobrancaAssinatura.getCobrancaRegistroList()) {
                cobrancaRegistroListCobrancaRegistroToAttach = em.getReference(cobrancaRegistroListCobrancaRegistroToAttach.getClass(), cobrancaRegistroListCobrancaRegistroToAttach.getId());
                attachedCobrancaRegistroList.add(cobrancaRegistroListCobrancaRegistroToAttach);
            }
            cobrancaAssinatura.setCobrancaRegistroList(attachedCobrancaRegistroList);
            em.persist(cobrancaAssinatura);
            if (cliente != null) {
                cliente.getCobrancaAssinaturaList().add(cobrancaAssinatura);
                cliente = em.merge(cliente);
            }
            for (CobrancaRegistro cobrancaRegistroListCobrancaRegistro : cobrancaAssinatura.getCobrancaRegistroList()) {
                CobrancaAssinatura oldAssinaturaOfCobrancaRegistroListCobrancaRegistro = cobrancaRegistroListCobrancaRegistro.getAssinatura();
                cobrancaRegistroListCobrancaRegistro.setAssinatura(cobrancaAssinatura);
                cobrancaRegistroListCobrancaRegistro = em.merge(cobrancaRegistroListCobrancaRegistro);
                if (oldAssinaturaOfCobrancaRegistroListCobrancaRegistro != null) {
                    oldAssinaturaOfCobrancaRegistroListCobrancaRegistro.getCobrancaRegistroList().remove(cobrancaRegistroListCobrancaRegistro);
                    oldAssinaturaOfCobrancaRegistroListCobrancaRegistro = em.merge(oldAssinaturaOfCobrancaRegistroListCobrancaRegistro);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaAssinatura cobrancaAssinatura) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaAssinatura persistentCobrancaAssinatura = em.find(CobrancaAssinatura.class, cobrancaAssinatura.getId());
            Cliente clienteOld = persistentCobrancaAssinatura.getCliente();
            Cliente clienteNew = cobrancaAssinatura.getCliente();
            List<CobrancaRegistro> cobrancaRegistroListOld = persistentCobrancaAssinatura.getCobrancaRegistroList();
            List<CobrancaRegistro> cobrancaRegistroListNew = cobrancaAssinatura.getCobrancaRegistroList();
            List<String> illegalOrphanMessages = null;
            for (CobrancaRegistro cobrancaRegistroListOldCobrancaRegistro : cobrancaRegistroListOld) {
                if (!cobrancaRegistroListNew.contains(cobrancaRegistroListOldCobrancaRegistro)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CobrancaRegistro " + cobrancaRegistroListOldCobrancaRegistro + " since its assinatura field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clienteNew != null) {
                clienteNew = em.getReference(clienteNew.getClass(), clienteNew.getId());
                cobrancaAssinatura.setCliente(clienteNew);
            }
            List<CobrancaRegistro> attachedCobrancaRegistroListNew = new ArrayList<CobrancaRegistro>();
            for (CobrancaRegistro cobrancaRegistroListNewCobrancaRegistroToAttach : cobrancaRegistroListNew) {
                cobrancaRegistroListNewCobrancaRegistroToAttach = em.getReference(cobrancaRegistroListNewCobrancaRegistroToAttach.getClass(), cobrancaRegistroListNewCobrancaRegistroToAttach.getId());
                attachedCobrancaRegistroListNew.add(cobrancaRegistroListNewCobrancaRegistroToAttach);
            }
            cobrancaRegistroListNew = attachedCobrancaRegistroListNew;
            cobrancaAssinatura.setCobrancaRegistroList(cobrancaRegistroListNew);
            cobrancaAssinatura = em.merge(cobrancaAssinatura);
            if (clienteOld != null && !clienteOld.equals(clienteNew)) {
                clienteOld.getCobrancaAssinaturaList().remove(cobrancaAssinatura);
                clienteOld = em.merge(clienteOld);
            }
            if (clienteNew != null && !clienteNew.equals(clienteOld)) {
                clienteNew.getCobrancaAssinaturaList().add(cobrancaAssinatura);
                clienteNew = em.merge(clienteNew);
            }
            for (CobrancaRegistro cobrancaRegistroListNewCobrancaRegistro : cobrancaRegistroListNew) {
                if (!cobrancaRegistroListOld.contains(cobrancaRegistroListNewCobrancaRegistro)) {
                    CobrancaAssinatura oldAssinaturaOfCobrancaRegistroListNewCobrancaRegistro = cobrancaRegistroListNewCobrancaRegistro.getAssinatura();
                    cobrancaRegistroListNewCobrancaRegistro.setAssinatura(cobrancaAssinatura);
                    cobrancaRegistroListNewCobrancaRegistro = em.merge(cobrancaRegistroListNewCobrancaRegistro);
                    if (oldAssinaturaOfCobrancaRegistroListNewCobrancaRegistro != null && !oldAssinaturaOfCobrancaRegistroListNewCobrancaRegistro.equals(cobrancaAssinatura)) {
                        oldAssinaturaOfCobrancaRegistroListNewCobrancaRegistro.getCobrancaRegistroList().remove(cobrancaRegistroListNewCobrancaRegistro);
                        oldAssinaturaOfCobrancaRegistroListNewCobrancaRegistro = em.merge(oldAssinaturaOfCobrancaRegistroListNewCobrancaRegistro);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaAssinatura.getId();
                if (findCobrancaAssinatura(id) == null) {
                    throw new NonexistentEntityException("The cobrancaAssinatura with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaAssinatura cobrancaAssinatura;
            try {
                cobrancaAssinatura = em.getReference(CobrancaAssinatura.class, id);
                cobrancaAssinatura.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaAssinatura with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CobrancaRegistro> cobrancaRegistroListOrphanCheck = cobrancaAssinatura.getCobrancaRegistroList();
            for (CobrancaRegistro cobrancaRegistroListOrphanCheckCobrancaRegistro : cobrancaRegistroListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CobrancaAssinatura (" + cobrancaAssinatura + ") cannot be destroyed since the CobrancaRegistro " + cobrancaRegistroListOrphanCheckCobrancaRegistro + " in its cobrancaRegistroList field has a non-nullable assinatura field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cliente cliente = cobrancaAssinatura.getCliente();
            if (cliente != null) {
                cliente.getCobrancaAssinaturaList().remove(cobrancaAssinatura);
                cliente = em.merge(cliente);
            }
            em.remove(cobrancaAssinatura);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaAssinatura> findCobrancaAssinaturaEntities() {
        return findCobrancaAssinaturaEntities(true, -1, -1);
    }

    public List<CobrancaAssinatura> findCobrancaAssinaturaEntities(int maxResults, int firstResult) {
        return findCobrancaAssinaturaEntities(false, maxResults, firstResult);
    }

    private List<CobrancaAssinatura> findCobrancaAssinaturaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaAssinatura.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaAssinatura findCobrancaAssinatura(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaAssinatura.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaAssinaturaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaAssinatura> rt = cq.from(CobrancaAssinatura.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
