/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.TipoUsuario;
import dominio.Regiao;
import dominio.Cliente;
import java.util.ArrayList;
import java.util.List;
import dominio.Visita;
import dominio.Contrato;
import dominio.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws PreexistingEntityException, Exception {
        if (usuario.getClienteList() == null) {
            usuario.setClienteList(new ArrayList<Cliente>());
        }
        if (usuario.getVisitaList() == null) {
            usuario.setVisitaList(new ArrayList<Visita>());
        }
//        if (usuario.getContratoList() == null) {
//            usuario.setContratoList(new ArrayList<Contrato>());
//        }
        if (usuario.getContratoList1() == null) {
            usuario.setContratoList1(new ArrayList<Contrato>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoUsuario tipo = usuario.getTipo();
            if (tipo != null) {
                tipo = em.getReference(tipo.getClass(), tipo.getId());
                usuario.setTipo(tipo);
            }
            Regiao regiao = usuario.getRegiao();
            if (regiao != null) {
                regiao = em.getReference(regiao.getClass(), regiao.getId());
                usuario.setRegiao(regiao);
            }
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : usuario.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getId());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            usuario.setClienteList(attachedClienteList);
            List<Visita> attachedVisitaList = new ArrayList<Visita>();
            for (Visita visitaListVisitaToAttach : usuario.getVisitaList()) {
                visitaListVisitaToAttach = em.getReference(visitaListVisitaToAttach.getClass(), visitaListVisitaToAttach.getId());
                attachedVisitaList.add(visitaListVisitaToAttach);
            }
            usuario.setVisitaList(attachedVisitaList);
            List<Contrato> attachedContratoList = new ArrayList<Contrato>();
//            for (Contrato contratoListContratoToAttach : usuario.getContratoList()) {
//                contratoListContratoToAttach = em.getReference(contratoListContratoToAttach.getClass(), contratoListContratoToAttach.getNrContrato());
//                attachedContratoList.add(contratoListContratoToAttach);
//            }
//            usuario.setContratoList(attachedContratoList);
            List<Contrato> attachedContratoList1 = new ArrayList<Contrato>();
            for (Contrato contratoList1ContratoToAttach : usuario.getContratoList1()) {
                contratoList1ContratoToAttach = em.getReference(contratoList1ContratoToAttach.getClass(), contratoList1ContratoToAttach.getNrContrato());
                attachedContratoList1.add(contratoList1ContratoToAttach);
            }
            usuario.setContratoList1(attachedContratoList1);
            em.persist(usuario);
            if (tipo != null) {
                tipo.getUsuarioList().add(usuario);
                tipo = em.merge(tipo);
            }
            if (regiao != null) {
                regiao.getUsuarioList().add(usuario);
                regiao = em.merge(regiao);
            }
            for (Cliente clienteListCliente : usuario.getClienteList()) {
                Usuario oldUsuarioOfClienteListCliente = clienteListCliente.getUsuario();
                clienteListCliente.setUsuario(usuario);
                clienteListCliente = em.merge(clienteListCliente);
                if (oldUsuarioOfClienteListCliente != null) {
                    oldUsuarioOfClienteListCliente.getClienteList().remove(clienteListCliente);
                    oldUsuarioOfClienteListCliente = em.merge(oldUsuarioOfClienteListCliente);
                }
            }
            for (Visita visitaListVisita : usuario.getVisitaList()) {
                Usuario oldUsuarioOfVisitaListVisita = visitaListVisita.getUsuario();
                visitaListVisita.setUsuario(usuario);
                visitaListVisita = em.merge(visitaListVisita);
                if (oldUsuarioOfVisitaListVisita != null) {
                    oldUsuarioOfVisitaListVisita.getVisitaList().remove(visitaListVisita);
                    oldUsuarioOfVisitaListVisita = em.merge(oldUsuarioOfVisitaListVisita);
                }
            }
            for (Contrato contratoList1Contrato : usuario.getContratoList1()) {
                Usuario oldUsuarioOfContratoList1Contrato = contratoList1Contrato.getUsuario();
                contratoList1Contrato.setUsuario(usuario);
                contratoList1Contrato = em.merge(contratoList1Contrato);
                if (oldUsuarioOfContratoList1Contrato != null) {
                    oldUsuarioOfContratoList1Contrato.getContratoList1().remove(contratoList1Contrato);
                    oldUsuarioOfContratoList1Contrato = em.merge(oldUsuarioOfContratoList1Contrato);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsuario(usuario.getId()) != null) {
                throw new PreexistingEntityException("Usuario " + usuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getId());
            TipoUsuario tipoOld = persistentUsuario.getTipo();
            TipoUsuario tipoNew = usuario.getTipo();
            Regiao regiaoOld = persistentUsuario.getRegiao();
            Regiao regiaoNew = usuario.getRegiao();
            List<Cliente> clienteListOld = persistentUsuario.getClienteList();
            List<Cliente> clienteListNew = usuario.getClienteList();
            List<Visita> visitaListOld = persistentUsuario.getVisitaList();
            List<Visita> visitaListNew = usuario.getVisitaList();
//            List<Contrato> contratoListOld = persistentUsuario.getContratoList();
//            List<Contrato> contratoListNew = usuario.getContratoList();
            List<Contrato> contratoList1Old = persistentUsuario.getContratoList1();
            List<Contrato> contratoList1New = usuario.getContratoList1();
            List<String> illegalOrphanMessages = null;
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cliente " + clienteListOldCliente + " since its usuario field is not nullable.");
                }
            }
            for (Visita visitaListOldVisita : visitaListOld) {
                if (!visitaListNew.contains(visitaListOldVisita)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Visita " + visitaListOldVisita + " since its usuario field is not nullable.");
                }
            }
//            for (Contrato contratoListOldContrato : contratoListOld) {
//                if (!contratoListNew.contains(contratoListOldContrato)) {
//                    if (illegalOrphanMessages == null) {
//                        illegalOrphanMessages = new ArrayList<String>();
//                    }
//                    illegalOrphanMessages.add("You must retain Contrato " + contratoListOldContrato + " since its vendedor field is not nullable.");
//                }
//            }
            for (Contrato contratoList1OldContrato : contratoList1Old) {
                if (!contratoList1New.contains(contratoList1OldContrato)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Contrato " + contratoList1OldContrato + " since its usuario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (tipoNew != null) {
                tipoNew = em.getReference(tipoNew.getClass(), tipoNew.getId());
                usuario.setTipo(tipoNew);
            }
            if (regiaoNew != null) {
                regiaoNew = em.getReference(regiaoNew.getClass(), regiaoNew.getId());
                usuario.setRegiao(regiaoNew);
            }
//            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
//            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
//                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getId());
//                attachedClienteListNew.add(clienteListNewClienteToAttach);
//            }
//            clienteListNew = attachedClienteListNew;
//            usuario.setClienteList(clienteListNew);
//            List<Visita> attachedVisitaListNew = new ArrayList<Visita>();
//            for (Visita visitaListNewVisitaToAttach : visitaListNew) {
//                visitaListNewVisitaToAttach = em.getReference(visitaListNewVisitaToAttach.getClass(), visitaListNewVisitaToAttach.getId());
//                attachedVisitaListNew.add(visitaListNewVisitaToAttach);
//            }
//            visitaListNew = attachedVisitaListNew;
//            usuario.setVisitaList(visitaListNew);
//            List<Contrato> attachedContratoListNew = new ArrayList<Contrato>();
//            for (Contrato contratoListNewContratoToAttach : contratoListNew) {
//                contratoListNewContratoToAttach = em.getReference(contratoListNewContratoToAttach.getClass(), contratoListNewContratoToAttach.getNrContrato());
//                attachedContratoListNew.add(contratoListNewContratoToAttach);
//            }
//            contratoListNew = attachedContratoListNew;
//            usuario.setContratoList(contratoListNew);
//            List<Contrato> attachedContratoList1New = new ArrayList<Contrato>();
//            for (Contrato contratoList1NewContratoToAttach : contratoList1New) {
//                contratoList1NewContratoToAttach = em.getReference(contratoList1NewContratoToAttach.getClass(), contratoList1NewContratoToAttach.getNrContrato());
//                attachedContratoList1New.add(contratoList1NewContratoToAttach);
//            }
//            contratoList1New = attachedContratoList1New;
//            usuario.setContratoList1(contratoList1New);
            usuario = em.merge(usuario);
            if (tipoOld != null && !tipoOld.equals(tipoNew)) {
                tipoOld.getUsuarioList().remove(usuario);
                tipoOld = em.merge(tipoOld);
            }
            if (tipoNew != null && !tipoNew.equals(tipoOld)) {
                tipoNew.getUsuarioList().add(usuario);
                tipoNew = em.merge(tipoNew);
            }
            if (regiaoOld != null && !regiaoOld.equals(regiaoNew)) {
                regiaoOld.getUsuarioList().remove(usuario);
                regiaoOld = em.merge(regiaoOld);
            }
            if (regiaoNew != null && !regiaoNew.equals(regiaoOld)) {
                regiaoNew.getUsuarioList().add(usuario);
                regiaoNew = em.merge(regiaoNew);
            }
//            for (Cliente clienteListNewCliente : clienteListNew) {
//                if (!clienteListOld.contains(clienteListNewCliente)) {
//                    Usuario oldUsuarioOfClienteListNewCliente = clienteListNewCliente.getUsuario();
//                    clienteListNewCliente.setUsuario(usuario);
//                    clienteListNewCliente = em.merge(clienteListNewCliente);
//                    if (oldUsuarioOfClienteListNewCliente != null && !oldUsuarioOfClienteListNewCliente.equals(usuario)) {
//                        oldUsuarioOfClienteListNewCliente.getClienteList().remove(clienteListNewCliente);
//                        oldUsuarioOfClienteListNewCliente = em.merge(oldUsuarioOfClienteListNewCliente);
//                    }
//                }
//            }
//            for (Visita visitaListNewVisita : visitaListNew) {
//                if (!visitaListOld.contains(visitaListNewVisita)) {
//                    Usuario oldUsuarioOfVisitaListNewVisita = visitaListNewVisita.getUsuario();
//                    visitaListNewVisita.setUsuario(usuario);
//                    visitaListNewVisita = em.merge(visitaListNewVisita);
//                    if (oldUsuarioOfVisitaListNewVisita != null && !oldUsuarioOfVisitaListNewVisita.equals(usuario)) {
//                        oldUsuarioOfVisitaListNewVisita.getVisitaList().remove(visitaListNewVisita);
//                        oldUsuarioOfVisitaListNewVisita = em.merge(oldUsuarioOfVisitaListNewVisita);
//                    }
//                }
//            }
//            for (Contrato contratoList1NewContrato : contratoList1New) {
//                if (!contratoList1Old.contains(contratoList1NewContrato)) {
//                    Usuario oldUsuarioOfContratoList1NewContrato = contratoList1NewContrato.getUsuario();
//                    contratoList1NewContrato.setUsuario(usuario);
//                    contratoList1NewContrato = em.merge(contratoList1NewContrato);
//                    if (oldUsuarioOfContratoList1NewContrato != null && !oldUsuarioOfContratoList1NewContrato.equals(usuario)) {
//                        oldUsuarioOfContratoList1NewContrato.getContratoList1().remove(contratoList1NewContrato);
//                        oldUsuarioOfContratoList1NewContrato = em.merge(oldUsuarioOfContratoList1NewContrato);
//                    }
//                }
//            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = usuario.getId();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Cliente> clienteListOrphanCheck = usuario.getClienteList();
            for (Cliente clienteListOrphanCheckCliente : clienteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Cliente " + clienteListOrphanCheckCliente + " in its clienteList field has a non-nullable usuario field.");
            }
            List<Visita> visitaListOrphanCheck = usuario.getVisitaList();
            for (Visita visitaListOrphanCheckVisita : visitaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Visita " + visitaListOrphanCheckVisita + " in its visitaList field has a non-nullable usuario field.");
            }
//            List<Contrato> contratoListOrphanCheck = usuario.getContratoList();
//            for (Contrato contratoListOrphanCheckContrato : contratoListOrphanCheck) {
//                if (illegalOrphanMessages == null) {
//                    illegalOrphanMessages = new ArrayList<String>();
//                }
//                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Contrato " + contratoListOrphanCheckContrato + " in its contratoList field has a non-nullable vendedor field.");
//            }
            List<Contrato> contratoList1OrphanCheck = usuario.getContratoList1();
            for (Contrato contratoList1OrphanCheckContrato : contratoList1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Contrato " + contratoList1OrphanCheckContrato + " in its contratoList1 field has a non-nullable usuario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TipoUsuario tipo = usuario.getTipo();
            if (tipo != null) {
                tipo.getUsuarioList().remove(usuario);
                tipo = em.merge(tipo);
            }
            Regiao regiao = usuario.getRegiao();
            if (regiao != null) {
                regiao.getUsuarioList().remove(usuario);
                regiao = em.merge(regiao);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    
    public Usuario findUsuarioByEmail(String usuario) {
        EntityManager em = getEntityManager();
        try {
            return (Usuario) em.createNamedQuery("Usuario.findByEmail").setParameter("email", usuario).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }
    
    public List<Usuario> findUsuarioVendedor() {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT * FROM usuario WHERE tipo = 4 or tipo = 1";
            return em.createNativeQuery(sql, Usuario.class).getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Usuario> findUsuarioFotografo() {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT * FROM usuario WHERE tipo = 3 or tipo = 1";
            return em.createNativeQuery(sql, Usuario.class).getResultList();
        } finally {
            em.close();
        }
    }

}
