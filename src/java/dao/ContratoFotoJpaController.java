/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Contrato;
import dominio.ContratoFoto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class ContratoFotoJpaController implements Serializable {

    public ContratoFotoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ContratoFoto contratoFoto) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Contrato contrato = contratoFoto.getContrato();
            if (contrato != null) {
                contrato = em.getReference(contrato.getClass(), contrato.getNrContrato());
                contratoFoto.setContrato(contrato);
            }
            em.persist(contratoFoto);
            if (contrato != null) {
                contrato.getContratoFotoList().add(contratoFoto);
                contrato = em.merge(contrato);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ContratoFoto contratoFoto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ContratoFoto persistentContratoFoto = em.find(ContratoFoto.class, contratoFoto.getId());
            Contrato contratoOld = persistentContratoFoto.getContrato();
            Contrato contratoNew = contratoFoto.getContrato();
            if (contratoNew != null) {
                contratoNew = em.getReference(contratoNew.getClass(), contratoNew.getNrContrato());
                contratoFoto.setContrato(contratoNew);
            }
            contratoFoto = em.merge(contratoFoto);
            if (contratoOld != null && !contratoOld.equals(contratoNew)) {
                contratoOld.getContratoFotoList().remove(contratoFoto);
                contratoOld = em.merge(contratoOld);
            }
            if (contratoNew != null && !contratoNew.equals(contratoOld)) {
                contratoNew.getContratoFotoList().add(contratoFoto);
                contratoNew = em.merge(contratoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = contratoFoto.getId();
                if (findContratoFoto(id) == null) {
                    throw new NonexistentEntityException("The contratoFoto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ContratoFoto contratoFoto;
            try {
                contratoFoto = em.getReference(ContratoFoto.class, id);
                contratoFoto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The contratoFoto with id " + id + " no longer exists.", enfe);
            }
            Contrato contrato = contratoFoto.getContrato();
            if (contrato != null) {
                contrato.getContratoFotoList().remove(contratoFoto);
                contrato = em.merge(contrato);
            }
            em.remove(contratoFoto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ContratoFoto> findContratoFotoEntities() {
        return findContratoFotoEntities(true, -1, -1);
    }

    public List<ContratoFoto> findContratoFotoEntities(int maxResults, int firstResult) {
        return findContratoFotoEntities(false, maxResults, firstResult);
    }

    private List<ContratoFoto> findContratoFotoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ContratoFoto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ContratoFoto findContratoFoto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ContratoFoto.class, id);
        } finally {
            em.close();
        }
    }

    public int getContratoFotoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ContratoFoto> rt = cq.from(ContratoFoto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List findByContrato(int parseInt) {
        EntityManager em = getEntityManager();
        try {
            return em.createNativeQuery("SELECT id from contrato_foto where contrato = ?").setParameter(1, parseInt).getResultList();
        } finally {
            em.close();
        }
    }
    
}
