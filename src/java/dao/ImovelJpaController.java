/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dominio.Imovel;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.Endereco;
import dominio.Cliente;
import dominio.Usuario;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class ImovelJpaController implements Serializable {

    public ImovelJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Imovel apartamento) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Endereco endereco = apartamento.getEndereco();
            if (endereco != null) {
                endereco = em.getReference(endereco.getClass(), endereco.getId());
                apartamento.setEndereco(endereco);
            }
            Cliente cliente = apartamento.getCliente();
            if (cliente != null) {
                cliente = em.getReference(cliente.getClass(), cliente.getId());
                apartamento.setCliente(cliente);
            }
            em.persist(apartamento);
            if (endereco != null) {
                endereco.getImovelList().add(apartamento);
                endereco = em.merge(endereco);
            }
            if (cliente != null) {
                cliente.getImovelList().add(apartamento);
                cliente = em.merge(cliente);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Imovel apartamento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Imovel persistentImovel = em.find(Imovel.class, apartamento.getId());
            Endereco enderecoOld = persistentImovel.getEndereco();
            Endereco enderecoNew = apartamento.getEndereco();
            Cliente clienteOld = persistentImovel.getCliente();
            Cliente clienteNew = apartamento.getCliente();
            if (enderecoNew != null) {
                enderecoNew = em.getReference(enderecoNew.getClass(), enderecoNew.getId());
                apartamento.setEndereco(enderecoNew);
            }
            if (clienteNew != null) {
                clienteNew = em.getReference(clienteNew.getClass(), clienteNew.getId());
                apartamento.setCliente(clienteNew);
            }
            apartamento = em.merge(apartamento);
            if (enderecoOld != null && !enderecoOld.equals(enderecoNew)) {
                enderecoOld.getImovelList().remove(apartamento);
                enderecoOld = em.merge(enderecoOld);
            }
            if (enderecoNew != null && !enderecoNew.equals(enderecoOld)) {
                enderecoNew.getImovelList().add(apartamento);
                enderecoNew = em.merge(enderecoNew);
            }
            if (clienteOld != null && !clienteOld.equals(clienteNew)) {
                clienteOld.getImovelList().remove(apartamento);
                clienteOld = em.merge(clienteOld);
            }
            if (clienteNew != null && !clienteNew.equals(clienteOld)) {
                clienteNew.getImovelList().add(apartamento);
                clienteNew = em.merge(clienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = apartamento.getId();
                if (findImovel(id) == null) {
                    throw new NonexistentEntityException("The apartamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Imovel apartamento;
            try {
                apartamento = em.getReference(Imovel.class, id);
                apartamento.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The apartamento with id " + id + " no longer exists.", enfe);
            }
            Endereco endereco = apartamento.getEndereco();
            if (endereco != null) {
                endereco.getImovelList().remove(apartamento);
                endereco = em.merge(endereco);
            }
            Cliente cliente = apartamento.getCliente();
            if (cliente != null) {
                cliente.getImovelList().remove(apartamento);
                cliente = em.merge(cliente);
            }
            em.remove(apartamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Imovel> findImovelEntities() {
        return findImovelEntities(true, -1, -1);
    }

    public List<Imovel> findImovelEntities(int maxResults, int firstResult) {
        return findImovelEntities(false, maxResults, firstResult);
    }

    private List<Imovel> findImovelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Imovel.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Imovel findImovel(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Imovel.class, id);
        } finally {
            em.close();
        }
    }

    public int getImovelCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Imovel> rt = cq.from(Imovel.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List findByVendedorSemVisita(Usuario usuario){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("Imovel.findByVendedorSemVisita").setParameter("usuario", usuario).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }finally{
            em.close();
        }
    }
    
    public List findSemVisita(){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("Imovel.findSemVisita").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }finally{
            em.close();
        }
    }
    
    public List<Imovel> findByCliente(Cliente cliente){
        EntityManager em = getEntityManager();
        try{
            return em.createNamedQuery("Imovel.findByCliente").setParameter("cliente", cliente).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }finally{
            em.close();
        }
    }
    
}
