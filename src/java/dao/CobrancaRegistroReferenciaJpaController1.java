/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import dominio.CobrancaFatura;
import dominio.CobrancaRegistroReferencia;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Joana
 */
public class CobrancaRegistroReferenciaJpaController1 implements Serializable {

    public CobrancaRegistroReferenciaJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CobrancaRegistroReferencia cobrancaRegistroReferencia) {
        if (cobrancaRegistroReferencia.getCobrancaFaturaList() == null) {
            cobrancaRegistroReferencia.setCobrancaFaturaList(new ArrayList<CobrancaFatura>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<CobrancaFatura> attachedCobrancaFaturaList = new ArrayList<CobrancaFatura>();
            for (CobrancaFatura cobrancaFaturaListCobrancaFaturaToAttach : cobrancaRegistroReferencia.getCobrancaFaturaList()) {
                cobrancaFaturaListCobrancaFaturaToAttach = em.getReference(cobrancaFaturaListCobrancaFaturaToAttach.getClass(), cobrancaFaturaListCobrancaFaturaToAttach.getId());
                attachedCobrancaFaturaList.add(cobrancaFaturaListCobrancaFaturaToAttach);
            }
            cobrancaRegistroReferencia.setCobrancaFaturaList(attachedCobrancaFaturaList);
            em.persist(cobrancaRegistroReferencia);
            for (CobrancaFatura cobrancaFaturaListCobrancaFatura : cobrancaRegistroReferencia.getCobrancaFaturaList()) {
                CobrancaRegistroReferencia oldRegistroOfCobrancaFaturaListCobrancaFatura = cobrancaFaturaListCobrancaFatura.getRegistro();
                cobrancaFaturaListCobrancaFatura.setRegistro(cobrancaRegistroReferencia);
                cobrancaFaturaListCobrancaFatura = em.merge(cobrancaFaturaListCobrancaFatura);
                if (oldRegistroOfCobrancaFaturaListCobrancaFatura != null) {
                    oldRegistroOfCobrancaFaturaListCobrancaFatura.getCobrancaFaturaList().remove(cobrancaFaturaListCobrancaFatura);
                    oldRegistroOfCobrancaFaturaListCobrancaFatura = em.merge(oldRegistroOfCobrancaFaturaListCobrancaFatura);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CobrancaRegistroReferencia cobrancaRegistroReferencia) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaRegistroReferencia persistentCobrancaRegistroReferencia = em.find(CobrancaRegistroReferencia.class, cobrancaRegistroReferencia.getId());
            List<CobrancaFatura> cobrancaFaturaListOld = persistentCobrancaRegistroReferencia.getCobrancaFaturaList();
            List<CobrancaFatura> cobrancaFaturaListNew = cobrancaRegistroReferencia.getCobrancaFaturaList();
            List<String> illegalOrphanMessages = null;
            for (CobrancaFatura cobrancaFaturaListOldCobrancaFatura : cobrancaFaturaListOld) {
                if (!cobrancaFaturaListNew.contains(cobrancaFaturaListOldCobrancaFatura)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CobrancaFatura " + cobrancaFaturaListOldCobrancaFatura + " since its registro field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<CobrancaFatura> attachedCobrancaFaturaListNew = new ArrayList<CobrancaFatura>();
            for (CobrancaFatura cobrancaFaturaListNewCobrancaFaturaToAttach : cobrancaFaturaListNew) {
                cobrancaFaturaListNewCobrancaFaturaToAttach = em.getReference(cobrancaFaturaListNewCobrancaFaturaToAttach.getClass(), cobrancaFaturaListNewCobrancaFaturaToAttach.getId());
                attachedCobrancaFaturaListNew.add(cobrancaFaturaListNewCobrancaFaturaToAttach);
            }
            cobrancaFaturaListNew = attachedCobrancaFaturaListNew;
            cobrancaRegistroReferencia.setCobrancaFaturaList(cobrancaFaturaListNew);
            cobrancaRegistroReferencia = em.merge(cobrancaRegistroReferencia);
            for (CobrancaFatura cobrancaFaturaListNewCobrancaFatura : cobrancaFaturaListNew) {
                if (!cobrancaFaturaListOld.contains(cobrancaFaturaListNewCobrancaFatura)) {
                    CobrancaRegistroReferencia oldRegistroOfCobrancaFaturaListNewCobrancaFatura = cobrancaFaturaListNewCobrancaFatura.getRegistro();
                    cobrancaFaturaListNewCobrancaFatura.setRegistro(cobrancaRegistroReferencia);
                    cobrancaFaturaListNewCobrancaFatura = em.merge(cobrancaFaturaListNewCobrancaFatura);
                    if (oldRegistroOfCobrancaFaturaListNewCobrancaFatura != null && !oldRegistroOfCobrancaFaturaListNewCobrancaFatura.equals(cobrancaRegistroReferencia)) {
                        oldRegistroOfCobrancaFaturaListNewCobrancaFatura.getCobrancaFaturaList().remove(cobrancaFaturaListNewCobrancaFatura);
                        oldRegistroOfCobrancaFaturaListNewCobrancaFatura = em.merge(oldRegistroOfCobrancaFaturaListNewCobrancaFatura);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cobrancaRegistroReferencia.getId();
                if (findCobrancaRegistroReferencia(id) == null) {
                    throw new NonexistentEntityException("The cobrancaRegistroReferencia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CobrancaRegistroReferencia cobrancaRegistroReferencia;
            try {
                cobrancaRegistroReferencia = em.getReference(CobrancaRegistroReferencia.class, id);
                cobrancaRegistroReferencia.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cobrancaRegistroReferencia with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CobrancaFatura> cobrancaFaturaListOrphanCheck = cobrancaRegistroReferencia.getCobrancaFaturaList();
            for (CobrancaFatura cobrancaFaturaListOrphanCheckCobrancaFatura : cobrancaFaturaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CobrancaRegistroReferencia (" + cobrancaRegistroReferencia + ") cannot be destroyed since the CobrancaFatura " + cobrancaFaturaListOrphanCheckCobrancaFatura + " in its cobrancaFaturaList field has a non-nullable registro field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(cobrancaRegistroReferencia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CobrancaRegistroReferencia> findCobrancaRegistroReferenciaEntities() {
        return findCobrancaRegistroReferenciaEntities(true, -1, -1);
    }

    public List<CobrancaRegistroReferencia> findCobrancaRegistroReferenciaEntities(int maxResults, int firstResult) {
        return findCobrancaRegistroReferenciaEntities(false, maxResults, firstResult);
    }

    private List<CobrancaRegistroReferencia> findCobrancaRegistroReferenciaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CobrancaRegistroReferencia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CobrancaRegistroReferencia findCobrancaRegistroReferencia(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CobrancaRegistroReferencia.class, id);
        } finally {
            em.close();
        }
    }

    public int getCobrancaRegistroReferenciaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CobrancaRegistroReferencia> rt = cq.from(CobrancaRegistroReferencia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
