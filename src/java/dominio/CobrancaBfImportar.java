/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Joana
 */
@Entity
@Table(name = "cobranca_bf_importar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobrancaBfImportar.findAll", query = "SELECT c FROM CobrancaBfImportar c"),
    @NamedQuery(name = "CobrancaBfImportar.findByCode", query = "SELECT c FROM CobrancaBfImportar c WHERE c.code = :code"),
    @NamedQuery(name = "CobrancaBfImportar.findByNome", query = "SELECT c FROM CobrancaBfImportar c WHERE c.nome = :nome"),
    @NamedQuery(name = "CobrancaBfImportar.findByCnpjCpf", query = "SELECT c FROM CobrancaBfImportar c WHERE c.cnpjCpf = :cnpjCpf"),
    @NamedQuery(name = "CobrancaBfImportar.findByDescricao", query = "SELECT c FROM CobrancaBfImportar c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "CobrancaBfImportar.findByDtEmissao", query = "SELECT c FROM CobrancaBfImportar c WHERE c.dtEmissao = :dtEmissao"),
    @NamedQuery(name = "CobrancaBfImportar.findByDtVencimento", query = "SELECT c FROM CobrancaBfImportar c WHERE c.dtVencimento = :dtVencimento"),
    @NamedQuery(name = "CobrancaBfImportar.findByValor", query = "SELECT c FROM CobrancaBfImportar c WHERE c.valor = :valor"),
    @NamedQuery(name = "CobrancaBfImportar.findByValorPago", query = "SELECT c FROM CobrancaBfImportar c WHERE c.valorPago = :valorPago"),
    @NamedQuery(name = "CobrancaBfImportar.findByDtPagamento", query = "SELECT c FROM CobrancaBfImportar c WHERE c.dtPagamento = :dtPagamento"),
    @NamedQuery(name = "CobrancaBfImportar.findByStatus", query = "SELECT c FROM CobrancaBfImportar c WHERE c.status = :status")})
public class CobrancaBfImportar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "code")
    private Integer code;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "cnpj_cpf")
    private String cnpjCpf;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.DATE)
    private Date dtEmissao;
    @Basic(optional = false)
    @Column(name = "dt_vencimento")
    @Temporal(TemporalType.DATE)
    private Date dtVencimento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private Double valor;
    @Column(name = "valor_pago")
    private Double valorPago;
    @Column(name = "dt_pagamento")
    @Temporal(TemporalType.DATE)
    private Date dtPagamento;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;

    public CobrancaBfImportar() {
    }

    public CobrancaBfImportar(Integer code) {
        this.code = code;
    }

    public CobrancaBfImportar(Integer code, String nome, String cnpjCpf, String descricao, Date dtEmissao, Date dtVencimento, String status) {
        this.code = code;
        this.nome = nome;
        this.cnpjCpf = cnpjCpf;
        this.descricao = descricao;
        this.dtEmissao = dtEmissao;
        this.dtVencimento = dtVencimento;
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpjCpf() {
        return cnpjCpf;
    }

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobrancaBfImportar)) {
            return false;
        }
        CobrancaBfImportar other = (CobrancaBfImportar) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.CobrancaBfImportar[ code=" + code + " ]";
    }
    
}
