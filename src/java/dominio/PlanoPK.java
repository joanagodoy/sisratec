/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Joana
 */
@Embeddable
public class PlanoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "edicao_contrato")
    private int edicaoContrato;
    @Basic(optional = false)
    @Column(name = "pts_vista")
    private int ptsVista;

    public PlanoPK() {
    }

    public PlanoPK(int edicaoContrato, int ptsVista) {
        this.edicaoContrato = edicaoContrato;
        this.ptsVista = ptsVista;
    }

    public int getEdicaoContrato() {
        return edicaoContrato;
    }

    public void setEdicaoContrato(int edicaoContrato) {
        this.edicaoContrato = edicaoContrato;
    }

    public int getPtsVista() {
        return ptsVista;
    }

    public void setPtsVista(int ptsVista) {
        this.ptsVista = ptsVista;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) edicaoContrato;
        hash += (int) ptsVista;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanoPK)) {
            return false;
        }
        PlanoPK other = (PlanoPK) object;
        if (this.edicaoContrato != other.edicaoContrato) {
            return false;
        }
        if (this.ptsVista != other.ptsVista) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.PlanoPK[ edicaoContrato=" + edicaoContrato + ", ptsVista=" + ptsVista + " ]";
    }
    
}
