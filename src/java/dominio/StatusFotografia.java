/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "status_fotografia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StatusFotografia.findAll", query = "SELECT s FROM StatusFotografia s"),
    @NamedQuery(name = "StatusFotografia.findById", query = "SELECT s FROM StatusFotografia s WHERE s.id = :id"),
    @NamedQuery(name = "StatusFotografia.findByDescricao", query = "SELECT s FROM StatusFotografia s WHERE s.descricao = :descricao")})
public class StatusFotografia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
    private List<Fotografia> fotografiaList;

    public StatusFotografia() {
    }

    public StatusFotografia(Integer id) {
        this.id = id;
    }

    public StatusFotografia(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<Fotografia> getFotografiaList() {
        return fotografiaList;
    }

    public void setFotografiaList(List<Fotografia> fotografiaList) {
        this.fotografiaList = fotografiaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StatusFotografia)) {
            return false;
        }
        StatusFotografia other = (StatusFotografia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.StatusFotografia[ id=" + id + " ]";
    }
    
}
