/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findById", query = "SELECT c FROM Cliente c WHERE c.id = :id"),
    @NamedQuery(name = "Cliente.findByResponsavel", query = "SELECT c FROM Cliente c WHERE c.responsavel = :responsavel"),
    @NamedQuery(name = "Cliente.findByCnpjAtivo", query = "SELECT c FROM Cliente c WHERE c.cnpj = :cnpj AND c.ativo = true"),
    @NamedQuery(name = "Cliente.findByCnpj", query = "SELECT c FROM Cliente c WHERE c.cnpj = :cnpj"),
    @NamedQuery(name = "Cliente.findByRazSocial", query = "SELECT c FROM Cliente c WHERE c.razSocial = :razSocial"),
    @NamedQuery(name = "Cliente.findByNomeFantasia", query = "SELECT c FROM Cliente c WHERE c.nomeFantasia = :nomeFantasia"),
    @NamedQuery(name = "Cliente.findByTelefone1", query = "SELECT c FROM Cliente c WHERE c.telefone1 = :telefone1"),
    @NamedQuery(name = "Cliente.findByTelefone2", query = "SELECT c FROM Cliente c WHERE c.telefone2 = :telefone2"),
    @NamedQuery(name = "Cliente.findByEmail", query = "SELECT c FROM Cliente c WHERE c.email = :email"),
    @NamedQuery(name = "Cliente.findByVendedor", query = "SELECT c FROM Cliente c WHERE c.vendedor = :vendedor"),
    @NamedQuery(name = "Cliente.findSemContratoByVendedor", query = "SELECT c FROM Cliente c WHERE c.vendedor = :usuario AND c.id NOT IN (SELECT con.cliente.id FROM Contrato con)"),
    @NamedQuery(name = "Cliente.findVisitasRelizadas", query = "SELECT c FROM Cliente c, Contrato con, Visita V WHERE con.cliente.id = c.id AND con.nrContrato = v.nrContrato.nrContrato AND v.status.id = 2 AND v.dtVisita = :yesterday"),
    @NamedQuery(name = "Cliente.findClientesAntigos", query = "SELECT c FROM Cliente c, Contrato con WHERE con.cliente.id = c.id AND con.valorMensal != 0.00"),
    @NamedQuery(name = "Cliente.findClientesAnuaisVindi", query = "SELECT c FROM Cliente c, Contrato con WHERE con.cliente.id = c.id AND con.pagamento = 1 AND c.ativo = true AND c.codigoVindi IS NOT NULL"),
    @NamedQuery(name = "Cliente.findByStatus", query = "SELECT c FROM Cliente c WHERE c.ativo = :ativo"),
    @NamedQuery(name = "Cliente.findByPeriodicidade", query = "SELECT c FROM Cliente c WHERE c.periodicidade = :periodicidade"),
    @NamedQuery(name = "Cliente.findByAdimplencia", query = "SELECT c FROM Cliente c WHERE c.adimplencia = :adimplencia AND c.ativo = true"),
    @NamedQuery(name = "Cliente.findByUltPtsVista", query = "SELECT c FROM Cliente c WHERE c.ultPtsVista = :ultPtsVista AND c.ativo = :ativo"),
    @NamedQuery(name = "Cliente.findByPeriodicidadeAdimplencia", query = "SELECT c FROM Cliente c WHERE c.periodicidade = :periodicidade AND c.adimplencia = :adimplencia"),
    @NamedQuery(name = "Cliente.findByStatusPeriodicidade", query = "SELECT c FROM Cliente c WHERE c.ativo = :ativo AND c.periodicidade = :periodicidade"),
    @NamedQuery(name = "Cliente.findByStatusAdimplencia", query = "SELECT c FROM Cliente c WHERE c.ativo = :ativo AND c.adimplencia = :adimplencia"),
    @NamedQuery(name = "Cliente.findByStatusPeriodicidadeAdimplencia", query = "SELECT c FROM Cliente c WHERE c.ativo = :ativo AND c.periodicidade = :periodicidade AND c.adimplencia = :adimplencia"),
    @NamedQuery(name = "Cliente.findSemContrato", query = "SELECT c FROM Cliente c WHERE c.id NOT IN (SELECT con.cliente.id FROM Contrato con)")})
public class Cliente implements Serializable {
//    @Basic(optional = false)
//    @Column(name = "endereco")
//    private int endereco;
//    @Basic(optional = false)
//    @Column(name = "usuario")
//    private int usuario;
//    @Basic(optional = false)
//    @Column(name = "vendedor")
//    private int vendedor;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<CobrancaFatura> cobrancaFaturaList;
    @Column(name = "assinatura_cancelada")
    private Boolean assinaturaCancelada;
    @Column(name = "possui_outro_ativo")
    private Boolean possuiOutroAtivo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<CobrancaAssinatura> cobrancaAssinaturaList;
    @OneToMany(mappedBy = "cliente")
    private List<Visita> visitaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<Imovel> apartamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<Cobranca> cobrancaList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "responsavel")
    private String responsavel;
    @Column(name = "codigo_vindi")
    private Integer codigoVindi;
    @Column(name = "codigo_assinatura_vindi")
    private Integer codigoAssinaturaVindi;
    @Basic(optional = false)
    @Column(name = "cnpj_cpf")
    private String cnpj;
    @Column(name = "inscricao_estadual_municipal")
    private String inscricao;
    @Column(name = "rg")
    private String rg;
    @Basic(optional = false)
    @Column(name = "raz_social")
    private String razSocial;
    @Basic(optional = false)
    @Column(name = "nome_fantasia")
    private String nomeFantasia;
    @Basic(optional = false)
    @Column(name = "telefone1")
    private String telefone1;
    @Column(name = "telefone2")
    private String telefone2;
    @Column(name = "telefone3")
    private String telefone3;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Column(name = "email2")
    private String email2;
    @Column(name = "ativo")
    private boolean ativo;
    @Column(name = "dt_criacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCriacao;
    @Column(name = "dt_ultima_fatura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtUltimaFatura;
    @Column(name = "dt_primeira_fatura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPrimeiraFatura;
    @Column(name = "dt_proximo_vencimento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtProximoVencimento;
    @Column(name = "dt_alteracao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAlteracao;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuario;
    @JoinColumn(name = "vendedor", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario vendedor;
    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Endereco endereco;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCliente")
    private List<Fotografia> fotografiaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<Contrato> contratoList;
    @Column(name = "ult_pts_vista")
    private int ultPtsVista;
    @Column(name = "dt_cobranca")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCobranca;
    @JoinColumn(name = "ramo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Ramo ramo;
    @Basic(optional = false)
    @Column(name = "adimplencia")
    private int adimplencia;
    @Basic(optional = false)
    @Column(name = "periodicidade")
    private Integer periodicidade;
    @Basic(optional = false)
    @Column(name = "meses_faltantes")
    private int mesesFaltantes;
    @Column(name = "total_recorrencias_pagas")
    private Double totalRecorrenciasPagas;
    @Column(name = "ult_pts_vista_multp")
    private int ultPtsVistaMultp;
    @Column(name = "total_meses_recorrencias_pagas")
    private int totalMesesRecorrenciasPagas;

    public Cliente() {
    }

    public Cliente(Integer id) {
        this.id = id;
    }

    public Cliente(Integer id, String responsavel, String cnpj, String razSocial, String nomeFantasia, String telefone1, String telefone2, String email) {
        this.id = id;
        this.responsavel = responsavel;
        this.cnpj = cnpj;
        this.razSocial = razSocial;
        this.nomeFantasia = nomeFantasia;
        this.telefone1 = telefone1;
        this.telefone2 = telefone2;
        this.email = email;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazSocial() {
        return razSocial;
    }

    public void setRazSocial(String razSocial) {
        this.razSocial = razSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @XmlTransient
    public List<Fotografia> getFotografiaList() {
        return fotografiaList;
    }

    public void setFotografiaList(List<Fotografia> fotografiaList) {
        this.fotografiaList = fotografiaList;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Cliente[ id=" + id + " ]";
    }

    /**
     * @return the telefone3
     */
    public String getTelefone3() {
        return telefone3;
    }

    /**
     * @param telefone3 the telefone3 to set
     */
    public void setTelefone3(String telefone3) {
        this.telefone3 = telefone3;
    }

    /**
     * @return the email2
     */
    public String getEmail2() {
        return email2;
    }

    /**
     * @param email2 the email2 to set
     */
    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    /**
     * @return the dtCriacao
     */
    public Date getDtCriacao() {
        return dtCriacao;
    }

    /**
     * @param dtCriacao the dtCriacao to set
     */
    public void setDtCriacao(Date dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

    /**
     * @return the dtAlteracao
     */
    public Date getDtAlteracao() {
        return dtAlteracao;
    }

    /**
     * @param dtAlteracao the dtAlteracao to set
     */
    public void setDtAlteracao(Date dtAlteracao) {
        this.dtAlteracao = dtAlteracao;
    }

    /**
     * @return the codigoVindi
     */
    public Integer getCodigoVindi() {
        return codigoVindi;
    }

    /**
     * @param codigoVindi the codigoVindi to set
     */
    public void setCodigoVindi(Integer codigoVindi) {
        this.codigoVindi = codigoVindi;
    }

    /**
     * @return the codigoAssinaturaVindi
     */
    public Integer getCodigoAssinaturaVindi() {
        return codigoAssinaturaVindi;
    }

    /**
     * @param codigoAssinaturaVindi the codigoAssinaturaVindi to set
     */
    public void setCodigoAssinaturaVindi(Integer codigoAssinaturaVindi) {
        this.codigoAssinaturaVindi = codigoAssinaturaVindi;
    }

    /**
     * @return the inscricao
     */
    public String getInscricao() {
        return inscricao;
    }

    /**
     * @param inscricao the inscricao to set
     */
    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    /**
     * @return the rg
     */
    public String getRg() {
        return rg;
    }

    /**
     * @param rg the rg to set
     */
    public void setRg(String rg) {
        this.rg = rg;
    }

    /**
     * @return the ativo
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * @param ativo the ativo to set
     */
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    /**
     * @return the vendedor
     */
    public Usuario getVendedor() {
        return vendedor;
    }

    /**
     * @param vendedor the vendedor to set
     */
    public void setVendedor(Usuario vendedor) {
        this.vendedor = vendedor;
    }

    @XmlTransient
    public List<Cobranca> getCobrancaList() {
        return cobrancaList;
    }

    public void setCobrancaList(List<Cobranca> cobrancaList) {
        this.cobrancaList = cobrancaList;
    }

    @XmlTransient
    public List<Imovel> getImovelList() {
        return apartamentoList;
    }

    public void setImovelList(List<Imovel> apartamentoList) {
        this.apartamentoList = apartamentoList;
    }

    @XmlTransient
    public List<Visita> getVisitaList() {
        return visitaList;
    }

    public void setVisitaList(List<Visita> visitaList) {
        this.visitaList = visitaList;
    }

    /**
     * @return the dtUltimaFatura
     */
    public Date getDtUltimaFatura() {
        return dtUltimaFatura;
    }

    /**
     * @param dtUltimaFatura the dtUltimaFatura to set
     */
    public void setDtUltimaFatura(Date dtUltimaFatura) {
        this.dtUltimaFatura = dtUltimaFatura;
    }

    /**
     * @return the possuiOutroAtivo
     */
    public boolean isPossuiOutroAtivo() {
        return possuiOutroAtivo;
    }

    /**
     * @param possuiOutroAtivo the possuiOutroAtivo to set
     */
    public void setPossuiOutroAtivo(boolean possuiOutroAtivo) {
        this.possuiOutroAtivo = possuiOutroAtivo;
    }

    /**
     * @return the ultPtsVista
     */
    public int getUltPtsVista() {
        return ultPtsVista;
    }

    /**
     * @param ultPtsVista the ultPtsVista to set
     */
    public void setUltPtsVista(int ultPtsVista) {
        this.ultPtsVista = ultPtsVista;
    }

    /**
     * @return the dtCobranca
     */
    public Date getDtCobranca() {
        return dtCobranca;
    }

    /**
     * @param dtCobranca the dtCobranca to set
     */
    public void setDtCobranca(Date dtCobranca) {
        this.dtCobranca = dtCobranca;
    }

    /**
     * @return the ramo
     */
    public Ramo getRamo() {
        return ramo;
    }

    /**
     * @param ramo the ramo to set
     */
    public void setRamo(Ramo ramo) {
        this.ramo = ramo;
    }

    public Boolean getAssinaturaCancelada() {
        return assinaturaCancelada;
    }

    public void setAssinaturaCancelada(Boolean assinaturaCancelada) {
        this.assinaturaCancelada = assinaturaCancelada;
    }

    public Boolean getPossuiOutroAtivo() {
        return possuiOutroAtivo;
    }

    public void setPossuiOutroAtivo(Boolean possuiOutroAtivo) {
        this.possuiOutroAtivo = possuiOutroAtivo;
    }

    @XmlTransient
    public List<CobrancaAssinatura> getCobrancaAssinaturaList() {
        return cobrancaAssinaturaList;
    }

    public void setCobrancaAssinaturaList(List<CobrancaAssinatura> cobrancaAssinaturaList) {
        this.cobrancaAssinaturaList = cobrancaAssinaturaList;
    }

//    public int getEndereco() {
//        return endereco;
//    }
//
//    public void setEndereco(int endereco) {
//        this.endereco = endereco;
//    }
//
//    public int getUsuario() {
//        return usuario;
//    }
//
//    public void setUsuario(int usuario) {
//        this.usuario = usuario;
//    }
//
//    public int getVendedor() {
//        return vendedor;
//    }
//
//    public void setVendedor(int vendedor) {
//        this.vendedor = vendedor;
//    }
    @XmlTransient
    public List<CobrancaFatura> getCobrancaFaturaList() {
        return cobrancaFaturaList;
    }

    public void setCobrancaFaturaList(List<CobrancaFatura> cobrancaFaturaList) {
        this.cobrancaFaturaList = cobrancaFaturaList;
    }

    /**
     * @return the adimplencia
     */
    public int getAdimplencia() {
        return adimplencia;
    }

    /**
     * @param adimplencia the adimplencia to set
     */
    public void setAdimplencia(int adimplencia) {
        this.adimplencia = adimplencia;
    }

    /**
     * @return the periodicidade
     */
    public Integer getPeriodicidade() {
        return periodicidade;
    }

    /**
     * @param periodicidade the periodicidade to set
     */
    public void setPeriodicidade(Integer periodicidade) {
        this.periodicidade = periodicidade;
    }

    /**
     * @return the dtPrimeiraFatura
     */
    public Date getDtPrimeiraFatura() {
        return dtPrimeiraFatura;
    }

    /**
     * @param dtPrimeiraFatura the dtPrimeiraFatura to set
     */
    public void setDtPrimeiraFatura(Date dtPrimeiraFatura) {
        this.dtPrimeiraFatura = dtPrimeiraFatura;
    }

    /**
     * @return the totalRecorrenciasPagas
     */
    public Double getTotalRecorrenciasPagas() {
        return totalRecorrenciasPagas;
    }

    /**
     * @param totalRecorrenciasPagas the totalRecorrenciasPagas to set
     */
    public void setTotalRecorrenciasPagas(Double totalRecorrenciasPagas) {
        this.totalRecorrenciasPagas = totalRecorrenciasPagas;
    }

    /**
     * @return the mesesFaltantes
     */
    public int getMesesFaltantes() {
        return mesesFaltantes;
    }

    /**
     * @param mesesFaltantes the mesesFaltantes to set
     */
    public void setMesesFaltantes(int mesesFaltantes) {
        this.mesesFaltantes = mesesFaltantes;
    }

    /**
     * @return the ultPtsVistaMultp
     */
    public int getUltPtsVistaMultp() {
        return ultPtsVistaMultp;
    }

    /**
     * @param ultPtsVistaMultp the ultPtsVistaMultp to set
     */
    public void setUltPtsVistaMultp(int ultPtsVistaMultp) {
        this.ultPtsVistaMultp = ultPtsVistaMultp;
    }

    /**
     * @return the dtProximoVencimento
     */
    public Date getDtProximoVencimento() {
        return dtProximoVencimento;
    }

    /**
     * @param dtProximoVencimento the dtProximoVencimento to set
     */
    public void setDtProximoVencimento(Date dtProximoVencimento) {
        this.dtProximoVencimento = dtProximoVencimento;
    }

    /**
     * @return the totalMesesRecorrenciasPagas
     */
    public int getTotalMesesRecorrenciasPagas() {
        return totalMesesRecorrenciasPagas;
    }

    /**
     * @param totalMesesRecorrenciasPagas the totalMesesRecorrenciasPagas to set
     */
    public void setTotalMesesRecorrenciasPagas(int totalMesesRecorrenciasPagas) {
        this.totalMesesRecorrenciasPagas = totalMesesRecorrenciasPagas;
    }

}
