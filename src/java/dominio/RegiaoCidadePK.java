/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author joana
 */
@Embeddable
public class RegiaoCidadePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "regiao")
    private int regiao;
    @Basic(optional = false)
    @Column(name = "cidade")
    private int cidade;

    public RegiaoCidadePK() {
    }

    public RegiaoCidadePK(int regiao, int cidade) {
        this.regiao = regiao;
        this.cidade = cidade;
    }

    public int getRegiao() {
        return regiao;
    }

    public void setRegiao(int regiao) {
        this.regiao = regiao;
    }

    public int getCidade() {
        return cidade;
    }

    public void setCidade(int cidade) {
        this.cidade = cidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) regiao;
        hash += (int) cidade;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegiaoCidadePK)) {
            return false;
        }
        RegiaoCidadePK other = (RegiaoCidadePK) object;
        if (this.regiao != other.regiao) {
            return false;
        }
        if (this.cidade != other.cidade) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.RegiaoCidadePK[ regiao=" + regiao + ", cidade=" + cidade + " ]";
    }
    
}
