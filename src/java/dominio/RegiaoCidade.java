/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author joana
 */
@Entity
@Table(name = "regiao_cidade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegiaoCidade.findAll", query = "SELECT r FROM RegiaoCidade r")
    , @NamedQuery(name = "RegiaoCidade.findByRegiao", query = "SELECT r FROM RegiaoCidade r WHERE r.regiaoCidadePK.regiao = :regiao")
    , @NamedQuery(name = "RegiaoCidade.findByCidade", query = "SELECT r FROM RegiaoCidade r WHERE r.regiaoCidadePK.cidade = :cidade")})
public class RegiaoCidade implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegiaoCidadePK regiaoCidadePK;

    public RegiaoCidade() {
    }

    public RegiaoCidade(RegiaoCidadePK regiaoCidadePK) {
        this.regiaoCidadePK = regiaoCidadePK;
    }

    public RegiaoCidade(int regiao, int cidade) {
        this.regiaoCidadePK = new RegiaoCidadePK(regiao, cidade);
    }

    public RegiaoCidadePK getRegiaoCidadePK() {
        return regiaoCidadePK;
    }

    public void setRegiaoCidadePK(RegiaoCidadePK regiaoCidadePK) {
        this.regiaoCidadePK = regiaoCidadePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regiaoCidadePK != null ? regiaoCidadePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegiaoCidade)) {
            return false;
        }
        RegiaoCidade other = (RegiaoCidade) object;
        if ((this.regiaoCidadePK == null && other.regiaoCidadePK != null) || (this.regiaoCidadePK != null && !this.regiaoCidadePK.equals(other.regiaoCidadePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.RegiaoCidade[ regiaoCidadePK=" + regiaoCidadePK + " ]";
    }
    
}
