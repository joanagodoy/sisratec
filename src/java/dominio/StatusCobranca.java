/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "status_cobranca")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StatusCobranca.findAll", query = "SELECT s FROM StatusCobranca s"),
    @NamedQuery(name = "StatusCobranca.findById", query = "SELECT s FROM StatusCobranca s WHERE s.id = :id"),
    @NamedQuery(name = "StatusCobranca.findByDescricao", query = "SELECT s FROM StatusCobranca s WHERE s.descricao = :descricao")})
public class StatusCobranca implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
    private List<Cobranca> cobrancaList;

    public StatusCobranca() {
    }

    public StatusCobranca(Integer id) {
        this.id = id;
    }

    public StatusCobranca(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<Cobranca> getCobrancaList() {
        return cobrancaList;
    }

    public void setCobrancaList(List<Cobranca> cobrancaList) {
        this.cobrancaList = cobrancaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StatusCobranca)) {
            return false;
        }
        StatusCobranca other = (StatusCobranca) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.StatusCobranca[ id=" + id + " ]";
    }
    
}
