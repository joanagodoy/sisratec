/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "fotografia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fotografia.findAll", query = "SELECT f FROM Fotografia f"),
    @NamedQuery(name = "Fotografia.findById", query = "SELECT f FROM Fotografia f WHERE f.id = :id"),
    @NamedQuery(name = "Fotografia.findByStatus", query = "SELECT f FROM Fotografia f WHERE f.status.id = :status"),
    @NamedQuery(name = "Fotografia.findFotografo", query = "SELECT f FROM Fotografia f WHERE f.status.id = 1 OR f.status.id = 4 ORDER BY f.status.id DESC"),
    @NamedQuery(name = "Fotografia.findEditor", query = "SELECT f FROM Fotografia f WHERE f.status.id IN (2, 7, 4)"),
    @NamedQuery(name = "Fotografia.findByCliente", query = "SELECT f FROM Fotografia f WHERE f.idCliente = :cliente"),
    @NamedQuery(name = "Fotografia.findByDtPostagem", query = "SELECT f FROM Fotografia f WHERE f.dtPostagem = :dtPostagem")})
public class Fotografia implements Serializable {

    /**
     * @return the faltouPts
     */
    public boolean isFaltouPts() {
        return faltouPts;
    }

    /**
     * @param faltouPts the faltouPts to set
     */
    public void setFaltouPts(boolean faltouPts) {
        this.faltouPts = faltouPts;
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Column(name = "link_fotos")
    private String linkFotos;
    @Column(name = "dt_postagem")
    @Temporal(TemporalType.DATE)
    private Date dtPostagem;
    @Column(name = "faltou_pts")
    private boolean faltouPts;
    @Column(name = "problema_fotografo")
    private boolean problemaFotografo;
    @Column(name = "problema_editor")
    private boolean problemaEditor;
    @Column(name = "observacao")
    private String obs;
//    @Column(name = "dt_envio_boleto")
//    @Temporal(TemporalType.DATE)
//    private Date dtEnvioBoleto;
    @Column(name = "dt_envio_editor")
    @Temporal(TemporalType.DATE)
    private Date dtEnvioEditor;
    @Column(name = "dt_confirmacao_entrega")
    @Temporal(TemporalType.DATE)
    private Date dtConfirmacaoEntrega;
//    @Column(name = "dt_envio_editor")
//    @Temporal(TemporalType.DATE)
//    private Date dtEnvioPagto;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusFotografia status;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente idCliente;
    @JoinColumn(name = "imovel", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Imovel imovel;

    public Fotografia() {
    }

    public Fotografia(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLinkFotos() {
        return linkFotos;
    }

    public void setLinkFotos(String linkFotos) {
        this.linkFotos = linkFotos;
    }

    public Date getDtPostagem() {
        return dtPostagem;
    }

    public void setDtPostagem(Date dtPostagem) {
        this.dtPostagem = dtPostagem;
    }

//    public Date getDtEnvioBoleto() {
//        return dtEnvioBoleto;
//    }
//
//    public void setDtEnvioBoleto(Date dtEnvioBoleto) {
//        this.dtEnvioBoleto = dtEnvioBoleto;
//    }
//
    public Date getDtEnvioEditor() {
        return dtEnvioEditor;
    }

    public void setDtEnvioEditor(Date dtEnvioEditor) {
        this.dtEnvioEditor = dtEnvioEditor;
    }

    public StatusFotografia getStatus() {
        return status;
    }

    public void setStatus(StatusFotografia status) {
        this.status = status;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fotografia)) {
            return false;
        }
        Fotografia other = (Fotografia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Fotografia[ id=" + id + " ]";
    }

    /**
     * @return the obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * @param obs the obs to set
     */
    public void setObs(String obs) {
        this.obs = obs;
    }

    /**
     * @return the dtConfirmacaoEntrega
     */
    public Date getDtConfirmacaoEntrega() {
        return dtConfirmacaoEntrega;
    }

    /**
     * @param dtConfirmacaoEntrega the dtConfirmacaoEntrega to set
     */
    public void setDtConfirmacaoEntrega(Date dtConfirmacaoEntrega) {
        this.dtConfirmacaoEntrega = dtConfirmacaoEntrega;
    }

    /**
     * @return the problemaFotografo
     */
    public boolean isProblemaFotografo() {
        return problemaFotografo;
    }

    /**
     * @param problemaFotografo the problemaFotografo to set
     */
    public void setProblemaFotografo(boolean problemaFotografo) {
        this.problemaFotografo = problemaFotografo;
    }

    /**
     * @return the problemaEditor
     */
    public boolean isProblemaEditor() {
        return problemaEditor;
    }

    /**
     * @param problemaEditor the problemaEditor to set
     */
    public void setProblemaEditor(boolean problemaEditor) {
        this.problemaEditor = problemaEditor;
    }

    /**
     * @return the imovel
     */
    public Imovel getImovel() {
        return imovel;
    }

    /**
     * @param imovel the imovel to set
     */
    public void setImovel(Imovel imovel) {
        this.imovel = imovel;
    }
}
