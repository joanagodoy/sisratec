/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "visita")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Visita.findAll", query = "SELECT v FROM Visita v"),
    @NamedQuery(name = "Visita.findById", query = "SELECT v FROM Visita v WHERE v.id = :id"),
    @NamedQuery(name = "Visita.findByPtsVista", query = "SELECT v FROM Visita v WHERE v.ptsVista = :ptsVista"),
    @NamedQuery(name = "Visita.findByClienteImovelNull", query = "SELECT v FROM Visita v WHERE v.cliente = :cliente AND v.imovel IS NULL"),
    @NamedQuery(name = "Visita.findByCliente", query = "SELECT v FROM Visita v WHERE v.cliente = :cliente"),
    @NamedQuery(name = "Visita.findByClienteImovel", query = "SELECT v FROM Visita v WHERE v.cliente = :cliente AND v.imovel =:imovel"),
    @NamedQuery(name = "Visita.findByImovel", query = "SELECT v FROM Visita v WHERE v.imovel = :imovel"),
    @NamedQuery(name = "Visita.findByDtRegiaoVisita", query = "SELECT v FROM Visita v WHERE v.regiao = :regiao AND v.dtVisita between :dataInicial and :dataFinal"),
    @NamedQuery(name = "Visita.findByDtVendedorVisita", query = "SELECT v FROM Visita v WHERE (v.status.id = 2 OR v.status.id = 4) AND v.cliente.vendedor = :vendedor AND v.dtVisita between :dataInicial and :dataFinal"),
    @NamedQuery(name = "Visita.findByDtFotografoVisita", query = "SELECT v FROM Visita v WHERE (v.status.id = 2 OR v.status.id = 4) AND v.fotografo = :fotografo AND v.dtVisita between :dataInicial and :dataFinal"),
    @NamedQuery(name = "Visita.findByDtVisita", query = "SELECT v FROM Visita v WHERE (v.status.id = 2 OR v.status.id = 4) AND v.dtVisita between :dataInicial and :dataFinal"),
    @NamedQuery(name = "Visita.findPendenteByDtVisita", query = "SELECT v FROM Visita v WHERE v.status.id = 1 AND v.dtVisita = :dtVisita"),
    @NamedQuery(name = "Visita.findByDtSistema", query ="SELECT v FROM Visita v WHERE v.dtSistema = :dtSistema")})
public class Visita implements Serializable {    
    @Column(name = "dt_envio_boleto")
    @Temporal(TemporalType.DATE)
    private Date dtEnvioBoleto;
    @Column(name = "pts_vista_realizado")
    private Integer ptsVistaRealizado;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dt_visita")
    @Temporal(TemporalType.DATE)
    private Date dtVisita;
    @Basic(optional = false)
    @Column(name = "hr_visita_inicial")
    @Temporal(TemporalType.TIME)
    private Date hrVisitaInicial;
    @Basic(optional = false)
    @Column(name = "hr_visita_final")
    @Temporal(TemporalType.TIME)
    private Date hrVisitaFinal;
    @Basic(optional = false)
    @Column(name = "pts_vista")
    private int ptsVista;
    @Basic(optional = false)
    @Column(name = "obs")
    private String obs;
//    @Basic(optional = false)
//    @Column(name = "dt_envio_boleto")
//    @Temporal(TemporalType.DATE)
//    private Date dtEnvioBoleto;
    @Basic(optional = false)
    @Column(name = "dt_sistema")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtSistema;
//    @Basic(optional = false)
//    @Column(name = "dt_hora_visita")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date dtHoraVisita;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusVisita status;
    @JoinColumn(name = "nr_contrato", referencedColumnName = "nr_contrato")
    @ManyToOne(optional = false)
    private Contrato nrContrato;
    @JoinColumn(name = "imovel", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Imovel imovel;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuario;
    @JoinColumn(name = "fotografo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario fotografo;
    @JoinColumn(name = "regiao", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Regiao regiao;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente cliente;
    
    public Visita() {
    }

    public Visita(Integer id) {
        this.id = id;
    }

    public Visita(Integer id, Date dtVisita, Date hrVisitaInicial, Date hrVisitaFinal, int ptsVista, int motivoAlteracao, Date dtSistema) {
        this.id = id;
        this.dtVisita = dtVisita;
        this.hrVisitaInicial = hrVisitaInicial;
        this.hrVisitaFinal = hrVisitaFinal;
        this.ptsVista = ptsVista;
        this.dtSistema = dtSistema;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDtVisita() {
        return dtVisita;
    }

    public void setDtVisita(Date dtVisita) {
        this.dtVisita = dtVisita;
    }

    public Date getHrVisitaInicial() {
        return hrVisitaInicial;
        
    }

    public void setHrVisitaInicial(Date hrVisitaInicial) {
        this.hrVisitaInicial = hrVisitaInicial;
    }
    
    public Date getHrVisitaFinal() {
        return hrVisitaFinal;
        
    }

    public void setHrVisitaFinal(Date hrVisitaFinal) {
        this.hrVisitaFinal = hrVisitaFinal;
    }
    
    

    public int getPtsVista() {
        return ptsVista;
    }

    public void setPtsVista(int ptsVista) {
        this.ptsVista = ptsVista;
    }
//    public Date getDtEnvioBoleto() {
//        return dtEnvioBoleto;
//    }
//
//    public void setDtEnvioBoleto(Date dtEnvioBoleto) {
//        this.dtEnvioBoleto = dtEnvioBoleto;
//    }

    public Date getDtSistema() {
        return dtSistema;
    }

    public void setDtSistema(Date dtSistema) {
        this.dtSistema = dtSistema;
    }

    public StatusVisita getStatus() {
        return status;
    }

    public void setStatus(StatusVisita status) {
        this.status = status;
    }

    public Contrato getNrContrato() {
        return nrContrato;
    }

    public void setNrContrato(Contrato nrContrato) {
        this.nrContrato = nrContrato;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visita)) {
            return false;
        }
        Visita other = (Visita) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Visita[ id=" + id + " ]";
    }

    /**
     * @return the obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * @param obs the obs to set
     */
    public void setObs(String obs) {
        this.obs = obs;
    }

    /**
     * @return the regiao
     */
    public Regiao getRegiao() {
        return regiao;
    }

    /**
     * @param regiao the regiao to set
     */
    public void setRegiao(Regiao regiao) {
        this.regiao = regiao;
    }

    /**
     * @return the ptsVistaRealizado
     */
    public int getPtsVistaRealizado() {
        return ptsVistaRealizado;
    }

    /**
     * @param ptsVistaRealizado the ptsVistaRealizado to set
     */
    public void setPtsVistaRealizado(int ptsVistaRealizado) {
        this.ptsVistaRealizado = ptsVistaRealizado;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the fotografo
     */
    public Usuario getFotografo() {
        return fotografo;
    }

    /**
     * @param fotografo the fotografo to set
     */
    public void setFotografo(Usuario fotografo) {
        this.fotografo = fotografo;
    }

    /**
     * @return the imovel
     */
    public Imovel getImovel() {
        return imovel;
    }

    /**
     * @param imovel the imovel to set
     */
    public void setImovel(Imovel imovel) {
        this.imovel = imovel;
    }

    public Date getDtEnvioBoleto() {
        return dtEnvioBoleto;
    }

    public void setDtEnvioBoleto(Date dtEnvioBoleto) {
        this.dtEnvioBoleto = dtEnvioBoleto;
    }

    public void setPtsVistaRealizado(Integer ptsVistaRealizado) {
        this.ptsVistaRealizado = ptsVistaRealizado;
    }

    /**
     * @return the dtHoraVisita
     */
//    public Date getDtHoraVisita() {
//        return dtHoraVisita;
//    }

    /**
     * @param dtHoraVisita the dtHoraVisita to set
     */
//    public void setDtHoraVisita(Date dtHoraVisita) {
//        this.dtHoraVisita = dtHoraVisita;
//    }
    
}
