/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author julia
 */
@Entity
@Table(name = "cobranca_fatura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobrancaFatura.findAll", query = "SELECT c FROM CobrancaFatura c")
    , @NamedQuery(name = "CobrancaFatura.findById", query = "SELECT c FROM CobrancaFatura c WHERE c.id = :id")
    , @NamedQuery(name = "CobrancaFatura.findByDtEmissao", query = "SELECT c FROM CobrancaFatura c WHERE c.dtEmissao = :dtEmissao")
    , @NamedQuery(name = "CobrancaFatura.findByDtVencimento", query = "SELECT c FROM CobrancaFatura c WHERE c.dtVencimento = :dtVencimento")
    , @NamedQuery(name = "CobrancaFatura.findByDtPagamento", query = "SELECT c FROM CobrancaFatura c WHERE c.dtPagamento = :dtPagamento")
    , @NamedQuery(name = "CobrancaFatura.findByValor", query = "SELECT c FROM CobrancaFatura c WHERE c.valor = :valor")
    , @NamedQuery(name = "CobrancaFatura.findByCliente", query = "SELECT c FROM CobrancaFatura c WHERE c.cliente = :cliente")
    , @NamedQuery(name = "CobrancaFatura.findByRegistroReferencia", query = "SELECT c FROM CobrancaFatura c WHERE c.registro IS NULL")
    , @NamedQuery(name = "CobrancaFatura.findByIdFaturaVindi", query = "SELECT c FROM CobrancaFatura c WHERE c.idFaturaVindi = :idFaturaVindi")
    , @NamedQuery(name = "CobrancaFatura.findByCodeBf", query = "SELECT c FROM CobrancaFatura c WHERE c.codeBf = :codeBf")
    , @NamedQuery(name = "CobrancaFatura.findByIdCobrancaVindi", query = "SELECT c FROM CobrancaFatura c WHERE c.idCobrancaVindi = :idCobrancaVindi")})
public class CobrancaFatura implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "valor")
    private Double valor;
     @Basic(optional = false)
    @Column(name = "valor_pago")
    private Double valorPago;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente cliente;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.DATE)
    private Date dtEmissao;
    @Basic(optional = false)
    @Column(name = "dt_vencimento")
    @Temporal(TemporalType.DATE)
    private Date dtVencimento;
    @Column(name = "dt_pagamento")
    @Temporal(TemporalType.DATE)
    private Date dtPagamento;
    @Column(name = "id_cobranca_vindi")
    private int idCobrancaVindi;
    @Column(name = "id_fatura_vindi")
    private int idFaturaVindi;  
    @Column(name = "reference_bf")
    private String referenceBf;
    @Column(name = "code_bf")
    private int codeBf;
    @Column(name = "qtd_meses")
    private int qtdMeses;
    @Column(name = "descricao_fatura")
    private String descricaoFatura; 
    @JoinColumn(name = "registro", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CobrancaRegistroReferencia registro;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CobrancaFaturaStatus status;
    @Column(name = "dt_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date dtAtualizacao;

    public CobrancaFatura() {
    }

    public CobrancaFatura(Integer id) {
        this.id = id;
    }

    public CobrancaFatura(Integer id, Date dtEmissao, Date dtVencimento, Double valor) {
        this.id = id;
        this.dtEmissao = dtEmissao;
        this.dtVencimento = dtVencimento;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }


    public CobrancaRegistroReferencia getRegistro() {
        return registro;
    }

    public void setRegistro(CobrancaRegistroReferencia registro) {
        this.registro = registro;
    }

    public CobrancaFaturaStatus getStatus() {
        return status;
    }

    public void setStatus(CobrancaFaturaStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobrancaFatura)) {
            return false;
        }
        CobrancaFatura other = (CobrancaFatura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.CobrancaFatura[ id=" + id + " ]";
    }


    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    /**
     * @return the idCobrancaVindi
     */
    public int getIdCobrancaVindi() {
        return idCobrancaVindi;
    }

    /**
     * @param idCobrancaVindi the idCobrancaVindi to set
     */
    public void setIdCobrancaVindi(int idCobrancaVindi) {
        this.idCobrancaVindi = idCobrancaVindi;
    }

    /**
     * @return the idFaturaVindi
     */
    public int getIdFaturaVindi() {
        return idFaturaVindi;
    }

    /**
     * @param idFaturaVindi the idFaturaVindi to set
     */
    public void setIdFaturaVindi(int idFaturaVindi) {
        this.idFaturaVindi = idFaturaVindi;
    }

    /**
     * @return the descricaoFatura
     */
    public String getDescricaoFatura() {
        return descricaoFatura;
    }

    /**
     * @param descricaoFatura the descricaoFatura to set
     */
    public void setDescricaoFatura(String descricaoFatura) {
        this.descricaoFatura = descricaoFatura;
    }

    /**
     * @return the referenceBf
     */
    public String getReferenceBf() {
        return referenceBf;
    }

    /**
     * @param referenceBf the referenceBf to set
     */
    public void setReferenceBf(String referenceBf) {
        this.referenceBf = referenceBf;
    }

    /**
     * @return the codeBf
     */
    public int getCodeBf() {
        return codeBf;
    }

    /**
     * @param codeBf the codeBf to set
     */
    public void setCodeBf(int codeBf) {
        this.codeBf = codeBf;
    }

    /**
     * @return the valorPago
     */
    public Double getValorPago() {
        return valorPago;
    }

    /**
     * @param valorPago the valorPago to set
     */
    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    /**
     * @return the qtdMeses
     */
    public int getQtdMeses() {
        return qtdMeses;
    }

    /**
     * @param qtdMeses the qtdMeses to set
     */
    public void setQtdMeses(int qtdMeses) {
        this.qtdMeses = qtdMeses;
    }

    /**
     * @return the dtAtualizacao
     */
    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    /**
     * @param dtAtualizacao the dtAtualizacao to set
     */
    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }
    
}
