/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "contrato")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrato.findAll", query = "SELECT c FROM Contrato c"),
    @NamedQuery(name = "Contrato.findByNrContrato", query = "SELECT c FROM Contrato c WHERE c.nrContrato = :nrContrato"),
    @NamedQuery(name = "Contrato.findByDtVenda", query = "SELECT c FROM Contrato c WHERE c.dtVenda = :dtVenda"),
    @NamedQuery(name = "Contrato.findByPagamento", query = "SELECT c FROM Contrato c WHERE c.pagamento = :pagamento"),
    @NamedQuery(name = "Contrato.findByVendedor", query = "SELECT c FROM Contrato c WHERE c.cliente.vendedor = :vendedor"),
    @NamedQuery(name = "Contrato.findByCliente", query = "SELECT c FROM Contrato c WHERE c.cliente = :cliente"),
    @NamedQuery(name = "Contrato.findByVendedorSemVisitaOLD", query = "SELECT c FROM Contrato c WHERE c.cliente.vendedor = :vendedor AND c.nrContrato NOT IN (SELECT v.nrContrato.nrContrato FROM Visita v)"),
    @NamedQuery(name = "Contrato.findSemVisitaOLD", query = "SELECT c FROM Contrato c WHERE (c.tipoCliente = 'Estabelecimento' AND c.nrContrato NOT IN (SELECT v.nrContrato.nrContrato FROM Visita v)) OR (c.vendaEstabelecimentoImobiliaria = true AND c.nrContrato NOT IN (SELECT v.nrContrato.nrContrato FROM Visita v WHERE v.imovel IS NULL))"),
    @NamedQuery(name = "Contrato.findByVendedorSemVisita", query = "SELECT c FROM Contrato c WHERE c.cliente.vendedor = :vendedor AND c.visitaAberto = true"),
    @NamedQuery(name = "Contrato.findSemVisita", query = "SELECT c FROM Contrato c WHERE c.visitaAberto = true"),
    @NamedQuery(name = "Contrato.findByObs", query = "SELECT c FROM Contrato c WHERE c.obs = :obs")})
public class Contrato implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "valor_mensal")
    private double valorMensal;
    @Basic(optional = false)
    @Column(name = "venda_estabelecimento_imobiliaria")
    private boolean vendaEstabelecimentoImobiliaria;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nrContrato")
    private List<Imovel> imovelList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "nr_contrato")
    private Integer nrContrato;
    @Basic(optional = false)
    @Column(name = "dt_venda")
    @Temporal(TemporalType.DATE)
    private Date dtVenda;
    @Basic(optional = false)
    @Column(name = "pagamento")
    private int pagamento;
    @Basic(optional = false)
    @Column(name = "tipo_cliente")
    private String tipoCliente;
    @Column(name = "qtd_imovel")
    private int qtdImovel;
    @Column(name = "obs")
    private String obs;
    @Column(name = "dt_criacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCriacao;
    @Column(name = "dt_alteracao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAlteracao;
    @Column(name = "dt_entrega")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEntrega;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato")
    private List<ContratoFoto> contratoFotoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nrContrato")
    private List<Visita> visitaList;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusContrato status;
//    @JoinColumn(name = "vendedor", referencedColumnName = "id")
//    @ManyToOne(optional = false)
//    private Usuario vendedor;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuario;
    @Column(name = "consultoria")
    private boolean consultoria;
    @Column(name = "ficha_google")
    private boolean fichaGoogle;
    @Column(name = "analise_anual")
    private boolean analiseAnual;
    @Column(name = "analise_mensal")
    private boolean analiseMensal;
    @Column(name = "site")
    private boolean site;
    @Column(name = "visita_aberto")
    private boolean visitaAberto;
    @Column(name = "edicao")
    private Integer edicao;
    @Column(name = "desconto")
    private Double desconto;

    public Contrato() {
    }

    public Contrato(Integer nrContrato) {
        this.nrContrato = nrContrato;
    }

    public Contrato(Integer nrContrato, Date dtVenda, int pagamento) {
        this.nrContrato = nrContrato;
        this.dtVenda = dtVenda;
        this.pagamento = pagamento;
    }

    public Integer getNrContrato() {
        return nrContrato;
    }

    public void setNrContrato(Integer nrContrato) {
        this.nrContrato = nrContrato;
    }

    public Date getDtVenda() {
        return dtVenda;
    }

    public void setDtVenda(Date dtVenda) {
        this.dtVenda = dtVenda;
    }

    public int getPagamento() {
        return pagamento;
    }

    public void setPagamento(int pagamento) {
        this.pagamento = pagamento;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @XmlTransient
    public List<ContratoFoto> getContratoFotoList() {
        return contratoFotoList;
    }

    public void setContratoFotoList(List<ContratoFoto> contratoFotoList) {
        this.contratoFotoList = contratoFotoList;
    }

    @XmlTransient
    public List<Visita> getVisitaList() {
        return visitaList;
    }

    public void setVisitaList(List<Visita> visitaList) {
        this.visitaList = visitaList;
    }

    public StatusContrato getStatus() {
        return status;
    }

    public void setStatus(StatusContrato status) {
        this.status = status;
    }

//    public Usuario getVendedor() {
//        return vendedor;
//    }
//
//    public void setVendedor(Usuario vendedor) {
//        this.vendedor = vendedor;
//    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nrContrato != null ? nrContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.nrContrato == null && other.nrContrato != null) || (this.nrContrato != null && !this.nrContrato.equals(other.nrContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Contrato[ nrContrato=" + nrContrato + " ]";
    }

    

    /**
     * @return the dtEntrega
     */
    public Date getDtEntrega() {
        return dtEntrega;
    }

    /**
     * @param dtEntrega the dtEntrega to set
     */
    public void setDtEntrega(Date dtEntrega) {
        this.dtEntrega = dtEntrega;
    }

    /**
     * @return the dtCriacao
     */
    public Date getDtCriacao() {
        return dtCriacao;
    }

    /**
     * @param dtCriacao the dtCriacao to set
     */
    public void setDtCriacao(Date dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

    /**
     * @return the dtAlteracao
     */
    public Date getDtAlteracao() {
        return dtAlteracao;
    }

    /**
     * @param dtAlteracao the dtAlteracao to set
     */
    public void setDtAlteracao(Date dtAlteracao) {
        this.dtAlteracao = dtAlteracao;
    }

    /**
     * @return the consultoria
     */
    public boolean isConsultoria() {
        return consultoria;
    }

    /**
     * @param consultoria the consultoria to set
     */
    public void setConsultoria(boolean consultoria) {
        this.consultoria = consultoria;
    }

    /**
     * @return the fichaGoogle
     */
    public boolean isFichaGoogle() {
        return fichaGoogle;
    }

    /**
     * @param fichaGoogle the fichaGoogle to set
     */
    public void setFichaGoogle(boolean fichaGoogle) {
        this.fichaGoogle = fichaGoogle;
    }

    /**
     * @return the analiseAnual
     */
    public boolean isAnaliseAnual() {
        return analiseAnual;
    }

    /**
     * @param analiseAnual the analiseAnual to set
     */
    public void setAnaliseAnual(boolean analiseAnual) {
        this.analiseAnual = analiseAnual;
    }

    /**
     * @return the analiseMensal
     */
    public boolean isAnaliseMensal() {
        return analiseMensal;
    }

    /**
     * @param analiseMensal the analiseMensal to set
     */
    public void setAnaliseMensal(boolean analiseMensal) {
        this.analiseMensal = analiseMensal;
    }

    /**
     * @return the site
     */
    public boolean isSite() {
        return site;
    }

    /**
     * @param site the site to set
     */
    public void setSite(boolean site) {
        this.site = site;
    }

    /**
     * @return the valorMensal
     */
    public double getValorMensal() {
        return valorMensal;
    }

    /**
     * @param valorMensal the valorMensal to set
     */
    public void setValorMensal(double valorMensal) {
        this.valorMensal = valorMensal;
    }

    /**
     * @return the tipoCliente
     */
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
     * @param tipoCliente the tipoCliente to set
     */
    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    /**
     * @return the qtdImovel
     */
    public int getQtdImovel() {
        return qtdImovel;
    }

    /**
     * @param qtdImovel the qtdImovel to set
     */
    public void setQtdImovel(int qtdImovel) {
        this.qtdImovel = qtdImovel;
    }

    @XmlTransient
    public List<Imovel> getImovelList() {
        return imovelList;
    }

    public void setImovelList(List<Imovel> imovelList) {
        this.imovelList = imovelList;
    }

    /**
     * @return the vendaEstabelecimentoImobiliaria
     */
    public boolean isVendaEstabelecimentoImobiliaria() {
        return vendaEstabelecimentoImobiliaria;
    }

    /**
     * @param vendaEstabelecimentoImobiliaria the vendaEstabelecimentoImobiliaria to set
     */
    public void setVendaEstabelecimentoImobiliaria(boolean vendaEstabelecimentoImobiliaria) {
        this.vendaEstabelecimentoImobiliaria = vendaEstabelecimentoImobiliaria;
    }

    /**
     * @return the visitaAberto
     */
    public boolean isVisitaAberto() {
        return visitaAberto;
    }

    /**
     * @param visitaAberto the visitaAberto to set
     */
    public void setVisitaAberto(boolean visitaAberto) {
        this.visitaAberto = visitaAberto;
    }

    /**
     * @return the edicao
     */
    public Integer getEdicao() {
        return edicao;
    }

    /**
     * @param edicao the edicao to set
     */
    public void setEdicao(Integer edicao) {
        this.edicao = edicao;
    }

    /**
     * @return the desconto
     */
    public Double getDesconto() {
        return desconto;
    }

    /**
     * @param desconto the desconto to set
     */
    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }
    
}
