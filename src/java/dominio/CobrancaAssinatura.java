/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author julia
 */
@Entity
@Table(name = "cobranca_assinatura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobrancaAssinatura.findAll", query = "SELECT c FROM CobrancaAssinatura c")
    , @NamedQuery(name = "CobrancaAssinatura.findById", query = "SELECT c FROM CobrancaAssinatura c WHERE c.id = :id")
    , @NamedQuery(name = "CobrancaAssinatura.findByDtInicio", query = "SELECT c FROM CobrancaAssinatura c WHERE c.dtInicio = :dtInicio")
    , @NamedQuery(name = "CobrancaAssinatura.findByPeriodicidade", query = "SELECT c FROM CobrancaAssinatura c WHERE c.periodicidade = :periodicidade")
    , @NamedQuery(name = "CobrancaAssinatura.findByDesconto", query = "SELECT c FROM CobrancaAssinatura c WHERE c.desconto = :desconto")
    , @NamedQuery(name = "CobrancaAssinatura.findByStatus", query = "SELECT c FROM CobrancaAssinatura c WHERE c.cliente.ativo = :ativo")
    , @NamedQuery(name = "CobrancaAssinatura.findByAdimplencia", query = "SELECT c FROM CobrancaAssinatura c WHERE c.adimplencia = :adimplencia")
    , @NamedQuery(name = "CobrancaAssinatura.findByStatusPeriodicidade", query = "SELECT c FROM CobrancaAssinatura c WHERE c.cliente.ativo = :ativo AND c.periodicidade = :periodicidade")
    , @NamedQuery(name = "CobrancaAssinatura.findByStatusAdimplencia", query = "SELECT c FROM CobrancaAssinatura c WHERE c.cliente.ativo = :ativo AND c.adimplencia = :adimplencia")
    , @NamedQuery(name = "CobrancaAssinatura.findByPeriodicidadeAdimplencia", query = "SELECT c FROM CobrancaAssinatura c WHERE c.periodicidade = :periodicidade AND c.adimplencia = :adimplencia")
    , @NamedQuery(name = "CobrancaAssinatura.findByStatusPeriodicidadeAdimplencia", query = "SELECT c FROM CobrancaAssinatura c WHERE c.cliente.ativo = :ativo AND c.periodicidade = :periodicidade AND c.adimplencia = :adimplencia")
    , @NamedQuery(name = "CobrancaAssinatura.findByCliente", query = "SELECT c FROM CobrancaAssinatura c WHERE c.cliente = :cliente")
    , @NamedQuery(name = "CobrancaAssinatura.findByFormaPagamento", query = "SELECT c FROM CobrancaAssinatura c WHERE c.formaPagamento = :formaPagamento")})
public class CobrancaAssinatura implements Serializable {

    @Basic(optional = false)
    @Column(name = "desconto")
    private Double desconto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assinatura")
    private List<CobrancaRegistro> cobrancaRegistroList;
    @Basic(optional = false)
    @Column(name = "adimplencia")
    private int adimplencia;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dt_inicio")
    @Temporal(TemporalType.DATE)
    private Date dtInicio;
    @Basic(optional = false)
    @Column(name = "periodicidade")
    private int periodicidade;
    @Basic(optional = false)
    @Column(name = "forma_pagamento")
    private int formaPagamento;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente cliente;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assinatura")
//    private List<CobrancaRegistro> cobrancaRegistroList;

    public CobrancaAssinatura() {
    }

    public CobrancaAssinatura(Integer id) {
        this.id = id;
    }

    public CobrancaAssinatura(Integer id, Date dtInicio, int periodicidade, Double desconto, int formaPagamento) {
        this.id = id;
        this.dtInicio = dtInicio;
        this.periodicidade = periodicidade;
        this.desconto = desconto;
        this.formaPagamento = formaPagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(Date dtInicio) {
        this.dtInicio = dtInicio;
    }

    public int getPeriodicidade() {
        return periodicidade;
    }

    public void setPeriodicidade(int periodicidade) {
        this.periodicidade = periodicidade;
    }


    public int getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(int formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

//    @XmlTransient
//    public List<CobrancaRegistro> getCobrancaRegistroList() {
//        return cobrancaRegistroList;
//    }
//
//    public void setCobrancaRegistroList(List<CobrancaRegistro> cobrancaRegistroList) {
//        this.cobrancaRegistroList = cobrancaRegistroList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobrancaAssinatura)) {
            return false;
        }
        CobrancaAssinatura other = (CobrancaAssinatura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.CobrancaAssinatura[ id=" + id + " ]";
    }


    public int getAdimplencia() {
        return adimplencia;
    }

    public void setAdimplencia(int adimplencia) {
        this.adimplencia = adimplencia;
    }


    @XmlTransient
    public List<CobrancaRegistro> getCobrancaRegistroList() {
        return cobrancaRegistroList;
    }

    public void setCobrancaRegistroList(List<CobrancaRegistro> cobrancaRegistroList) {
        this.cobrancaRegistroList = cobrancaRegistroList;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }
    
}
