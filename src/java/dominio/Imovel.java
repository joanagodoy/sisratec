/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "imovel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Imovel.findAll", query = "SELECT a FROM Imovel a"),
    @NamedQuery(name = "Imovel.findById", query = "SELECT a FROM Imovel a WHERE a.id = :id"),
    @NamedQuery(name = "Imovel.findByObs", query = "SELECT a FROM Imovel a WHERE a.obs = :obs"),
    @NamedQuery(name = "Imovel.findByCliente", query = "SELECT a FROM Imovel a WHERE a.cliente = :cliente"),
    @NamedQuery(name = "Imovel.findByVendedorSemVisitaOLD", query = "SELECT a FROM Imovel a INNER JOIN Cliente c ON a.cliente.id = c.id WHERE c.vendedor = :usuario AND a.id NOT IN (SELECT v.imovel.id FROM Visita v)"),
    @NamedQuery(name = "Imovel.findSemVisitaOLD", query = "SELECT a FROM Imovel a WHERE a.id NOT IN (SELECT v.imovel.id FROM Visita v)"),
    @NamedQuery(name = "Imovel.findByVendedorSemVisita", query = "SELECT a FROM Imovel a INNER JOIN Cliente c ON a.cliente.id = c.id WHERE c.vendedor = :usuario AND a.visitaAberto = true"),
    @NamedQuery(name = "Imovel.findSemVisita", query = "SELECT a FROM Imovel a WHERE a.visitaAberto = true")})
public class Imovel implements Serializable {
    @Column(name = "obs")
    private String obs;
    @OneToMany(mappedBy = "imovel")
    private List<Visita> visitaList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "ativo")
    private boolean ativo;
    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Endereco endereco;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "nr_contrato", referencedColumnName = "nr_contrato")
    @ManyToOne(optional = false)
    private Contrato nrContrato;
    @Column(name = "visita_aberto")
    private boolean visitaAberto;

    public Imovel() {
    }

    public Imovel(Integer id) {
        this.id = id;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imovel)) {
            return false;
        }
        Imovel other = (Imovel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Imovel[ id=" + id + " ]";
    }

    /**
     * @return the nr_contrato
     */
    public Contrato getNrContrato() {
        return nrContrato;
    }

    /**
     * @param nr_contrato the nr_contrato to set
     */
    public void setNrContrato(Contrato nrContrato) {
        this.nrContrato = nrContrato;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the ativo
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * @param ativo the ativo to set
     */
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @XmlTransient
    public List<Visita> getVisitaList() {
        return visitaList;
    }

    public void setVisitaList(List<Visita> visitaList) {
        this.visitaList = visitaList;
    }

    /**
     * @return the visitaAberto
     */
    public boolean isVisitaAberto() {
        return visitaAberto;
    }

    /**
     * @param visitaAberto the visitaAberto to set
     */
    public void setVisitaAberto(boolean visitaAberto) {
        this.visitaAberto = visitaAberto;
    }
    
}
