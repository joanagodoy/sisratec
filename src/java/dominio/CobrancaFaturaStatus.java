/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author julia
 */
@Entity
@Table(name = "cobranca_fatura_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobrancaFaturaStatus.findAll", query = "SELECT c FROM CobrancaFaturaStatus c")
    , @NamedQuery(name = "CobrancaFaturaStatus.findById", query = "SELECT c FROM CobrancaFaturaStatus c WHERE c.id = :id")
    , @NamedQuery(name = "CobrancaFaturaStatus.findByDescricao", query = "SELECT c FROM CobrancaFaturaStatus c WHERE c.descricao = :descricao")})
public class CobrancaFaturaStatus implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
    private List<CobrancaFatura> cobrancaFaturaList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;

    public CobrancaFaturaStatus() {
    }

    public CobrancaFaturaStatus(Integer id) {
        this.id = id;
    }

    public CobrancaFaturaStatus(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobrancaFaturaStatus)) {
            return false;
        }
        CobrancaFaturaStatus other = (CobrancaFaturaStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.CobrancaFaturaStatus[ id=" + id + " ]";
    }

    @XmlTransient
    public List<CobrancaFatura> getCobrancaFaturaList() {
        return cobrancaFaturaList;
    }

    public void setCobrancaFaturaList(List<CobrancaFatura> cobrancaFaturaList) {
        this.cobrancaFaturaList = cobrancaFaturaList;
    }
    
}
