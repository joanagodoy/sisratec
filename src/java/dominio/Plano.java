/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Joana
 */
@Entity
@Table(name = "plano")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plano.findAll", query = "SELECT p FROM Plano p"),
    @NamedQuery(name = "Plano.findByProdutoVindi", query = "SELECT p FROM Plano p WHERE p.produtoVindi = :produtoVindi"),
    @NamedQuery(name = "Plano.findByValorAnual", query = "SELECT p FROM Plano p WHERE p.valorAnual = :valorAnual"),
    @NamedQuery(name = "Plano.findByEdicaoContrato", query = "SELECT p FROM Plano p WHERE p.planoPK.edicaoContrato = :edicaoContrato"),
    @NamedQuery(name = "Plano.findByPtsVista", query = "SELECT p FROM Plano p WHERE p.planoPK.ptsVista = :ptsVista")})
public class Plano implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PlanoPK planoPK;
    @Basic(optional = false)
    @Column(name = "produto_vindi")
    private int produtoVindi;
    @Basic(optional = false)
    @Column(name = "valor_anual")
    private double valorAnual;

    public Plano() {
    }

    public Plano(PlanoPK planoPK) {
        this.planoPK = planoPK;
    }

    public Plano(PlanoPK planoPK, int produtoVindi, double valorAnual) {
        this.planoPK = planoPK;
        this.produtoVindi = produtoVindi;
        this.valorAnual = valorAnual;
    }

    public Plano(int edicaoContrato, int ptsVista) {
        this.planoPK = new PlanoPK(edicaoContrato, ptsVista);
    }

    public PlanoPK getPlanoPK() {
        return planoPK;
    }

    public void setPlanoPK(PlanoPK planoPK) {
        this.planoPK = planoPK;
    }

    public int getProdutoVindi() {
        return produtoVindi;
    }

    public void setProdutoVindi(int produtoVindi) {
        this.produtoVindi = produtoVindi;
    }

    public double getValorAnual() {
        return valorAnual;
    }

    public void setValorAnual(double valorAnual) {
        this.valorAnual = valorAnual;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planoPK != null ? planoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plano)) {
            return false;
        }
        Plano other = (Plano) object;
        if ((this.planoPK == null && other.planoPK != null) || (this.planoPK != null && !this.planoPK.equals(other.planoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Plano[ planoPK=" + planoPK + " ]";
    }
    
}
