/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "plano_vindi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanoVindi.findAll", query = "SELECT p FROM PlanoVindi p"),
    @NamedQuery(name = "PlanoVindi.findById", query = "SELECT p FROM PlanoVindi p WHERE p.planoVindiPK.id = :id"),
    @NamedQuery(name = "PlanoVindi.findByItem", query = "SELECT p FROM PlanoVindi p WHERE p.planoVindiPK.item = :item AND p.imobiliaria = :imobiliaria")})
public class PlanoVindi implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PlanoVindiPK planoVindiPK;
    @Basic(optional = false)
    @Column(name = "imobiliaria")
    private boolean imobiliaria;

    public PlanoVindi() {
    }

    public PlanoVindi(PlanoVindiPK planoVindiPK) {
        this.planoVindiPK = planoVindiPK;
    }

    public PlanoVindi(int id, String item) {
        this.planoVindiPK = new PlanoVindiPK(id, item);
    }

    public PlanoVindiPK getPlanoVindiPK() {
        return planoVindiPK;
    }

    public void setPlanoVindiPK(PlanoVindiPK planoVindiPK) {
        this.planoVindiPK = planoVindiPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planoVindiPK != null ? planoVindiPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanoVindi)) {
            return false;
        }
        PlanoVindi other = (PlanoVindi) object;
        if ((this.planoVindiPK == null && other.planoVindiPK != null) || (this.planoVindiPK != null && !this.planoVindiPK.equals(other.planoVindiPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.PlanoVindi[ planoVindiPK=" + planoVindiPK + " ]";
    }

    /**
     * @return the imobiliaria
     */
    public boolean isImobiliaria() {
        return imobiliaria;
    }

    /**
     * @param imobiliaria the imobiliaria to set
     */
    public void setImobiliaria(boolean imobiliaria) {
        this.imobiliaria = imobiliaria;
    }
    
}
