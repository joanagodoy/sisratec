/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author julia
 */
@Entity
@Table(name = "cobranca_registro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobrancaRegistro.findAll", query = "SELECT c FROM CobrancaRegistro c")
    , @NamedQuery(name = "CobrancaRegistro.findById", query = "SELECT c FROM CobrancaRegistro c WHERE c.id = :id")
    , @NamedQuery(name = "CobrancaRegistro.findByDescricao", query = "SELECT c FROM CobrancaRegistro c WHERE c.descricao = :descricao")})
public class CobrancaRegistro implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @JoinColumn(name = "assinatura", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CobrancaAssinatura assinatura;
    @JoinColumn(name = "referencia", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CobrancaRegistroReferencia referencia;

    public CobrancaRegistro() {
    }

    public CobrancaRegistro(Integer id) {
        this.id = id;
    }

    public CobrancaRegistro(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public CobrancaAssinatura getAssinatura() {
        return assinatura;
    }

    public void setAssinatura(CobrancaAssinatura assinatura) {
        this.assinatura = assinatura;
    }

    public CobrancaRegistroReferencia getReferencia() {
        return referencia;
    }

    public void setReferencia(CobrancaRegistroReferencia referencia) {
        this.referencia = referencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobrancaRegistro)) {
            return false;
        }
        CobrancaRegistro other = (CobrancaRegistro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.CobrancaRegistro[ id=" + id + " ]";
    }

       
}
