/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "cobranca")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cobranca.findAll", query = "SELECT c FROM Cobranca c"),
    @NamedQuery(name = "Cobranca.findById", query = "SELECT c FROM Cobranca c WHERE c.id = :id"),
    @NamedQuery(name = "Cobranca.findByCodVindi", query = "SELECT c FROM Cobranca c WHERE c.codVindi = :codVindi"),
    @NamedQuery(name = "Cobranca.findByValor", query = "SELECT c FROM Cobranca c WHERE c.valor = :valor"),
    @NamedQuery(name = "Cobranca.findByVencimento", query = "SELECT c FROM Cobranca c WHERE c.vencimento = :vencimento")})
public class Cobranca implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "cod_vindi")
    private Integer codVindi;
    @Basic(optional = false)
    @Column(name = "valor")
    private long valor;
    @Column(name = "vencimento")
    @Temporal(TemporalType.DATE)
    private Date vencimento;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusCobranca status;
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cliente cliente;

    public Cobranca() {
    }

    public Cobranca(Integer id) {
        this.id = id;
    }

    public Cobranca(Integer id, long valor) {
        this.id = id;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodVindi() {
        return codVindi;
    }

    public void setCodVindi(Integer codVindi) {
        this.codVindi = codVindi;
    }

    public long getValor() {
        return valor;
    }

    public void setValor(long valor) {
        this.valor = valor;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public StatusCobranca getStatus() {
        return status;
    }

    public void setStatus(StatusCobranca status) {
        this.status = status;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cobranca)) {
            return false;
        }
        Cobranca other = (Cobranca) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Cobranca[ id=" + id + " ]";
    }
    
}
