/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Joana
 */
@Entity
@Table(name = "cobranca_listagem_debora")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobrancaListagemDebora.findAll", query = "SELECT c FROM CobrancaListagemDebora c"),
    @NamedQuery(name = "CobrancaListagemDebora.findByCliente", query = "SELECT c FROM CobrancaListagemDebora c WHERE c.cliente = :cliente"),
    @NamedQuery(name = "CobrancaListagemDebora.findByVencimentoFatura", query = "SELECT c FROM CobrancaListagemDebora c WHERE c.vencimentoFatura = :vencimentoFatura"),
    @NamedQuery(name = "CobrancaListagemDebora.findByPeriodicidade", query = "SELECT c FROM CobrancaListagemDebora c WHERE c.periodicidade = :periodicidade"),
    @NamedQuery(name = "CobrancaListagemDebora.findByValor", query = "SELECT c FROM CobrancaListagemDebora c WHERE c.valor = :valor"),
    @NamedQuery(name = "CobrancaListagemDebora.findByStatus", query = "SELECT c FROM CobrancaListagemDebora c WHERE c.status = :status")})
public class CobrancaListagemDebora implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cliente")
    private Integer cliente;
    @Basic(optional = false)
    @Column(name = "vencimento_fatura")
    @Temporal(TemporalType.DATE)
    private Date vencimentoFatura;
    @Basic(optional = false)
    @Column(name = "periodicidade")
    private int periodicidade;
    @Basic(optional = false)
    @Column(name = "valor")
    private double valor;
    @Basic(optional = false)
    @Column(name = "status")
    private boolean status;

    public CobrancaListagemDebora() {
    }

    public CobrancaListagemDebora(Integer cliente) {
        this.cliente = cliente;
    }

    public CobrancaListagemDebora(Integer cliente, Date vencimentoFatura, int periodicidade, double valor, boolean status) {
        this.cliente = cliente;
        this.vencimentoFatura = vencimentoFatura;
        this.periodicidade = periodicidade;
        this.valor = valor;
        this.status = status;
    }

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }

    public Date getVencimentoFatura() {
        return vencimentoFatura;
    }

    public void setVencimentoFatura(Date vencimentoFatura) {
        this.vencimentoFatura = vencimentoFatura;
    }

    public int getPeriodicidade() {
        return periodicidade;
    }

    public void setPeriodicidade(int periodicidade) {
        this.periodicidade = periodicidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliente != null ? cliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobrancaListagemDebora)) {
            return false;
        }
        CobrancaListagemDebora other = (CobrancaListagemDebora) object;
        if ((this.cliente == null && other.cliente != null) || (this.cliente != null && !this.cliente.equals(other.cliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.CobrancaListagemDebora[ cliente=" + cliente + " ]";
    }
    
}
