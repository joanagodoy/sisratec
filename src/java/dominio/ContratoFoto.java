/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "contrato_foto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContratoFoto.findAll", query = "SELECT c FROM ContratoFoto c"),
    @NamedQuery(name = "ContratoFoto.findById", query = "SELECT c FROM ContratoFoto c WHERE c.id = :id")})
public class ContratoFoto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Lob
    @Column(name = "foto")
    private byte[] foto;
    @JoinColumn(name = "contrato", referencedColumnName = "nr_contrato")
    @ManyToOne(optional = false)
    private Contrato contrato;

    public ContratoFoto() {
    }

    public ContratoFoto(Integer id) {
        this.id = id;
    }

    public ContratoFoto(Contrato contrato, byte[] foto) {
        this.contrato = contrato;
        this.foto = foto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoFoto)) {
            return false;
        }
        ContratoFoto other = (ContratoFoto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.ContratoFoto[ id=" + id + " ]";
    }
    
}
