/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class DateConverter {
    static SimpleDateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat apkDate = new SimpleDateFormat("yyyyMMddHHmm");
    
    public static String toSQL(Date data){
        if(data == null){
            return "null";
        }
        return sqlDate.format(data);
    }
    
    public static String toApk(Date data){
        if(data == null){
            return "null";
        }
        return apkDate.format(data);
    }

    public static Date fromApk(String name) throws ParseException {
        return apkDate.parse(name);
    }
}
