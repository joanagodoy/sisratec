/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
 
/**
 *
 * @author Usuario
 */
public class DecimalConverter {

    public static BigDecimal converterStringEmDecimal(String string) {
        if (string.length() == 0) {
            return BigDecimal.ZERO;
        }
        string = string.replace(',', 'p');
        string = string.replaceAll("[.]", "");
        string = string.replace('p', '.');
        if (string.contains("R$")) {
            string = string.substring(3);
        }
        return new BigDecimal(string);

    }

    public static BigDecimal converterCoefEmDecimal(String string) {
        string = string.replace(',', '.');
        double d = Double.valueOf(string);
        BigDecimal bd = new BigDecimal(d);
        return bd.setScale(7, RoundingMode.DOWN);

    }

    public static String converterDecimalEmStringSemRS(BigDecimal f) {

        return NumberFormat.getCurrencyInstance().format(f).substring(3);

    }

    public static String converterDecimalEmStringSemRSCoef(BigDecimal f) {

        return f.toString();

    }

    public static String converterDecimalEmStringComRS(BigDecimal f) {

        return NumberFormat.getCurrencyInstance().format(f);

    }
}
