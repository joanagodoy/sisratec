/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Usuario
 */
public class Utils {

    public static Date dateTime(Date date, Date time) {
        return new Date(
                date.getYear(), date.getMonth(), date.getDay(),
                time.getHours(), time.getMinutes(), time.getSeconds()
        );
    }

    public static String diferencaEmHorasEMinutos(Date data1, Date data2) {

        if (data1 == null || data2 == null) {
            return "";
        }

        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        String diferencaEmHorasEMinutos;
        int hora1 = Integer.valueOf(formatHora.format(data1).substring(0, 2));
        int min1 = Integer.valueOf(formatHora.format(data1).substring(3, 5));
        int hora2 = Integer.valueOf(formatHora.format(data2).substring(0, 2));
        int min2 = Integer.valueOf(formatHora.format(data2).substring(3, 5));

        int resultadoHora;
        int resultadoMin;

        resultadoHora = hora2 - hora1;

        if (min1 > min2) {

            if (resultadoHora > 0) {
                resultadoHora--;
            }
            resultadoMin = 60 - (min1 - min2);

        } else {

            resultadoMin = min2 - min1;
        }

        String horas = String.valueOf(resultadoHora);
        String min = String.valueOf(resultadoMin);

        if (horas.length() < 2) {
            horas = "0" + horas;
        }
        if (min.length() < 2) {
            min = "0" + min;
        }

        diferencaEmHorasEMinutos = horas + ":" + min;

        return diferencaEmHorasEMinutos;
    }

    public static double diferencaEmMinutos(Date dataInicial, Date dataFinal) {
        double result;
        long diferenca = dataFinal.getTime() - dataInicial.getTime();
        double diferencaEmMinutos = (diferenca / 1000) / 60; //resultado é diferença entre as datas em minutos  
        long segundosRestantes = (diferenca / 1000) % 60; //calcula os segundos restantes  
        result = diferencaEmMinutos + (segundosRestantes / 60d); //transforma os segundos restantes em minutos  

        return result;
    }

    public static String getRangeFromWeekAndYear(int week, int year) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        String out = "";
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        out += sdf.format(cal.getTime());
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        out += " - " + sdf.format(cal.getTime());
        return out;
    }

    public static String[] rangeSemanas() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        String semanaPar = "";
        String semanaImpar = "";
        Date dataAtual = Calendar.getInstance().getTime();
        SimpleDateFormat weekFormat = new SimpleDateFormat("w");
        int semanaAno = Integer.parseInt(weekFormat.format(dataAtual));
        if ((semanaAno + 1) % 2 == 0) {

            cal.set(Calendar.WEEK_OF_YEAR, semanaAno);
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            semanaPar = (sdf.format(cal.getTime()));
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            semanaPar += "  até  " + (sdf.format(cal.getTime()));

            cal.set(Calendar.WEEK_OF_YEAR, semanaAno + 1);
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            semanaImpar = (sdf.format(cal.getTime()));
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            semanaImpar += "  até  " + (sdf.format(cal.getTime()));
        } else if ((semanaAno + 1) % 2 != 0) {

            cal.set(Calendar.WEEK_OF_YEAR, semanaAno);
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            semanaImpar = (sdf.format(cal.getTime()));
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            semanaImpar += "  até  " + (sdf.format(cal.getTime()));

            cal.set(Calendar.WEEK_OF_YEAR, semanaAno + 1);
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            semanaPar = (sdf.format(cal.getTime()));
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            semanaPar += "  até  " + (sdf.format(cal.getTime()));
        }
        return new String[]{semanaImpar, semanaPar};
    }

}
