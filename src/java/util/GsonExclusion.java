/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 *
 * @author Usuario
 */
public class GsonExclusion implements ExclusionStrategy {

        public boolean shouldSkipClass(Class<?> arg0) {
            return false;
        }

        public boolean shouldSkipField(FieldAttributes f) {
            return (f.getName().endsWith("List") || f.getName().endsWith("List1"));
        }
        
        public static Gson GSON(){
            return new GsonBuilder().setExclusionStrategies(new GsonExclusion()).create();
        }

    }