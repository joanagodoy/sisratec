/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class SQLConverter {

    public static <T> T cv(T conv) {
        if(conv == null){
            return (T) "null";
        }
        if (conv instanceof String) {
            if(conv == null || ((String)conv).equals("null")){
                return (T)"''";
            }
            return (T) replace(((String) conv).replace("\n", " "));
        }
        if (conv instanceof Date) {
            Date date = (Date) conv;
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
             if(cal.get(Calendar.YEAR) == 1970){
                 return (T) formatarTime((Date) conv);
            } else if (cal.get(Calendar.HOUR) == 0 && cal.get(Calendar.MINUTE) == 0) {
                return (T) formatarDate((Date) conv);
            } else{
                return (T) formatarDateTime((Date) conv);
            }
        } if(conv instanceof BigDecimal){
            return (T)((BigDecimal)conv).setScale(3,BigDecimal.ROUND_UP).toPlainString();
        }
        return conv;
    }

    public static String replace(String string) {
        if (string == null) {
            return "''";
        } else {
            return "'" + string.replace("'", " ") + "'";
        }
    }

    public static String formatarDate(Date data) {

        if (data == null) {
            return "null";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            return "'" + sdf.format(data) + "'";
        }
    }

    public static String formatarDateTime(Date data) {

        if (data == null) {
            return "null";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            return "'" + sdf.format(data) + "'";
        }
    }
    public static String formatarTime(Date data) {

        if (data == null) {
            return "null";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            return "'" + sdf.format(data) + "'";
        }
    }
}
