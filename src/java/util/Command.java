package util;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class Command {

    public enum Escope {

        session, request;
    }
    protected HttpServletRequest request;
    protected HttpServletResponse response;

    public Command init(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        return this;
    }
    
    public void print(Object s ){
//        System.out.println(s);
    }

    public <T> T readObject(String name, Escope escope) {
        Object retorno = null;
        switch (escope) {
            case request:
                retorno = this.request.getAttribute(name);
                break;
            case session:
                retorno = this.request.getSession().getAttribute(name);
                break;
        }
        return (T)retorno;
    }
    
    public <T> T readObject(String name, Escope escope,T obj) {
        Object retorno = null;
        switch (escope) {
            case request:
                retorno = this.request.getAttribute(name);
                break;
            case session:
                retorno = this.request.getSession().getAttribute(name);
                break;
        }
        return (T)retorno;
    }

    public String read(String name) {
        String retorno = null;
        retorno = request.getParameter(name);
        return retorno;
    }

    public void write(String name, Object var, Escope escope) {
        switch (escope) {
            case request:
                this.request.setAttribute(name, var);
                break;
            case session:
                this.request.getSession().setAttribute(name, var);
                break;
        }
    }

    public void forward(String url) {
        try {
            this.request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void redirect(String url) {
        try {
            this.response.sendRedirect(url);
        }
        catch (IOException ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void execute(String action) throws ServletException, IOException {
        try {
            Method method = this.getClass().getDeclaredMethod(action);
            method.invoke(this);
        } catch (Exception e) {
            this.response.getWriter().println(e.getMessage());
        }
    }
}
