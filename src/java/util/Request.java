/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;


import java.io.*;
import java.net.*;

/**
 *
 * @author Usuario
 */
public class Request {

    public static String excuteRequest(String targetURL)  {
        URL url;
        HttpURLConnection connection = null;
        try {
            ;
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is,"UTF-8"));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return "error";

        }
    }
}
