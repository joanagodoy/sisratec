/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.math.BigDecimal;

/**
 *
 * @author Usuario
 */
public class Converter {

    public static Integer converterParaInteger(Object o) {
        if (o instanceof Integer) {
            return (Integer) o;
        } else if (o instanceof Double) {
            return ((Double) o).intValue();
        } else if (o instanceof Long) {
            return ((Long) o).intValue();
        } else if (o instanceof String) {
            return Integer.valueOf((String) o);
        } else if (o instanceof BigDecimal) {
            return ((BigDecimal) o).intValue();
        } else {
            return null;
        }
    }

    public static BigDecimal converterParaBigDecimal(Object o) {
        if (o instanceof Integer) {
            return new BigDecimal((Integer) o);
        } else if (o instanceof Double) {
            return new BigDecimal((Double) o);
        } else if (o instanceof Long) {
            return new BigDecimal((Long) o);
        } else if (o instanceof String) {
            return new BigDecimal((String) o);
        } else if (o instanceof BigDecimal) {
            return ((BigDecimal) o);
        } else {
            return null;
        }
    }

    public static Double converterParaDouble(Object o) {
        if (o instanceof Integer) {
            return new Double((Integer) o);
        } else if (o instanceof Double) {
            return (Double) o;
        } else if (o instanceof Long) {
            return new Double((Long) o);
        } else if (o instanceof String) {
            return new Double((String) o);
        } else if (o instanceof BigDecimal) {
            return ((BigDecimal) o).doubleValue();
        } else {
            return null;
        }
    }

    public static Long converterParaLong(Object o) {
        if (o instanceof Integer) {
            return new Long((Integer) o);
        } else if (o instanceof Double) {
            return ((Double) o).longValue();
        } else if (o instanceof Long) {
            return (Long) o;
        } else if (o instanceof String) {
            return new Long((String) o);
        } else if (o instanceof BigDecimal) {
            return ((BigDecimal) o).longValue();
        } else {
            return null;
        }
    }

}
