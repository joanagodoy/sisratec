/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;


import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class Mailer {

    public static void send(String anexo, String fileName, String anexo2, String fileName2, String assunto, String conteudo, String emailDestino) throws Exception {
         
        final String username = "isratec.informativo@gmail.com";
	final String password = "b76890b80";
                
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
//            if (config.getConexaoSsl()) {
//                props.put("mail.smtp.socketFactory.port", config.getPorta());
//                props.put("mail.smtp.socketFactory.class",
//                        "javax.net.ssl.SSLSocketFactory");
//            }
            Authenticator authenticator = null;
            authenticator = new Authenticator() {
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            };
            Session session = Session.getInstance(props, authenticator);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username, "Isratec"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDestino));
            message.setSubject(assunto, "utf-8");
//            message.setContent(conteudo, "text/html; charset=utf-8");
            
            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(conteudo, "text/html; charset=utf-8");
            
            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);
                
            if(anexo!=null){
                // Part two is attachment
                messageBodyPart = new MimeBodyPart();
//                anexo = "../web/files/Apresentacao_Isratec.pdf";
                DataSource source = new FileDataSource(anexo);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(fileName);
                multipart.addBodyPart(messageBodyPart);
            }
            
            if(anexo2!=null){
                // Part two is attachment
                messageBodyPart = new MimeBodyPart();
//                anexo = "../web/files/Apresentacao_Isratec.pdf";
                DataSource source = new FileDataSource(anexo2);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(fileName2);
                multipart.addBodyPart(messageBodyPart);
            }
            
//             MimeBodyPart attachment= new MimeBodyPart();
//    ByteArrayDataSource ds = new ByteArrayDataSource(arrayInputStream, "application/pdf"); 
//    attachment.setDataHandler(new DataHandler(ds));
//    attachment.setFileName("Report.pdf");
//    mimeMultipart.addBodyPart(attachment);
    
            
//            if(anexo2!=null){
//                // Part two is attachment
//                messageBodyPart = new MimeBodyPart();
//                DataSource source = new FileDataSource(anexo2);
//                messageBodyPart.setDataHandler(new DataHandler(source));
//                messageBodyPart.setFileName("Apresentação Isratour");
//                multipart.addBodyPart(messageBodyPart);
//            }
           
            // Send the complete message parts
            message.setContent(multipart);
            Transport.send(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } finally {
        }
    }

}
