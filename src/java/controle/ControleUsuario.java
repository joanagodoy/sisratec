package controle;

import dao.ClienteJpaController;
import dao.UsuarioJpaController;
import dominio.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.Cookie;
import util.Command;
import util.Crypt;
import util.GsonExclusion;

/**
 *
 * @author Usuario
 */
public class ControleUsuario extends Command {

    

    public void alterarSenha() {
        forward("alterar_senha.jsp");
    }

    public void atualizarDados() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        EntityManager em = emf.createEntityManager();
        Usuario user = readObject("usuario", Escope.session);
        try {
            String token = read("token");
            em.getTransaction().begin();
            em.createNativeQuery("UPDATE usuario SET token = ? WHERE id = ?").setParameter(1, token).setParameter(2, user.getId()).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }
    }

    public void alteraSenha() throws Exception {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            String senhaAtual = read("senhaAtual");
            String novaSenha = read("novaSenha");
            Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
            if (Crypt.criptografar(senhaAtual).equals(usuario.getSenha())) {
                Usuario user = (Usuario) request.getSession().getAttribute("usuario");
                user.setSenha(Crypt.criptografar(novaSenha));
                new UsuarioJpaController(emf).edit(user);
                write("usuario", user, Escope.session);
                response.getWriter().write("ok");
            } else {
                response.getWriter().write("erro");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().write("erro");
        } finally {
            emf.close();
        }
    }

    public void logout() {
        try {
            request.getSession().invalidate();
            response.sendRedirect("");
        } catch (Exception ex) {
            Logger.getLogger(ControleUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
