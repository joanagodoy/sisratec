/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.ContratoJpaController;
import dao.FotografiaJpaController;
import dao.StatusFotografiaJpaController;
import dao.VisitaJpaController;
import dominio.Contrato;
import dominio.Fotografia;
import dominio.Usuario;
import dominio.Visita;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import util.Command;
import util.GsonExclusion;
import util.Mailer;
/**
 *
 * @author Usuario
 */
public class ControleFotografia extends Command {
    
    public String removerMascara(String str){
        return str.replaceAll("\\D", "");
    }
    
    public void fotografia(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {    
            write("statusFotografia", new StatusFotografiaJpaController(emf).findStatusFotografiaEntities(), Escope.request);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forward("fotografia.jsp");
            emf.close();
        }
    }
    
    public void buscarFotografia() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            if (read("id") != null) {
                response.getWriter().print(GsonExclusion.GSON().toJson(new FotografiaJpaController(emf).findFotografia(Integer.parseInt(read("id")))));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Usuario user = readObject("usuario", Escope.session);
            System.out.println(read("status"));
//            if(user.getTipo().getId().equals(3)){ //fotografo
//                response.getWriter().print(GsonExclusion.GSON().toJson(new FotografiaJpaController(emf).findFotografiasFotografo()));
//            }else{
            List<Fotografia> fotos = null;
            if(read("status").equals("todos")){
                fotos = new FotografiaJpaController(emf).findFotografiaEntities();
            }else{
                fotos = new FotografiaJpaController(emf).findFotografiasByStatus(Integer.parseInt(read("status")));
            }
            
            response.getWriter().print(GsonExclusion.GSON().toJson(fotos));
//            }
//            else if (user.getTipo().getId().equals(5)){//editor
//                response.getWriter().print(GsonExclusion.GSON().toJson(new FotografiaJpaController(emf).findFotografiasEditor()));
//            }else{
//                response.getWriter().print(GsonExclusion.GSON().toJson(new FotografiaJpaController(emf).findFotografiaEntities()));
//            }
        
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {            
            Usuario user = readObject("usuario", Escope.session);
            Fotografia foto = new FotografiaJpaController(emf).findFotografia(Integer.parseInt(read("id")));
//            System.out.println(read("dataEnvio"));
            String obs = "";
            if(read("dataEnvio") != null && read("dataEnvio") != ""){
                obs = "Enviou Editor";
                foto.setDtEnvioEditor(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataEnvio")));
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(7));//Pendente Confirmação
            }
            if(read("dataConfirmacao") != null && read("dataConfirmacao") != ""){
                obs = "Confirmou Recebimento";
                foto.setDtConfirmacaoEntrega(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataConfirmacao")));
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(2));
            }else if(read("faltouPts") != null && "on".equals(read("faltouPts"))){
                foto.setFaltouPts(true);
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(4));
            }
            
            if(read("dataPostagem") != null && read("dataPostagem") != ""){ 
                obs = "Postou";
                foto.setDtPostagem(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataPostagem")));
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(3));
            } else if(read("problema") != null && "on".equals(read("problema"))){
                foto.setProblemaFotografo(true);
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(4));
            }
            
            if(read("link") != "" && read("link") != null){
                obs = "Informou Link";
                foto.setLinkFotos(read("link"));
//                Contrato contrato = new ContratoJpaController(emf).findByClienteImovelNull(foto.getIdCliente());
//                if(contrato.getTipoCliente().equals("Estabelecimento")){
//                    foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(5));
//                }else{
//                    foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(9));
//                }
                if(foto.getImovel() == null){
                    foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(5));
                }else{
                    foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(9));
                }
                
                
            } else if(read("editorProblema") != null && "on".equals(read("editorProblema"))){
                foto.setProblemaEditor(true);
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(8));
            }     
//            System.out.println(obs);
            if(!obs.equals("")){
                if(read("novaobs") == null || read("novaobs") == ""){
                    foto.setObs(foto.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": "+obs : foto.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": "+ obs.toUpperCase());
                }else{
                    foto.setObs(foto.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": "+ obs + " - " + read("novaobs") : foto.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": "+ obs + " - " + read("novaobs").toUpperCase());
                }
            }else{
                if(read("novaobs") == null || read("novaobs") == ""){
                    foto.setObs(foto.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() : foto.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome());
                }else{
                    foto.setObs(foto.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": "+read("novaobs") : foto.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": "+ read("novaobs").toUpperCase());
                }
            }
            new FotografiaJpaController(emf).edit(foto);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
//    public void enviarFoto() throws IOException {
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {
//            System.out.println(read("idContrato"));
//            Collection<Part> parts = request.getParts();
//            for (Part p : parts) {
//                if (p.getContentType() != null) {
//                    byte[] file = IOUtils.toByteArray(p.getInputStream());
//                    
//                    new ContratoFotoJpaController(emf).create(new ContratoFoto(new ContratoJpaController(emf).findContrato(Integer.parseInt(read("idContrato"))), file));
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            response.getWriter().print("<script>window.parent.loading('close');window.parent.buscarFotos();</script>");
//            emf.close();
//        }
//    }
    
//    public void buscarFoto() throws IOException {
//
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {
//            if (read("contrato") != null) {
//                System.out.println(read("contrato"));
//                response.setContentType("application/json");
//                response.setCharacterEncoding("UTF-8");
//                response.getWriter().print(GsonExclusion.GSON().toJson(new ContratoFotoJpaController(emf).findByContrato(Integer.parseInt(read("contrato")))));
//            } 
//            else if (read("id") != null) {
//                response.setContentType("image/jpeg");
//                ContratoFoto f = null;
//                try {
//                    f = new ContratoFotoJpaController(emf).findContratoFoto(Integer.parseInt(read("id")));
//                } catch (Exception e) {
//
//                }
//                if (f != null) {
//                    InputStream is = new ByteArrayInputStream(f.getFoto());
//                    IOUtils.copy(is, response.getOutputStream());
//                }
//                response.getOutputStream().close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            emf.close();
//        }

//    }
    
    public void alterarDataEnvio() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            List<String> ids = new Gson().fromJson(read("id"), new TypeToken<List<String>>() {
            }.getType());
            Usuario user = readObject("usuario", Escope.session);
            
            for (String s : ids) {
                Fotografia fotografia = new FotografiaJpaController(emf).findFotografia(Integer.parseInt(s));
                
                if(fotografia.getStatus().getId().equals(1)){
                    fotografia.setObs(fotografia.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": Enviou Editor" : fotografia.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": Enviou Editor");
                    fotografia.setDtEnvioEditor(new Date());
                    fotografia.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(7));
                    new FotografiaJpaController(emf).edit(fotografia);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(response.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    } 
    
    public void alterarDataPostagem() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            List<String> ids = new Gson().fromJson(read("id"), new TypeToken<List<String>>() {
            }.getType());
            Usuario user = readObject("usuario", Escope.session);
            
            for (String s : ids) {
                Fotografia fotografia = new FotografiaJpaController(emf).findFotografia(Integer.parseInt(s));
                
                if(fotografia.getStatus().getId().equals(2) || fotografia.getStatus().getId().equals(4)){
                    fotografia.setDtPostagem(new Date());
                    fotografia.setObs(fotografia.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": Postou" : fotografia.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": Postou");
                    fotografia.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(3));
                    new FotografiaJpaController(emf).edit(fotografia);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(response.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
//    public void confirmarEntrega() throws IOException {
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {
//            Fotografia fotografia = new FotografiaJpaController(emf).findFotografia(Integer.parseInt(read("id")));
//            if(fotografia.getStatus().getId().equals(7)){
//                fotografia.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(2));
//                new FotografiaJpaController(emf).edit(fotografia);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.sendError(response.SC_BAD_REQUEST);
//        } finally {
//            emf.close();
//        }
//    }
    
    
    public void enviarEmailLink() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveImovelNFinalizado = false;
        try {
            String path = request.getServletContext().getRealPath("/");     
            String anexo = path + File.separatorChar + "files" + File.separatorChar + "Perguntas Frequentes.pdf";
            String anexo2 = path + File.separatorChar + "files" + File.separatorChar + "Tutorial Facebook.pdf";
            List<String> ids = new Gson().fromJson(read("id"), new TypeToken<List<String>>() {
            }.getType());
            Usuario user = readObject("usuario", Escope.session);
            
            for (String s : ids) {
                Fotografia fotografia = new FotografiaJpaController(emf).findFotografia(Integer.parseInt(s));
                Contrato contrato = new ContratoJpaController(emf).findByCliente(fotografia.getIdCliente());
                if(contrato.getTipoCliente().equals("Estabelecimento")){
//                    String conteudo = "Prezado Cliente, parabéns por ter tomado a decisão de fazer parte do futuro, se juntando a onda 360º. \n <br><br><br>" +
//                                    "Nós na Isratec enxergamos a oportunidade de servir você e fazer uma contribuição para o seu sucesso uma grande honra e privilégio. \n <br><br><br>" +
//                                    "Nossos padrões de qualidade são os mais rígidos que existem no mercado, sua opinião é importante para nós, sinta-se a vontade em nos enviar um feedback para que possamos melhorar nosso serviço.\n <br><br><br>" +
//                                    "Este é o <a href="+fotografia.getLinkFotos()+">Link para o Tour</a> do seu estabelecimento, ficou extraordinário não é mesmo?! <br><br><br>" +
////                                    "Para que você  possa divulgar este Tour no Facebook, é só seguir o <a href='https://drive.google.com/file/d/1oe-zBEIQFLKXhil1Nu1hgEOAyDhng77H/view'>Tutorial</a>.  <br><br><br>"+
//                                    "Dúvidas frequentes estão sendo enviadas em anexo, se a sua não consta no documento, entre em contato. <br><br><br>"+
//                                    "Atenciosamente, <br>"+
//                                    "Equipe IsraTour.<br><br>";
//
//                    conteudo += "<img src='http://isratec.com.br/resources/assinatura.jpeg'>";

                    String conteudo = getHTML(contrato.getCliente().getResponsavel().split(" ")[0], fotografia.getLinkFotos().trim());

                    fotografia.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(6));
                    fotografia.setObs(fotografia.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": Enviou E-mail" : fotografia.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": Enviou E-mail");
                    new FotografiaJpaController(emf).edit(fotografia);
                    System.out.println(read("isCobranca"));
                    if(read("isCobranca").equals("1")){ //verifica se é para enviar cobrança
                        new ControleCobranca().registrarCobranca(fotografia.getIdCliente(), "recorrencia");
                    }
                    
                   Mailer.send(anexo, "Perguntas Frequentes.pdf", anexo2, "Tutorial Add Tour Facebook.pdf", "Sua Empresa Aberta para o Mundo", conteudo, fotografia.getIdCliente().getEmail());
                }else{
                    if(fotografia.getStatus().getId().equals(9) || fotografia.getStatus().getId().equals(5)){
                        List<Visita> visitas = new VisitaJpaController(emf).findVisitasByCliente(fotografia.getIdCliente());
                        for (Visita visita : visitas) {
                            if(visita.getStatus().getId().equals(1)){
                                houveImovelNFinalizado = true;
                            }
                        }
                        List<Fotografia> fotos = new FotografiaJpaController(emf).findFotografiasByCliente(fotografia.getIdCliente());
                        for (Fotografia foto : fotos) {
                            if(!foto.getStatus().getId().equals(9) && !foto.getStatus().getId().equals(5)){
                                houveImovelNFinalizado = true;
                            }
                        }
                        if(!houveImovelNFinalizado){
//                            String conteudo = "Prezado Cliente, parabéns por ter tomado a decisão de fazer parte do futuro, se juntando a onda 360º. \n <br><br><br>" +
//                                        "Nós na Isratec enxergamos a oportunidade de servir você e fazer uma contribuição para o seu sucesso uma grande honra e privilégio. \n <br><br><br>" +
//                                        "Nossos padrões de qualidade são os mais rígidos que existem no mercado, sua opinião é importante para nós, sinta-se a vontade em nos enviar um feedback para que possamos melhorar nosso serviço.\n <br><br><br>" +
//                                        "Segue abaixo os links para o Tour Virtual de cada imóvel contratado: <br><br>";
//
//
//                            
//                            String linkImobiliaria = "";
//                            for (Fotografia foto : fotos) {
//                                if(foto.getImovel() != null){
//                                    if(foto.getImovel().isAtivo()){
//                                        conteudo += "<a href="+foto.getLinkFotos()+">"+foto.getImovel().getDescricao()+"</a><br>";
//                                    }
//                                }else{
//                                    linkImobiliaria = foto.getLinkFotos();
//                                }
//                            }
//                            if(contrato.isVendaEstabelecimentoImobiliaria()){
//                                conteudo += "<br><br>E este é o <a href="+linkImobiliaria+">Link para o Tour</a> do seu estabelecimento, ficou extraordinário não é mesmo?! <br><br><br>";
////                                            "Para que você  possa divulgar este Tour no Facebook, é só seguir o <a href='https://drive.google.com/file/d/1oe-zBEIQFLKXhil1Nu1hgEOAyDhng77H/view'>Tutorial</a>.";
//                            }   
//                            conteudo += "<br><br><br>Dúvidas frequentes estão sendo enviadas em anexo, se a sua não consta no documento, entre em contato. <br><br><br>"+
//                                        "Atenciosamente, <br>"+
//                                        "Equipe IsraTour.<br><br>";
//                            conteudo += "<img src='http://isratec.com.br/resources/assinatura.jpeg'>";
//
//                            for (Fotografia foto : fotos) {
//                                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(6));
//                                foto.setObs(foto.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": Enviou E-mail" : foto.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": Enviou E-mail");
//                                new FotografiaJpaController(emf).edit(foto);
//                            }
//
//                            new ControleCobranca().registrarCobranca(fotografia.getIdCliente());
//                            Mailer.send(anexo, "Perguntas Frequentes.pdf", anexo2, "Tutorial Add Tour Facebook.pdf", "Sua Empresa Aberta para o Mundo", conteudo, fotografia.getIdCliente().getEmail());
                        }
                    }
                    //encontrar todas as fotos do cliente onde imovel != null
                    //verificar se todas estão finalizadas
                    //se tiver alguma não finalizada gerar erro
                    //se todas estiverem finalizadas enviar email e cobrança
                   
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(response.SC_BAD_REQUEST);
        } finally {
            if(houveImovelNFinalizado){
                response.sendError(response.SC_FOUND);
            }
            emf.close();
        }
    }
    
    public void reenviarEmailLink() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveImovelNFinalizado = false;
        try {
            String path = request.getServletContext().getRealPath("/");     
            String anexo = path + File.separatorChar + "files" + File.separatorChar + "Perguntas Frequentes.pdf";
            String anexo2 = path + File.separatorChar + "files" + File.separatorChar + "Tutorial Facebook.pdf";
            Usuario user = readObject("usuario", Escope.session);
                Fotografia fotografia = new FotografiaJpaController(emf).findFotografia(Integer.parseInt(read("id")));
//                    String conteudo = "Prezado Cliente, parabéns por ter tomado a decisão de fazer parte do futuro, se juntando a onda 360º. \n <br><br><br>" +
//                                    "Nós na Isratec enxergamos a oportunidade de servir você e fazer uma contribuição para o seu sucesso uma grande honra e privilégio. \n <br><br><br>" +
//                                    "Nossos padrões de qualidade são os mais rígidos que existem no mercado, sua opinião é importante para nós, sinta-se a vontade em nos enviar um feedback para que possamos melhorar nosso serviço.\n <br><br><br>" +
//                                    "Este é o <a href="+fotografia.getLinkFotos()+">Link para o Tour</a> do seu estabelecimento, ficou extraordinário não é mesmo?! <br><br><br>" +
////                                    "Para que você  possa divulgar este Tour no Facebook, é só seguir o <a href='https://drive.google.com/file/d/1oe-zBEIQFLKXhil1Nu1hgEOAyDhng77H/view'>Tutorial</a>.  <br><br><br>"+
//                                    "Dúvidas frequentes estão sendo enviadas em anexo, se a sua não consta no documento, entre em contato. <br><br><br>"+
//                                    "Atenciosamente, <br>"+
//                                    "Equipe IsraTour.<br><br>";
//
//                    conteudo += "<img src='http://isratec.com.br/resources/assinatura.jpeg'>";

                    String conteudo = getHTML(fotografia.getIdCliente().getResponsavel().split(" ")[0], fotografia.getLinkFotos().trim());

                    fotografia.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(6));
                    fotografia.setObs(fotografia.getObs() == null ? new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome()+": Reenviou E-mail" : fotografia.getObs() + "\n" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + user.getNome() + ": Reenviou E-mail");
                    new FotografiaJpaController(emf).edit(fotografia);
                    Mailer.send(anexo, "Perguntas Frequentes.pdf", anexo2, "Tutorial Add Tour Facebook.pdf", "Sua Empresa Aberta para o Mundo", conteudo, fotografia.getIdCliente().getEmail());
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(response.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public String getHTML(String nomeCliente, String link) throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            nomeCliente = nomeCliente.substring(0, 1).toUpperCase() + nomeCliente.substring(1).toLowerCase();
//            String nomeCliente = "Joana";
//            String link = "https://goo.gl/maps/hyLZkSxqc5v";
           String html = "<div style=\"background:#f7f7f7;margin:0;padding:0\">\n" +
"  <div style=\"background-color:#f7f7f7\">\n" +
"\n" +
"   <div style=\"border:1px solid #d9d9d9;margin:0px auto;max-width:600px;background:white\">\n" +
"	<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;background:white;border-collapse:collapse\" align=\"center\" border=\"0\">\n" +
"		<tbody>\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-bottom:0px;border-collapse:collapse\">\n" +
"		<div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px;padding-top:0px;border-collapse:collapse\">\n" +
"		<div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px 0px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:600px;border-collapse:collapse\">\n" +
"		<img height=\"auto\" src=\"http://isratec.com.br/resources/cabecalho-link-tour.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;line-height:100%\" width=\"600\"></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div>\n" +
"		\n" +
"		<div style=\"margin:0px auto;max-width:600px\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\">\n" +
"		<tbody>\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 40px 0px;border-collapse:collapse\"><div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;border-collapse:collapse\"><div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\">\n" +
"		<div style=\"color:#555;font-size:36px;font-weight:book;line-height:1.0;text-align:center;\">\n" +
"		<strong>\n" +
"		<a style='color:#1ad4cc';>\n" +
"		Olá "+nomeCliente+",<br> </a>\n" +
"		<a style='color:#1ad4cc';>\n" +
"		tudo bem?\n" +
"		</a>\n" +
"		</strong>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse;\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">\n" +
"		<strong>Nós da Isratec acreditamos que a tecnologia é a chave<br>\n" +
"		para a criação de soluções revolucionárias.\n" +
"		</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#ababab;line-height:1.5;text-align:center\">\n" +
"		<strong>Por isso, gostamos de pensar fora da caixa e trazer ideias<br> \n" +
"		visionárias que ajudam as empresas a estar um passo à frente. \n" +
"		</strong>\n" +
"		<br></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-top:0px;border-collapse:collapse\"><div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:10px 25px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:50px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"50\" width=\"57\" src=\"http://isratec.com.br/resources/quadradinhos-flutuantes.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;line-height:100%\" ></td></tr></tbody></table></td></tr></tbody></table></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#1ad4cc;line-height:1.5;text-align:center\">\n" +
"		<strong>É COM PRAZER QUE INFORMAMOS QUE O TOUR VIRTUAL DO SEU ESTABELECIMENTO ESTÁ PRONTO!</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div style=\"color:#555;font-size:16px;line-height:1.5;text-align:center\"></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">Este é o <a href="+link+">link</a> para acessá-lo, ficou extraordinário não é mesmo?! </div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">Nossos padrões de qualidade são os mais rígidos que existem no mercado, sua opinião é importante para nós, sinta-se a vontade em nos enviar um feedback para que possamos melhorar nosso serviço. </div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:0px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#ababab;line-height:1.5;text-align:center\">\n" +
"		<strong>Qualquer dúvida, ficamos disponíveis para lhe ajudar! Abraços.\n" +
"		</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		</tbody></table>\n" +
"		\n" +
"		</div></td></tr></tbody></table></div>\n" +
"		<!-- <div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;border-collapse:collapse\"><div class=\"m_-5901057937628262040mj-column-per-100 m_-5901057937628262040outlook-group-fix\" style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;border-collapse:collapse\" align=\"center\"><div style=\"color:#ababab;font-size:20px;line-height:1.5;text-align:center\">Abraços, -->\n" +
"					<!-- <br> <strong>Equipe Isratec</strong></div></td></tr></tbody></table></div></td></tr></tbody></table></div> -->\n" +
"				<!-- </td></tr></tbody></table></div></td></tr> -->\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px 0px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:600px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"auto\" src=\"http://isratec.com.br/resources/faixa-icone.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;line-height:100%\" width=\"600\"></td></tr></tbody></table></td></tr>\n" +
"			\n" +
"      <div style=\"margin:0px auto;max-width:600px;max-height:50px\">\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse;background-color:#082835\" align=\"center\" border=\"0\" ><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-top:0px;border-collapse:collapse\"><div>\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:10px 25px;border-collapse:collapse\" align=\"center\"><div><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse\">\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"	  \n" +
"	  <td style=\"font-size:0px;vertical-align:middle;width:20px;height:20px;border-collapse:collapse\">\n" +
"	  <a href=\"https://www.facebook.com/isratecoficial/\" target=\"_blank\" \n" +
"	  data-saferedirecturl=\"https://www.facebook.com/isratecoficial/\">\n" +
"	  \n" +
"	  <img alt=\"nutwitter\" src=\"http://isratec.com.br/resources/face-icone.png\" style=\"display:block;border-radius:3px;border:0;line-height:100%;outline:none;text-decoration:none\" height=\"20\" width=\"20\"></a></td>\n" +
"	  </tr></tbody></table>\n" +
"	  \n" +
"	  </td>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse;\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"			  \n" +
"		  <td class=\"font-small-text\" style=\"font-size: 10px;vertical-align:middle;width:auto;height:auto;border-collapse:collapse;color:#fff\">\n" +
"			@isratecoficial\n" +
"		  </td>\n" +
"		  </tr></tbody></table>\n" +
"	  </td>\n" +
"	  </tr></tbody></table>\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse\">\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr><td style=\"font-size:0px;vertical-align:middle;width:20px;height:20px;border-collapse:collapse\"><a href=\"https://www.instagram.com/isratec.oficial/\" target=\"_blank\" data-saferedirecturl=\"https://www.instagram.com/isratec.oficial/\"><img alt=\"nuinstagram\" src=\"http://isratec.com.br/resources/insta-icone.png\" style=\"display:block;border-radius:3px;border:0;line-height:100%;outline:none;text-decoration:none\" height=\"20\" width=\"20\" ></a></td></tr></tbody></table></td>\n" +
"	  \n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse;\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"			  \n" +
"		  <td class=\"font-small-text\" style=\"font-size:10px;vertical-align:middle;width:auto;height:auto;border-collapse:collapse;color:#fff\">\n" +
"			@isratec.oficial\n" +
"		  </td>\n" +
"		  </tr></tbody></table>\n" +
"	  \n" +
"	  </td>\n" +
"	  </tr></tbody></table>\n" +
"	  	  \n" +
"	  </div></td></tr></tbody></table></div></td></tr></tbody></table></div>\n" +
"      </tbody>\n" +
"	</table>\n" +
"	</div>\n" +
"	 </div>\n" +
"\n" +
"</div></div></div>";
        
           return html;
//           Mailer.send(null, "Apresentação Isratec.pdf", "Seja bem-vindo a revolução!", html, "jooohg@gmail.com");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            emf.close();
        }
    }
}
