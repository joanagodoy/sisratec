/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import dao.ClienteJpaController;
import dao.CobrancaFaturaJpaController;
import dao.CobrancaFaturaStatusJpaController;
import dao.CobrancaRegistroReferenciaJpaController;
import dao.ContratoJpaController;
import dao.PlanoJpaController;
import dominio.Cliente;
import dominio.CobrancaFatura;
import dominio.CobrancaFaturaStatus;
import dominio.CobrancaRegistroReferencia;
import dominio.Contrato;
import dominio.Plano;
import dominio.PlanoPK;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import static org.apache.commons.lang3.time.DateUtils.addDays;
import org.json.JSONObject;
import util.Command;
import util.GsonExclusion;

/**
 *
 * @author Usuario
 */
public class ControlePainelCobranca extends Command {
    
    String emailMaster = "joana.isratec@gmail.com";
    
    public String removerMascara(String str){
        return str.replaceAll("\\D", "");
    }
    
//    public void buscarAssinaturas() throws IOException {
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {
//            response.setContentType("application/json");
//            response.setCharacterEncoding("UTF-8");
//            
//            String status = read("status");
//            Integer adimplencia = Integer.parseInt(read("adimplencia"));
//            Integer periodicidade = Integer.parseInt(read("periodicidade"));
//            boolean statusboolean = false;
//            
//            System.out.println(status);
//            System.out.println(adimplencia);
//            System.out.println(periodicidade);
//            
//            
//            if(status.equals("Todos")){
//                if(adimplencia == 0){
//                    if(periodicidade == 0){
//                        //todos
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaEntities()));
//                    }else{
//                        //periodicidade
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByPeriodicidade(periodicidade)));
//                    }
//                }else{
//                    if(periodicidade == 0){
//                        //adimplencia
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByAdimplencia(adimplencia)));
//                    }else{
//                        //adimcplencia e periodicidade
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByPeriodicidadeAdimplencia(periodicidade, adimplencia)));
//                    }
//                }
//            }else{
//                statusboolean = status.equals("true");
//                if(adimplencia == 0){
//                    if(periodicidade == 0){
//                        //status
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByStatus(statusboolean)));
//                    }else{
//                        //byStatus e Periodicidade
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByStatusPeriodicade(statusboolean, periodicidade)));
//                    }
//                }else{
//                    if(periodicidade == 0){
//                        //by status e adimplencia
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByStatusAdimplencia(statusboolean, adimplencia)));
//                    }else{
//                        //by status adimplencia e periodicidade
//                        response.getWriter().print(GsonExclusion.GSON().toJson(new CobrancaAssinaturaJpaController(emf).findCobrancaAssinaturaByStatusPeriodicidadeAdimplencia(statusboolean, periodicidade, adimplencia)));
//                    }
//                    
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            emf.close();
//        }
//    }    
    
     public void buscarClientes() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            
            String status = read("status");
            Integer adimplencia = Integer.parseInt(read("adimplencia"));
            Integer periodicidade = Integer.parseInt(read("periodicidade"));
            boolean statusboolean = false;
            
            System.out.println(status);
            System.out.println(adimplencia);
            System.out.println(periodicidade);
            
//            List<Cliente> clientes = new ClienteJpaController(emf).findClienteEntities();
            
            if(status.equals("Todos")){
                if(adimplencia == 0){
                    if(periodicidade == 2){
                        //todos
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteEntities()));
//                        List result = new ArrayList<>();
//                        for (Cliente cliente : clientes) {
//                            HashMap<String, Object> map = new HashMap();
//                            map.put("id", cliente.getId());
//                            map.put("nomeFantasia", cliente.getNomeFantasia());
//                            map.put("razSocial", cliente.getRazSocial());
//                            map.put("razSocial", cliente.getRazSocial());
//                            map.put("periodicidade", cliente.getRazSocial());
//                            map.put("adimplencia", cliente.getAdimplencia());
//                            map.put("adimplencia", cliente.getAdimplencia());
//                            result.add(map);
//                        }
//                        response.getWriter().print(GsonExclusion.GSON().toJson(result));
                    }else{
                        //periodicidade
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findByPeriodicidade(periodicidade)));
                    }
                }else{
                    if(periodicidade == 2){
                        //adimplencia
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findByAdimplencia(adimplencia)));
                    }else{
                        //adimcplencia e periodicidade
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteByPeriodicidadeAdimplencia(periodicidade, adimplencia)));
                    }
                }
            }else{
                statusboolean = status.equals("true");
                if(adimplencia == 0){
                    if(periodicidade == 2){
                        //status
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteByStatus(statusboolean)));
                    }else{
                        //byStatus e Periodicidade
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteByStatusPeriodicade(statusboolean, periodicidade)));
                    }
                }else{
                    if(periodicidade == 2){
                        //by status e adimplencia
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findByStatusAdimplencia(statusboolean, adimplencia)));
                    }else{
                        //by status adimplencia e periodicidade
                        response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteByStatusPeriodicidadeAdimplencia(statusboolean, periodicidade, adimplencia)));
                    }
                    
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
     
    public void buscarFaturasCliente() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("cliente")));
            List<CobrancaFatura> faturas = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByCliente(cliente);
            response.getWriter().print(GsonExclusion.GSON().toJson(faturas));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscarMesesFaltantes() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("cliente")));
            response.getWriter().print(cliente.getMesesFaltantes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvarCobranca() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("idCliente")));
            cliente.setAdimplencia(Integer.parseInt(read("adimplencia")));
            cliente.setDtPrimeiraFatura(new SimpleDateFormat("dd/MM/yyyy").parse(read("dtPrimeiraFatura")));
            cliente.setDtProximoVencimento(new SimpleDateFormat("dd/MM/yyyy").parse(read("dtProximoVencimento")));
            new ClienteJpaController(emf).edit(cliente);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void criarCobranca() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("cliente")));
//            cliente.setPeriodicidade(Integer.parseInt(read("formcriarperiodicidade")));
//            new ClienteJpaController(emf).edit(cliente);
//            Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
//            contrato.setPagamento(Integer.parseInt(read("formcriarperiodicidade")));
//            new ContratoJpaController(emf).edit(contrato);
            
//            if(read("formcriarperiodicidade") == "1" || read("formcriarperiodicidade") == "0"){

                
            
            if(Double.parseDouble(read("valor").replace(".", "").replace(",", ".")) > 191.20){
                if(cliente.getCodigoVindi() == null){
                    response.sendError(HttpServletResponse.SC_EXPECTATION_FAILED);
                }else{
                    new ControleCobranca().criarCobrancaVindi(cliente, new SimpleDateFormat("dd/MM/yyy").parse(read("vencimento")), Double.parseDouble(read("valor").replace(".", "").replace(",", ".")), read("descricao"),  Integer.parseInt(read("qtdMeses")), Integer.parseInt(read("referencia")));
                }
            }else{
                new ControleCobranca().criarCobrancaRecorrenciaBF(cliente, new SimpleDateFormat("dd/MM/yyy").parse(read("vencimento")), Double.parseDouble(read("valor").replace(".", "").replace(",", ".")), read("descricao"), Integer.parseInt(read("referencia")),  Integer.parseInt(read("qtdMeses")), false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void salvarFatura() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFatura(Integer.parseInt(read("idFatura")));
            CobrancaRegistroReferencia referencia = new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(Integer.parseInt(read("referenciaFatura")));
            CobrancaFaturaStatus status = new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(Integer.parseInt(read("statusFatura")));
            
            fatura.setRegistro(referencia);
            fatura.setStatus(status);
            if(read("dtPagamentoFatura") != ""){
                fatura.setDtPagamento(new SimpleDateFormat("dd/MM/yyyy").parse(read("dtPagamentoFatura")));
            }
            fatura.setQtdMeses(Integer.parseInt(read("qtdMesesFatura")));
            new CobrancaFaturaJpaController(emf).edit(fatura);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
}