/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import dao.CidadeJpaController;
import dao.EnderecoJpaController;
import dao.ImovelJpaController;
import dao.VisitaJpaController;
import dominio.Endereco;
import dominio.Imovel;
import dominio.Usuario;
import dominio.Visita;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import util.Command;
import util.GsonExclusion;

/**
 *
 * @author Usuario
 */
public class ControleImovel extends Command {
    
//    public String removerMascara(String str){
//        return str.replaceAll("\\D", "");
//    }
    
    public void buscarImovel() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            if (read("id") != null) {
                Imovel imovel = new ImovelJpaController(emf).findImovel(Integer.parseInt(read("id")));    
                response.getWriter().print(GsonExclusion.GSON().toJson(imovel));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Usuario user = readObject("usuario", Escope.session);
            response.getWriter().print(GsonExclusion.GSON().toJson(new ImovelJpaController(emf).findImovelEntities()));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Usuario user = readObject("usuario", Escope.session);
            Imovel imovel = new ImovelJpaController(emf).findImovel(Integer.parseInt(read("id")));
            if(imovel.isAtivo()){
                if(!"on".equals(read("ativo"))){
                    Imovel newImovel = new Imovel();
                    newImovel.setCliente(imovel.getCliente());
                    newImovel.setNrContrato(imovel.getNrContrato());
                    List<Imovel> imoveis = new ImovelJpaController(emf).findByCliente(imovel.getCliente());
                    int count = imoveis.size() + 1;
                    newImovel.setDescricao("Imóvel "+ count);
                    newImovel.setAtivo(true);
                    new ImovelJpaController(emf).create(newImovel);
                    
                    imovel.setAtivo(false);
                }
            }
            //se imovel não tiver visita realizada ou cancelada, deixa alterar
            Visita visita = new VisitaJpaController(emf).findByImovel(imovel);

            if(visita == null || visita.getStatus().getId() == 1){
                System.out.println(visita.getStatus().getId());
                imovel.setDescricao(read("descricao"));
                Endereco endereco = new Endereco();
                endereco.setBairro(read("bairro"));
                endereco.setCep(read("cep"));
                endereco.setCidade(new CidadeJpaController(emf).findByNome(read("cidade")));
                endereco.setUf(read("uf"));
                endereco.setNumero(read("numero"));
                endereco.setRua(read("rua"));
                endereco.setComplemento(read("complemento"));

                if(imovel.getEndereco() == null){
                    new EnderecoJpaController(emf).create(endereco);
                    imovel.setEndereco(endereco);
                }else{
                    endereco.setId(imovel.getEndereco().getId());
                    new EnderecoJpaController(emf).edit(endereco);
                }
            }
                
            new ImovelJpaController(emf).edit(imovel);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
}
