/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import dao.EstadoJpaController;
import dao.RegiaoCidadeJpaController;
import dao.RegiaoJpaController;
import dao.TipoUsuarioJpaController;
import dao.UsuarioJpaController;
import dominio.Regiao;
import dominio.RegiaoCidade;
import dominio.RegiaoCidadePK;
import dominio.TipoUsuario;
import dominio.Usuario;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import util.Command;
import util.GsonExclusion;

/**
 *
 * @author Usuario
 */
public class ControleAdministracao extends Command {
    
    public void administracao() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            String tipo = read("u");
            switch (tipo) {                
                case "regiao":
                    write("estados", new EstadoJpaController(emf).findEstadoEntities(), Escope.request);
                    break;
                case "usuario":
                    write("tipos", new TipoUsuarioJpaController(emf).findTipoUsuarioEntities(), Escope.request);
                    write("regioes", new RegiaoJpaController(emf).findRegiaoAtivo(true), Escope.request);
                    break;  
                 case "ramo":
                    break;  
            }
            write("u", tipo, Escope.request);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forward("administracao.jsp");
            emf.close();
        }
    }
    
    public void buscarUsuario(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            if (read("id") != null) {
                response.getWriter().print(GsonExclusion.GSON().toJson(new UsuarioJpaController(emf).findUsuario(Integer.parseInt(read("id")))));
            }else{                
//                List<Usuario> usuarios = new UsuarioJpaController(emf).findUsuarioEntities();
                response.getWriter().print(GsonExclusion.GSON().toJson(new UsuarioJpaController(emf).findUsuarioEntities()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvarUsuario() throws IOException{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Usuario usuario = new Usuario();
            usuario.setEmail(read("email").toLowerCase());
            usuario.setNome(read("nome"));
            usuario.setSenha(util.Crypt.criptografar(read("senha")));
            usuario.setTipo(new TipoUsuarioJpaController(emf).findTipoUsuario(Integer.parseInt(read("tipo"))));
            usuario.setRegiao(new RegiaoJpaController(emf).findRegiao(Integer.parseInt(read("regiao"))));
            
            if (read("id").isEmpty()) {
                usuario.setAtivo(true);
                new UsuarioJpaController(emf).create(usuario);
            }else{   
                if(!"on".equals(read("ativo"))){
                    usuario.setAtivo(false);
                }else{
                    usuario.setAtivo(true);
                }
                    
                usuario.setId(Integer.parseInt(read("id")));
                new UsuarioJpaController(emf).edit(usuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void buscarRegiao(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            if (read("id") != null) {
                    response.getWriter().print(GsonExclusion.GSON().toJson(new RegiaoJpaController(emf).findRegiao(Integer.parseInt(read("id")))));
            }else{         
                if(read("ativo") != null){
                    response.getWriter().print(GsonExclusion.GSON().toJson(new RegiaoJpaController(emf).findRegiaoAtivo(true)));
                }else{
                    response.getWriter().print(GsonExclusion.GSON().toJson(new RegiaoJpaController(emf).findRegiaoEntities()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvarRegiao() throws IOException{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Regiao regiao = new Regiao();
            regiao.setDescricao(read("descricao"));
            
            if (read("id").isEmpty()) {
                regiao.setAtivo(true);
                new RegiaoJpaController(emf).create(regiao);
            }else{   
                System.out.println(read("ativo"));
                if(!"on".equals(read("ativo"))){
                    regiao.setAtivo(false);
                }else{
                    regiao.setAtivo(true);
                }
                regiao.setId(Integer.parseInt(read("id")));
                new RegiaoJpaController(emf).edit(regiao);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void buscarCidadesDisponiveis(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            System.out.println(read("uf"));
            List<Object[]> cidades = new RegiaoCidadeJpaController(emf).findCidadesDisponiveisByEstado(read("uf"));
            List result = new ArrayList<>();
            for (Object[] cidade : cidades) {
                HashMap<String, Object> map = new HashMap();
                map.put("id", cidade[0]);
                map.put("nome", cidade[1]);
                result.add(map);
            }
            System.out.println(result);
            response.getWriter().print(GsonExclusion.GSON().toJson(result));
                       
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscarCidadesAssociadas(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            System.out.println(read("regiao"));
            List<Object[]> cidades = new RegiaoCidadeJpaController(emf).findCidadesAssociadasByRegiao(read("regiao"));
            List result = new ArrayList<>();
            for (Object[] cidade : cidades) {
                HashMap<String, Object> map = new HashMap();
                map.put("id", cidade[0]);
                map.put("nome", cidade[1]);
                result.add(map);
            }
            System.out.println(result);
            response.getWriter().print(GsonExclusion.GSON().toJson(result));
                       
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvarAssociacaoRegiaoCidade() throws IOException{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        EntityManager em = emf.createEntityManager();
        try {
            //delete todas as associações atuais da regiao
            Integer idRegiao = Integer.parseInt(read("regiao"));
            String sqlDeleta = "DELETE FROM regiao_cidade WHERE regiao = ?";
            em.getTransaction().begin();
            em.createNativeQuery(sqlDeleta).setParameter(1, idRegiao).executeUpdate();
            em.getTransaction().commit();
            //insere novas assiciacoes
            String jsonAssociacao = read("associacao");
            Type typeHash = new TypeToken<List<HashMap<String, Object>>>() {
            }.getType();
            List<HashMap<String, Object>> associacoes = new GsonBuilder().create().fromJson(jsonAssociacao, typeHash);
            for (HashMap h : associacoes) {
                RegiaoCidade rc = new RegiaoCidade();
                Double d = new Double(Double.parseDouble(associacoes.get(associacoes.indexOf(h)).get("id").toString()));
                int value = d.intValue();
                rc.setRegiaoCidadePK(new RegiaoCidadePK(idRegiao, value));
                new RegiaoCidadeJpaController(emf).create(rc);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            em.close();
            emf.close();
        }
    }
}

