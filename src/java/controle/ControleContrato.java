/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import dao.ClienteJpaController;
import dao.ContratoFotoJpaController;
import dao.ContratoJpaController;
import dao.ImovelJpaController;
import dominio.Cliente;
import dominio.CobrancaRegistro;
import dominio.Contrato;
import dominio.ContratoFoto;
import dominio.Imovel;
import dominio.StatusContrato;
import dominio.Usuario;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;
import util.Command;
import util.GsonExclusion;
import util.Mailer;





/**
 *
 * @author Usuario
 */
public class ControleContrato extends Command {
    
    public String removerMascara(String str){
        return str.replaceAll("\\D", "");
    }
    
    public void contrato(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {            
            if(read("u") != null){
                write("open", true, Escope.request);
            }else{
                write("open", false, Escope.request);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forward("contrato.jsp");
            emf.close();
        }
    }
    
    public void buscarContrato() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            if (read("id") != null) {
                response.getWriter().print(GsonExclusion.GSON().toJson(new ContratoJpaController(emf).findContrato(Integer.parseInt(read("id")))));
            }else if(read("idCliente") != null){
                Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("idCliente")));
                response.getWriter().print(GsonExclusion.GSON().toJson(new ContratoJpaController(emf).findByCliente(cliente)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Usuario user = readObject("usuario", Escope.session);
            if(read("visita") != null && read("visita") != ""){//se buscar cotratos sem visita 
                List<Contrato> contratos = null;
                List<Imovel> imoveis = null;
                if(user.getTipo().getId().equals(4)){//se usuario Vendedor
                    contratos = new ContratoJpaController(emf).findByVendedorSemVisita(user);
                    imoveis = new ImovelJpaController(emf).findByVendedorSemVisita(user);
                }else{
                    contratos = new ContratoJpaController(emf).findSemVisita();
                    imoveis = new ImovelJpaController(emf).findSemVisita();
                }
                
                ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
                for (Contrato contrato : contratos) {
                    HashMap<String, Object> hash = new HashMap<String, Object>();
                    hash.put("contrato", contrato);
                    hash.put("cliente", contrato.getCliente());                        
                    hash.put("imovel", null);                        
                    result.add(hash);
                }
                for (Imovel imovel : imoveis) {
                    HashMap<String, Object> hash = new HashMap<String, Object>();
                    hash.put("contrato", imovel.getNrContrato());
                    hash.put("cliente", imovel.getCliente());                        
                    hash.put("imovel", imovel.getId());                        
                    hash.put("descricao", imovel.getDescricao());                        
                    result.add(hash);
                }
                response.getWriter().print(GsonExclusion.GSON().toJson(result));
            }else{
                if(user.getTipo().getId().equals(4)){//se usuario Vendedor
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ContratoJpaController(emf).findByVendedor(user)));
                }else{
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ContratoJpaController(emf).findContratoEntities()));
                }     
            }       
            
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvar() throws IOException, Exception {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean erro = false;
        String nomeCliente = "";
        String emailCliente = "";
        try {            
            Usuario user = readObject("usuario", Escope.session);
            if(read("id")  == "" || read("id") == null){
                if(new ContratoJpaController(emf).findContrato(Integer.parseInt(read("nr"))) == null){
                    Contrato contrato = new Contrato();
                    contrato.setNrContrato(Integer.parseInt(read("nr")));
                    contrato.setDtVenda(new Date());
                    contrato.setObs(read("obs").toUpperCase());
                    contrato.setPagamento(Integer.parseInt(read("pagamento")));
                    contrato.setStatus(new StatusContrato(1));
                    contrato.setUsuario(user);
//                    contrato.setVendedor(new ClienteJpaController(emf).findCliente(Integer.parseInt(read("cliente"))).getVendedor());
                    contrato.setDtCriacao(new Date());
                    contrato.setAnaliseAnual("on".equals(read("analiseanual")));
                    contrato.setAnaliseMensal("on".equals(read("analisemensal")));
                    contrato.setConsultoria("on".equals(read("consultoria")));
                    contrato.setSite("on".equals(read("site")));
                    contrato.setFichaGoogle("on".equals(read("ficha")));
                    contrato.setCliente(new ClienteJpaController(emf).findCliente(Integer.parseInt(read("cliente"))));
                    contrato.setVisitaAberto("on".equals(read("visitaaberto")));
                    contrato.setTipoCliente(read("tipoContrato"));
                    contrato.setVendaEstabelecimentoImobiliaria("on".equals(read("vendaEstabelecimentoImobiliaria")));
                    contrato.setDesconto(Double.parseDouble(read("desconto")));
                    contrato.setEdicao(Integer.parseInt(read("edicao")));
                    if(read("tipoContrato").equals("Imobiliaria")){
                        int qtdImoveis = Integer.parseInt(read("qtdImovel"));
                        contrato.setQtdImovel(qtdImoveis);
                        
                        for (int i = 1; i <= qtdImoveis; i++) {
                            Imovel imovel = new Imovel();
                            imovel.setCliente(contrato.getCliente());
                            imovel.setNrContrato(contrato);
                            imovel.setDescricao("Imóvel "+ i);
                            imovel.setAtivo(true);
                            new ImovelJpaController(emf).create(imovel);
                        }
                    }else{
                        contrato.setQtdImovel(0);
                    }
                    new ContratoJpaController(emf).create(contrato);
                    
                    nomeCliente = contrato.getCliente().getResponsavel().split(" ")[0];
                    emailCliente = contrato.getCliente().getEmail();
                                                        
                    //cria registro assinatura do cliente
//                    CobrancaAssinatura assinatura = new CobrancaAssinatura();
//                    assinatura.setCliente(contrato.getCliente());
//                    assinatura.setDtInicio(new Date());
//                    assinatura.setPeriodicidade(Integer.parseInt(read("pagamento")));
//                    assinatura.setFormaPagamento(0); //ainda não definida
//                    if(Integer.parseInt(read("pagamento")) == 12){
//                        assinatura.setFormaPagamento(12);//assinatura vindi                        
//                    }else{
//                        assinatura.setFormaPagamento(1);//boleto
//                    }
//                    assinatura.setDesconto(Double.parseDouble(read("desconto")));
//                    assinatura.setAdimplencia(1);
//
//                    new CobrancaAssinaturaJpaController(emf).create(assinatura);
                                      
                    atualizaPeriodicidadeCliente(contrato.getCliente(), read("pagamento"));
                }else{
                    erro = true;//para não enviar e-mail de boas vindas
                    response.sendError(HttpServletResponse.SC_FOUND);
                }
                
            }else{
                erro = true;//para não enviar e-mail de boas vindas
                Contrato contrato = new ContratoJpaController(emf).findContrato(Integer.parseInt(read("nr")));
//                contrato.setNrContrato(Integer.parseInt(read("nr")));
                contrato.setObs(read("obs").toUpperCase());
                contrato.setPagamento(Integer.parseInt(read("pagamento")));
                contrato.setEdicao(Integer.parseInt(read("edicao")));
                contrato.setUsuario(user);
                contrato.setDesconto(Double.parseDouble(read("desconto")));
//                contrato.setVendedor(user);
                contrato.setVisitaAberto("on".equals(read("visitaaberto")));
                contrato.setDtAlteracao(new Date());
                contrato.setAnaliseAnual("on".equals(read("analiseanual")));
                contrato.setAnaliseMensal("on".equals(read("analisemensal")));
                contrato.setConsultoria("on".equals(read("consultoria")));
                contrato.setSite("on".equals(read("site")));
                contrato.setFichaGoogle("on".equals(read("ficha")));
                if(user.getTipo().getId().equals(4)){//se usuario vendedor
                    int compare = new Date().compareTo(contrato.getDtCriacao());
                    if(compare > 15){ // se data de alteração maior que 15 min da criação
                        response.sendError(HttpServletResponse.SC_CONFLICT);
                    }else{
                        new ContratoJpaController(emf).edit(contrato);
                    }
                }else{
                    if(read("dataEntrega") != null && !"".equals(read("dataEntrega"))){
                        contrato.setDtEntrega(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataEntrega")));
                    }
                    new ContratoJpaController(emf).edit(contrato);
                }
                atualizaPeriodicidadeCliente(contrato.getCliente(), read("pagamento"));
            }
            
        } catch (Exception e) {
            erro = true;//para não enviar e-mail de boas vindas
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
            if(!erro){
//                enviaEmailBoasVindas(nomeCliente, emailCliente.toLowerCase());
            }
        }
    }
        
    public void atualizaPeriodicidadeCliente(Cliente cliente, String pagamento) throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {  
            cliente.setPeriodicidade(Integer.parseInt(pagamento));
            new ClienteJpaController(emf).edit(cliente);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
        
        
//    public void criaCobrancaAssinatura(Cliente cliente) throws IOException {
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {            
//            //cria registro assinatura do cliente
//            CobrancaAssinatura assinatura = new CobrancaAssinatura();
//            assinatura.setCliente(cliente);
//            assinatura.setDtInicio(new Date());
//            assinatura.setPeriodicidade(Integer.parseInt(read("pagamento")));
//            assinatura.setFormaPagamento(0); //ainda não definida
////                    if(Integer.parseInt(read("pagamento")) == 12){
////                        assinatura.setFormaPagamento(2);//assinatura vindi                        
////                    }else{
////                        assinatura.setFormaPagamento(1);//boleto
////                    }
//            assinatura.setDesconto(Double.parseDouble(read("desconto")));
//            assinatura.setAdimplencia(1);
//
//            new CobrancaAssinaturaJpaController(emf).create(assinatura);
//            
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
//        } finally {
//            emf.close();
//        }
//    }
    
    
    public void addImovel() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {            
            Usuario user = readObject("usuario", Escope.session);
            Contrato contrato = new ContratoJpaController(emf).findContrato(Integer.parseInt(read("contratoAddImovel")));
            int qtdImoveis = Integer.parseInt(read("qtdImovelAdd"));
            for (int i = contrato.getQtdImovel() + 1; i <= contrato.getQtdImovel() + qtdImoveis; i++) {
                Imovel imovel = new Imovel();
                imovel.setCliente(contrato.getCliente());
                imovel.setNrContrato(contrato);
                imovel.setDescricao("Imóvel "+ i);
                new ImovelJpaController(emf).create(imovel);
            }
            contrato.setQtdImovel(contrato.getQtdImovel() + qtdImoveis);
            new ContratoJpaController(emf).edit(contrato);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void enviarFoto() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            System.out.println(read("idContrato"));
            Collection<Part> parts = request.getParts();
            for (Part p : parts) {
                if (p.getContentType() != null) {
                    byte[] file = IOUtils.toByteArray(p.getInputStream());
                    
                    new ContratoFotoJpaController(emf).create(new ContratoFoto(new ContratoJpaController(emf).findContrato(Integer.parseInt(read("idContrato"))), file));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.getWriter().print("<script>window.parent.loading('close');window.parent.buscarFotos();</script>");
            emf.close();
        }
    }
    
    public void buscarFoto() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            if (read("contrato") != null) {
                System.out.println(read("contrato"));
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(GsonExclusion.GSON().toJson(new ContratoFotoJpaController(emf).findByContrato(Integer.parseInt(read("contrato")))));
            } else if (read("id") != null) {
                response.setContentType("image/jpeg");
                ContratoFoto f = null;
                try {
                    f = new ContratoFotoJpaController(emf).findContratoFoto(Integer.parseInt(read("id")));
                } catch (Exception e) {

                }
                if (f != null) {
                    InputStream is = new ByteArrayInputStream(f.getFoto());
                    IOUtils.copy(is, response.getOutputStream());
                }
                response.getOutputStream().close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void excluirFoto() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            new ContratoFotoJpaController(emf).destroy(Integer.parseInt(read("foto")));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void enviaEmailBoasVindas(String nomeCliente, String emailCliente) throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            nomeCliente = nomeCliente.substring(0, 1).toUpperCase() + nomeCliente.substring(1).toLowerCase();
            String path = request.getServletContext().getRealPath("/");     
            String anexo = path + File.separatorChar + "files" + File.separatorChar + "Apresentacao Isratec.pdf";
//            String nomeCliente = "Leonardo Godoy".split(" ")[0];
//            String emailCliente = "jooohg@gmail.com";
            String html = "<div style=\"background:#f7f7f7;margin:0;padding:0\">\n" +
"  <div style=\"background-color:#f7f7f7\">\n" +
"\n" +
"   <div style=\"border:1px solid #d9d9d9;margin:0px auto;max-width:600px;background:white\">\n" +
"	<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;background:white;border-collapse:collapse\" align=\"center\" border=\"0\">\n" +
"		<tbody>\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-bottom:0px;border-collapse:collapse\">\n" +
"		\n" +
"		\n" +
"		<div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px;padding-top:0px;border-collapse:collapse\">\n" +
"		<div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px 0px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:600px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"auto\" src=\"http://isratec.com.br/resources/cabecalho.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;line-height:100%\" width=\"600\"></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div>\n" +
"		\n" +
"		<!-- texto -->\n" +
"		<div style=\"margin:0px auto;max-width:600px\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\">\n" +
"		<tbody>\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 40px 0px;border-collapse:collapse\"><div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;border-collapse:collapse\"><div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\">\n" +
"		<div style=\"color:#555;font-size:36px;font-weight:book;line-height:1.0;text-align:center;\">\n" +
"		\n" +
"\n" +
"		<strong>\n" +
"		<a style='color:#1ad4cc';>\n" +
"		Olá "+nomeCliente+",<br> </a>\n" +
"		<a style='color:#1ad4cc';>\n" +
"		tudo bem?\n" +
"		</a>\n" +
"		</strong>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse;\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">\n" +
"		<strong>Nós da Isratec acreditamos que a tecnologia é a chave<br>\n" +
"		para a criação de soluções revolucionárias.\n" +
"		</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#ababab;line-height:1.5;text-align:center\">\n" +
"		<strong>Por isso, gostamos de pensar fora da caixa e trazer ideias<br> \n" +
"		visionárias que ajudam as empresas a estar um passo à frente. \n" +
"		</strong>\n" +
"		<br></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-top:0px;border-collapse:collapse\"><div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:10px 25px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:50px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"50\" src=\"http://isratec.com.br/resources/quadradinhos-flutuantes.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;line-height:100%\" width=\"57\"></td></tr></tbody></table></td></tr></tbody></table></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#1ad4cc;line-height:1.5;text-align:center\">\n" +
"		<strong>AGORA É POR NOSSA CONTA!</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div style=\"color:#555;font-size:16px;line-height:1.5;text-align:center\"></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">Nosso fotógrafo entrará em contato para coletar o material visual<br> do ambiente que você deseja.</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">Em seguida, editamos e lhe enviamos o link com o seu tour virtual.</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:0px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#ababab;line-height:1.5;text-align:center\">\n" +
"		<strong>Qualquer dúvida, ficamos disponíveis para lhe ajudar! Abraços.\n" +
"		</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		</tbody></table>\n" +
"		\n" +
"		</div></td></tr></tbody></table></div>\n" +
"		<!-- <div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;border-collapse:collapse\"><div class=\"m_-5901057937628262040mj-column-per-100 m_-5901057937628262040outlook-group-fix\" style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;border-collapse:collapse\" align=\"center\"><div style=\"color:#ababab;font-size:20px;line-height:1.5;text-align:center\">Abraços, -->\n" +
"					<!-- <br> <strong>Equipe Isratec</strong></div></td></tr></tbody></table></div></td></tr></tbody></table></div> -->\n" +
"				<!-- </td></tr></tbody></table></div></td></tr> -->\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px 0px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:600px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"auto\" src=\"http://isratec.com.br/resources/faixa-icone.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;line-height:100%\" width=\"600\"></td></tr></tbody></table></td></tr>\n" +
"			\n" +
"      <div style=\"margin:0px auto;max-width:600px;max-height:50px\">\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse;background-color:#082835\" align=\"center\" border=\"0\" ><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-top:0px;border-collapse:collapse\"><div>\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:10px 25px;border-collapse:collapse\" align=\"center\"><div><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse\">\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"	  \n" +
"	  <td style=\"font-size:0px;vertical-align:middle;width:20px;height:20px;border-collapse:collapse\">\n" +
"	  <a href=\"https://www.facebook.com/isratecoficial/\" target=\"_blank\" \n" +
"	  data-saferedirecturl=\"https://www.facebook.com/isratecoficial/\">\n" +
"	  \n" +
"	  <img alt=\"nutwitter\" src=\"http://isratec.com.br/resources/face-icone.png\" style=\"display:block;border-radius:3px;border:0;line-height:100%;outline:none;text-decoration:none\" height=\"20\" width=\"20\"></a></td>\n" +
"	  </tr></tbody></table>\n" +
"	  \n" +
"	  </td>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse;\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"			  \n" +
"		  <td class=\"font-small-text\" style=\"font-size: 10px;vertical-align:middle;width:auto;height:auto;border-collapse:collapse;color:#fff\">\n" +
"			@isratecoficial\n" +
"		  </td>\n" +
"		  </tr></tbody></table>\n" +
"	  </td>\n" +
"	  </tr></tbody></table>\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse\">\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr><td style=\"font-size:0px;vertical-align:middle;width:20px;height:20px;border-collapse:collapse\"><a href=\"https://www.instagram.com/isratec.oficial/\" target=\"_blank\" data-saferedirecturl=\"https://www.instagram.com/isratec.oficial/\"><img alt=\"nuinstagram\" src=\"http://isratec.com.br/resources/insta-icone.png\" style=\"display:block;border-radius:3px;border:0;line-height:100%;outline:none;text-decoration:none\" height=\"20\" width=\"20\" ></a></td></tr></tbody></table></td>\n" +
"	  \n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse;\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"			  \n" +
"		  <td class=\"font-small-text\" style=\"font-size: 10px;vertical-align:middle;width:auto;height:auto;border-collapse:collapse;color:#fff\">\n" +
"			@isratec.oficial\n" +
"		  </td>\n" +
"		  </tr></tbody></table>\n" +
"	  \n" +
"	  </td>\n" +
"	  </tr></tbody></table>\n" +
"	  	  \n" +
"	  </div></td></tr></tbody></table></div></td></tr></tbody></table></div>\n" +
"      </tbody>\n" +
"	</table>\n" +
"	</div>\n" +
"	 </div>\n" +
"\n" +
"</div></div></div>";
            
        Mailer.send(anexo, "Apresentacao Isratec.pdf", null, null, "Seja bem-vindo a revolução!", html, emailCliente);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
}
