/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Command;

/**
 *
 * @author mbruzaca
 */
@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"}, initParams = {
    @WebInitParam(name = "propertiesFile", value = "Commands.properties"),
    @WebInitParam(name = "persistence", value = "GSVPU")
})
@MultipartConfig
public class Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
//    @Override
//    public void init(ServletConfig config) throws ServletException {
//        super.init(config); //To change body of generated methods, choose Tools | Templates.
//        //config.get
//    }

    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
//        System.out.print("Chamada ao Servlet - ");
        System.out.print("MODULO: "+request.getAttribute("m"));
        System.out.println(" ACAO: "+request.getAttribute("a"));
        getCommand((String)request.getAttribute("m")).init(request, response).execute((String)request.getAttribute("a"));

    }

    private Command getCommand(String module) throws ServletException {
        try {
            Properties prop = new Properties();
            prop.load(this.getClass().getResource(this.getServletConfig().getInitParameter("propertiesFile")).openStream());
            String classString = prop.getProperty(module);
            Class classClass = Class.forName(classString);
            Object classObject = classClass.newInstance();
            if (classObject instanceof Command) {
                return (Command) classObject;
            } else {
                throw new ServletException("Command não encontrado");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServletException("Modulo não encontrado: " + ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
