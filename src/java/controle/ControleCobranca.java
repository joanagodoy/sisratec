                                                                                                                                                    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import dao.ClienteJpaController;
import dao.CobrancaBfImportarJpaController;
import dao.CobrancaFaturaJpaController;
import dao.CobrancaFaturaStatusJpaController;
import dao.CobrancaRegistroReferenciaJpaController;
import dao.ContratoJpaController;
import dao.PlanoJpaController;
import dao.PlanoVindiJpaController;
import dominio.Cliente;
import dominio.CobrancaBfImportar;
import dominio.CobrancaFatura;
import dominio.CobrancaFaturaStatus;
import dominio.Contrato;
import dominio.PlanoVindi;
import dominio.PlanoVindiPK;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import static org.apache.commons.lang3.time.DateUtils.addDays;
import org.json.JSONObject;
import util.Command;
import util.GsonExclusion;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import util.Mailer;
import dominio.Plano;
import dominio.PlanoPK;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONArray;


/**
 *
 * @author Usuario
 */
public class ControleCobranca extends Command{
    //producao
    private final String chave_vindi = "e4EvoHz-KR9o8RaIszOvucI684RwZZNIc3PUd2NpKOU";
    private final String url_vindi = "https://app.vindi.com.br:443/api/v1/";
    //teste
//    private final String chave_vindi = "7mXgDvqolrPLUdHRU0UkW3RNHHYucSwy7O-jejGGTyc";
//    private final String url_vindi = "https://sandbox-app.vindi.com.br/api/v1/";
    
    //producao
    String token_bf = "8F5649EFCA91C16953E45F326B14D6D2755EFDC837F379DFBDB6BDFF3B05A91E";
    String url_bf = "https://www.boletobancario.com/boletofacil/integration/api/v1";
    
//    String token_bf_fisica = "8F5649EFCA91C16953E45F326B14D6D2755EFDC837F379DFBDB6BDFF3B05A91E";
//    String url_bf = "https://www.boletobancario.com/boletofacil/integration/api/v1";
    
    //teste
//    String token_bf = "A40982DE6FC39081B34BC9845126A14100781991FD004AFFC7679FA892A5C1C7";
//    String url_bf = "https://sandbox.boletobancario.com/boletofacil/integration/api/v1";
    
//    String ambiente = "teste";
    String ambiente = "";
    
    final ReentrantLock lock = new ReentrantLock();

    public void criarParametrosCobrancaRecorrenciaBF(Cliente cliente, boolean isRecorrencia) throws Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("criarParametrosCobrancaRecorrenciaBF");
        try{
            Date vencimento = addDays(new Date(), 7);
            Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
            PlanoPK planoPK = new PlanoPK(contrato.getEdicao(), cliente.getUltPtsVistaMultp());
            Plano plano = new PlanoJpaController(emf).findPlano(planoPK);
            
            Double valor = 0.0;
            String descricao = "";
            int qtdMeses = 0;
            int referencia = 0;
            if(cliente.getPeriodicidade() == 1){
                qtdMeses = 12;
                referencia = 2;
                descricao = "Anuidade Tour Virtual Google Street View";
                if(contrato.getEdicao() == 1){//20% de desconto
                    valor = plano.getValorAnual() - (plano.getValorAnual() * 0.2);
                }else{//10% de desconto
                    valor = plano.getValorAnual() - (plano.getValorAnual() * 0.1);
                }
            }else if(cliente.getPeriodicidade() == 6){
                referencia = 10;
                qtdMeses = 6;
                descricao = "Semestralidade Tour Virtual Isratec";
                valor = plano.getValorAnual() / 2;
            }else if(cliente.getPeriodicidade() == 4){
                referencia = 4;
                qtdMeses = 4;
                descricao = "Quadrimestralidade Tour Virtual Isratec";
                valor = plano.getValorAnual() / 3;
            }else if(cliente.getPeriodicidade() == 3){
                referencia = 3;
                qtdMeses = 3;
                descricao = "Trimestralidade Tour Virtual Isratec";
                valor = plano.getValorAnual() / 4;
            }
            
            //            double valorAdicional = 0.0;
//            if(contrato.isAnaliseAnual()){
//                valorAdicional = 43.20;                
//            }else if(contrato.isAnaliseAnual()){
//                valorAdicional = valorAdicional + 86.40;
//            }
//            if(contrato.isConsultoria()){
//                valorAdicional = valorAdicional + 133.20;
//            }
//            if(contrato.isFichaGoogle()){
//                valorAdicional = valorAdicional + 37.97;
//            }
            criarCobrancaRecorrenciaBF(cliente, vencimento, valor, descricao, referencia, qtdMeses, isRecorrencia);
            
        }catch(Exception e){
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro criarParametrosCobrancaRecorrenciaBF", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia(), "jooohg@gmail.com");
            }
        }
    }
    
    
    public void criarCobrancaRecorrenciaBF(Cliente cliente, Date vencimento, Double valor, String descricao, int referencia, int qtdMeses, boolean isRecorrencia) throws Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("criarCobrancaRecorrenciaBF");
        Map<String,Object> params = new LinkedHashMap<>();
        try{
            URL url = new URL(url_bf+"/issue-charge");
            
            params.put("token", token_bf);
            
            params.put("payerName", cliente.getNomeFantasia());
            params.put("payerCpfCnpj", cliente.getCnpj());
            params.put("payerEmail", cliente.getEmail());
            params.put("payerSecondaryEmail", cliente.getEmail2());
            params.put("payerPhone", cliente.getTelefone1());
            params.put("dueDate", new SimpleDateFormat("dd/MM/yyy").format(vencimento));
            params.put("description", descricao);
            params.put("amount", valor);
            
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }   
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);
            
//            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            int status = conn.getResponseCode();
            System.out.println(status);
            switch (status) {
                case 200:
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    System.out.println(sb.toString());
//                    https://stackoverflow.com/questions/26358684/converting-bufferedreader-to-jsonobject-or-map
//                    http://mvnrepository.com/artifact/org.json/json/20131018
                    JSONObject json = new JSONObject(sb.toString());
                    System.out.println(json.getJSONObject("data").getJSONArray("charges").getJSONObject(0).getLong("code"));
                    String codigo = String.valueOf(json.getJSONObject("data").getJSONArray("charges").getJSONObject(0).getLong("code"));
                    
                    CobrancaFatura fatura = new CobrancaFatura();
                    fatura.setCliente(cliente);
                    fatura.setCodeBf(Integer.parseInt(codigo));
                    fatura.setDescricaoFatura(descricao);
                    fatura.setDtEmissao(new Date());
                    fatura.setDtVencimento(vencimento);
                    fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
                    fatura.setValor(valor);
                    fatura.setReferenceBf("juridica");
                    
//                   if(periodicidade == 6){//semestral
                        fatura.setQtdMeses(qtdMeses);
                        fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(referencia));    
//                    }else if(periodicidade == 3){//trimestral
//                        fatura.setQtdMeses(periodicidade);
//                        fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(3));    
//                    }else if(periodicidade == 4){//quadrimestral
//                        fatura.setQtdMeses(periodicidade);
//                        fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(4));    
//                    }
                    
                    new CobrancaFaturaJpaController(emf).create(fatura);
                    
//                    cliente.setDtUltimaFatura(new Date());
//                    cliente.setPeriodicidade(periodicidade);
                    
//                    System.out.println(isRecorrencia);
                    if(isRecorrencia){
                        Calendar calendar = Calendar.getInstance(); 
                        calendar.add(Calendar.MONTH, cliente.getPeriodicidade());
                        cliente.setDtProximoVencimento(addDays(calendar.getTime(),7));
                    }
                    new ClienteJpaController(emf).edit(cliente);
                case 201:
                    
//                    return sb.toString();
                    
                    //gravar no banco o código enviado
                    
                case 400:
                    //enviar email erro 400
                default:
                    //enviar email
            }
//            for (int c; (c = in.read()) >= 0;)
//                System.out.print((char)c);

//            in.close();
            conn.disconnect();
        }catch(Exception e){
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro criarCobrancaRecorrenciaBF", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + GsonExclusion.GSON().toJson(params), "jooohg@gmail.com");
            }
        }
    }
    
    public void atualizarCobrancasFromBFDiario() throws Exception{
        System.out.println("atualizarCobrancasFromBFDiario");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        StringWriter errors = new StringWriter();
        try{
            Map<String,Object> params = new LinkedHashMap<>();
            params.put("token", token_bf);
//            params.put("beginPaymentDate", new SimpleDateFormat("dd/MM/yyy").format(new Date()));
            params.put("beginPaymentDate", new SimpleDateFormat("dd/MM/yyy").format(addDays(new Date(), -4)));
//            params.put("beginDueDate", new SimpleDateFormat("dd/MM/yyy").format(new Date()));
            params.put("endPaymentDate", new SimpleDateFormat("dd/MM/yyy").format(new Date()));

            StringBuilder stringBuilder = new StringBuilder(url_bf+"/list-charges");
            stringBuilder.append("?");
           
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
//            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            stringBuilder.append(postData);
            
            URL url = new URL(stringBuilder.toString());
           
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");            
            connection.setRequestProperty("Accept-Charset", "UTF-8");
         
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8")); 
            int status = connection.getResponseCode();
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            
            JSONObject json = new JSONObject(sb.toString());
                    
            switch (status) {
                case 200:
                    JSONArray cobrancas = json.getJSONObject("data").getJSONArray("charges");

                    for(int i=0; i<cobrancas.length(); i++){
                        System.out.println(cobrancas.getJSONObject(i).getLong("code"));
//                        try{
                            if(cobrancas.getJSONObject(i).getJSONArray("payments").getJSONObject(0) != null){
                               Integer codeBF = cobrancas.getJSONObject(i).getInt("code");
                               CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByCodeBF(codeBF);
                               if (fatura != null){
                                   fatura.setValorPago(cobrancas.getJSONObject(i).getJSONArray("payments").getJSONObject(0).getDouble("amount"));
                                   fatura.setDtPagamento(new SimpleDateFormat("dd/mm/yyyy").parse(cobrancas.getJSONObject(i).getJSONArray("payments").getJSONObject(0).getString("date")));
                                   
                                   if(!Objects.equals(fatura.getValor(), fatura.getValorPago())){
                                        System.out.println(fatura.getValor());
                                        System.out.println(fatura.getValorPago());
                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(7));
                                    }else{
                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
                                    }
                                   
                                    new CobrancaFaturaJpaController(emf).edit(fatura);
                               }else{
                                   Mailer.send(null,null,null,null, "Fatura BF Não encontrada no Sisratec", String.valueOf(cobrancas.getJSONObject(i).getLong("code")), "jooohg@gmail.com");
                               }
                            }
//                            else{
//                                if(fatura.getDtVencimento().compareTo(new Date()) < 0){//se estiver vencido
//                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
//                                        
//                                        //atualizaAdimplencia
//                                        if(fatura.getCliente().getAdimplencia() != 3){//se cliente não está no advogado
//                                            fatura.getCliente().setAdimplencia(2);
//                                            new ClienteJpaController(emf).edit(cliente);
//                                            //se fatura está pendente e data de vencimento é menor que hoje
//                                        }
//                                    }
//                            }                            
//                        }catch(Exception e){
//                            
//                        }finally{
//                            
//                        }
                                
                    }  
                    break;
                case 201:
//                    System.out.println(json.getJSONObject("data").getJSONArray("charges").getJSONObject(0).getJSONArray("payments"));
//                    System.out.println(json.getJSONObject("data").getJSONArray("charges").getJSONObject(0).getJSONArray("payments").getJSONObject(0));
        //                    String codigo = json.getJSONObject("data").getJSONArray("payments").getJSONObject(0).getLong("code");
                   
//                    return sb.toString();
                    
                    //gravar no banco o código enviado
                    break;
                case 400:
                    //enviar email erro 400
                    System.out.println(json.getJSONObject("errorMessage"));
                    break;
                default:
                    //enviar email
                break;
            }
            
            
//            for (int c; (c = in.read()) >= 0;)
//                System.out.print((char)c);

//            in.close();
        
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro atualizarCobrancasFromBFDiario", errors.toString(), "jooohg@gmail.com");
            }
        }
    }
//    public void atualizarCobrancasVencidasFromBF() throws Exception{
//        System.out.println("atualizarCobrancasVencidasFromBF");
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        boolean houveErro = false;
//        StringWriter errors = new StringWriter();
//        try{
//            Map<String,Object> params = new LinkedHashMap<>();
//            params.put("token", token_bf);
////            params.put("beginPaymentDate", new SimpleDateFormat("dd/MM/yyy").format(new Date()));
////            params.put("beginPaymentDate", new SimpleDateFormat("dd/MM/yyy").format(addDays(new Date(), -4)));
//            params.put("beginDueDate", new SimpleDateFormat("dd/MM/yyy").format(addDays(new Date(), -1)));
//            params.put("endDueDate", new SimpleDateFormat("dd/MM/yyy").format(addDays(new Date(), -1)));
////            params.put("endPaymentDate", new SimpleDateFormat("dd/MM/yyy").format(new Date()));
//
//            StringBuilder stringBuilder = new StringBuilder(url_bf+"/list-charges");
//            stringBuilder.append("?");
//           
//            StringBuilder postData = new StringBuilder();
//            for (Map.Entry<String,Object> param : params.entrySet()) {
//                if (postData.length() != 0) postData.append('&');
//                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
//                postData.append('=');
//                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
//            }
////            byte[] postDataBytes = postData.toString().getBytes("UTF-8");
//
//            stringBuilder.append(postData);
//            
//            URL url = new URL(stringBuilder.toString());
//           
//            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//            connection.setRequestMethod("GET");            
//            connection.setRequestProperty("Accept-Charset", "UTF-8");
//         
//            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8")); 
//            int status = connection.getResponseCode();
//            StringBuilder sb = new StringBuilder();
//            String line;
//            while ((line = br.readLine()) != null) {
//                sb.append(line+"\n");
//            }
//            
//            JSONObject json = new JSONObject(sb.toString());
//                    
//            switch (status) {
//                case 200:
//                    JSONArray cobrancas = json.getJSONObject("data").getJSONArray("charges");
//
//                    for(int i=0; i<cobrancas.length(); i++){
//                        System.out.println(cobrancas.getJSONObject(i).getLong("code"));
////                        try{
//                            if(cobrancas.getJSONObject(i).getJSONArray("payments").getJSONObject(0) != null){
//                               if(cobrancas.getJSONObject(i).getString("payNumber") == null){
//                                    Integer codeBF = cobrancas.getJSONObject(i).getInt("code");
//                                    CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByCodeBF(codeBF);
//                                     if (fatura != null){
//    //                                   fatura.setValorPago(cobrancas.getJSONObject(i).getJSONArray("payments").getJSONObject(0).getDouble("amount"));
//    //                                   fatura.setDtPagamento(new SimpleDateFormat("dd/mm/yyyy").parse(cobrancas.getJSONObject(i).getJSONArray("payments").getJSONObject(0).getString("date")));
//    //                                   
//    //                                   if(!Objects.equals(fatura.getValor(), fatura.getValorPago())){
//    //                                        System.out.println(fatura.getValor());
//    //                                        System.out.println(fatura.getValorPago());
//    //                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(7));
//    //                                    }else{
//    //                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
//    //                                    }
//                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
//                                        new CobrancaFaturaJpaController(emf).edit(fatura);
//                                   }else{
//                                       Mailer.send(null,null,null,null, "Fatura BF Não encontrada no Sisratec", String.valueOf(cobrancas.getJSONObject(i).getLong("code")), "jooohg@gmail.com");
//                                   }
//                               }
//                            }
////                            else{
////                                if(fatura.getDtVencimento().compareTo(new Date()) < 0){//se estiver vencido
////                                        fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
////                                        
////                                        //atualizaAdimplencia
////                                        if(fatura.getCliente().getAdimplencia() != 3){//se cliente não está no advogado
////                                            fatura.getCliente().setAdimplencia(2);
////                                            new ClienteJpaController(emf).edit(cliente);
////                                            //se fatura está pendente e data de vencimento é menor que hoje
////                                        }
////                                    }
////                            }                            
////                        }catch(Exception e){
////                            
////                        }finally{
////                            
////                        }
//                                
//                    }  
//                    break;
//                case 201:
////                    System.out.println(json.getJSONObject("data").getJSONArray("charges").getJSONObject(0).getJSONArray("payments"));
////                    System.out.println(json.getJSONObject("data").getJSONArray("charges").getJSONObject(0).getJSONArray("payments").getJSONObject(0));
//        //                    String codigo = json.getJSONObject("data").getJSONArray("payments").getJSONObject(0).getLong("code");
//                   
////                    return sb.toString();
//                    
//                    //gravar no banco o código enviado
//                    break;
//                case 400:
//                    //enviar email erro 400
//                    System.out.println(json.getJSONObject("errorMessage"));
//                    break;
//                default:
//                    //enviar email
//                break;
//            }
//            
//            
////            for (int c; (c = in.read()) >= 0;)
////                System.out.print((char)c);
//
////            in.close();
//        
//            atualizarCobrancasVencidasFromBF();
//        } catch (Exception ex) {
//            houveErro = true;
//            ex.printStackTrace(new PrintWriter(errors));
//        } finally {
//            emf.close();
//            if(houveErro){
//                Mailer.send(null,null,null,null, "Erro atualizarCobrancasVencidasFromBFDiario", errors.toString(), "jooohg@gmail.com");
//            }
//        }
//    }
    
    public void atualizarCobrancasVencidas() throws Exception{
        System.out.println("atualizarCobrancasVencidas");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        StringWriter errors = new StringWriter();
        try{
            //busca todas faturas que venceram ontem
            List<CobrancaFatura> faturas = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByDtVencimento(addDays(new Date(), -1));
            for (CobrancaFatura fatura : faturas) {
                if(fatura.getStatus().getId() == 1){//se fatura ainda em aberto
//                    if(fatura.getCodeBf() != 0){
                            fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
                            new CobrancaFaturaJpaController(emf).edit(fatura);
                            Cliente cliente = new ClienteJpaController(emf).findCliente(fatura.getCliente().getId());
                            //atualizaAdimplencia
                            if(cliente.getAdimplencia() != 3){//se cliente não está no advogado
                                cliente.setAdimplencia(2);
                                new ClienteJpaController(emf).edit(cliente);
                                //se fatura está pendente e data de vencimento é menor que hoje
                            }
//                    }
                }
            }
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro atualizarCobrancasVencidasFromBF", errors.toString(), "jooohg@gmail.com");
            }
        }
    }
        
     public JSONObject requestBuscaVindi(String tabela, Map<String,Object> params) throws IOException, Exception{
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        System.out.println("requestBuscaVindi");
        JSONObject json = null;
        boolean houveErro = false;
        try {
            StringBuilder stringBuilder = new StringBuilder(url_vindi + tabela);
            stringBuilder.append("?");
            String userpass = chave_vindi + ":01" ;
            String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
//            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            stringBuilder.append(postData);
            
            System.out.println(stringBuilder);
            
            URL url = new URL(stringBuilder.toString());
           
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty  ("Authorization", basicAuth);
            connection.setRequestMethod("GET");            
            connection.setRequestProperty("Accept-Charset", "UTF-8");
         
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8")); 
            int status = connection.getResponseCode();
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            
            json = new JSONObject(sb.toString());
        }catch(Exception e){
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro requestBuscaVindi", tabela + "\n" + GsonExclusion.GSON().toJson(params), "jooohg@gmail.com");
            }
            return json;
        }
    }
    
    public JSONObject requestCriaVindi(String tabela, Map<String,Object> params) throws IOException, Exception{
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        System.out.println("requestCriaVindi");
        boolean houveErro = false;
        JSONObject json = null;
        try {
            System.out.println(params);
            URL url = new URL (url_vindi + "/" + tabela); //costumers/bills/charges
            String userpass = chave_vindi + ":01" ;
            String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
            
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty  ("Authorization", basicAuth);
            connection.setRequestMethod("POST");
            connection.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
            connection.setRequestProperty(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
            connection.setDoOutput(true);
            
            OutputStream os = connection.getOutputStream();
            os.write(GsonExclusion.GSON().toJson(params).getBytes("UTF-8"));
            os.close();

            StringBuilder sb;
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8")); 
            int status = connection.getResponseCode();
            sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }

            json = new JSONObject(sb.toString());
        }catch(Exception e){
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro requestCriaVindi", tabela + "\n" + GsonExclusion.GSON().toJson(params), "jooohg@gmail.com");
                return null;
            }else{
                return json;    
            }
        }
    }
    
    public void buscarClientesVindi() throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        Map<String,Object> params = new LinkedHashMap<>();
        houveErro = false;
        System.out.println("buscarClientesVindi");
        try {
        for (int page = 1; page <= 11; page++) {
            
            
            params.put("page", page);
            params.put("per_page", "50");
            params.put("sort_by", "name");
            params.put("sort_order", "asc");
            
            JSONObject json = requestBuscaVindi("costumers", params);
                    
            JSONArray clientes = json.getJSONArray("customers");
            
            System.out.println(clientes);
            
            ArrayList<Cliente> clientesAtualizar = new ArrayList<>();
            for(int i=0; i<clientes.length(); i++){
//                System.out.println(clientes.getJSONObject(i).getString("name"));
                Cliente cliente = null;
                try{
                    cliente = new ClienteJpaController(emf).findByCnpj(clientes.getJSONObject(i).getString("registry_code"));
                }catch(Exception e){
                    cliente = null;
                }
                if(cliente != null){
                    cliente.setCodigoVindi(clientes.getJSONObject(i).getInt("id"));
                    new ClienteJpaController(emf).edit(cliente);
                    try{
                        if(clientes.getJSONObject(i).getString("code") != null){
//                            clientesAtualizar.add(cliente);
                            System.out.println("ENCONTRADO;" + clientes.getJSONObject(i).getInt("id") +";"+ clientes.getJSONObject(i).getString("name") +";"+ clientes.getJSONObject(i).getString("code"));
                        }
                    }catch(Exception e){
                        
                        clientesAtualizar.add(cliente);
//                        System.out.println("Encontrado Sem Code;" + clientes.getJSONObject(i).getInt("id") +";"+ clientes.getJSONObject(i).getString("name"));
                        System.out.println("Encontrado Sem Code;{\"id\":" + clientes.getJSONObject(i).getInt("id") +",\"code\":"+ cliente.getId()+"}");
                    }
                }else{
                    try{
                        cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(clientes.getJSONObject(i).getString("code")));
                        cliente.setCodigoVindi(clientes.getJSONObject(i).getInt("id"));
                        new ClienteJpaController(emf).edit(cliente);
                        System.out.println("Encontrado ID;" + clientes.getJSONObject(i).getInt("id") +";"+ clientes.getJSONObject(i).getString("name") +";"+ clientes.getJSONObject(i).getString("code"));
                    }catch(Exception e){
                        System.out.println("N Encontrado  Sem Code;" + clientes.getJSONObject(i).getInt("id") +";"+ clientes.getJSONObject(i).getString("name"));
                    }
                }
            }
            
//            for (Cliente cliente : clientesAtualizar) {
//                atualizarClienteVindi(cliente);
//            }
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro buscarClientesVindi", GsonExclusion.GSON().toJson(params), "jooohg@gmail.com");
            }
        }
    }
    
//    public void registrarCobrancaVisita(Visita visita)throws IOException{
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {
//            System.out.println("RegistrarCobrancaVisita");
//            Cliente cliente = new ClienteJpaController(emf).findCliente(visita.getCliente().getId());
//            if(cliente.getCodigoVindi() == null){
//                System.out.println("Call cadastraClieteVindi");
//                criarClienteVindi(visita.getCliente(),"registrarCobrancaVisita", visita);
//            }else{
//                System.out.println("Call criaFaturaVindi");
//                Contrato contrato = new ContratoJpaController(emf).findByCliente(visita.getCliente());
//                if(contrato.getTipoCliente().equals("Estabelecimento")){
//                    criaFaturaVindi(visita.getCliente(), visita);
//                }
//            }
//            
//        } catch(Exception e) {
//            e.printStackTrace();
//        }
//    }    
    
    public void CALLregistrarCobranca() throws IOException{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {   
                Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("cliente")));
                String tipo = read("tipo");
                registrarCobranca(cliente, tipo);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
      
    public void registrarCobranca(Cliente cliente, String tipo) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("registrarCobranca");
        try {   
                Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
                if(contrato.getTipoCliente().equals("Estabelecimento")){
                    if(cliente.getCodigoVindi() == null){
                        criarClienteVindi(cliente, "registrarCobranca", tipo);
                    }else{
                        if(tipo.equals("visita")){
                            criarCobrancaVisitaVindi(cliente);
                        }else{
                            if(cliente.getDtPrimeiraFatura() == null){
                                cliente.setDtPrimeiraFatura(new Date());
                                new ClienteJpaController(emf).edit(cliente);
                            }
                            if(cliente.getPeriodicidade() == 12){
                                criarAssinaturaVindi(cliente);
                            }else if(cliente.getPeriodicidade() == 1 || cliente.getPeriodicidade() == 0){
                                criarCobrancaRecorrenciaVindi(cliente);    
                            }else{
                                criarParametrosCobrancaRecorrenciaBF(cliente, true);
                            }
                        }
                    }
                }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro registrarCobranca", "Cliente: " + cliente.getId() + " " + cliente.getNomeFantasia() + "\n" + tipo, "jooohg@gmail.com");
            }
        }
    }    
    
    public void criarClienteVindi(Cliente cliente, String origem, String tipo) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        Map<String,Object> params = new LinkedHashMap<>();
        System.out.println("CadastraClienteVindi");
        try {
            params.put("id", cliente.getId());
            params.put("name", cliente.getNomeFantasia());
            params.put("email", cliente.getEmail());
            params.put("registry_code", cliente.getCnpj());
            params.put("code", cliente.getId());
//            params.put("status", "active");
//            params.put("created_at", "active");
//            params.put("updated_at", "active");
            //List addresses = new ArrayList<>();
            HashMap<String, Object> address = new HashMap();
            address.put("street", cliente.getEndereco().getRua());
            address.put("number", cliente.getEndereco().getNumero());
            address.put("neighborhood", cliente.getEndereco().getBairro());
            address.put("city", cliente.getEndereco().getCidade().getNome());
            address.put("zipcode", cliente.getEndereco().getCep());
            address.put("state", cliente.getEndereco().getUf());
            address.put("country", "BR");
            //addresses.add(address);
            params.put("address", address);
            if(!cliente.getTelefone1().equals("")){
                List phones = new ArrayList<>();
                HashMap<String, Object> phone = new HashMap();
                phone.put("phone_type", cliente.getTelefone1().length() > 12 ? "mobile" : "landline");
                phone.put("number", "55"+cliente.getTelefone1()); 
                phones.add(phone);
                if(!cliente.getTelefone2().equals("")){
                    phone = new HashMap();
                    phone.put("phone_type", cliente.getTelefone2().length() > 12 ? "mobile" : "landline");
                    phone.put("number", "55"+cliente.getTelefone2()); 
                    phones.add(phone);
                }
                if(!cliente.getTelefone3().equals("")){
                    phone = new HashMap();
                    phone.put("phone_type", cliente.getTelefone3().length() > 12 ? "mobile" : "landline");
                    phone.put("number", "55"+cliente.getTelefone3());
                    phones.add(phone);
                }
                params.put("phones", phones);
            }

            JSONObject json = requestCriaVindi("customers", params);
                
            
            Cliente clienteEdit = new ClienteJpaController(emf).findCliente(cliente.getId());
//                        System.out.println(json.getJSONObject("customer").getInt("id"));
            clienteEdit.setCodigoVindi(json.getJSONObject("customer").getInt("id"));
            new ClienteJpaController(emf).edit(clienteEdit);

            if (lock.tryLock(5, TimeUnit.SECONDS)) {
                if(origem.equals("registrarCobranca")){
                    registrarCobranca(clienteEdit, tipo);
                }
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro Cadastra Cliente Vindi", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + GsonExclusion.GSON().toJson(params), "jooohg@gmail.com");
            }else{
               
            }
        }
    }
    
    private int buscarPlanoVindi(Cliente cliente) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        System.out.println("buscarPlanoVindi");
        boolean houveErro = false;
        Map<String,Object> params = new LinkedHashMap<>();
        try {
            
//            List billItens = new ArrayList<>();
//                Map<String,Object> billItem = new LinkedHashMap<>();
            Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
            String produto = "";
            String nomePlano = "Mensal";
            String nomePlanoAdicionais = "";
            
//            List<Fotografia> fotos = new FotografiaJpaController(emf).findFotografiasByCliente(cliente);
//            for (Fotografia foto : fotos) {
                int pts = cliente.getUltPtsVista();
//                if(foto.getImovel() == null){
//                     pts = new VisitaJpaController(emf).findByClienteImovelNull(cliente).getPtsVistaRealizado();
//                }else{
//                    pts = new VisitaJpaController(emf).findByClienteImovel(cliente, foto.getImovel()).getPtsVistaRealizado();
//                }
                    
                

    //            if(contrato.getPagamento() == 1){
    //                nomePlano = "Anual";
    //
    //                if(pts > 0 && pts <= 5){
    //                    nomePlano += " até 5 Pontos de Vista";
    //                    produto = "160655"; //anuidade 5 pts
    //                } else if(pts > 5 && pts <= 10){
    //                    nomePlano += " até 10 Pontos de Vista";
    //                    produto = "160656"; //anuidade 10 pts
    //                } else if(pts > 10 && pts <= 15){
    //                    nomePlano += " até 15 Pontos de Vista";
    //                    produto = "160657"; //anuidade 15 pts
    //                } else if(pts > 15 && pts <= 20){
    //                    nomePlano += " até 20 Pontos de Vista";
    //                    produto = "179487"; //anuidade 20 pts
    //                } else if(pts > 20 && pts <= 25){
    //                    nomePlano += " até 25 Pontos de Vista";
    //                    produto = "179488"; //anuidade 25 pts
    //                }else{
    //                    throw new Exception();
    //                }
    //                
    //                if(contrato.isAnaliseAnual()){
    //                    nomePlano += " Análise Anual";
    //                    produto += ", 160019"; //analise anual pgto anual                
    //                }else if(contrato.isAnaliseMensal()){
    //                    nomePlano += " Análise Mensal";
    //                    produto += ", 160020"; //analise mensal pgto mensal
    //                }
    //
    //                if(contrato.isConsultoria()){
    //                    nomePlano += " Consultoria";
    //                    produto += ", 160677"; //consultoria pagto mensal
    //                }
    //            }else if(contrato.getPagamento() == 2){

            if(ambiente.equals("teste"))        {
                produto = "13442";
                nomePlano = "Plano Teste";
            }else{

                    if(pts > 0 && pts <= 5){
                        nomePlano += " até 5 Pontos de Vista";
                        if(produto.equals("")){
                            produto += "157687"; //mensalidade 5 pts
                        }else{
                            produto += ", 157687"; //mensalidade 5 pts
                        } 
                    } else if(pts > 5 && pts <= 10){
                        nomePlano += " até 10 Pontos de Vista";
                        if(produto.equals("")){
                            produto += "157688"; //mensalidade 10 pts
                        }else{
                            produto += ", 157688"; //mensalidade 10 pts
                        }
                    } else if(pts > 10 && pts <= 15){
                        nomePlano += " até 15 Pontos de Vista";
                        if(produto.equals("")){
                            produto += "157689"; //mensalidade 15 pts
                        }else{
                            produto += ", 157689"; //mensalidade 15 pts
                        }
                    } else if(pts > 15 && pts <= 20){
                        nomePlano += " até 20 Pontos de Vista";
                        if(produto.equals("")){
                            produto += "160652"; //mensalidade 20 pts
                        }else{
                            produto += ", 160652"; //mensalidade 20 pts
                        }
                    } else if(pts > 20 && pts <= 25){
                        nomePlano += " até 25 Pontos de Vista";
                        if(produto.equals("")){
                            produto += "160995"; //mensalidade 25 pts
                        }else{
                            produto += ", 160995"; //mensalidade 25 pts
                        }
                        
                    }else{
                        throw new Exception();
                    }
//            }
            if(contrato.isAnaliseAnual()){
                nomePlanoAdicionais += " Análise Anual";
                produto += ", 160272"; //analise anual pgto mensal                
            }else if(contrato.isAnaliseMensal()){
                nomePlanoAdicionais += " Análise Mensal";
                produto += ", 160270"; //analise mensal pgto mensal
            }
            if(contrato.isConsultoria()){
                nomePlanoAdicionais += " Consultoria";
                produto += ", 160016"; //consultoria pagto mensal
            }
            if(contrato.isFichaGoogle()){
                nomePlanoAdicionais += " Ficha";
                produto += ", 160018"; //cadastro google
            }
            }
            boolean isImobiliaria = false;
            String novoNomePlano = nomePlano;
            if(contrato.getTipoCliente().equals("Imobiliaria")){
                isImobiliaria = true;
                novoNomePlano = "Mensal Imobiliaria" + nomePlanoAdicionais;
            }
            
            PlanoVindi plano = new PlanoVindiJpaController(emf).findPlanoVindiByItem(produto, isImobiliaria);
            System.out.println(novoNomePlano);
            if(plano == null){
                //criar novo plano            
                params.put("name", novoNomePlano);
                params.put("interval", "months");
                params.put("interval_count", 1);
                params.put("billing_trigger_type", "day_of_month");
                params.put("billing_trigger_day", 5);                
                params.put("status", "active");            
                
                List list = new ArrayList<>();
                String[] produtos = produto.split(",");
                for (String produtoSplit : produtos) {
                    String aux = produtoSplit.trim();
                    Map<String,Object> map = new LinkedHashMap<>();
                    if(aux.equals("160018")){//se ficha google só cobra uma vez
                        map.put("cycles", 1);              
                        map.put("product_id", aux);              
                    }else{
                        map.put("product_id", aux);
                    }
                    list.add(map);
                }
                params.put("plan_items", list);
                
                JSONObject json = requestBuscaVindi("plans", params);
                
                PlanoVindiPK novoPK = new PlanoVindiPK(json.getJSONObject("plan").getInt("id"), produto);
                PlanoVindi novo = new PlanoVindi(novoPK);
                novo.setImobiliaria(isImobiliaria);
                new PlanoVindiJpaController(emf).create(novo);
                return json.getJSONObject("plan").getInt("id");
            }else{
                return plano.getPlanoVindiPK().getId();
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null, null,null,null, "Erro Buscar Plano Vindi", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + params, "jooohg@gmail.com");
            }
        }
        return 0;        
    }
    
    public void criarAssinaturaVindi(Cliente cliente) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        Map<String,Object> params = new LinkedHashMap<>();
        System.out.println("criarAssinaturaVindi");
        try {
            int plano = buscarPlanoVindi(cliente);
            if(plano != 0){
                
                params.put("start_at", new SimpleDateFormat("yyy-MM-dd").format(new Date()));
                params.put("plan_id", plano);//mensalidade
                params.put("customer_id", cliente.getCodigoVindi());
                params.put("payment_method_code", "credit_card");

                JSONObject json = requestCriaVindi("subscriptions", params);
                
                cliente.setCodigoAssinaturaVindi(json.getJSONObject("subscription").getInt("id"));
                new ClienteJpaController(emf).edit(cliente);

//                CobrancaFatura fatura = new CobrancaFatura();
//                fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
////                        fatura.setRegistro(registro);
//                new CobrancaFaturaJpaController(emf).create(fatura);
                        
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro Cadastra Assinatura Vindi", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + params, "jooohg@gmail.com");
            }
        }
    }
    
    public void criarCobrancaVisitaVindi(Cliente cliente) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        HashMap<String,Object> params = new LinkedHashMap<>();
        System.out.println("criarCobrancaVisitaVindi");
        try {
            ArrayList<Map<String,Object>> billItens = new ArrayList<>();
            Map<String,Object> billItem = new LinkedHashMap<>();
            int pts = cliente.getUltPtsVista();
            String produto = "";
            if(ambiente.equals("teste")){
                produto = "21128";
            }else{
                produto = "164326";
            }
            double valor = 0.0;

            valor = 0;

            if(pts > 0 && pts <= 5){
                valor = 137.49;
            }else if(pts > 5 && pts <= 10){
                valor = 177.49;
            }else if(pts > 10 && pts <= 15){
                valor = 217.49;
            }else if(pts > 15 && pts <= 20){
                valor = 257.49;
            }else if(pts > 20 && pts <= 25){
                valor = 297.49;
            }else if(pts > 25 && pts <= 30){
                valor = 337.49;
            }else if(pts > 30 && pts <= 35){
                valor = 377.49;
            }else{
                throw new Exception();
            }

            billItem.put("amount", valor);
            billItem.put("product_id", produto);
            billItens.add(billItem);
            
            params.put("bill_items", billItens);
            params.put("customer_id", cliente.getCodigoVindi());
            params.put("payment_method_code", "bank_slip");            
            Date vencimento = addDays(new Date(), 7);
            params.put("due_at", new SimpleDateFormat("yyy-MM-dd").format(vencimento));
            
            JSONObject json = requestCriaVindi("bills", params);
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro Cria Parametros Cobranca Visita", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + params, "jooohg@gmail.com");
            }
        }
    }
    
    public void criarCobrancaVindi(Cliente cliente, Date dtVencimento, Double valor,  String descricao, int qtdMeses, int referencia) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        HashMap<String,Object> params = new LinkedHashMap<>();
        System.out.println("criarCobrancaRecorrenciaVindi");
        try {
            ArrayList<Map<String,Object>> billItens = new ArrayList<>();
            Map<String,Object> billItem = new LinkedHashMap<>();
           
                int produto = 0;
                if(ambiente.equals("teste")){
                    produto = 24398;
                }else{
                    produto = 474270;
                }
                billItem.put("amount", valor);
                billItem.put("product_id", produto);
                billItem.put("description", descricao);
                billItens.add(billItem);
                params.put("bill_items", billItens);
                params.put("customer_id", cliente.getCodigoVindi());
                params.put("payment_method_code", "bank_slip");            
                Date vencimento = dtVencimento;
                params.put("due_at", new SimpleDateFormat("yyy-MM-dd").format(vencimento));

                JSONObject json = requestCriaVindi("bills", params);
                
                if(json != null){
                    
                }   
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro Cria Cobranca Vindi via Painel", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + params, "jooohg@gmail.com");
            }
        }
    }
    
    public void criarCobrancaRecorrenciaVindi(Cliente cliente) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        HashMap<String,Object> params = new LinkedHashMap<>();
        System.out.println("criarCobrancaRecorrenciaVindi");
        try {
            ArrayList<Map<String,Object>> billItens = new ArrayList<>();
            Map<String,Object> billItem = new LinkedHashMap<>();
           
            Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
            PlanoPK planoPK = new PlanoPK(contrato.getEdicao(), cliente.getUltPtsVistaMultp());
            Plano plano = new PlanoJpaController(emf).findPlano(planoPK);
            Double valor = 0.0;
            if(cliente.getPeriodicidade() == 1){
                if(contrato.getEdicao() == 1){//20% de desconto
                    valor = plano.getValorAnual() - (plano.getValorAnual() * 0.2);
                }else{//10% de desconto
                    valor = plano.getValorAnual() - (plano.getValorAnual() * 0.1);
                }
            }

            double valorAdicional = 0.0;
            if(contrato.isAnaliseAnual()){
                valorAdicional = 43.20;
                billItem = new LinkedHashMap<>();
                billItem.put("amount", 43.20);
                billItem.put("product_id", "160019"); //analise anual pgto anual
                billItens.add(billItem);
            }else if(contrato.isAnaliseAnual()){
                valorAdicional = valorAdicional + 86.40;
                billItem = new LinkedHashMap<>();
                billItem.put("amount", 86.40);
                billItem.put("product_id", "160020"); //analise mensal pgto anual
                billItens.add(billItem);
            }
            if(contrato.isConsultoria()){
                valorAdicional = valorAdicional + 133.20;
                billItem = new LinkedHashMap<>();
                billItem.put("amount", 133.20);
                billItem.put("product_id", "160677"); //consultoria pagto anual
                billItens.add(billItem);
            }
            if(contrato.isFichaGoogle()){
                valorAdicional = valorAdicional + 37.97;
                billItem = new LinkedHashMap<>();
                billItem.put("amount", 37.97);
                billItem.put("product_id", "160018"); //cadastro google
                billItens.add(billItem);
            }
            
//            Calendar cal = Calendar.getInstance(); 
            if(cliente.getPeriodicidade() == 1) {
                int produto = 0;
                if(ambiente.equals("teste")){
                    produto = 13442;
                }else{
                    produto = plano.getProdutoVindi();
                }
                billItem.put("amount", valor + valorAdicional);
                billItem.put("product_id", produto);
                billItens.add(billItem);
                params.put("bill_items", billItens);
                params.put("customer_id", cliente.getCodigoVindi());
                params.put("payment_method_code", "bank_slip");            
                Date vencimento = addDays(new Date(), 7);
                params.put("due_at", new SimpleDateFormat("yyy-MM-dd").format(vencimento));

                JSONObject json = requestCriaVindi("bills", params);
                
                if(json != null){
                    cliente.setDtUltimaFatura(new Date());
                    if(cliente.getPeriodicidade() == 1){
                        Calendar calendar = Calendar.getInstance(); 
                        calendar.add(Calendar.YEAR, 1);
                        cliente.setDtProximoVencimento(addDays(calendar.getTime(),7));
                    }
                    new ClienteJpaController(emf).edit(cliente);
                }
                
            }            
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro Cria Cobranca Recorrente", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + params, "jooohg@gmail.com");
            }
        }
    }
     
    public void criarAssinaturaVindiAntigos(Cliente cliente) throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        Map<String,Object> params = new LinkedHashMap<>();
//        System.out.println("criarAssinaturaVindi");
        try {
            int plano = 0;
            Double valorMensal = new ContratoJpaController(emf).findByCliente(cliente).getValorMensal();

            if(valorMensal.equals(23.90)){
                plano = 49514;
            }else if(valorMensal.equals(19.90)){
                plano = 53669;
            }else if(valorMensal.equals(13.99)){
                plano = 53670;
            }else if(valorMensal.equals(15.45)){
                plano = 53671;
            }else if(valorMensal.equals(14.99)){
                plano = 53683;
            }else if(valorMensal.equals(14.49)){
                plano = 53684;
            }else if(valorMensal.equals(15.49)){
                plano = 53673;
            }else if(valorMensal.equals(16.47)){
                plano = 53688;
                plano = 53678;
            }else if(valorMensal.equals(29.90)){
                plano = 53673;
            }else if(valorMensal.equals(27.90)){
            }

            if(plano != 0){

                params.put("start_at", new SimpleDateFormat("yyy-MM-dd").format(new Date()));
                params.put("plan_id", plano);//mensalidade
                params.put("customer_id", cliente.getCodigoVindi());
                params.put("payment_method_code", "credit_card");            

                JSONObject json = requestCriaVindi("subscriptions", params);

                cliente.setCodigoAssinaturaVindi(json.getJSONObject("subscription").getInt("id"));
                new ClienteJpaController(emf).edit(cliente);

            }else{
                System.out.println("CLIENTE SEM PLANO: " + cliente.getId());
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            if(houveErro){
                Mailer.send(null, null,null,null, "Erro Cadastra Assinatura Vindi Antigos", "Cliente: " + cliente.getId() +" - "+ cliente.getNomeFantasia() + "\n" + params, "jooohg@gmail.com");
            }
        }
    }
           
    public void atualizarRegistroRefenciaFaturasVindi() throws IOException, Exception {
        System.out.println("atualizarRegistroRefenciaFaturasVindi");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        StringWriter errors = new StringWriter();
        
        Map<String,Object> params = new LinkedHashMap<>();
        try {            
            List<CobrancaFatura> cobrancas = new CobrancaFaturaJpaController(emf).findCobrancaFaturaRegistroNull();
            
            for (CobrancaFatura cobranca : cobrancas) {                
                      
                    
                    JSONObject json = requestBuscaVindi("bills/"+cobranca.getIdFaturaVindi(), params);
                    JSONObject faturaVindi = json.getJSONObject("bill");

//                    for (int i = 0; i < faturaVindi.length(); i++) {
//                        int idFatura = cobranca.getIdFaturaVindi();
                        
                        JSONArray itens = faturaVindi.getJSONArray("bill_items");
//                        System.out.println(itens);
                        //verifica se tem mais de um produto
                        //se sim: 
                        CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByIDFaturaVindi(cobranca.getIdFaturaVindi());
                        if(fatura != null){
                            try{
                                fatura.setDescricaoFatura(itens.getJSONObject(0).getString("description"));
                            }catch(Exception e){
                                
                            }
                            if(itens.length() > 1){
                                fatura.setQtdMeses(0);
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(8));
                            }else{
                                int produto = itens.getJSONObject(0).getJSONObject("product").getInt("id");
                                if(produto == 160655 || produto == 160656 || produto == 160657 || produto == 179487 || produto == 179488 || produto == 333046 || produto == 333049){
                                    //anuidade
                                    fatura.setQtdMeses(12);
                                    fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(2));
                                }else if(produto == 164326){
                                    //visita fotografica
                                    fatura.setQtdMeses(0);
                                    fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(1));
                                }else if(produto == 157687 || produto == 157688 || produto == 157689 || produto == 160652 || produto == 160995){
                                    //mensalidade
                                    fatura.setQtdMeses(1);
                                    fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(7));
                                }else if(produto == 175888 || produto == 175890 || produto == 175891 || produto == 175893 || produto == 175895 || produto == 175896 || produto == 175898 || produto == 175899 || produto == 175900 || produto == 175901 || produto == 337045 || produto == 157516){
                                    //MENSALIDADEs antigas
                                    fatura.setQtdMeses(12);
                                    fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(7));
                                }else{
                                    fatura.setQtdMeses(0);
                                    fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(8));
                                }
                            }
//                        }
                        
                        new CobrancaFaturaJpaController(emf).edit(fatura);
                    }
                    
            }
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro AtualizarRegistroRefenciaFaturasVindi", errors.toString(), "jooohg@gmail.com");
            }else{
                calcularTotalRecorrenciasPagas();
            }
        }
    }
    
     public boolean possuiVisitaVencida(Cliente cliente) throws IOException, Exception {
        System.out.println("possuiVisitaVencida");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        StringWriter errors = new StringWriter();
        try {
            return new CobrancaFaturaJpaController(emf).findQtdVisitaVencidaByCliente(cliente) != 0;
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
            return true;
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro possuiVisitaVencida", errors.toString(), "jooohg@gmail.com");
            }
        }
    }
     
            
    public void calcularMesesFaltantes() throws IOException, Exception {
        System.out.println("calcularMesesFaltantes");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        StringWriter errors = new StringWriter();
        try {
            List<Cliente> clientes = new ClienteJpaController(emf).findClienteByStatus(true);
            
            for (Cliente cliente : clientes) {
                System.out.println(cliente.getId());
                // && cliente.getDtProximoVencimento() != null
                if(cliente.getDtPrimeiraFatura() != null){
                    String primeiraFatura = new SimpleDateFormat("yyyy-MM-dd").format(cliente.getDtPrimeiraFatura());
//                    String proximaFatura = new SimpleDateFormat("yyyy-MM-dd").format(cliente.getDtProximoVencimento());
                    
                    String atual = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    System.out.println(atual);
                    System.out.println(primeiraFatura);
                    
                   long monthsBetween = ChronoUnit.MONTHS.between(
                    LocalDate.parse(primeiraFatura).withDayOfMonth(1),
                    LocalDate.parse(atual).withDayOfMonth(1));
                   
//                   long monthsBetweenFinal = ChronoUnit.MONTHS.between(
//                    LocalDate.parse(atual).withDayOfMonth(1),
//                    LocalDate.parse(proximaFatura).withDayOfMonth(1));
                   
                   System.out.println("monthsBetween" + monthsBetween);
                   
//                   Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
//                   PlanoPK planoPK = new PlanoPK(contrato.getEdicao(), cliente.getUltPtsVistaMultp());
//                   Plano plano = new PlanoJpaController(emf).findPlano(planoPK);
//                   
//                    if(plano != null){
//                   
//                        Double mensalidadesPagas  = 0.0;
//     //                   try{
//                            double valorAnual = plano.getValorAnual();
//                            System.out.println("periodicidade" + cliente.getPeriodicidade());
//                            if(cliente.getPeriodicidade().equals(1)){
//                                System.out.println("edicao"  + contrato.getEdicao());
//                                if(contrato.getEdicao().equals(1)){
//                                    valorAnual = plano.getValorAnual() - (plano.getValorAnual()*0.2);
//                                }else{
//                                    valorAnual = plano.getValorAnual() - (plano.getValorAnual()*0.1);
//                                }
//                            }
//                            System.out.println("valorAnual" + valorAnual);
//                            System.out.println("getTotalRecorrenciasPagas()" + cliente.getTotalRecorrenciasPagas());
//                            mensalidadesPagas = cliente.getTotalRecorrenciasPagas()/(valorAnual/12);
//     //                   }catch(Exception e){
//     //                       mensalidadesPagas  = 0.0;
//     //                   }
//                        System.out.println("mensalidadesPagas" + mensalidadesPagas);
                        double mesesFaltantes = monthsBetween - cliente.getTotalMesesRecorrenciasPagas();

                        cliente.setMesesFaltantes((int) mesesFaltantes);
                        if((int) mesesFaltantes > 0){
                            if(cliente.getAdimplencia() == 1){
                                cliente.setAdimplencia(2);
                            }
                        }else{
                            if(!possuiVisitaVencida(cliente)){
                                cliente.setAdimplencia(1);
                            }else{
                                cliente.setAdimplencia(2);
                            }
                        }
                        new ClienteJpaController(emf).edit(cliente);
//                    }
                }
            }
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro calcularMesesFaltantes", errors.toString(), "jooohg@gmail.com");
            }
        }
    }
    
    public void calcularTotalRecorrenciasPagas() throws IOException, Exception {
        System.out.println("calcularTotalRecorrenciasPagas");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        StringWriter errors = new StringWriter();
        try {
            List<Cliente> clientes = new ClienteJpaController(emf).findClienteByStatus(true);
            
            for (Cliente cliente : clientes) {
                double total = 0;
                int totalMeses = 0;
                List<CobrancaFatura> faturas = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByCliente(cliente);
                for (CobrancaFatura fatura : faturas) {
                    if(fatura.getStatus().getId() == 3 || fatura.getStatus().getId() == 7){
                        if(fatura.getRegistro().getId() == 2 || fatura.getRegistro().getId() == 3 || fatura.getRegistro().getId() == 4 | fatura.getRegistro().getId() == 6 || fatura.getRegistro().getId() == 7 || fatura.getRegistro().getId() == 9 || fatura.getRegistro().getId() == 10 || fatura.getRegistro().getId() == 11){
                            total += fatura.getValor();
                            totalMeses += fatura.getQtdMeses();
                        }
                    }
                }
                cliente.setTotalRecorrenciasPagas(total);
                cliente.setTotalMesesRecorrenciasPagas(totalMeses);
                new ClienteJpaController(emf).edit(cliente);
            }
            
            calcularMesesFaltantes();
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro calcularTotalRecorrenciasPagas", errors.toString(), "jooohg@gmail.com");
            }
        }
    }
    
    public void atualizarCobrancasFromVindiDiario() throws IOException, Exception {
        System.out.println("atualizarCobrancasFromVindiDiario");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        String erro = "";
        StringWriter errors = new StringWriter();
        try {
            Map<String,Object> params = new LinkedHashMap<>();
            params.put("page", "1");
            params.put("per_page", "50");
            params.put("sort_by", "created_at");
            params.put("sort_order", "asc");
            String data = new SimpleDateFormat("yyyy-MM-dd").format(addDays(new Date(), -1));
            params.put("query", "updated_at:"+data);
            
            JSONObject json = requestBuscaVindi("charges", params);
            JSONArray cobrancas = json.getJSONArray("charges");
            
//            System.out.println(cobrancas);
            
            for(int i=0; i<cobrancas.length(); i++){
                int idCobrancaVindi = cobrancas.getJSONObject(i).getInt("id");
                String code;
                Cliente cliente = null;
                try{
                    code = cobrancas.getJSONObject(i).getJSONObject("customer").getString("code");
                    cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(code));
                }catch(Exception e){
                    System.out.println(cobrancas.getJSONObject(i).getJSONObject("customer"));
                    code = null;
                }
                
                Date dtVencimento;
                try{
                    dtVencimento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("due_at").substring(0, 10));
                }catch(Exception e){
                    System.out.println(cobrancas.getJSONObject(i).getJSONObject("customer"));
                    System.out.println("DTPAGAMENTO null");
                    dtVencimento = null;
                }
                
                if(code == null){
                    houveErro = true;
                    erro = "Cliente com Code no Vindi Nulo " + cobrancas.getJSONObject(i).getJSONObject("customer");
                }else if(cliente == null){
                    houveErro = true;
                    erro = "Cliente não existe no BD e existe no Vindi " + cobrancas.getJSONObject(i).getJSONObject("customer");
                }else{
                    CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByIDCobrancaVindi(idCobrancaVindi);
                    
                    if(fatura == null){
                       
                        //se não existe no nosso sistema, criar
                        CobrancaFatura novafatura = new CobrancaFatura();
                        novafatura.setIdCobrancaVindi(idCobrancaVindi);
                        novafatura.setValor(cobrancas.getJSONObject(i).getDouble("amount"));
                        novafatura.setCliente(cliente);
                        
                        int idFaturaVindi = cobrancas.getJSONObject(i).getJSONObject("bill").getInt("id");
                        novafatura.setIdFaturaVindi(idFaturaVindi);
                        Date dtEmissao = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("created_at").substring(0, 10));
                        novafatura.setDtEmissao(dtEmissao);
                                                
                        novafatura.setDtVencimento(dtVencimento);
                        
                        if(cobrancas.getJSONObject(i).getString("status").equals("paid")){
                            Date dtPagamento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("updated_at").substring(0, 10));
                            novafatura.setDtPagamento(dtPagamento);
                            if(!Objects.equals(novafatura.getValor(), novafatura.getValorPago())){
                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(7));
                            }else{
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
                            }
                        }else if(cobrancas.getJSONObject(i).getString("status").equals("canceled")){
                            novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(2));
                        }else if(cobrancas.getJSONObject(i).getString("status").equals("scheduled")){
                            novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(6));
                        }else if(cobrancas.getJSONObject(i).getString("status").equals("pending")){
                            if(dtVencimento.compareTo(new Date()) < 0){//se estiver vencido
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
                            }else{
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
                            }
                        }else{
                            novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(4));
                        }
                        new CobrancaFaturaJpaController(emf).create(novafatura);
                    }else{
                        if(cobrancas.getJSONObject(i).getString("status").equals("paid")){
                            Date dtPagamento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("updated_at").substring(0, 10));
                            fatura.setDtPagamento(dtPagamento);
                            fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
                        }else if(cobrancas.getJSONObject(i).getString("status").equals("canceled")){
                            fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(2));
                        }
                        
                        new CobrancaFaturaJpaController(emf).edit(fatura);
                    }
                    
                    //atualizaAdimplencia
                    if(cliente.getAdimplencia() != 3){//se cliente não está no advogado
                        if(cobrancas.getJSONObject(i).getString("status").equals("pending")){
                            if(dtVencimento.compareTo(new Date()) < 0){
                                cliente.setAdimplencia(2);
                                new ClienteJpaController(emf).edit(cliente);
                                //se fatura está pendente e data de vencimento é menor que hoje
                            }
                        }
                    }
                }
            }
            atualizarRegistroRefenciaFaturasVindi();
            
            
        } catch (Exception ex) {
            houveErro = true;
            ex.printStackTrace(new PrintWriter(errors));
        } finally {
            emf.close();
            if(houveErro){
                if(erro.equals("")){
                    Mailer.send(null,null,null,null, "Erro Atualiza Cobranca From Vindi Diario", errors.toString(), "jooohg@gmail.com");
                }else{
                    Mailer.send(null,null,null,null, "Erro Atualiza Cobranca From Vindi Diario", erro, "jooohg@gmail.com");
                }
            }
        }
    }
    
    public void atualizarTodasCobrancasFromVindi() throws IOException, Exception {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("atualizarTodasCobrancasFromVindi");
        try {
            
            for (int page = 1; page < 35; page++) {

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("page", page);
                params.put("per_page", "50");
                params.put("sort_by", "created_at");
                params.put("sort_order", "asc");
    //            String data = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                

                JSONObject json = requestBuscaVindi("charges", params);
                JSONArray cobrancas = json.getJSONArray("charges");

                System.out.println(cobrancas.length());

                for(int i=0; i<cobrancas.length(); i++){
                    int idCobrancaVindi = cobrancas.getJSONObject(i).getInt("id");
                    String code;
                    Cliente cliente = null;
                    try{
                        code = cobrancas.getJSONObject(i).getJSONObject("customer").getString("code");
                        cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(code));
                    }catch(Exception e){
                        System.out.println(cobrancas.getJSONObject(i).getJSONObject("customer"));
                        code = null;
                    }

                    Date dtVencimento;
                    try{
                        dtVencimento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("due_at").substring(0, 10));
                    }catch(Exception e){
                        System.out.println(cobrancas.getJSONObject(i).getJSONObject("customer"));
                        System.out.println("DTPAGAMENTO null");
                        dtVencimento = null;
                    }

                    if(code == null){
                        System.out.println("Código Nulo");
                    }else if(cliente == null){
                        System.out.println(cobrancas.getJSONObject(i).getJSONObject("customer"));
                        System.out.println("Cliente não existe no BD e existe no Vindi");
                    }else{
                        CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByIDCobrancaVindi(idCobrancaVindi);

                        if(fatura == null){

                            //se não existe no nosso sistema, criar
                            CobrancaFatura novafatura = new CobrancaFatura();
                            novafatura.setIdCobrancaVindi(idCobrancaVindi);
                            novafatura.setValor(cobrancas.getJSONObject(i).getDouble("amount"));
                            novafatura.setCliente(cliente);

                            int idFaturaVindi = cobrancas.getJSONObject(i).getJSONObject("bill").getInt("id");
                            novafatura.setIdFaturaVindi(idFaturaVindi);
    //                        System.out.println("DT CRIAÇÃO" + faturas.getJSONObject(i).getString("created_at").substring(0, 10));
    //                        System.out.println("DT VENCIMENTO" + faturas.getJSONObject(i).getString("due_at").substring(0, 10));
                            Date dtEmissao = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("created_at").substring(0, 10));
                            novafatura.setDtEmissao(dtEmissao);

                            novafatura.setDtVencimento(dtVencimento);

                            if(cobrancas.getJSONObject(i).getString("status").equals("paid")){
                                Date dtPagamento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("updated_at").substring(0, 10));
                                novafatura.setDtPagamento(dtPagamento);
                                novafatura.setValorPago(cobrancas.getJSONObject(i).getJSONObject("last_transaction").getDouble("amount"));
                                if(!Objects.equals(novafatura.getValor(), novafatura.getValorPago())){
                                    System.out.println(novafatura.getValor());
                                    System.out.println(novafatura.getValorPago());
                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(7));
                                }else{
                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
                                }
                            }else if(cobrancas.getJSONObject(i).getString("status").equals("canceled")){
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(2));
                            }else if(cobrancas.getJSONObject(i).getString("status").equals("scheduled")){
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(6));
                            }else if(cobrancas.getJSONObject(i).getString("status").equals("pending")){
                                if(dtVencimento.compareTo(new Date()) < 0){//se estiver vencido
                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
                                }else{
                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
                                }
                            }else{
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(4));
                            }
                            new CobrancaFaturaJpaController(emf).create(novafatura);
                        }else{
                            //se existe, atualizar
    //                        if(cobrancas.getJSONObject(i).getString("status").equals("paid")){
    //                            Date dtPagamento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("updated_at").substring(0, 10));
    //                            fatura.setDtPagamento(dtPagamento);
    //                            fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
    //                            new CobrancaFaturaJpaController(emf).edit(fatura);
    //                        }
                        }

                        //atualizaAdimplencia
                        if(cliente.getAdimplencia() != 3){//se cliente não está no advogado
                            if(cobrancas.getJSONObject(i).getString("status").equals("pending")){
                                if(dtVencimento.compareTo(new Date()) < 0){
                                    cliente.setAdimplencia(2);
                                    new ClienteJpaController(emf).edit(cliente);
                                    //se fatura está pendente e data de vencimento é menor que hoje
                                }
                            }
                        }
                    }
                }
            }
            
         atualizarRegistroRefenciaFaturasVindi();
         calcularTotalRecorrenciasPagas();
        } catch (Exception e) {
            houveErro = true;
            e.printStackTrace();
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro AtualizarTodasCobrancasFromVindi", null, "jooohg@gmail.com");
            }
        }
    }
    
    public void atualizarFaturasAgendadasFromVindi() throws IOException, Exception {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("atualizarFaturasAgendadasFromVindi");
        try {
            
            for (int page = 1; page < 35; page++) {

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("page", page);
                params.put("per_page", "50");
                params.put("query", "status=scheduled");
                params.put("sort_by", "created_at");
                params.put("sort_order", "asc");
    //            String data = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                

                JSONObject json = requestBuscaVindi("bills", params);
                JSONArray faturas = json.getJSONArray("bills");

                System.out.println(faturas.length());

                for(int i=0; i<faturas.length(); i++){
                    int idFaturaVindi = faturas.getJSONObject(i).getInt("id");
                    String code;
                    Cliente cliente = null;
                    try{
                        code = faturas.getJSONObject(i).getJSONObject("customer").getString("code");
                        cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(code));
                    }catch(Exception e){
                        System.out.println(faturas.getJSONObject(i).getJSONObject("customer"));
                        code = null;
                    }

                    Date dtVencimento;
                    try{
                        dtVencimento = new SimpleDateFormat("yyyy-MM-dd").parse(faturas.getJSONObject(i).getString("due_at").substring(0, 10));
                    }catch(Exception e){
                        System.out.println(faturas.getJSONObject(i).getJSONObject("customer"));
                        System.out.println("DTPAGAMENTO null");
                        dtVencimento = null;
                    }

                    if(code == null){
                        System.out.println("Código Nulo");
                    }else if(cliente == null){
                        System.out.println(faturas.getJSONObject(i).getJSONObject("customer"));
                        System.out.println("Cliente não existe no BD e existe no Vindi");
                    }else{
                        CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByIDFaturaVindi(idFaturaVindi);

                        if(fatura == null){

                            //se não existe no nosso sistema, criar
                            CobrancaFatura novafatura = new CobrancaFatura();
                            novafatura.setValor(faturas.getJSONObject(i).getDouble("amount"));
                            novafatura.setCliente(cliente);

//                            int idFaturaVindi = faturas.getJSONObject(i).getJSONObject("bill").getInt("id");
                            novafatura.setIdFaturaVindi(idFaturaVindi);
    //                        System.out.println("DT CRIAÇÃO" + faturas.getJSONObject(i).getString("created_at").substring(0, 10));
    //                        System.out.println("DT VENCIMENTO" + faturas.getJSONObject(i).getString("due_at").substring(0, 10));
                            Date dtEmissao = new SimpleDateFormat("yyyy-MM-dd").parse(faturas.getJSONObject(i).getString("created_at").substring(0, 10));
                            novafatura.setDtEmissao(dtEmissao);

                            novafatura.setDtVencimento(dtVencimento);

//                            if(faturas.getJSONObject(i).getString("status").equals("paid")){
//                                Date dtPagamento = new SimpleDateFormat("yyyy-MM-dd").parse(faturas.getJSONObject(i).getString("updated_at").substring(0, 10));
//                                novafatura.setDtPagamento(dtPagamento);
//                                novafatura.setValorPago(faturas.getJSONObject(i).getJSONObject("last_transaction").getDouble("amount"));
//                                if(!Objects.equals(novafatura.getValor(), novafatura.getValorPago())){
//                                    System.out.println(novafatura.getValor());
//                                    System.out.println(novafatura.getValorPago());
//                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(7));
//                                }else{
//                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
//                                }
//                            }else if(faturas.getJSONObject(i).getString("status").equals("canceled")){
//                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(2));
//                            }else if(faturas.getJSONObject(i).getString("status").equals("scheduled")){
                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(6));
//                            }else if(faturas.getJSONObject(i).getString("status").equals("pending")){
//                                if(dtVencimento.compareTo(new Date()) < 0){//se estiver vencido
//                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
//                                }else{
//                                    novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
//                                }
//                            }else{
//                                novafatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(4));
//                            }
                            new CobrancaFaturaJpaController(emf).create(novafatura);
                        }else{
                            System.out.println("Fatura existe " + idFaturaVindi);
                            //se existe, atualizar
    //                        if(cobrancas.getJSONObject(i).getString("status").equals("paid")){
    //                            Date dtPagamento = new SimpleDateFormat("yyyy-MM-dd").parse(cobrancas.getJSONObject(i).getString("updated_at").substring(0, 10));
    //                            fatura.setDtPagamento(dtPagamento);
    //                            fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
    //                            new CobrancaFaturaJpaController(emf).edit(fatura);
    //                        }
                        }

                        //atualizaAdimplencia
//                        if(cliente.getAdimplencia() != 3){//se cliente não está no advogado
//                            if(faturas.getJSONObject(i).getString("status").equals("pending")){
//                                if(dtVencimento.compareTo(new Date()) < 0){
//                                    cliente.setAdimplencia(2);
//                                    new ClienteJpaController(emf).edit(cliente);
//                                    //se fatura está pendente e data de vencimento é menor que hoje
//                                }
//                            }
//                        }
                    }
                }
            }
            
         atualizarRegistroRefenciaFaturasVindi();
//         calcularTotalRecorrenciasPagas();
        } catch (Exception e) {
            houveErro = true;
            e.printStackTrace();
        } finally {
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro atualizarFaturasAgendadasFromVindi", null, "jooohg@gmail.com");
            }
        }
    }
    
//    public void buscaClientesListagem() throws Exception{
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        List<CobrancaListagemDebora> lista = new CobrancaListagemDeboraJpaController(emf).findCobrancaListagemDeboraEntities();
//        for (CobrancaListagemDebora cliente : lista) {
//            criaFaturaVindiListagem(cliente);
//        }
//    }
    
//    public void criaFaturaVindiListagem(CobrancaListagemDebora cobranca) throws IOException, Exception{
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        boolean houveErro = false;
//        HashMap<String,Object> params = new LinkedHashMap<>();
//        try {
//            //verificar codigo vindi
//            //verificar valor a ser cobrado (se cliente tiver algum serviço especial contratado)
//            //verificar campo ultimo ponto de vista
//                       
//            Cliente cliente = new ClienteJpaController(emf).findCliente(cobranca.getCliente());
//            System.out.println(cliente.getId());
//            System.out.println(cliente.getCodigoVindi());
//            
//            
//            params.put("customer_id", cliente.getCodigoVindi());
//            params.put("payment_method_code", "bank_slip");     
//            Date emissao = addDays(cobranca.getVencimentoFatura(), -10);
//            System.out.println(emissao);
//            params.put("billing_at", new SimpleDateFormat("yyy-MM-dd").format(emissao));
//            params.put("due_at", new SimpleDateFormat("yyy-MM-dd").format(cobranca.getVencimentoFatura()));
//                
//                ArrayList<Map<String,Object>> billItens = new ArrayList<>();
//                Map<String,Object> billItem = new LinkedHashMap<>();
//                
//                Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
////                List<Fotografia> fotos = new FotografiaJpaController(emf).findFotografiasByCliente(cliente);
////                for (Fotografia foto : fotos) {
//                    billItem = new LinkedHashMap<>();
//                    int pts = cliente.getUltPtsVista();
////                    if(foto.getImovel() == null){
////                         pts = new VisitaJpaController(emf).findVisita(visita.getId()).getPtsVistaRealizado();
////                    }else{
////                        pts = new VisitaJpaController(emf).findByClienteImovel(cliente, foto.getImovel()).getPtsVistaRealizado();
////                    }
//                    
//
//                    double valorPts = 0;
//                    String produto = "";
//                    double valor = 0.0;
//
////                    if(visita == null){
//                        if (contrato.getEdicao() == 1){
//                            if(pts > 0 && pts <= 5){
//                                valorPts = 19.90;
//                                produto = "160655"; //anuidade 5 pts
//                            }else if(pts > 5 && pts <= 10){
//                                valorPts = 23.90;
//                                produto = "160656"; //anuidade 10 pts
//                            }else if(pts > 10 && pts <= 15){
//                                valorPts = 27.90;
//                                produto = "160657"; //anuidade 15 pts
//                            }else if(pts > 15 && pts <= 20){
//                                valorPts = 31.90;
//                                produto = "179487"; //anuidade 20 pts
//                            } else if(pts > 20 && pts <= 25){
//                                valorPts = 35.90;
//                                produto = "179488"; //anuidade 25 pts
//                            }else if(pts > 25 && pts <= 30){
//                                valorPts = 39.90;
//                                produto = "333046"; //anuidade 30 pts
//                            } else if(pts > 30 && pts <= 35){
//                                valorPts = 43.90;
//                                produto = "333049"; //anuidade 35 pts
//                            }else{
//                                throw new Exception();
//                            }   
//                            
//                            valor = (valorPts*12) - (valorPts*12*0.2);
//                            
//                        }else if (contrato.getEdicao() == 2){
//                            if(pts > 0 && pts <= 5){
//                                valorPts = 29.10;
//                                produto = "160655"; //anuidade 5 pts
//                            }else if(pts > 5 && pts <= 10){
//                                valorPts = 33.00;
//                                produto = "160656"; //anuidade 10 pts
//                            }else if(pts > 10 && pts <= 15){
//                                valorPts = 36.90;
//                                produto = "160657"; //anuidade 15 pts
//                            }else if(pts > 15 && pts <= 20){
//                                valorPts = 40.80;
//                                produto = "179487"; //anuidade 20 pts
//                            } else if(pts > 20 && pts <= 25){
//                                valorPts = 44.70;
//                                produto = "179488"; //anuidade 25 pts
//                            }else if(pts > 25 && pts <= 30){
//                                valorPts = 48.60;
//                                produto = "333046"; //anuidade 30 pts
//                            } else if(pts > 30 && pts <= 35){
//                                valorPts = 52.50;
//                                produto = "333049"; //anuidade 35 pts
//                            }else{
//                                throw new Exception();
//                            }     
//                            
//                            valor = (valorPts*12) - (valorPts*12*0.1);
//
//                        }else if (contrato.getEdicao() == 3){
//                            if(pts > 0 && pts <= 5){
//                                valorPts = 29.10;
//                                produto = "160655"; //anuidade 5 pts
//                            }else if(pts > 5 && pts <= 10){
//                                valorPts = 36.10;
//                                produto = "160656"; //anuidade 10 pts
//                            }else if(pts > 10 && pts <= 15){
//                                valorPts = 43.10;
//                                produto = "160657"; //anuidade 15 pts
//                            }else if(pts > 15 && pts <= 20){
//                                valorPts = 50.10;
//                                produto = "179487"; //anuidade 20 pts
//                            } else if(pts > 20 && pts <= 25){
//                                valorPts = 57.10;
//                                produto = "179488"; //anuidade 25 pts
//                            } else if(pts > 25 && pts <= 30){
//                                valorPts = 64.10;
//                                produto = "333046"; //anuidade 30 pts
//                            } else if(pts > 30 && pts <= 35){
//                                valorPts = 71.10;
//                                produto = "333049"; //anuidade 35 pts
//                            }else{
//                                throw new Exception();
//                            }
//                            
//                            valor = (valorPts*12) - (valorPts*12*0.1);
//                        }
//                        
//                        billItem.put("amount", valor);
//                        billItem.put("product_id", produto);
//                        billItens.add(billItem);
////                    }else{
////                        produto = "164326";
//////                        produto = "15154";//produto sandbox
////                        valor = 0;
////                        
////                        if(pts > 0 && pts <= 5){
////                            valor = 137.49;
////                        }else if(pts > 5 && pts <= 10){
////                            valor = 177.49;
////                        }else if(pts > 10 && pts <= 15){
////                            valor = 217.49;
////                        }else if(pts > 15 && pts <= 20){
////                            valor = 257.49;
////                        }else if(pts > 20 && pts <= 25){
////                            valor = 297.49;
////                        }else if(pts > 25 && pts <= 30){
////                            valor = 337.49;
////                        }else if(pts > 30 && pts <= 35){
////                            valor = 377.49;
////                        }else{
////                            throw new Exception();
////                        }
////                        
//////                        System.out.println(valor);
////                        billItem.put("amount", valor);
////                        billItem.put("product_id", produto); //Visita Fotográfica
////                        billItens.add(billItem);
////                    }
////                }            
//                    double valorAdicional = 0.0;
////                if(visita == null){                    
//                    if(contrato.isAnaliseAnual()){
//                        valorAdicional = 43.20;
//                        billItem = new LinkedHashMap<>();
//                        billItem.put("amount", 43.20);
//                        billItem.put("product_id", "160019"); //analise anual pgto anual
//                        billItens.add(billItem);
//                    }else if(contrato.isAnaliseAnual()){
//                        valorAdicional = valorAdicional + 86.40;
//                        billItem = new LinkedHashMap<>();
//                        billItem.put("amount", 86.40);
//                        billItem.put("product_id", "160020"); //analise mensal pgto anual
//                        billItens.add(billItem);
//                    }
//                    if(contrato.isConsultoria()){
//                        valorAdicional = valorAdicional + 133.20;
//                        billItem = new LinkedHashMap<>();
//                        billItem.put("amount", 133.20);
//                        billItem.put("product_id", "160677"); //consultoria pagto anual
//                        billItens.add(billItem);
//                    }
//                    if(contrato.isFichaGoogle()){
//                        valorAdicional = valorAdicional + 37.97;
//                        billItem = new LinkedHashMap<>();
//                        billItem.put("amount", 37.97);
//                        billItem.put("product_id", "160018"); //cadastro google
//                        billItens.add(billItem);
//                    }
////                }
//            params.put("bill_items", billItens);
//            
//            JSONObject json = requestCriaVindi("bills", params);
//
//            
//////                    CobrancaFatura fatura = new CobrancaFatura();
//////                    fatura.setDtEmissao(new Date());
//////                    fatura.setDtVencimento(vencimento);
//////                    fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
//////                    fatura.setValor(valor + valorAdicional);
//////                    fatura.setRegistro(registro);
//////                    new CobrancaFaturaJpaController(emf).create(fatura);
//////                                       
//////                    //se não é fatura de visita        
//////                    if(visita == null){
//////                        cliente.setDtUltimaFatura(new Date());
////////                            json.getJSONObject("subscription").getInt("id"));
//////                        new ClienteJpaController(emf).edit(cliente);
//////                    }
//        } catch(Exception e) {
//            houveErro = true;
//            e.printStackTrace();
//        }finally{
//            if(houveErro){
//                Mailer.send(null,null,null,null, "Erro Cadastra Fatura Vindi Lis", "Cliente: " + cobranca.getCliente() + "\n" + params, "jooohg@gmail.com");
//            }
//        }
//    }
    
    public void buscarVencimentosRecorrenciasDiario() throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        List<Cliente> clientes = null;
        boolean houveErro = false;
        System.out.println("buscarVencimentosRecorrenciasDiario");
        try {
//            ArrayList<Integer> meses_periodicidades = new ArrayList<>();
//            meses_periodicidades.add(12); // anual
//            meses_periodicidades.add(6); // semestral
//            meses_periodicidades.add(3); // trimestral
//            meses_periodicidades.add(4); // quadrimestral
//            
//            for (Integer mes_periodicidade : meses_periodicidades) {
//                Calendar calendar = Calendar.getInstance(); 
//                calendar.add(Calendar., -mes_periodicidade);
//                System.out.println(calendar.getTime());
                Date dtVencimento = addDays(new Date(), 7);
                clientes = new ClienteJpaController(emf).findClientesRecorrenciasVencidas(dtVencimento);
                for (Cliente cliente : clientes) {
                    System.out.println(cliente.getId() + " " + cliente.getNomeFantasia());
                    if(cliente.getAdimplencia() != 3){
                        if(cliente.getPeriodicidade() == 1){
                            criarCobrancaRecorrenciaVindi(cliente);
                        }else if(cliente.getPeriodicidade() == 3 || cliente.getPeriodicidade() == 4 || cliente.getPeriodicidade() == 6){
                            criarParametrosCobrancaRecorrenciaBF(cliente, true);
                        }
                    }
                }
//            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro buscarVencimentosRecorrenciasDiario", null, "jooohg@gmail.com");
            }
        }
    }
    
    public void cadastrarFaturaFromImportacaoBF() throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("cadastrarFaturaFromImportacaoBF");
        try {
            List<CobrancaBfImportar> cobrancas = new CobrancaBfImportarJpaController(emf).findCobrancaBfImportarEntities();
            for (CobrancaBfImportar cobranca : cobrancas) {
                CobrancaFatura verificarfatura = new CobrancaFaturaJpaController(emf).findCobrancaFaturaByCodeBF(cobranca.getCode());
                if(verificarfatura == null){
//                    System.out.println("CNPJ " + cobranca.getCnpjCpf().replace(".", "").replace("/", "").replace("-", ""));
                    Cliente cliente = null;
                    try{
                        cliente = new ClienteJpaController(emf).findByCnpj(cobranca.getCnpjCpf().replace(".", "").replace("/", "").replace("-", ""));
                        if(cliente != null){
    //                        System.out.println(cliente.getId());
                            CobrancaFatura fatura = new CobrancaFatura();
                            fatura.setCliente(cliente);
                            fatura.setCodeBf(cobranca.getCode());
                            fatura.setDescricaoFatura(cobranca.getDescricao());
                            fatura.setDtEmissao(cobranca.getDtEmissao());
                            fatura.setDtVencimento(cobranca.getDtVencimento());
                            fatura.setDtPagamento(cobranca.getDtPagamento());
                            fatura.setValor(cobranca.getValor());
                            fatura.setValorPago(cobranca.getValorPago());
                            fatura.setReferenceBf("juridica");

                            String status = cobranca.getStatus();
                            if(status.equals("Pago")){
                                fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(3));
                            }else if(status.equals("Pago com valor difer")){
                                fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(7));
                            }else if(status.equals("Cancelado")){
                                fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(2));
                            }else if(status.equals("Vencido")){
                                fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(5));
                            }else if(status.equals("Aguardando Pagamento")){
                                fatura.setStatus(new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatus(1));
                            }

                            String descricao = cobranca.getDescricao();
                            if(descricao.contains("anuidade") || descricao.contains("Anuidade")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(2));
                            }else if(descricao.contains("Visita") || descricao.contains("visita")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(1));
                            }else if(descricao.contains("trimestral") || descricao.contains("Trimestral") || descricao.contains("Trimestralidade")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(3));
                            }else if(descricao.contains("quadrimestral") || descricao.contains("Quadrimestralidade") || descricao.contains("Quadrimestral")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(4));
                            }else if(descricao.contains("Rescisao") || descricao.contains("rescisao")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(5));
                            }else if(descricao.contains("Acordo")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(6));
                            }else if(descricao.contains("Mensalidade") || descricao.contains("Mensalidades") || descricao.contains("mensalidades")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(7));
                            }else if(descricao.contains("semestral") || descricao.contains("Semestral")){
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(10));
                            }else {
                                fatura.setRegistro(new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferencia(8));
                            }

                            new CobrancaFaturaJpaController(emf).create(fatura);
                        }else{
                            System.out.println("Cliente não encontrato da cobranca: " + cobranca.getCnpjCpf() + " " + cobranca.getNome());
                        }
                    }catch(Exception e){
                        System.out.println("Cliente não encontrato da cobranca: " + cobranca.getCnpjCpf() + " " + cobranca.getNome());
                        continue;
                    }
                }
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            emf.close();
            if(houveErro){
                Mailer.send(null,null,null,null, "Erro buscarVencimentosRecorrenciasDiario", null, "jooohg@gmail.com");
            }
        }
    }
}