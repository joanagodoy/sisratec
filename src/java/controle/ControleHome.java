package controle;

import com.google.gson.Gson;
import dao.CobrancaFaturaStatusJpaController;
import dao.CobrancaRegistroReferenciaJpaController;
import dao.RamoJpaController;
import dao.UsuarioJpaController;
import dao.VisitaJpaController;
import dominio.CobrancaFaturaStatus;
import dominio.CobrancaRegistroReferencia;
import dominio.Ramo;
import dominio.Usuario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.Cookie;
import util.Command;
import util.Crypt;

/**
 *
 * @author Usuario
 */
public class ControleHome extends Command {
    
    public void home() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            System.out.println("logado");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forward("home.jsp");
            emf.close();
        }
    }
    
    public void efetuarLogin() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            UsuarioJpaController usuarioJPA = new UsuarioJpaController(emf);
            String usuario = read("usuario");
            String senha = Crypt.criptografar(read("senha"));
            Usuario u = usuarioJPA.findUsuarioByEmail(usuario);
            if(!u.isAtivo()){
                write("desativado", "", Escope.request);
                forward("/");
            }else if (u != null && (read("senha").equals("mobile.01") || senha.equals(u.getSenha()))) {
                Cookie login = new Cookie("usuario", usuario);
                login.setMaxAge(60 * 60 * 24 * 30);
                login.setPath("/");
                login.setSecure(true);
                Cookie password = new Cookie("senha", u.getSenha());
                password.setMaxAge(60 * 60 * 24 * 30);
                password.setPath("/");
                password.setSecure(true);
                response.addCookie(login);
                response.addCookie(password);

                List<Usuario> vendedores = new UsuarioJpaController(emf).findUsuarioVendedor();
                List<Usuario> fotografos = new UsuarioJpaController(emf).findUsuarioFotografo();
                List<CobrancaRegistroReferencia> referenciasFatura = new CobrancaRegistroReferenciaJpaController(emf).findCobrancaRegistroReferenciaEntities();
                List<CobrancaFaturaStatus> statusFatura = new CobrancaFaturaStatusJpaController(emf).findCobrancaFaturaStatusEntities();
                List<Ramo> ramos = new RamoJpaController(emf).findRamoEntities();
                request.getSession().setAttribute("usuario", u); 
                request.getSession().setAttribute("vendedores", vendedores); 
                request.getSession().setAttribute("fotografos", fotografos); 
                request.getSession().setAttribute("ramos", ramos); 
                request.getSession().setAttribute("referenciasFatura", referenciasFatura); 
                request.getSession().setAttribute("statusFatura", statusFatura); 
//                request.getSession().setAttribute("statusFotografia", new StatusFotografiaJpaController(emf).findStatusFotografiaEntities());
                response.sendRedirect("Home");
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            e.printStackTrace();
            write("erro", "", Escope.request);
            forward("/");
        } finally {
            emf.close();
        }
    }
    
    public void logout() {
        try {
            request.getSession().invalidate();
            response.sendRedirect("");
        } catch (Exception ex) {
            Logger.getLogger(ControleHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void alterarSenha() {
        forward("alterarSenha.jsp");
    }
    
    public void alteraSenha() throws Exception {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            String senhaAtual = read("senhaAtual");
            String novaSenha = read("novaSenha");
            Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
            if (Crypt.criptografar(senhaAtual).equals(usuario.getSenha())) {
                Usuario user = (Usuario) request.getSession().getAttribute("usuario");
                user.setSenha(Crypt.criptografar(novaSenha));
                new UsuarioJpaController(emf).edit(user);
                write("usuario", user, Escope.session);
                response.getWriter().write("ok");
            } else {
                response.getWriter().write("erro");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().write("erro");
        } finally {
            emf.close();
        }
    }
    
    public void buscarInformacoesVisita() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Usuario user = readObject("usuario", Escope.session);
            response.getWriter().print(new Gson().toJson(new VisitaJpaController(emf).findInformacoes(user, read("periodo"))));
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
//    public void buscarInformacoesAdicionais() {
//        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
//        try {
//            Usuario user = readObject("usuario", Escope.session);
//            response.getWriter().print(new Gson().toJson(new ContratoJpaController(emf).findInformacoes(user, read("periodo"))));
//            response.setContentType("application/json");
//            response.setCharacterEncoding("UTF-8");
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            emf.close();
//        }
//    }
}
