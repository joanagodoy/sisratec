package controle;

import dao.UsuarioJpaController;
import dao.VisitaJpaController;
import dominio.Usuario;
import dominio.Visita;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import util.Command;
import util.DecimalConverter;

/**
 *
 * @author Usuario
 */
public class ControleComissao extends Command {
    
    public void buscarValorComissaoVendedor() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        Usuario user = readObject("usuario", Escope.session);
        try {
            Usuario vendedor = null;
            if(user.getTipo().getId() != 4){
                try{
                    vendedor = new UsuarioJpaController(emf).findUsuario(Integer.parseInt(read("vendedor")));
                }catch(Exception e){

                }                
            }else{
                vendedor = user;
            }
            Date dataInicial = null;
            Date dataFinal = null;
            try {
                dataInicial = new SimpleDateFormat("dd/MM/yyyy").parse(read("dataInicial"));
            } catch (Exception e) {
//                    e.printStackTrace();
            }
            try {
                dataFinal = new SimpleDateFormat("dd/MM/yyyy").parse(read("dataFinal"));
            } catch (Exception e) {
//              e.printStackTrace();
            }
            
            //BUSCAR VISITAS REALIZADAS OU PAGAS NO PERIODO
            List<Visita> visitas = new VisitaJpaController(emf).findVisitas(dataInicial, dataFinal, vendedor);
            Double totalComissao = 0.0;
            
            if (read("excel") == null) {
                for (Visita visita : visitas) {
                    Integer totalContrato = 1000+(100*visita.getPtsVista());
                    totalComissao += totalContrato * 0.035;
                }
                response.getWriter().print(totalComissao);
            } else {
                HSSFWorkbook workbook = new HSSFWorkbook();
                HSSFSheet firstSheet = workbook.createSheet("Relatório");
                CellStyle styleHeader = workbook.createCellStyle();
                styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
                HSSFFont fontHeader = workbook.createFont();
                fontHeader.setFontHeightInPoints((short) 12);
                fontHeader.setFontName("Arial");
                fontHeader.setColor(IndexedColors.BLACK.getIndex());
                fontHeader.setBoldweight(Font.BOLDWEIGHT_BOLD);
                fontHeader.setItalic(false);
                styleHeader.setFont(fontHeader);
                CellStyle styleDefault = workbook.createCellStyle();
                styleDefault.setAlignment(CellStyle.ALIGN_CENTER);
                HSSFFont fontDefault = workbook.createFont();
                fontDefault.setFontHeightInPoints((short) 10);
                fontDefault.setFontName("Arial");
                fontDefault.setColor(IndexedColors.BLACK.getIndex());
                fontDefault.setBoldweight((short) 0);
                fontDefault.setItalic(false);
                styleHeader.setFont(fontHeader);
                firstSheet.createFreezePane(0, 1);
                HSSFRow row = firstSheet.createRow(0);
                row.createCell(0).setCellValue("Estabelecimento");
                row.getCell(0).setCellStyle(styleHeader);
                row.createCell(1).setCellValue("Data Venda");
                row.getCell(1).setCellStyle(styleHeader);
                row.createCell(2).setCellValue("Data Visita");
                row.getCell(2).setCellStyle(styleHeader);
                row.createCell(3).setCellValue("Pontos de Vista");
                row.getCell(3).setCellStyle(styleHeader);
                row.createCell(4).setCellValue("Total Comissão");
                row.getCell(4).setCellStyle(styleHeader);
                row.createCell(5).setCellValue("Vendedor");
                row.getCell(5).setCellStyle(styleHeader);
                firstSheet.setColumnWidth(0, 8000);
                firstSheet.setColumnWidth(1, 8000);
                firstSheet.setColumnWidth(2, 8000);
                firstSheet.setColumnWidth(3, 8000);
                firstSheet.setColumnWidth(4, 8000);
                firstSheet.setColumnWidth(5, 8000);
                int i = 1;
                for (Visita visita : visitas) {
                    row = firstSheet.createRow(i);
                    row.createCell(0).setCellValue(visita.getCliente().getNomeFantasia());
                    row.getCell(0).setCellStyle(styleDefault);
                    row.createCell(1).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(visita.getNrContrato().getDtVenda()));
                    row.getCell(1).setCellStyle(styleDefault);
                    row.createCell(2).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(visita.getDtVisita()));
                    row.getCell(2).setCellStyle(styleDefault);
                    row.createCell(3).setCellValue(visita.getPtsVista());
                    row.getCell(3).setCellStyle(styleDefault);
                    Double comissao = 0.0;
                    if(visita.getPtsVista() != 0){
                        Integer totalContrato = 1000+(100*visita.getPtsVista());
                        comissao = totalContrato * 0.035;
                        totalComissao += comissao; 
                    }
                    row.createCell(4).setCellValue(DecimalConverter.converterDecimalEmStringComRS(BigDecimal.valueOf(comissao)));
                    row.getCell(4).setCellStyle(styleDefault);
                    row.createCell(5).setCellValue(visita.getCliente().getVendedor().getNome());
                    row.getCell(5).setCellStyle(styleDefault);
                    i++;
                }
                row = firstSheet.createRow(i);
                row.createCell(4).setCellValue(DecimalConverter.converterDecimalEmStringComRS(BigDecimal.valueOf(totalComissao)));
                row.getCell(4).setCellStyle(styleDefault);
                row.createCell(3).setCellValue("Soma");
                row.getCell(3).setCellStyle(styleHeader);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                workbook.write(bos);
                bos.flush();
                bos.close();
                response.setHeader("Set-Cookie", "fileDownload=true; path=/");
                response.setHeader("Content-disposition", "attachment; filename=Relatorio_Comissão_Vendedor.xls");
                response.setContentType("application/vnd.ms-excel");
                response.getOutputStream().write(bos.toByteArray());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscarValorComissaoFotografo() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        Usuario user = readObject("usuario", Escope.session);
        try {
            Usuario fotografo = null;
            if(user.getTipo().getId() != 4 && user.getTipo().getId() != 3){
                try{
                    fotografo = new UsuarioJpaController(emf).findUsuario(Integer.parseInt(read("fotografo")));
                }catch(Exception e){

                }                
            }else{
                fotografo = user;
            }
            Date dataInicial = null;
            Date dataFinal = null;
            try {
                dataInicial = new SimpleDateFormat("dd/MM/yyyy").parse(read("dataInicial"));
            } catch (Exception e) {
//                    e.printStackTrace();
            }
            try {
                dataFinal = new SimpleDateFormat("dd/MM/yyyy").parse(read("dataFinal"));
            } catch (Exception e) {
//              e.printStackTrace();
            }
            
            //BUSCAR VISITAS REALIZADAS OU PAGAS NO PERIODO
            List<Visita> visitas = new VisitaJpaController(emf).findVisitasByFotografo(dataInicial, dataFinal, fotografo);
            Double totalComissaoFotografo = 0.0;
            Double totalComissaoEditor = 0.0;
            
            if (read("excel") == null) {
                for (Visita visita : visitas) {
//                    int totalPontosAMais = visita.getPtsVistaRealizado() - visita.getPtsVista();
//                    System.out.println(totalPontosAMais);
                    totalComissaoFotografo += 45.00 + visita.getPtsVistaRealizado();
                }
                response.getWriter().print(totalComissaoFotografo);
            } else {
                HSSFWorkbook workbook = new HSSFWorkbook();
                HSSFSheet firstSheet = workbook.createSheet("Relatório");
                CellStyle styleHeader = workbook.createCellStyle();
                styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
                HSSFFont fontHeader = workbook.createFont();
                fontHeader.setFontHeightInPoints((short) 12);
                fontHeader.setFontName("Arial");
                fontHeader.setColor(IndexedColors.BLACK.getIndex());
                fontHeader.setBoldweight(Font.BOLDWEIGHT_BOLD);
                fontHeader.setItalic(false);
                styleHeader.setFont(fontHeader);
                CellStyle styleDefault = workbook.createCellStyle();
                styleDefault.setAlignment(CellStyle.ALIGN_CENTER);
                HSSFFont fontDefault = workbook.createFont();
                fontDefault.setFontHeightInPoints((short) 10);
                fontDefault.setFontName("Arial");
                fontDefault.setColor(IndexedColors.BLACK.getIndex());
                fontDefault.setBoldweight((short) 0);
                fontDefault.setItalic(false);
                styleHeader.setFont(fontHeader);
                firstSheet.createFreezePane(0, 1);
                HSSFRow row = firstSheet.createRow(0);
                row.createCell(0).setCellValue("Estabelecimento");
                row.getCell(0).setCellStyle(styleHeader);
                row.createCell(1).setCellValue("Data Venda");
                row.getCell(1).setCellStyle(styleHeader);
                row.createCell(2).setCellValue("Data Visita");
                row.getCell(2).setCellStyle(styleHeader);
                row.createCell(3).setCellValue("Pontos de Vista Vendidos");
                row.getCell(3).setCellStyle(styleHeader);
                row.createCell(4).setCellValue("Pontos de Vista Realizados");
                row.getCell(4).setCellStyle(styleHeader);
                row.createCell(5).setCellValue("Dif. Pontos de Vista");
                row.getCell(5).setCellStyle(styleHeader);
                row.createCell(6).setCellValue("Vl. Comissão Fotógrafo");
                row.getCell(6).setCellStyle(styleHeader);
                 row.createCell(7).setCellValue("Vl. Comissão Editor");
                row.getCell(7).setCellStyle(styleHeader);
                row.createCell(8).setCellValue("Fotógrafo");
                row.getCell(8).setCellStyle(styleHeader);
                row.createCell(9).setCellValue("Região");
                row.getCell(9).setCellStyle(styleHeader);
                firstSheet.setColumnWidth(0, 8000);
                firstSheet.setColumnWidth(1, 8000);
                firstSheet.setColumnWidth(2, 8000);
                firstSheet.setColumnWidth(3, 8000);
                firstSheet.setColumnWidth(4, 8000);
                firstSheet.setColumnWidth(5, 8000);
                firstSheet.setColumnWidth(6, 8000);
                firstSheet.setColumnWidth(7, 8000);
                firstSheet.setColumnWidth(8, 8000);
                firstSheet.setColumnWidth(8, 8000);
                int i = 1;
                for (Visita visita : visitas) {
                    row = firstSheet.createRow(i);
                    row.createCell(0).setCellValue(visita.getCliente().getNomeFantasia());
                    row.getCell(0).setCellStyle(styleDefault);
                    row.createCell(1).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(visita.getNrContrato().getDtVenda()));
                    row.getCell(1).setCellStyle(styleDefault);
                    row.createCell(2).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(visita.getDtVisita()));
                    row.getCell(2).setCellStyle(styleDefault);
                    row.createCell(3).setCellValue(visita.getPtsVista());
                    row.getCell(3).setCellStyle(styleDefault);
                    row.createCell(4).setCellValue(visita.getPtsVistaRealizado());
                    row.getCell(4).setCellStyle(styleDefault);
                    Double comissaoFotógrafo = 0.0;
                    Double comissaoEditor = 0.0;
                    if(visita.getPtsVista() != 0){
                        int totalPontosAMais = visita.getPtsVistaRealizado() - visita.getPtsVista();
//                        System.out.println(totalPontosAMais);
                        comissaoFotógrafo = 45.00 + visita.getPtsVistaRealizado();
                        totalComissaoFotografo += comissaoFotógrafo;
                        
                        row.createCell(5).setCellValue(totalPontosAMais);
                        row.getCell(5).setCellStyle(styleDefault);
                        
                        comissaoEditor = 19.50 + (visita.getPtsVistaRealizado()/5)*8;
                        
                    }
                    row.createCell(6).setCellValue(DecimalConverter.converterDecimalEmStringComRS(BigDecimal.valueOf(comissaoFotógrafo)));
                    row.getCell(6).setCellStyle(styleDefault);
                    row.createCell(7).setCellValue(DecimalConverter.converterDecimalEmStringComRS(BigDecimal.valueOf(comissaoEditor)));
                    row.getCell(7).setCellStyle(styleDefault);
                    row.createCell(8).setCellValue(visita.getFotografo().getNome());
                    row.getCell(8).setCellStyle(styleDefault);
                    row.createCell(9).setCellValue(visita.getRegiao().getDescricao());
                    row.getCell(9).setCellStyle(styleDefault);
                    i++;
                }
                row = firstSheet.createRow(i);
                row.createCell(6).setCellValue(DecimalConverter.converterDecimalEmStringComRS(BigDecimal.valueOf(totalComissaoFotografo)));
                row.getCell(6).setCellStyle(styleDefault);
                row.createCell(5).setCellValue("Soma");
                row.getCell(5).setCellStyle(styleHeader);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                workbook.write(bos);
                bos.flush();
                bos.close();
                response.setHeader("Set-Cookie", "fileDownload=true; path=/");
                response.setHeader("Content-disposition", "attachment; filename=Relatorio_Comissão_Fotógrafo.xls");
                response.setContentType("application/vnd.ms-excel");
                response.getOutputStream().write(bos.toByteArray());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
}
