/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import dao.CidadeJpaController;
import dao.ClienteJpaController;
import dao.CobrancaFaturaJpaController;
import dao.ContratoJpaController;
import dao.EnderecoJpaController;
import dao.RamoJpaController;
import dao.UsuarioJpaController;
import dominio.Cliente;
import dominio.CobrancaFatura;
import dominio.Contrato;
import dominio.Endereco;
import dominio.Ramo;
import dominio.Usuario;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import util.Command;
import util.GsonExclusion;
import util.Mailer;

/**
 *
 * @author Usuario
 */
public class ControleCliente extends Command {
    
    String emailMaster = "joana.isratec@gmail.com";
    
    public String removerMascara(String str){
        return str.replaceAll("\\D", "");
    }
    
    public void cliente() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
//            response.setContentType("application/json");
//            response.setCharacterEncoding("UTF-8");
            System.out.println(read("id"));
            if (read("id") != null) {
                Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("id")));    
//                response.getWriter().print(GsonExclusion.GSON().toJson(cliente));
                write("cliente", cliente, Escope.request);
                forward("cliente.jsp");
            }else{
                forward("cliente_novo.jsp");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscarCliente() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            if (read("id") != null) {
                Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("id")));    
                response.getWriter().print(GsonExclusion.GSON().toJson(cliente));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void buscarClientes() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Usuario user = readObject("usuario", Escope.session);
            System.out.println(read("filtro"));
            if(read("contrato") != null){
                if(user.getTipo().getId().equals(4)){//se usuario Vendedor
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findSemContratoByVendedor(user)));
                }else{
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteSemContrato()));
                }  
//            }else if(read("ativo") != null){
//                response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteByStatus(true)));
            }else if(read("filtro") != null){
                if(read("filtro").equals("inativos")){
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteByStatus(false)));
                }else if(read("filtro").equals("adimplentes")){
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClientesByAdimplencia(1)));
                }else if(read("filtro").equals("inadimplentes")){
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClientesByAdimplencia(2)));
                }else if(read("filtro").equals("advogados")){
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClientesByAdimplencia(3)));
                }
            }else{
                if(user.getTipo().getId().equals(4)){//se usuario Vendedor
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findByVendedor(user)));
                }else{
                    response.getWriter().print(GsonExclusion.GSON().toJson(new ClienteJpaController(emf).findClienteEntities()));
                }  
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            String conteudo = "";
            Usuario user = readObject("usuario", Escope.session);
            Date date = new Date();
            Ramo ramo = new RamoJpaController(emf).findRamo(Integer.parseInt(read("ramo")));
            if("".equals(read("id")) || null == read("id")){ //NOVO
                Endereco endereco = new Endereco();
                endereco.setBairro(read("bairro"));
                endereco.setCep(read("cep"));
                endereco.setCidade(new CidadeJpaController(emf).findByNome(read("cidade")));
                endereco.setUf(read("uf"));
                endereco.setNumero(read("numero"));
                endereco.setRua(read("rua"));
                endereco.setComplemento(read("complemento"));
                new EnderecoJpaController(emf).create(endereco);
                Cliente cliente = new Cliente();
                cliente.setCnpj(removerMascara(read("cnpj")));
                if(read("inscricao") == null || "".equals(read("inscricao"))){
                    cliente.setRg(removerMascara(read("rg")));
                }else{
                    cliente.setInscricao(read("inscricao"));
                }
                cliente.setEmail(read("email").toLowerCase());
                cliente.setEmail2(read("email2").toLowerCase());
                cliente.setEndereco(new EnderecoJpaController(emf).findEndereco(endereco.getId()));
                cliente.setNomeFantasia(read("fantasia").toUpperCase());
                cliente.setRazSocial(read("razSocial").toUpperCase());
                cliente.setResponsavel(read("responsavel").toUpperCase());
                cliente.setTelefone1(removerMascara(read("telefone")));
                cliente.setTelefone2(removerMascara(read("telefone2")));
                cliente.setTelefone3(removerMascara(read("telefone3")));
                cliente.setUsuario(user);
//                cliente.setDtCobranca(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataCobranca")));
                cliente.setRamo(ramo);
                if(user.getTipo().getId()!=4){
                    cliente.setVendedor(new UsuarioJpaController(emf).findUsuario(Integer.parseInt(read("vendedorCliente"))));
                }else{
                    cliente.setVendedor(user);
                }
                cliente.setAtivo(true);
                cliente.setPossuiOutroAtivo(false);
                
                Cliente cnpj = new ClienteJpaController(emf).findByCnpjAtivo(removerMascara(read("cnpj")));
                if(cnpj != null){
                    response.getWriter().print("CNPJ;"+cnpj.getNomeFantasia());
//                    response.sendError(HttpServletResponse.SC_REQUEST_URI_TOO_LONG);
                }else{
                    cliente.setDtCriacao(date);
                    conteudo = "Cliente ID:" + cliente.getId() + "\n ; Fantasia:" + cliente.getNomeFantasia() + "\n ; Email1: " + cliente.getEmail() + "\n ; Email2: " + cliente.getEmail2() + " CRIADO";
                    Mailer.send(null, null, null, null, "Criação Cliente", conteudo, emailMaster);
                    new ClienteJpaController(emf).create(cliente);
                    System.out.println(cliente.getId());
                    response.getWriter().print("OK;"+cliente.getId());
                }                
            }else{//EDITAR
                Cliente findCliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("id")));
                findCliente.setDtAlteracao(date);
                findCliente.setCnpj(removerMascara(read("cnpj")));
                if(read("inscricao") == null || "".equals(read("inscricao"))){
                    findCliente.setRg(removerMascara(read("rg")));
                }else{
                    findCliente.setInscricao(read("inscricao"));
                }
                findCliente.setEmail(read("email").toLowerCase());
                findCliente.setEmail2(read("email2").toLowerCase());
                findCliente.setNomeFantasia(read("fantasia").toUpperCase());
                findCliente.setRazSocial(read("razSocial").toUpperCase());
                findCliente.setResponsavel(read("responsavel").toUpperCase());
                findCliente.setTelefone1(removerMascara(read("telefone")));
                findCliente.setTelefone2(removerMascara(read("telefone2")));
                findCliente.setTelefone3(removerMascara(read("telefone3")));
                findCliente.setUsuario(user);
                try{
                    findCliente.setDtCobranca(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataCobranca")));
                }catch(Exception e){
                    findCliente.setDtCobranca(null);
                }
                findCliente.setRamo(ramo);
                if(user.getTipo().getId()!=4){
                    findCliente.setVendedor(new UsuarioJpaController(emf).findUsuario(Integer.parseInt(read("vendedorCliente"))));
                }else{
                    findCliente.setVendedor(user);
                }
               
                if(!"on".equals(read("ativo"))){
                    findCliente.setAtivo(false);
                    if("on".equals(read("possuioutroativo"))){
                        findCliente.setPossuiOutroAtivo(true);
                        //email informando foi inativado mas possui outro ativo
                        conteudo = "Cliente ID:" + findCliente.getId() + "\n ; Fantasia:" + findCliente.getNomeFantasia() + "\n ; Email1: " + findCliente.getEmail() + "\n ; Email2: " + findCliente.getEmail2() + " desativado mas POSSUI outro ativo";
                    }else{
                        conteudo = "Cliente ID:" + findCliente.getId() + "\n ; Fantasia:" + findCliente.getNomeFantasia() + "\n ; Email1: " + findCliente.getEmail() + "\n ; Email2: " + findCliente.getEmail2() + " desativado SEM outro ativo";
                    }
                }else{
                    //se cliente não estava ativo e foi ativado..
                    if(!findCliente.isAtivo()){
                        conteudo = "Cliente ID:" + findCliente.getId() + "\n ; Fantasia:" + findCliente.getNomeFantasia() + "\n ; Email1: " + findCliente.getEmail() + "\n ; Email2: " + findCliente.getEmail2() + " ATIVADO";
                    }
                    findCliente.setAtivo(true);
                    findCliente.setPossuiOutroAtivo(false);
                }
                
                Mailer.send(null, null, null, null, "Ativação/Inativação Cliente", conteudo, emailMaster);
                
//                if(!"on".equals(read("possuioutroativo"))){
//                    findCliente.setPossuiOutroAtivo(false);
//                }
                
                Endereco endereco;
                //se cod Endereço = 1 é pq foi importado, então cria novo endereço.
                if(findCliente.getEndereco().getId().equals(1)){
                    endereco = new Endereco();
                    endereco.setBairro(read("bairro"));
                    endereco.setCep(read("cep"));
                    endereco.setCidade(new CidadeJpaController(emf).findByNome(read("cidade")));
                    endereco.setUf(read("uf"));
                    endereco.setNumero(read("numero"));
                    endereco.setRua(read("rua"));
                    endereco.setComplemento(read("complemento"));
                    new EnderecoJpaController(emf).create(endereco);
                    findCliente.setEndereco(endereco);
                }else{
                    endereco = new EnderecoJpaController(emf).findEndereco(findCliente.getEndereco().getId());
                    endereco.setBairro(read("bairro"));
                    endereco.setCep(read("cep"));
                    endereco.setCidade(new CidadeJpaController(emf).findByNome(read("cidade")));
                    endereco.setUf(read("uf"));
                    endereco.setNumero(read("numero"));
                    endereco.setRua(read("rua"));
                    endereco.setComplemento(read("complemento"));
                    new EnderecoJpaController(emf).edit(endereco);
                }
                
//                findCliente.setId(Integer.parseInt(read("id")));
                if(user.getTipo().getId().equals(4)){//se usuario vendedor
//                    System.out.println(new Date().getTime());
//                    System.out.println(findCliente.getDtSistema().getTime());
                    long diffInMillis = new Date().getTime() - findCliente.getDtCriacao().getTime();
//                    System.out.println(diffInMillis/1000/60);
                    if(diffInMillis/1000/60 > 15){ // se data de alteração maior que 15 min da criação
                        response.sendError(HttpServletResponse.SC_CONFLICT);
                    }else{
                        new ClienteJpaController(emf).edit(findCliente);
                    }
                }else{
                    new ClienteJpaController(emf).edit(findCliente);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void atualizaPtsVistaClientes() throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("atualizaPtsVistaClientes");
        try {
            List<Cliente> clientes = new ClienteJpaController(emf).findByUltPtsVista(true, 0);
            for (Cliente cliente : clientes) {
                //encontra outro cadastro do cliente
//                Cliente outroCliente = new ClienteJpaController(emf).findByCnpj(cliente.getCnpj());
//                cliente.setUltPtsVista(outroCliente.getUltPtsVista());
//                cliente.setUltPtsVistaMultp(outroCliente.getUltPtsVistaMultp());
//                new ClienteJpaController(emf).edit(cliente);
                
                
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            emf.close();
        }
    }
    
    public void buscarQtdTabsClientes() throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        System.out.println("buscarQtdTabsClientes");
        try {
            EntityManager em = emf.createEntityManager();
            List<Object> result = em.createNativeQuery("SELECT COUNT(*) FROM cliente WHERE ativo = 1 GROUP BY adimplencia UNION  SELECT COUNT(*) from cliente WHERE ativo = 0").getResultList();
            String resultado = "";
            for (Object qtds : result) {
                resultado += ";"+qtds;
            }
            System.out.println(resultado);
            response.getWriter().print(resultado);
        } catch(Exception e) {
            e.printStackTrace();
        }finally{
            emf.close();
        }
    }
    
    public void atualizaPeriodicidadeClientes() throws IOException, Exception{
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean houveErro = false;
        System.out.println("atualizaPeriodicidadeClientes");
        try {
            List<Cliente> clientes = new ClienteJpaController(emf).findClienteByStatus(true);
            for (Cliente cliente : clientes) {
                Contrato contrato = new ContratoJpaController(emf).findByCliente(cliente);
                if(cliente.getPeriodicidade() == null){
                    CobrancaFatura fatura = new CobrancaFaturaJpaController(emf).findUltCobrancaRecorrenteByCliente(cliente);
                    if(fatura != null){
                    if(fatura.getRegistro().getId() == 2){
                        cliente.setPeriodicidade(1);
                    }else if(fatura.getRegistro().getId() == 3){
                        cliente.setPeriodicidade(3);
                    }else if(fatura.getRegistro().getId() == 4){
                        cliente.setPeriodicidade(4);
                    }else if(fatura.getRegistro().getId() == 7){
                        cliente.setPeriodicidade(12);
                    }else if(fatura.getRegistro().getId() == 9){
                        cliente.setPeriodicidade(0);
                    }else if(fatura.getRegistro().getId() == 10){
                        cliente.setPeriodicidade(6);
                    }else if(fatura.getRegistro().getId() == 11){
                        cliente.setPeriodicidade(1);
                    }
                    new ClienteJpaController(emf).edit(cliente);
                    
                    contrato.setPagamento(cliente.getPeriodicidade());
                    new ContratoJpaController(emf).edit(contrato);
                    }
                }else{
                    //verifica se está correto com contrato
                    if(contrato.getPagamento() != cliente.getPeriodicidade()){
                        System.out.println("PERIODICIDADES DIVERGEM: Cliente:" + cliente.getId() + " " + cliente.getNomeFantasia());
                    }
                }             
            }
        } catch(Exception e) {
            houveErro = true;
            e.printStackTrace();
        }finally{
            emf.close();
        }
    }
    
}
