/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import dao.CidadeJpaController;
import dao.ClienteJpaController;
import dao.ContratoJpaController;
import dao.EnderecoJpaController;
import dao.FotografiaJpaController;
import dao.ImovelJpaController;
import dao.RegiaoJpaController;
import dao.StatusFotografiaJpaController;
import dao.StatusVisitaJpaController;
import dao.VisitaJpaController;
import dominio.Cliente;
import dominio.Contrato;
import dominio.Endereco;
import dominio.Fotografia;
import dominio.Imovel;
import dominio.StatusFotografia;
import dominio.Usuario;
import dominio.Visita;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.time.DateUtils;
import util.Command;
import util.GsonExclusion;
import util.Mailer;





/**
 *
 * @author Usuario
 */
public class ControleVisita extends Command {
    
    public String removerMascara(String str){
        return str.replaceAll("\\D", "");
    }
    
    public void listagem(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {         
            write("open", false, Escope.request);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forward("visita_listagem.jsp");
            emf.close();
        }
    }
    
    public void visita(){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {            
            if(read("u") != null){
                write("open", true, Escope.request);
            }else{
                write("open", false, Escope.request);
            }
//            write("regioes", new RegiaoJpaController(emf).findRegiaoEntities(), Escope.request);
//            request.setAttribute("regioes", new RegiaoJpaController(emf).findRegiaoEntities());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forward("visita.jsp");
            emf.close();
        }
    }
    
    public void buscarVisita() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Usuario user = readObject("usuario", Escope.session);
            Visita visita = new VisitaJpaController(emf).findVisita(Integer.parseInt(read("id")));
//            if (user.getTipo().getId() != 4 || visita.getUsuario().getId().equals(user.getId())) {
                response.getWriter().print(GsonExclusion.GSON().toJson(visita));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void finalizar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            boolean registrarCobrancaVisita = false;
            Usuario user = readObject("usuario", Escope.session);
            Visita visita = new VisitaJpaController(emf).findVisita(Integer.parseInt(read("idVisita")));
            visita.setDtSistema(new Date());
            if(read("status").equals("2")){
                visita.setFotografo(user);
                visita.setPtsVistaRealizado(Integer.parseInt(read("pts")));
                
                //atualiza pontos de vista do cliente
                Cliente cliente = new ClienteJpaController(emf).findCliente(visita.getCliente().getId());
                cliente.setUltPtsVista(Integer.parseInt(read("pts")));
                int pts = Integer.parseInt(read("pts"));
                 if(pts > 0 && pts <= 5){
                    cliente.setUltPtsVistaMultp(5);
                }else if(pts > 5 && pts <= 10){
                    cliente.setUltPtsVistaMultp(10);
                }else if(pts > 10 && pts <= 15){
                    cliente.setUltPtsVistaMultp(15);
                }else if(pts > 15 && pts <= 20){
                    cliente.setUltPtsVistaMultp(20);
                } else if(pts > 20 && pts <= 25){
                    cliente.setUltPtsVistaMultp(25);
                }else if(pts > 25 && pts <= 30){
                    cliente.setUltPtsVistaMultp(30);
                } else if(pts > 30 && pts <= 35){
                    cliente.setUltPtsVistaMultp(35);
                }else{
                    cliente.setUltPtsVistaMultp(Integer.parseInt(read("pts")));
                }   
                
                new ClienteJpaController(emf).edit(cliente);
                
                if(read("cobranca").equals("true")){//pagamento efetuado na hora da visita
                    visita.setStatus(new StatusVisitaJpaController(emf).findStatusVisita(4));//visita realizada e paga
                }else{
                    registrarCobrancaVisita = true;
                    visita.setStatus(new StatusVisitaJpaController(emf).findStatusVisita(2));//visita realizada
                }
                
                Fotografia foto = new Fotografia();
               // System.out.println(read("idImovel"));
                if(!read("idImovel").equals("undefined")){
                    Imovel imovel = new ImovelJpaController(emf).findImovel(Integer.parseInt(read("idImovel")));
                    foto.setImovel(imovel);
                }else{
                    foto.setImovel(null);
                }
                foto.setIdCliente(visita.getNrContrato().getCliente());
                foto.setStatus(new StatusFotografiaJpaController(emf).findStatusFotografia(1));
                foto.setFaltouPts(false);
                foto.setObs(new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " +user.getNome() + ": Visita Realizada. " + read("obs").toUpperCase());
                new FotografiaJpaController(emf).create(foto);
            }else if(read("status").equals("3")){
                visita.setStatus(new StatusVisitaJpaController(emf).findStatusVisita(3));//visita cancelada
                visita.setPtsVistaRealizado(0);
            }else if(read("status").equals("4")){
                visita.setStatus(new StatusVisitaJpaController(emf).findStatusVisita(4));//visita realizada e paga
            }
            new VisitaJpaController(emf).edit(visita);
            
            if(registrarCobrancaVisita){
                new ControleCobranca().registrarCobranca(visita.getCliente(), "visita");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
        }
    }
    
    public void buscar() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            Usuario user = readObject("usuario", Escope.session);
            List<Visita> visitas = null;
            List result = new ArrayList<>();
            SimpleDateFormat srtf = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat srdf = new SimpleDateFormat("yyyy-MM-dd");
//            boolean mostrarCancelados = Boolean.parseBoolean(read("cancelado"));
            if (read("regiao") != null && !read("regiao").isEmpty()) {
                visitas = new VisitaJpaController(emf).findVisitas(srdf.parse(read("dataInicial")), srdf.parse(read("dataFinal")), new RegiaoJpaController(emf).findRegiao(Integer.parseInt(read("regiao"))));
                for (Visita v : visitas) {
//                    if (v.getStatus().getId() == 2 && !mostrarCancelados) {
//                        continue;
//                    }
                    HashMap<String, Object> map = new HashMap();
//                    if (user.getTipo().getId() != 4 || Objects.equals(user.getId(), v.getUsuario().getId())) { //se usuario vendedor só mostra visitas dele
//                        map.put("title", "fa-user " + v.getUsuario().getNome() + (v.getUsuario().getUnidade() != null ? "\nfa-building " + v.getUsuario().getUnidade().getTorre() + " - " + v.getUsuario().getUnidade().getDescricao() : "") + (v.getUsuario().getTelefone() != null ? "\nfa-phone " + v.getUsuario().getTelefone() : ""));
//                        map.put("title", "fa-user " + v.getNrContrato().getCliente().getNomeFantasia() + "\nfa-building " + v.getNrContrato().getCliente().getEndereco().getCidade().getNome() + " - " + v.getNrContrato().getCliente().getEndereco().getBairro() + " - " + v.getNrContrato().getCliente().getEndereco().getRua() + " - " + v.getNrContrato().getCliente().getEndereco().getNumero() + ("".equals(v.getNrContrato().getCliente().getEndereco().getComplemento()) ? "" : " - " + v.getNrContrato().getCliente().getEndereco().getComplemento()));
                        map.put("title", v.getNrContrato().getCliente().getNomeFantasia() + "\n" + v.getNrContrato().getCliente().getEndereco().getCidade().getNome() + " - " + v.getNrContrato().getCliente().getEndereco().getBairro() + " - " + v.getNrContrato().getCliente().getEndereco().getRua() + " - " + v.getNrContrato().getCliente().getEndereco().getNumero() + ("".equals(v.getNrContrato().getCliente().getEndereco().getComplemento()) ? "" : " - " + v.getNrContrato().getCliente().getEndereco().getComplemento()));
                        
                            switch (v.getStatus().getId()) {
                                case 1:
                                    map.put("className", "pointer pendente");
                                    break;
                                case 2:
                                    map.put("className", "pointer realizado");
                                    break;
                                case 3:
                                    map.put("className", "pointer cancelado");
                                    break;
                                case 4:
                                    if(user.getTipo().getId().equals(1) || user.getTipo().getId().equals(2)){
                                        map.put("className", "pointer pago");
                                    }else{
                                        map.put("className", "pointer realizado");
                                    }
                                    break;
                            }                
                    map.put("id", v.getId());
                    map.put("usuario", v.getNrContrato().getUsuario().getId());
                    map.put("start", srdf.format(v.getDtVisita()) + "T" + srtf.format(v.getHrVisitaInicial()));
                    map.put("end", srdf.format(v.getDtVisita()) + "T" + srtf.format(v.getHrVisitaFinal()));
                    result.add(map);
                }
            }else if (read("regiao") == null) {
                if(read("idCliente") == null){
                    Date dataInicial = null;
                    Date dataFinal = null;
                    try {
                        dataInicial = new SimpleDateFormat("dd/MM/yyyy").parse(read("dataInicial"));
                    } catch (Exception e) {
    //                    e.printStackTrace();
                    }
                    try {
                        dataFinal = new SimpleDateFormat("dd/MM/yyyy").parse(read("dataFinal"));
                    } catch (Exception e) {
    //                    e.printStackTrace();
                    }

                    String status = read("status");
                    result = new VisitaJpaController(emf).findVisitas(user, status, dataInicial, dataFinal);
                }else{
                    Cliente cliente = new ClienteJpaController(emf).findCliente(Integer.parseInt(read("idCliente")));
                    result = new VisitaJpaController(emf).findByCliente(cliente);
                }
            }
            response.getWriter().print(GsonExclusion.GSON().toJson(result));
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
    public void salvar() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        boolean erro = false;
        String nomeCliente = "";
        String emailCliente = "";
        try {            
            Usuario user = readObject("usuario", Escope.session);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            System.out.println(read("id"));
            System.out.println(read("contrato"));
            if(read("id") != null && read("id") != ""){
                erro = true;//para não enviar e-mail de boas vindas
                Visita visita = new VisitaJpaController(emf).findVisita(Integer.parseInt(read("id")));
                visita.setDtSistema(new Date());
                visita.setDtVisita(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataVisita")));
                visita.setHrVisitaInicial(sdf.parse(read("horario")));
                Date incrementedDate = DateUtils.addHours(sdf.parse(read("horario")), 1);
                visita.setHrVisitaFinal(incrementedDate);
                visita.setObs(read("observacao").toUpperCase());
                visita.setPtsVista(Integer.parseInt(read("pts")));
                visita.setStatus(new StatusVisitaJpaController(emf).findStatusVisita(1));
                visita.setUsuario(user);
                
                if(read("contrato").split("-")[1].equals("undefined")){
                    visita.setImovel(null);
                }else{
                    Endereco endereco = visita.getImovel().getEndereco();
                    endereco.setBairro(read("bairro"));
                    endereco.setCep(read("cep"));
                    endereco.setCidade(new CidadeJpaController(emf).findByNome(read("cidade")));
                    endereco.setUf(read("uf"));
                    endereco.setNumero(read("numero"));
                    endereco.setRua(read("rua"));
                    endereco.setComplemento(read("complemento"));
                    new EnderecoJpaController(emf).edit(endereco);
                }
                
                new VisitaJpaController(emf).edit(visita);
            }else{
                Visita visita = new Visita();
                Contrato contrato = new ContratoJpaController(emf).findContrato(Integer.parseInt(read("contrato").split("-")[0]));
                contrato.setVisitaAberto(false);
                
                nomeCliente = contrato.getCliente().getResponsavel().split(" ")[0];
                emailCliente = contrato.getCliente().getEmail();
                
                visita.setNrContrato(contrato);
                visita.setCliente(contrato.getCliente());
               // System.out.println(read("contrato").split("-")[1]);
                if(read("contrato").split("-")[1].equals("undefined")){
                    visita.setImovel(null);
                }else{
                    Endereco endereco = new Endereco();
                    endereco.setBairro(read("bairro"));
                    endereco.setCep(read("cep"));
                    endereco.setCidade(new CidadeJpaController(emf).findByNome(read("cidade")));
                    endereco.setUf(read("uf"));
                    endereco.setNumero(read("numero"));
                    endereco.setRua(read("rua"));
                    endereco.setComplemento(read("complemento"));
                    Imovel imovel = new ImovelJpaController(emf).findImovel(Integer.parseInt(read("contrato").split("-")[1]));
                    if(imovel.getEndereco() == null){
                        new EnderecoJpaController(emf).create(endereco);
                    }else{
                        new EnderecoJpaController(emf).edit(endereco);
                    }
                    visita.setImovel(imovel);
                    imovel.setEndereco(endereco);
                    new ImovelJpaController(emf).edit(imovel);
                }
                Integer idRegiao = new RegiaoJpaController(emf).findRegiaoByContrato(read("contrato"));
                if(idRegiao == null){
                    response.sendError(HttpServletResponse.SC_CONFLICT);
                }
                visita.setRegiao(new RegiaoJpaController(emf).findRegiao(idRegiao));
                
                visita.setDtSistema(new Date());
                visita.setDtVisita(new SimpleDateFormat("dd/MM/yyyy").parse(read("dataVisita")));
                visita.setHrVisitaInicial(sdf.parse(read("horario")));
                Date incrementedDate = DateUtils.addHours(sdf.parse(read("horario")), 1);
                visita.setHrVisitaFinal(incrementedDate);
                visita.setObs(read("observacao").toUpperCase());
                visita.setPtsVista(Integer.parseInt(read("pts")));
                visita.setStatus(new StatusVisitaJpaController(emf).findStatusVisita(1));
                visita.setUsuario(user);
                
                new VisitaJpaController(emf).create(visita);
                new ContratoJpaController(emf).edit(contrato);
            }
        } catch (Exception e) {
            erro = true;
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {
            emf.close();
            if(!erro){
                enviaEmailBoasVindas(nomeCliente, emailCliente.toLowerCase());
            }
        }
    }
    
    public void buscarHorarios() throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
//            https://stackoverflow.com/questions/44619065/mysql-find-available-time-slots
//            SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd");
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Integer regiao = new RegiaoJpaController(emf).findRegiaoByContrato(read("contrato"));
            response.getWriter().print(GsonExclusion.GSON().toJson(new VisitaJpaController(emf).getHorariosDisponiveis(new SimpleDateFormat("dd/MM/yyyy").parse(read("data")), read("idVisita"), regiao)));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
//    String nomeCliente, String emailCliente
    public void enviaEmailBoasVindas(String nomeCliente, String emailCliente) throws IOException {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            nomeCliente = nomeCliente.substring(0, 1).toUpperCase() + nomeCliente.substring(1).toLowerCase();
            String path = request.getServletContext().getRealPath("/");     
            String anexo = path + File.separatorChar + "files" + File.separatorChar + "Apresentacao Isratec.pdf";
//            String nomeCliente = "Leonardo Godoy".split(" ")[0];
//            String emailCliente = "jooohg@gmail.com";
            String html = "<div style=\"background:#f7f7f7;margin:0;padding:0\">\n" +
"  <div style=\"background-color:#f7f7f7\">\n" +
"\n" +
"   <div style=\"border:1px solid #d9d9d9;margin:0px auto;max-width:600px;background:white\">\n" +
"	<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;background:white;border-collapse:collapse\" align=\"center\" border=\"0\">\n" +
"		<tbody>\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-bottom:0px;border-collapse:collapse\">\n" +
"		\n" +
"		\n" +
"		<div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px;padding-top:0px;border-collapse:collapse\">\n" +
"		<div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px 0px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:600px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"auto\" src=\"http://isratec.com.br/resources/cabecalho.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;line-height:100%\" width=\"600\"></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div>\n" +
"		\n" +
"		<!-- texto -->\n" +
"		<div style=\"margin:0px auto;max-width:600px\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\">\n" +
"		<tbody>\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 40px 0px;border-collapse:collapse\"><div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;border-collapse:collapse\"><div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\">\n" +
"		<div style=\"color:#555;font-size:36px;font-weight:book;line-height:1.0;text-align:center;\">\n" +
"		\n" +
"\n" +
"		<strong>\n" +
"		<a style='color:#1ad4cc';>\n" +
"		Olá "+nomeCliente+",<br> </a>\n" +
"		<a style='color:#1ad4cc';>\n" +
"		tudo bem?\n" +
"		</a>\n" +
"		</strong>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse;\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">\n" +
"		<strong>Nós da Isratec acreditamos que a tecnologia é a chave<br>\n" +
"		para a criação de soluções revolucionárias.\n" +
"		</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#ababab;line-height:1.5;text-align:center\">\n" +
"		<strong>Por isso, gostamos de pensar fora da caixa e trazer ideias<br> \n" +
"		visionárias que ajudam as empresas a estar um passo à frente. \n" +
"		</strong>\n" +
"		<br></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-top:0px;border-collapse:collapse\"><div style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:10px 25px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:50px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"50\" src=\"http://isratec.com.br/resources/quadradinhos-flutuantes.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;line-height:100%\" width=\"57\"></td></tr></tbody></table></td></tr></tbody></table></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#1ad4cc;line-height:1.5;text-align:center\">\n" +
"		<strong>AGORA É POR NOSSA CONTA!</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div style=\"color:#555;font-size:16px;line-height:1.5;text-align:center\"></div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">Nosso fotógrafo entrará em contato para coletar o material visual<br> do ambiente que você deseja.</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:20px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#435259;line-height:1.5;text-align:center\">Em seguida, editamos e lhe enviamos o link com o seu tour virtual.</div></td></tr>\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;padding-bottom:0px;border-collapse:collapse\" align=\"center\"><div class=\"font-small-text\" style=\"font-size: 16px;color:#ababab;line-height:1.5;text-align:center\">\n" +
"		<strong>Qualquer dúvida, ficamos disponíveis para lhe ajudar! Abraços.\n" +
"		</strong>\n" +
"		</div></td></tr>\n" +
"		\n" +
"		</tbody></table>\n" +
"		\n" +
"		</div></td></tr></tbody></table></div>\n" +
"		<!-- <div style=\"margin:0px auto;max-width:600px\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;border-collapse:collapse\"><div class=\"m_-5901057937628262040mj-column-per-100 m_-5901057937628262040outlook-group-fix\" style=\"vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px;border-collapse:collapse\" align=\"center\"><div style=\"color:#ababab;font-size:20px;line-height:1.5;text-align:center\">Abraços, -->\n" +
"					<!-- <br> <strong>Equipe Isratec</strong></div></td></tr></tbody></table></div></td></tr></tbody></table></div> -->\n" +
"				<!-- </td></tr></tbody></table></div></td></tr> -->\n" +
"		\n" +
"		<tr><td style=\"word-wrap:break-word;font-size:0px;padding:0px 0px;border-collapse:collapse\" align=\"center\"><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0px\" align=\"center\" border=\"0\"><tbody><tr><td style=\"width:600px;border-collapse:collapse\">\n" +
"		<img alt=\"\" height=\"auto\" src=\"http://isratec.com.br/resources/faixa-icone.png\" style=\"border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;line-height:100%\" width=\"600\"></td></tr></tbody></table></td></tr>\n" +
"			\n" +
"      <div style=\"margin:0px auto;max-width:600px;max-height:50px\">\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:0px;width:100%;border-collapse:collapse;background-color:#082835\" align=\"center\" border=\"0\" ><tbody><tr><td style=\"text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px;padding-top:0px;border-collapse:collapse\"><div>\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"word-wrap:break-word;font-size:0px;padding:10px 25px;border-collapse:collapse\" align=\"center\"><div><table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse\">\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"	  \n" +
"	  <td style=\"font-size:0px;vertical-align:middle;width:20px;height:20px;border-collapse:collapse\">\n" +
"	  <a href=\"https://www.facebook.com/isratecoficial/\" target=\"_blank\" \n" +
"	  data-saferedirecturl=\"https://www.facebook.com/isratecoficial/\">\n" +
"	  \n" +
"	  <img alt=\"nutwitter\" src=\"http://isratec.com.br/resources/face-icone.png\" style=\"display:block;border-radius:3px;border:0;line-height:100%;outline:none;text-decoration:none\" height=\"20\" width=\"20\"></a></td>\n" +
"	  </tr></tbody></table>\n" +
"	  \n" +
"	  </td>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse;\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"			  \n" +
"		  <td class=\"font-small-text\" style=\"font-size: 10px;vertical-align:middle;width:auto;height:auto;border-collapse:collapse;color:#fff\">\n" +
"			@isratecoficial\n" +
"		  </td>\n" +
"		  </tr></tbody></table>\n" +
"	  </td>\n" +
"	  </tr></tbody></table>\n" +
"	  \n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"float:none;display:inline-table;border-collapse:collapse\" align=\"center\" border=\"0\"><tbody><tr>\n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse\">\n" +
"	  <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr><td style=\"font-size:0px;vertical-align:middle;width:20px;height:20px;border-collapse:collapse\"><a href=\"https://www.instagram.com/isratec.oficial/\" target=\"_blank\" data-saferedirecturl=\"https://www.instagram.com/isratec.oficial/\"><img alt=\"nuinstagram\" src=\"http://isratec.com.br/resources/insta-icone.png\" style=\"display:block;border-radius:3px;border:0;line-height:100%;outline:none;text-decoration:none\" height=\"20\" width=\"20\" ></a></td></tr></tbody></table></td>\n" +
"	  \n" +
"	  <td style=\"padding:4px;vertical-align:middle;border-collapse:collapse;\">\n" +
"		<table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" style=\"background:transparent;border-radius:3px;border-collapse:collapse\" border=\"0\"><tbody><tr>\n" +
"			  \n" +
"		  <td class=\"font-small-text\" style=\"font-size: 10px;vertical-align:middle;width:auto;height:auto;border-collapse:collapse;color:#fff\">\n" +
"			@isratec.oficial\n" +
"		  </td>\n" +
"		  </tr></tbody></table>\n" +
"	  \n" +
"	  </td>\n" +
"	  </tr></tbody></table>\n" +
"	  	  \n" +
"	  </div></td></tr></tbody></table></div></td></tr></tbody></table></div>\n" +
"      </tbody>\n" +
"	</table>\n" +
"	</div>\n" +
"	 </div>\n" +
"\n" +
"</div></div></div>";
            
        Mailer.send(anexo, "Apresentacao Isratec.pdf", null, null, "Seja bem-vindo a revolução!", html, emailCliente);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
    
     public void buscarVisitasPendentesHoje() throws IOException, Exception {
        System.out.println("buscarVisitasPendentesHoje");
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("IsratourPU");
        try {
            List<Visita> visitas = new VisitaJpaController(emf).findVisitasPendentesByData(new Date());
            String conteudo = null;
            if(!visitas.isEmpty()){
                for (Visita visita : visitas) {
                    conteudo = conteudo + "\nCliente: " + visita.getCliente().getNomeFantasia() + " Horário: " + visita.getHrVisitaInicial() + " Região: " + visita.getRegiao().getDescricao();
                }

                Mailer.send(null, null, null, null, "Visitas de Hoje", conteudo, "contato@isratec.com.br");            
            }
            
        } catch (Exception e) {
            Mailer.send(null, null, null, null, "Erro buscarVisitasPendentesHoje", null, "contato@isratec.com.br");            
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
}
