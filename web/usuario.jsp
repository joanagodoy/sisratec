<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<script type="text/javascript" src="js/usuario.js?${version}"></script>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h2 class="page-title">Usuário</h2>
                        <div class="ml-auto text-left">
<!--                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <div class="container-fluid">
                <table class="table tablej display" style="width:100%;">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nome</th>
                            <th>Tipo Usuário</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

<div class="modal fade" id='dialogEditar'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuário</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id='form' name="form" onsubmit="try {
                            salvar();
                        } catch (e) {
                        }
                        return false;">
<!--                    <div class="form-group">
                        <label for="nome">Usuário</label>
                        <input type="text" required="" class="form-control" name="usuario" id="usuario" placeholder="Usuário">
                    </div>-->
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" required="" class="form-control" name="nome" id="nome" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" required="" class="form-control" name="senha" id="senha" placeholder="Senha">
                    </div> 
                    
                    <div class="form-group">
                        <label for="tipo">Tipo</label>
                        <select required="" class="form-control select2" name="tipo" id="tipo"  data-size="4" data-live-search="true" style="width: 205px;">
                            <c:forEach var="tipo" items="${tipos}">
                                <option value="${tipo.id}">${tipo.descricao}</option>
                            </c:forEach>
                        </select>
                    </div>             
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" required="" class="form-control" name="email" id="email" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <label for="tipo">Região Padrão</label>
                        <select required="" class="form-control select2" name="regiao" id="regiao"  data-size="4" data-live-search="true" style="width: 205px;">
                            <c:forEach var="regiao" items="${regioes}">
                                <option value="${regiao.id}">${regiao.descricao}</option>
                            </c:forEach>
                        </select>
                    </div> 
                    <div id="divAtivo" class="form-group">
                        <label for="telefone">Ativo</label>
                        <input type="checkbox" class="" name="ativo" id="ativo" >
                    </div>
                    <input type="hidden" name='id'>
                    <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                    <input type="reset" id="reset" style='display:none;'/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick='$("#form button[type=submit]").click();'>Salvar</button>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>