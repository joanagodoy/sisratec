<%-- 
    Document   : index
    Created on : 23/06/2015, 15:45:42
    Author     : Usuario
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>
<script src="js/cliente.js" type="text/javascript"></script>
<script src="js/contrato.js" type="text/javascript"></script>
<script src="js/visita_listagem_cliente.js" type="text/javascript"></script>
<script src="js/cobranca_cliente.js" type="text/javascript"></script>
<!DOCTYPE html>

<script>
    var idCliente = ${cliente.id};
    var razSocial = '${cliente.razSocial}';
</script>

<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h2 id="titleCliente" class="page-title"></h2>
                        <div class="ml-auto text-left">
<!--                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            
           <div class="container-fluid">
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"> <a class="nav-link" data-toggle="tab" href="#dados" onclick="editarCliente();" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Dados</span></a> </li>
                        <li class="active"> <a class="nav-link" data-toggle="tab" href="#contrato" onclick="editarContrato();" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Contrato</span></a> </li>
                        <li class="active"> <a class="nav-link" data-toggle="tab" href="#visita" onclick="listarVisitasCliente();" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Visita</span></a> </li>
                        <c:if test="${cliente.id != null}">
                            <li class="active"> <a class="nav-link" data-toggle="tab" href="#faturas" onclick="listarFaturas();" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Faturas</span></a> </li>
                            <li class="active"> <a class="nav-link" data-toggle="tab" href="#infCobranca" onclick="editarInfCobranca();" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Inf. Cobranças</span></a> </li>
                        </c:if>
                        <!--<li class="active"> <a class="nav-link" data-toggle="tab" href="#cobranca" onclick="listarCobrancas();" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Histórico Cobrança</span></a> </li>-->
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="dados" class="tab-pane fade">
                        <br>
                        <form class="form-horizontal" id='formCliente' name="formCliente" onsubmit="try {
                                        salvarCliente();
                                    } catch (e) {
                                    }
                                    return false;">
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label"  for="nome">Razão Social</label>
                                    <div class="col-sm-9">
                                        <input type="text" required="" class="form-control" name="razSocial" id="razSocial" placeholder="Razão Social">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Nome Fantasia</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="fantasia" id="fantasia" placeholder="Nome Fantasia">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Responsável</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="responsavel" id="responsavel" placeholder="Responsável">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Tipo Pessoa</label>
                                    <div class="col-sm-9">
                                    <select onchange="changeMask();" required="" id="tipo" name="tipo" class="select2 form-control custom-select" data-size="4" data-live-search="true" style="width: 205px;">
                                        <option value="1">Jurídica</option>
                                        <option value="2">Física</option>
                                    </select>
                                        </div>
                                </div>
                                <div class="form-group row">                        
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome" id="labelCnpjCpf"></label> 
                                    <!--<b id="cnpjAviso" style="color: red;font: bold 10px Arial; padding-right: 4px"></b>-->
                                    <div class="col-sm-9">
                                    <input onblur="validarCNPJCPF();" type="text" required="" class="form-control" name="cnpj" id="cnpj" placeholder="CNPJ ou CPF">
                                    </div>
                                </div>
                                <div class="form-group row" id="divInscricao">                        
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">IE</label>
                                    <!--<b id="cnpjAviso" style="color: red;font: bold 10px Arial; padding-right: 4px"></b>-->
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control" name="inscricao" id="inscricao" placeholder="Inscrição Estadual ou Municipal">
                                    </div>
                                </div>
                                <div class="form-group row" id="divRg">                        
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">RG</label> 
                                    <!--<b id="cnpjAviso" style="color: red;font: bold 10px Arial; padding-right: 4px"></b>-->
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control" name="rg" id="rg" placeholder="RG">
                                    </div>
                                </div>
                                <div class="form-group row"> 
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">CEP</label> 
                                    <!--<b id="cepAviso" style="color: red;font: bold 10px Arial; padding-right: 4px">CEP NÃO ENCONTRADO!</b>-->
                                    <div class="col-sm-9">
                                    <input onblur="buscarCEP(this);" type="text" required="" class="form-control cep" name="cep" id="cep" placeholder="CEP">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">UF</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="uf" id="uf" placeholder="UF">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Cidade</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="cidade" id="cidade" placeholder="Cidade">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Bairro</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="bairro" id="bairro" placeholder="Bairro">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Rua</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="rua" id="rua" placeholder="Rua">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Número</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control" name="numero" id="numero" placeholder="Número">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="nome">Complemento</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Complemento">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="telefone">Telefone</label>
                                    <div class="col-sm-9">
                                    <input type="text" required="" class="form-control telefone" name="telefone" id="telefone" placeholder="Telefone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="telefone">Telefone 2</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control telefone" name="telefone2" id="telefone2" placeholder="Telefone 2">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="telefone">Telefone 3</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control telefone" name="telefone3" id="telefone3" placeholder="Telefone 3">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="telefone">E-mail</label>
                                    <div class="col-sm-9">
                                    <input required="" type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="telefone">E-mail 2</label>
                                    <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email2" id="email2" placeholder="E-mail">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="telefone">Ramo</label>
                                    <div class="col-sm-9">
                                        <select required="" id="ramo" name="ramo" class="select2 form-control custom-select" data-live-search="true" >
                                        <c:forEach var="b" items="${ramos}">
                                            <option value="${b.id}">${b.descricao}</option>
                                        </c:forEach>
                                    </select>
                                    </div>
                                </div>
<!--                                <div class="form-group" id="divDtEntrega">
                                    <label class="col-sm-1 text-left control-label col-form-label" for="data">Data Cobrança</label>
                                    <div class="col-sm-9">
                                    <input type="text" readonly="" class="form-control datepickerTotal" name="dataCobranca" id="dataCobranca">
                                    </div>
                                </div>-->
                                <c:if test="${usuario.tipo.id != 4}"> 
                                    <div id="divVendedorCliente" class="form-group row">
                                        <label class="col-sm-1 text-left control-label col-form-label" for="telefone">Vendedor</label>
                                        <div class="col-sm-9">
                                        <!--<input type="text" class="form-control" name="vendedorCliente" id="vendedorCliente">-->
                                        <select id="vendedorCliente" name="vendedorCliente" class="select2 form-control custom-select" data-live-search="true" >
                                            <c:forEach var="b" items="${vendedores}">
                                                <option value="${b.id}">${b.nome}</option>
                                            </c:forEach>
                                        </select>
                                        </div>
                                    </div>
                                    <div id="divAtivo" class="form-group row">
                                        <label class="col-sm-1 text-left control-label col-form-label" for="ativo"></label>
                                        <div class="col-md-9">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="ativo" name="ativo">
                                                <label class="custom-control-label" for="ativo">Ativo</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="divPossuiOutroAtivo" class="form-group row">
                                        <label class="col-sm-1 text-left control-label col-form-label" for="possuioutroativo"></label>
                                        <div class="col-md-9">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="possuioutroativo">
                                                <label class="custom-control-label" for="possuioutroativo">Possui Outro Cadastro Ativo</label>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <input type="hidden" name='id'>
                                <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                                <input type="reset" id="reset" style='display:none;'/>
                            </form>
                            <div class="form-group row">
                                <label class="col-sm-1 text-left control-label col-form-label" for=""></label>
                                <div class="col-md-9">
                                    <!--<div class="custom-control custom-checkbox mr-sm-2">-->
                                        <button type="button" class="btn btn-success btn-responsive" onclick='$("#formCliente button[type=submit]").click();'>Salvar</button>
                                    <!--</div>-->
                                </div>
                            </div>

                        </div>
                        <div id="contrato" class="tab-pane fade">
                            <br>
                            <div class="form-group row">
                                    <label class="col-sm-3 text-left control-label col-form-label" for="visitaaberto">Fotos do Contrato</label>
                                    <div class="col-md-9">
                                        <!--<div class="custom-control custom-checkbox mr-sm-2">-->
                                            <span style="padding-left:10px;cursor: pointer;" data-toggle="tooltip" title="Abrir Fotos Contrato" onclick="carregarFotos();"><i class='fas fa-camera'></i></span>
                                        <!--</div>-->
                                    </div>
                                </div>
                            
                            <form class="form-horizontal" id='formContrato' name="formContrato" onsubmit="try {
                                        salvarContrato();
                                    } catch (e) {
                                    }
                                    return false;">
                              
                                <div class="form-group row">
                                    <label class="col-sm-3 text-left control-label col-form-label" for="visitaaberto"></label>
                                    <div class="col-md-9">
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input type="checkbox" class="custom-control-input" id="visitaaberto" name="visitaaberto">
                                            <label class="custom-control-label" for="visitaaberto">Agendar Visita</label>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="form-group row">                   
                                        <input type="checkbox" name="visitaaberto" id="visitaaberto"> Agendar Visita
                                </div>  -->
                                <div class="form-group row">
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Tipo Contrato</label>
                                    <div class="col-sm-9"><select onchange="changeTipoContrato()" id="tipoContrato" name="tipoContrato" class="form-control select2 custom-select" data-size="4" data-live-search="true" style="width: 205px;">
                                        <option value="Estabelecimento">Estabelecimento</option>
                                        <option value="Imobiliaria">Imobiliaria</option>
                                    </select>
                                    </div>
                                </div>
                                <div id="divNrContrato" class="form-group row">
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Nr Contrato (Mesmo da Folha)</label>
                                    <div class="col-sm-9"> <input type="number" required="" class="form-control" name="nr" id="nr" placeholder="Nr Contrato"> </div>
                                </div>
<!--                                <div class="form-group row" id="divSelectCliente">
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Cliente</label>
                                    <select id="cliente" name="cliente" class="form-control select2 custom-select" data-size="4" data-live-search="true" style="width: 205px;">

                                    </select>
                                </div>
                                <div class="form-group row" id="divCliente">
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Cliente</label>
                                    <div class="col-sm-9"> <input type="text" class="form-control" name="cliente" id="cliente"></div>
                                </div>-->
                                <div class="form-group row">                        
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Pagamento</label>
                                    <div class="col-sm-9"><select id="pagamento" name="pagamento" class="form-control select2 custom-select" data-size="4" data-live-search="false" style="width: 205px;">
                                        <option value="1">Anual</option>
                                        <option value="12">Mensal</option>
                                        <option value="4">Quadrimestral</option>
                                        <option value="3">Trimestral</option>
                                        <option value="0">À Vista</option>
                                        <option value="6">Semestralidade</option>
                                    </select>
                                    </div>
                                </div>  
                                <div class="form-group row" id="divCliente">
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">% Desconto (preencher decimais com . e não ,)</label>
                                    <div class="col-sm-9"> <input type="text" class="form-control" name="desconto" id="desconto" value="0.0"></div>
                                </div>
                                <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2}"> 
                                    <div class="form-group row" id="divDtEntrega">
                                        <label class="col-sm-3 text-left control-label col-form-label"  for="data">Data Entrega</label>
                                        <div class="col-sm-9"> <input type="text" class="form-control datepickerBefore" name="dataEntrega" id="dataEntrega"></div>
                                    </div>
                                    <div class="form-group row" id="divValorMensal">
                                        <label class="col-sm-3 text-left control-label col-form-label"  for="data">Valor Mensal (Clientes Contrato 2017)</label>
                                        <div class="col-sm-9"> <input type="text" class="form-control" name="valorMensal" id="valorMensal"></div>
                                    </div>
                                </c:if>
                                <div class="form-group row">                        
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Versão</label>
                                    <div class="col-sm-9"><select id="edicao" name="edicao" class="form-control select2 custom-select" data-size="4" data-live-search="false" style="width: 205px;">
                                        <option value="1">1º Versão</option>
                                        <option value="2">2º Versão</option>
                                        <option selected value="3">3º Versão</option>
                                    </select>
                                    </div>
                                </div>  
                                <div class="form-group row">                        
                                    <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Observação</label>
                                    <div class="col-sm-9"><textarea id="obs" name="obs" class="form-control"></textarea></div>  
                                </div> 
                                <div id="divAdicionais">
                                    <div class="form-group row">
                                        <label class="col-md-3">Serviços Adicionais</label>
                                        <div class="col-md-9">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="consultoria" name="consultoria">
                                                <label class="custom-control-label" for="consultoria">Consultoria</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="ficha" name="ficha" >
                                                <label class="custom-control-label" for="ficha">Editar/Criar Ficha Google Maps</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" name="analiseanual" id="analiseanual">
                                                <label class="custom-control-label" for="analiseanual">Análise Anual</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" name="analisemensal" id="analisemensal">
                                                <label class="custom-control-label" for="analisemensal">Análise Mensal</label>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <br>
                                    <h4>Serviços Adicionais</h4>
                                    <br>
                                    <div class="form-group">                        
                                        <label for="nome"></label>
                                        <input type="checkbox" name="consultoria" id="consultoria"> Consultoria
                                    </div>   
                                    <div class="form-group">                        
                                        <label for="nome">Editar/Criar Ficha Google Maps</label>
                                        <input type="checkbox" name="ficha" id="ficha" > Editar/Criar Ficha Google Maps
                                    </div>   
                                    <div class="form-group">                        
                                        <label for="nome">Análise Anual</label>
                                        <input type="checkbox" name="analiseanual" id="analiseanual"> Análise Anual
                                    </div>   
                                    <div class="form-group">                        
                                        <label for="nome">Análise Mensal</label>
                                        <input type="checkbox" name="analisemensal" id="analisemensal"> Análise Mensal
                                    </div>   
                                    <div class="form-group">                        
                                        <label for="nome">Análise Mensal</label>
                                        <input type="checkbox" name="site" id="site"> Site
                                    </div>-->
                                </div>
                                <div id="divImovel" style="display: none">
<!--                                    <div class="form-group row">                        
                                        <label for="nome">Análise Mensal</label>
                                        <input type="checkbox" name="vendaEstabelecimentoImobiliaria" id="vendaEstabelecimentoImobiliaria"> Venda para estabelecimento também
                                    </div>-->
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-left control-label col-form-label" for="vendaEstabelecimentoImobiliaria"></label>
                                        <div class="col-md-9">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="vendaEstabelecimentoImobiliaria" name="vendaEstabelecimentoImobiliaria">
                                                <label class="custom-control-label" for="vendaEstabelecimentoImobiliaria">Venda para estabelecimento também</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-left control-label col-form-label"  for="nome">Qtd. Imóvel</label>
                                        <div class="col-sm-9"> <input type="number" class="form-control" name="qtdImovel" id="qtdImovel"></div>
                                    </div>  
                                </div>
                                <input type="hidden" name='id' id="id">
                                <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                                <input type="reset" id="reset" style='display:none;'/>
                            </form>
                            <div class="form-group row">
                                <label class="col-sm-3 text-left control-label col-form-label" for=""></label>
                                <div class="col-md-9">
                                    <!--<button id="btLimpar" type="button" class="btn btn-danger" onclick="limparFormulario()">Limpar</button>-->
                                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>-->
                                    <button type="button" class="btn btn-success" onclick='$("#formContrato button[type=submit]").click();'>Salvar</button>
                                    <!--<button type="button" class="btn btn-success form-control-static pull-right" onclick='//$("#formContrato button[type=submit]").click();alterarCadastrao = true;'>Salvar e Cadastrar Visita</button>-->
                                    <!--<button id="btAddImovel" type="button" class="btn btn-primary" onclick='openAddImovel();'>Adicionar Imóvel</button>-->
                                </div>
                            </div>
                        </div>
                        <div id="visita" class="tab-pane fade">
                            <table id='tableVisitasCliente' class="table tablej" style="cursor:pointer" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data</th>
                                        <th>Status</th>
                                        <th>hrI</th>
                                        <th>hrF</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div id="faturas" class="tab-pane fade">
                            <br>
                            Meses Faltantes Pagar: <a id="qtdmesesfaltantes" name="qtdmesesfaltantes"></a>
                            <br>
                            <table id="tableFaturas" class="table tablej display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data de Emissão </th>
                                        <th>Data de Vencimento </th>
                                        <th>Data de Pagamento</th>
                                        <th>Valor</th>
                                        <th>Referência</th>
                                        <th>Qtd Meses</th>
                                        <th>Descrição</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    <div id="infCobranca" class="tab-pane fade">
                          <br>
                          <form id='formInfCobranca' name="formInfCobranca" onsubmit="try {
                                    salvarInfCobranca();
                                } catch (e) {
                                }
                                return false;">
                           <div class="form-group row">
                                <label class="col-sm-3 text-left control-label col-form-label" for="nome">Adimplência</label>
                                <div class="col-sm-9"><select id="adimplencia" name="adimplencia" class="select2 form-control custom-select" data-size="4" data-live-search="true" style="width: 205px;">
                                    <option value="1">Adimplente</option>
                                    <option value="2">Inadimplente</option>
                                    <option value="3">Advogado</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-left control-label col-form-label" for="data">Data Primeira Recorrência</label>
                                <div class="col-sm-9"><input placeholder="Data Primeira Fatura" type="text" class="form-control datepickerTotal" name="dtPrimeiraFatura" id="dtPrimeiraFatura"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-left control-label col-form-label" for="data">Data Próximo Vencimento</label>
                                <div class="col-sm-9"><input placeholder="Data Próximo Vencimento" type="text" class="form-control datepickerTotal" name="dtProximoVencimento" id="dtProximoVencimento"></div>
                            </div>
                            <input type="hidden" name='idCliente' id="idCliente">
                            <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                            <input type="reset" id="reset" style='display:none;'/>
                        </form>
                        <div class="form-group row">
                            <label class="col-sm-3 text-left control-label col-form-label" for=""></label>
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success btn-responsive" onclick='$("#formInfCobranca button[type=submit]").click();'>Salvar</button>
                            </div>
                        </div>
                      </div>
                  </div>
                </div>
            
            <div class="modal fade" id='dialogFotos'>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            
                            <h4 class="modal-title">Fotos Contrato</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <!--accept="image/*"-->
                            <form action="EnviarFotoContrato" accept="image/*" class="form form-inline" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="loading('open');">
                                <input class="form-inline" style="display: inline-block;" name="file" required type="file">
                                <input type="hidden" class="form-inline" name="idContrato" id="idContrato" value="0">
                                <input type="submit" class=" form-inline btn btn-default" name="submitBtn" value="Enviar">
                            </form>
                            <iframe id="upload_target" name="upload_target" src="#" style="display:none;"></iframe> 
                            <br>
                            <div id="divFotos">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--</div>-->

            
                
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            
<%@include file="footer.jsp" %>