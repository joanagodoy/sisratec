<%-- 
    Document   : index
    Created on : 23/06/2015, 15:45:42
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--<%@include file="header.jsp" %>--%>

<section >
<!--class="s-12 l-9 right"-->
    <c:choose>
        <c:when test="${u == null || u == 'usuario'}">
            <%@include file="usuario.jsp"%>
        </c:when>
        <c:when test="${u == 'regiao'}">
            <%@include file="regiao.jsp"%>
        </c:when>        
    </c:choose>

</section>
<!--<aside class="s-12 l-3" id="menuLateralAdministracao">
    <h3>Navegação</h3>
    <div class="aside-nav">
        <ul>
            <li><a href="Administracao?u=usuario">Usuários</a></li>
            <li><a href="Administracao?u=regiao">Regiões</a></li>
        </ul>
    </div>
</aside>-->
</div>
</div>
</div>

<%--<%@include file="footer.jsp" %>--%>

