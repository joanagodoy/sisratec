<%-- 
    Document   : index
    Created on : 23/06/2015, 15:45:42
    Author     : Usuario
--%>

<%@page import="dominio.Regiao"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.min.css">
<link rel="stylesheet" media="print" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.print.css">
<script type="text/javascript" src="js/libs/moment-with-locales.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/libs/fullcalendar/pt-br.js"></script>

<script type="text/javascript" src="js/visita.js?${version}"></script>

<script>
    open = ${open};  
</script>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h2 class="page-title">Visitas</h2>
                        <div class="ml-auto text-left">
<!--                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <div class="container-fluid">
                <div class="form-group">
                    <label>Região</label>
                    <select class="form-control select2" id="regiao" onchange='$("#calendar").fullCalendar("refetchEvents");'>
                    </select>
                </div>

                <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2 || usuario.tipo.id == 4}">
                    <button type="button" id="btNovaVisita" onclick="novaVisita();" class="btn btn-success" style="margin-top: 5px;">Nova Visita</button>  
                </c:if>
                <br><br>
                <div id="calendar" style="margin-top:"></div>
                <br><br>
<!--                <table style="background-color: #e9f0f5">
                    <tr>
                        <td align="left" width="25"><div class="boxLegenda" style="background-color: rgba(13, 0, 255, 1)"></div></td><td>Pendente</td>
                        <td align="left" width="25"><div class="boxLegenda" style="background-color: rgba(10, 236, 38, 1)"></div></td><td>Realizado</td>
                        <td align="left" width="25"><div class="boxLegenda" style="background-color: rgba(192, 1, 1, 1)"></div></td><td>Cancelado</td>
                        <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2}">
                            <td align="left" width="25"><div class="boxLegenda" style="background-color: rgba(169, 1, 192, 1)"></div></td><td>Pago</td>
                        </c:if>
                    </tr>
                </table>-->
            </div>
            
<div class="modal fade" id='dialogVisita'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Visita</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id='formVisita' onsubmit="try {
                            efetuarVisita();
                        } catch (e) {
                        }
                        return false;">
                    
                    <div class="form-group" id="divSelectCliente">
                        <label for="nome">Cliente</label>
                        <select onchange="onChangeCliente();buscarHorarios(null);" id="contrato" name="contrato" class="form-control select2" data-size="4" data-live-search="true" style="width: 205px;">
                            
                        </select>
                    </div>
                    <div class="form-group" id="divCliente">
                        <label for="nome">Cliente</label> 
                        <br>
                        <button onclick="abrirCliente();" id="cliente" name="cliente" type="button" class="buttonLink"></button>        <span id="divLinkMaps"></span>
                    </div>
                    <div class="form-group">
                        <label for="data">Data</label>
                        <input required readonly="" type="text" class="form-control datepicker" name="dataVisita" id="dataVisita" onchange="buscarHorarios(null);">
                    </div>
                    <div class="form-group">
                        <label for="horarioInicial">Horarios Disponíveis</label>                        
                        <select id="horario" name="horario" class="form-control select2" data-size="4" data-live-search="true" style="width: 205px;" >
                            
                        </select>  
                    </div> 
                    <div class="form-group ">
                        <label for="pontosVistaVendido">Pontos de Vista Vendidos</label>
                        <input type="number" required="" class="form-control" name="pts" id="pts">
                    </div> 
                    <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2 || usuario.tipo.id == 4}"> 
                        <div style="display: none" id="divVisitaRelizada" class="form-group ">
                            <label for="pontosVistaRealizado">Pontos de Vista Realizados</label>
                            <input type="number" class="form-control" name="ptsRealizado" id="ptsRealizado">
                        </div> 
                     </c:if>
                     <div id="divImovel">
                        <div class="form-group"> 
                            <label for="nome">CEP</label> <b id="cepAviso" style="display: none; color: red;font: bold 10px Arial; padding-right: 4px">CEP NÃO ENCONTRADO!</b>
                            <input onblur="buscarCEP(this);" type="text" class="form-control cep" name="cep" id="cep" placeholder="CEP">
                        </div>
                        <div class="form-group">
                            <label for="nome">UF</label>
                            <input type="text" class="form-control" name="uf" id="uf" placeholder="UF">
                        </div>
                        <div class="form-group">
                            <label for="nome">Cidade</label>
                            <input type="text" class="form-control" name="cidade" id="cidade" placeholder="Cidade">
                        </div>
                        <div class="form-group">
                            <label for="nome">Bairro</label>
                            <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro">
                        </div>
                        <div class="form-group">
                            <label for="nome">Rua</label>
                            <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua">
                        </div>
                        <div class="form-group">
                            <label for="nome">Número</label>
                            <input type="text" class="form-control" name="numero" id="numero" placeholder="Número">
                        </div>
                        <div class="form-group">
                            <label for="nome">Complemento</label>
                            <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Complemento">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="observacao">Observação</label>
                        <textarea name="observacao" class="form-control"></textarea>
                    </div>    
                    <div id="divFormStatus" class="form-group">
                        <label for="status">Status</label>
                        <input disabled="" type="text" id="status" name="status" class="form-control">   
                    </div>
                    <input type="hidden" name="id" id="id">
                    <button type="reset"  style='display:none;' class="btn btn-default">Submit</button>
                    <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer" style="text-align: left">
                <button type="button" class="btn btn-default " data-dismiss="modal">Fechar</button>
                <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2 || usuario.tipo.id == 4}"> 
                    <button type="button" class="btn btn-success" id="btSalvar" onclick='$("#formVisita button[type=submit]").click();'>Salvar</button>
                </c:if>
                <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 3}"> 
                    <button style="display: none" type="button" class="btn btn-success form-control-static pull-right" id="btRealizada"  onclick='confirmarFinalizar(2);'>Realizada</button>
                </c:if>
                <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 3 || usuario.tipo.id == 2}"> 
                    <button style="display: none" type="button" class="btn btn-danger form-control-static pull-right" id="btCancelada" onclick='confirmarFinalizar(3);'>Cancelada</button>
                </c:if>
                <button style="display: none" type="button" class="btn btn-success form-control-static pull-right" id="btPagamento"  onclick='finalizar(4);'>Pagamento Efetuado</button>    
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id='dialogConfirmarPts'>
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Conclusão da Visita</h4>
            </div>
            <div class="modal-body">
                <form id='formConfirmarPts' onsubmit="try {
                            finalizar(2);
                        } catch (e) {
                        }
                        return false;">
                    
                    <div class="form-group ">
                        <label for="horarioInicial">Pontos de Vista Relizado</label>
                        <input type="number" required="" class="form-control" name="ptsFeitos" id="ptsFeitos">
                    </div> 
                    <div class="form-group ">
                        <input type="checkbox" name="cobranca" id="cobranca"> Pagamento Visita Efetuada
                    </div> 
                    <div class="form-group">
                        <label for="observacao">Observação para Editor</label>
                        <textarea name="observacao" class="form-control"></textarea>
                    </div>                    
                    <button type="reset"  style='display:none;' class="btn btn-default">Submit</button>
                    <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick='$("#formConfirmarPts button[type=submit]").click();'>Salvar</button>
            </div>
        </div>
    </div>
</div>
  
<div class="modal fade" id='dialogExcluir'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancelar Visita</h4>
            </div>
            <div class="modal-body">
                <p>Tem certeza que deseja cancelar essa visita?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" onclick='finalizar(3)();'>Cancelar</button>
            </div>
        </div>
    </div>
</div>

    
<div class="modal fade" id='dialogCliente'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id='form' name="form">
                    <div class="form-group">
                        <label for="nome">Razão Social</label>
                        <input type="text" required="" class="form-control" name="razSocial" id="razSocial" placeholder="Razão Social">
                    </div>
                    <div class="form-group">
                        <label for="nome">Nome Fantasia</label>
                        <input type="text" required="" class="form-control" name="fantasia" id="fantasia" placeholder="Nome Fantasia">
                    </div>
                    <div class="form-group">
                        <label for="nome">Responsável</label>
                        <input type="text" required="" class="form-control" name="responsavel" id="responsavel" placeholder="Responsável">
                    </div>
                    <div class="form-group">
                        <label for="nome">Tipo Pessoa</label>
                        <select onchange="changeMask();" required="" id="tipo" name="tipo" class="form-control select2" data-size="4" data-live-search="true" style="width: 205px;">
                            <option value="1">Jurídica</option>
                            <option value="2">Física</option>
                        </select>
                    </div>
                    <div class="form-group">                        
                        <label for="nome" id="labelCnpjCpf"></label> <b id="cnpjAviso" style="color: red;font: bold 10px Arial; padding-right: 4px"></b>
                        <input onblur="validarCNPJCPF();" type="text" required="" class="form-control" name="cnpj" id="cnpj" placeholder="CNPJ ou CPF">
                    </div>
                    <div class="form-group" id="divInscricao">                        
                        <label for="nome">Inscrição Estadual/Municipal</label> <b id="cnpjAviso" style="color: red;font: bold 10px Arial; padding-right: 4px"></b>
                        <input type="text" class="form-control" name="inscricao" id="inscricao" placeholder="Inscrição Estadual ou Municipal">
                    </div>
                    <div class="form-group" id="divRg">                        
                        <label for="nome">RG</label> <b id="cnpjAviso" style="color: red;font: bold 10px Arial; padding-right: 4px"></b>
                        <input type="text" class="form-control" name="rg" id="rg" placeholder="RG">
                    </div>
                    <div class="form-group"> 
                        <label for="nome">CEP</label> 
                        <input type="text" required="" class="form-control cep" name="cep" id="cep" placeholder="CEP">
                    </div>
                    <div class="form-group">
                        <label for="nome">UF</label>
                        <input type="text" required="" class="form-control" name="uf" id="uf" placeholder="UF">
                    </div>
                    <div class="form-group">
                        <label for="nome">Cidade</label>
                        <input type="text" required="" class="form-control" name="cidade" id="cidade" placeholder="Cidade">
                    </div>
                    <div class="form-group">
                        <label for="nome">Bairro</label>
                        <input type="text" required="" class="form-control" name="bairro" id="bairro" placeholder="Bairro">
                    </div>
                    <div class="form-group">
                        <label for="nome">Rua</label>
                        <input type="text" required="" class="form-control" name="rua" id="rua" placeholder="Rua">
                    </div>
                    <div class="form-group">
                        <label for="nome">Número</label>
                        <input type="text" required="" class="form-control" name="numero" id="numero" placeholder="Número">
                    </div>
                    <div class="form-group">
                        <label for="nome">Complemento</label>
                        <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Complemento">
                    </div>
                    <div class="form-group">
                        <label for="telefone">Telefone</label>
                        <input type="text" required="" class="form-control telefone" name="telefone" id="telefone" placeholder="Telefone">
                    </div>
                    <div class="form-group">
                        <label for="telefone">Telefone 2</label>
                        <input type="text" class="form-control telefone" name="telefone2" id="telefone2" placeholder="Telefone 2">
                    </div>
                    <div class="form-group">
                        <label for="telefone">Telefone 3</label>
                        <input type="text" class="form-control telefone" name="telefone3" id="telefone3" placeholder="Telefone 3">
                    </div>
                    <div class="form-group">
                        <label for="telefone">E-mail</label>
                        <input required="" type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <label for="telefone">E-mail 2</label>
                        <input type="email" class="form-control" name="email2" id="email2" placeholder="E-mail">
                    </div>
                    <%--<c:if test="${usuario.tipo.id != 4}">--%> 
                        <div id="divVendedorCliente" style="display: none" class="form-group">
                            <label for="telefone">Vendedor</label>
                            <input type="text" class="form-control" name="vendedorCliente" id="vendedorCliente">
                        </div>
                        <div id="divAtivo" class="form-group">
                            <!--<label for="telefone">Ativo</label>-->
                            <input type="checkbox" name="ativo" id="ativo" > Ativo
                        </div>
                    <%--</c:if>--%>
                    <input type="hidden" name='id'>
                    <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                    <input type="reset" id="reset" style='display:none;'/>
                </form>
            </div>
            <div class="modal-footer" style="text-align: left">
                <button type="button" class="btn btn-default btn-responsive" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>

