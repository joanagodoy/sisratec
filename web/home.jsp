<%-- 
    Document   : index
    Created on : 23/06/2015, 15:45:42
    Author     : Usuario
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<script src="js/home.js?${version}"></script>
<!DOCTYPE html>

<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        
                        <h4 class="page-title">Dashboard</h4>
                        
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
               <div class="divDashboard" style="margin: 0;width:300px;">
                    <span class="fas fa-calendar-alt" style="font-size: 25px; color:black !important;"></span>
                    <div class="tituloDashboard">Visitas</div>
                    <center>
                        <table class="tableDashboard">
                            <tr>
                                <td class="tableDashBoardAtr" align="left"  style="width: 225px;">Agendadas</td>
                                <td class="tableDashBoardVal" id="informacoesPendentes"></td>
                            </tr>
                            <tr>
                                <td class="tableDashBoardAtr" align="left">Canceladas</td>
                                <td class="tableDashBoardVal" id="informacoesCancelados"></td>
                            </tr>
                            <tr>
                                <td class="tableDashBoardAtr">Realizadas</td>
                                <td class="tableDashBoardVal" id="informacoesRealizados"></td>
                            </tr>
                            <tr style="display:none;" id="trVisitasPagas">
                                <td class="tableDashBoardAtr">Pagas</td>
                                <td class="tableDashBoardVal" id="informacoesPagos"></td>
                            </tr>

                        </table>
                        <span class="botaoDashboard dashInformacoes"  name="semana" onclick="dashGeral(this);">Semana</span>
                        <span class="botaoDashboard dashInformacoes "  name="mes" onclick="dashGeral(this);">Mês</span>
                        <span class="botaoDashboard dashInformacoes"  name="ano" onclick="dashGeral(this);">Ano</span>
                    </center>
                </div>
                <c:if test="${usuario.tipo.id == 1}">
<!--                <div class="divDashboard" style="margin: 0;width:300px;">
                    <span class="fas fa-calendar-alt" style="font-size: 25px; color:black !important;"></span>
                    <div class="tituloDashboard">Vendas de Adicionais</div>
                    <center>
                        <div id="canvasVendasAdicionais" style="margin-top: 0px; display: inline-block;"></div> 
                        <div style="display: inline-block; vertical-align: top; margin-top: 15px;margin-left: 10px;">
                            <div style="font-size: 12px;margin-top:15px;">
                                <table id="tableLegendaEvolucaoVolume">
                                    <tr style="height: 22px;"><td align="left" width="25"><div class="boxLegenda"></div></td><td>Consultoria</td></tr>
                                    <tr style="height: 22px;"><td align="left" width="25"><div class="boxLegenda"></div></td><td>Ficha Google</td></tr>
                                    <tr style="height: 22px;"><td align="left" width="25"><div class="boxLegenda"></div></td><td>Análise Mensal</td></tr>
                                    <tr style="height: 22px;"><td align="left" width="25"><div class="boxLegenda"></div></td><td>Análise Anual</td></tr>
                                    <tr style="height: 22px;"><td align="left" width="25"><div class="boxLegenda"></div></td><td>Site</td></tr>
                                </table>
                            </div>
                        </div>
                        <span class="botaoDashboard dashInformacoes"  name="semanaAdicional" onclick="dashboardVendasAdicionais(this);">Semana</span>
                        <span class="botaoDashboard dashInformacoes "  name="mesAdicional" onclick="dashboardVendasAdicionais(this);">Mês</span>
                        <span class="botaoDashboard dashInformacoes"  name="anoAdicional" onclick="dasdashboardVendasAdicionaishGeral(this);">Ano</span>
                    </center>
                </div>-->
            </c:if>
            <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 4}">
            <div class="divDashboard" style="margin: 0;width:300px; background-color: #8bc34a6e !important;">
                <span class="fas fa-dollar-sign" style="font-size: 25px; color:black !important;"></span>
                <div class="tituloDashboard">Comissões Vendedores </div>
                <br>
                <span class="cursor tituloDashboard" title="Gerar arquivo xls" style="margin-left:125px;" onclick="comissaoExcel();"><i class="far fa-file-excel"></i></span>
                <center>
                    <table>
                        <c:if test="${usuario.tipo.id == 1}">
                        <tr>
                            <td style="width: 2px !important;">
                                <span style="vertical-align: 2px;">Vendedor</span>
                            </td>
                            <td>
                                <select onchange="buscarValorComissao()" data-width="180px" id="vendedorComissao" name="vendedorComissao" class="selectpicker" data-live-search="true" >
                                    <option>Todos</option>
                                    <c:forEach var="b" items="${vendedores}">
                                        <option value="${b.id}">${b.nome}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        </c:if>
                        <tr>
                            <td><span style="vertical-align: 10px;">Período</span></td>
                            <td>
                                <input onchange="buscarValorComissao()" style="width: 102px;" type="text" readonly="" class="form-control datepickerTotal" name="dataInicial" id="dataInicial">  
                                à  <input onchange="buscarValorComissao()" style="width: 102px;" type="text" readonly="" class="form-control datepickerTotal" name="dataFinal" id="dataFinal">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div style="font-size: 40px; font-weight: 700; line-height: 55px " id="divTotalComissao"></div>
                </center>
            </div> 
            </c:if>
            <c:if test="${usuario.tipo.id == 1}">
            <div class="divDashboard" style="margin: 0;width:300px;">
                <span class="fas fa-dollar-sign" style="font-size: 25px; color:black !important; background-color: #4a4dc36e !important;"></span>
                <div class="tituloDashboard">Comissões Fotógrafos</div>
                <br>
                <span class="cursor tituloDashboard" title="Gerar arquivo xls" style="margin-left:125px;" onclick="comissaoExcelFotografo();"><i class="far fa-file-excel"></i></span>
                <center>
                    <table>
                        <tr>
                            <td style="width: 2px !important;">
                                <span style="vertical-align: 2px;">Fotógrafo</span>
                            </td>
                            <td>
                                <select onchange="buscarValorComissaoFotografo()" data-width="180px" id="fotografoComissao" name="fotografoComissao" class="selectpicker" data-live-search="true" >
                                    <option>Todos</option>
                                    <c:forEach var="b" items="${fotografos}">
                                        <option value="${b.id}">${b.nome}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><span style="vertical-align: 10px;">Período</span></td>
                            <td>
                                <input onchange="buscarValorComissaoFotografo()" style="width: 102px;" type="text" readonly="" class="form-control datepickerTotal" name="dataInicialFotografo" id="dataInicialFotografo">  
                                à  <input onchange="buscarValorComissaoFotografo()" style="width: 102px;" type="text" readonly="" class="form-control datepickerTotal" name="dataFinalFotografo" id="dataFinalFotografo">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div style="font-size: 40px; font-weight: 700; line-height: 55px " id="divTotalComissaoFotografo"></div>
                </center>
            </div>            
            </c:if>
            
<%@include file="footer.jsp" %>