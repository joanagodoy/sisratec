<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<script type="text/javascript" charset="utf8" src="js/alterarSenha.js?${version}"></script>

<h1>Alterar Senha</h1>

<br>

<form style="padding-left: 5px;" onsubmit="return salvarSenha();">
    <div class="form-group">
        <label for="senhaAtual">Senha atual</label>
        <input type="password" required="" style="width: 300px;" class="form-control" name="senhaAtual" id="senhaAtual">
    </div>
    <div class="form-group">
        <label for="senhaNova">Nova senha</label>
        <input type="password" required="" style="width: 300px;" class="form-control" name="senhaNova" id="senhaNova">
    </div>
    <div class="form-group">
        <label for="senhaNovaConf">Confirmação</label>
        <input type="password" required="" style="width: 300px;" class="form-control" onblur="" name="senhaNovaConf" id="senhaNovaConf">
    </div>
        <input type="submit" id="submitAlterarSenha" style="display:none">
        <button type="button" class="btn btn-success" onclick="$('#submitAlterarSenha').click();">Alterar</button>
</form>

</div>
</div>
</div>

<%@include file="footer.jsp" %>