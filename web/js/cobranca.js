//var editor;

$(function() {
    listarClientes();   
    
//    editor = new $.fn.dataTable.Editor( {
//        ajax: {url: "", "dataSrc": ""
//            , "data": function(d) {
//                d.status = $("input[name='status']:checked").val();
//                d.adimplencia = $("input[name='adimplencia']:checked").val();
//                d.periodicidade = $("#periodicidade").val();
//            }
//        },
//        table: "#tableFaturas",
//        fields: [ {
//                label: "ID:",
//                name: "id"
//            }, {
//                label: "nomeFantasia:",
//                name: "nomeFantasia"
//            }, {
//                label: "razSocial:",
//                name: "razSocial"
//            }, {
//                label: "periodicidade:",
//                name: "periodicidade"
//            }, {
//                label: "adimplencia:",
//                name: "adimplencia"
//            }, {
//                label: "dtPrimeiraFatura:",
//                name: "dtPrimeiraFatura",
//                type: "datetime"
//            }, {
//                label: "dtProximoVencimento:",
//                name: "dtProximoVencimento",
//                type: "datetime"
//            }, {
//                label: "totalMesesRecorrenciasPagas:",
//                name: "totalMesesRecorrenciasPagas"
//            }, {
//                label: "ativo:",
//                name: "ativo"
//            }
//        ]
//    } );
});

function listarClientes() {
//    alert($("input[name='status']:checked").val());
//    alert($("input[name='adimplencia']:checked").val());
//    alert($("#periodicidade").val());
    $("#tableAssinaturas").dataTable().fnDestroy();
//    $("#tableAssinaturas").empty();
    $("#tableAssinaturas").html("<thead><tr><th>#</th><th>Nome Fantasia</th><th>Razão Social</th><th>Periodicidade</th><th>Adimplência</th><th>primeira fatura</th><th>proximo vencimento</th><th>Qtd Meses Faltantes</th><th>Status</th></tr></thead>");
    var table = $("#tableAssinaturas").DataTable({language: {url: "js/libs/dataTable.json"}, 
        "processing": true, 
        "dom": '<"toolbar">frtip',
        "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarCobrancasClientes", "dataSrc": ""
            , "data": function(d) {
                d.status = $("input[name='status']:checked").val();
                d.adimplencia = $("input[name='adimplencia']:checked").val();
                d.periodicidade = $("#periodicidade").val();
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "nomeFantasia"},
            {"data": "razSocial"},
            {"data": "periodicidade"},
            {"data": "adimplencia"},
            {"data": "dtPrimeiraFatura"},
            {"data": "dtProximoVencimento"},
            {"data": "mesesFaltantes"},
            {"data": "ativo"}
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "";
                    }else{
                        var dataFormatada = dateFormatter(data).split('/');
                        return '<span style="display:none" >'+(dataFormatada[2] + dataFormatada[1] + dataFormatada[0]) * 1+'</span>'+dateFormatter(data);
                    }
                },
                "visible": false,
                "targets": [5,6]
            },            
            {
                "render": function(data, type, row) {
                    //<i class='fas fa-dot-circle'>
                    if(data == true){
                            return " <span style=\"padding-left:10px;color: green;\" data-toggle=\"tooltip\" title=\"Ativo\"> Ativo </span>";
                    }else{
                        return " <span style=\"padding-left:10px;color: gray;\" data-toggle=\"tooltip\" title=\"Inativo\"> Inativo </i></span>";
                    }
                },
                "targets": 8
            }
            ,
            {
                "render": function(data, type, row) {
                    if(data == 1){
                        return " <span style=\"padding-left:10px;color: green;\" data-toggle=\"tooltip\" title=\"Adimplente\"> Adimplente </span>";
                    }else if(data == 2){
                        return " <span style=\"padding-left:10px;color: red;\" data-toggle=\"tooltip\" title=\"Inadimplente\"> Inadimplente </i></span>";
                    }else if(data == 3){
                        return " <span style=\"padding-left:10px;color: yellow;\" data-toggle=\"tooltip\" title=\"Advogado\"> Advogado </i></span>";
                    }else{
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"À Verificar\"> À Verificar </i></span>";
                    }
                },
                "targets": 4
            },
            {
                "render": function(data, type, row) {
                    if(data == 0){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"À Vista\"> À Vista </span>";
                    }else if(data == 1){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Anual\"> Anual </span>";
                    }else if(data == 12){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Mensal\"> Mensal </i></span>";
                    }else if(data == 4){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Quadrimestral\"> Quadrimestral </i></span>";
                    }else if(data == 3){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Trimestral\"> Trimestral </i></span>";
                    }else if(data == 6){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Semestral\"> Semestral </i></span>";
                    }else{
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Verificar\"> Verificar </i></span>";
                    }
                },
                "targets": 3
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    setTimeout(function() {
        $("div.toolbar").css("float", "left");
        $("div.toolbar").css("padding-bottom", "5px");
//        var selectTorre = $("#divSelectTorre").html();
//        $("#divSelectTorre").remove();
        $("div.toolbar").html('<button type="button" onclick="novo();" class="btn btn-success">Nova Cobrança</button> ');
    }, 500);
    $('#tableAssinaturas tbody').on('click', 'tr td:not(:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
        var nomeFantasia = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].nomeFantasia;
        var adimplencia = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].adimplencia;
        var dtPrimeiraFatura = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].dtPrimeiraFatura;
        var dtProximoVencimento = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].dtProximoVencimento;
//        var data = table.row( this ).data();
        editar(id, nomeFantasia, adimplencia, dtPrimeiraFatura, dtProximoVencimento);
    });
}

function novo() {
    loading("open");
    $("#form input[type=reset]").click();
    preencherClientes();
}

function preencherClientes(){
    $.post("BuscarClientes", {ativo: true}, function(data) {
        $('select[name=cliente]').html("");
        for (var a = 0; a < data.length; a++) {
                $("#cliente").append("<option value='" + data[a].id + "'>" + data[a].razSocial + "-"+ data[a].nomeFantasia + "</option>");
        }
        $("#cliente").selectpicker("refresh");
    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading("close");
        preencheDescricao();
        $("#dialogCriar").modal("show");
        
    });
}

 // Activate an inline edit on click of a table cell
$('#tableFaturas').on( 'click', 'tbody td:not(:first-child)', function (e) {
    editor.inline( this );
} );

function editar(idCliente, nomeFantasia, adimplencia, dtPrimeiraFatura, dtProximoVencimento) {
    $("#idCliente").val(idCliente);
    $("#dtPrimeiraFatura").val(dateFormatter(dtPrimeiraFatura));
    $("#dtProximoVencimento").val(dateFormatter(dtProximoVencimento));
    buscarMesesFaltantes(idCliente);
    $('select[name=adimplencia]').val(adimplencia);
    $('.selectpicker').selectpicker('refresh');
    $("#tituloFatura").html(nomeFantasia);
    $("#dialogListar").modal("show");
    loading("open");
    $("#tableFaturas").dataTable().fnDestroy();
    $("#tableFaturas").empty();
    $("#tableFaturas").html("<thead><tr><th>#</th><th>Data de Emissão </th> <th>Data de Vencimento </th><th>Data de Pagamento</th><th>Valor</th><th>Referência</th><th>Qtd Meses</th><th>Descrição</th><th>Status</th></tr></thead>");
    var table = $("#tableFaturas").DataTable({language: {url: "js/libs/dataTable.json"}, 
        "processing": true, 
        "dom": '<"toolbar">frtip', 
        "responsive": true,
        "order": [[2, "desc"]],
        "ajax": {url: "BuscarFaturasCliente", "dataSrc": ""
            , "data": function(d) {
                d.cliente = idCliente;
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "dtEmissao"},
            {"data": "dtVencimento"},
            {"data": "dtPagamento"},
            {"data": "valor"},
            {"data": "registro.descricao"},
            {"data": "qtdMeses"},
            {"data": "descricaoFatura"},
            {"data": "status.descricao"}
            
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "Pendente";
                    }else{
                        var dataFormatada = dateFormatter(data).split('/');
                        return '<span style="display:none" >'+(dataFormatada[2] + dataFormatada[1] + dataFormatada[0]) * 1+'</span>'+dateFormatter(data);
                    }
                },
                "targets": 1
            },
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "";
                    }else{
                        return data;
                    }
                },
                "targets": 7
            },
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "Pendente";
                    }else{
                        var dataFormatada = dateFormatter(data).split('/');
                        return '<span style="display:none" >'+(dataFormatada[2] + dataFormatada[1] + dataFormatada[0]) * 1+'</span>'+dateFormatter(data);
                    }
                },
                "targets": 2
            },
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "Pendente";
                    }else{
                        var dataFormatada = dateFormatter(data).split('/');
                        return '<span style="display:none" >'+(dataFormatada[2] + dataFormatada[1] + dataFormatada[0]) * 1+'</span>'+dateFormatter(data);
                    }
                },
                "targets": 3
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    $('#tableFaturas tbody').on('click', 'tr td:not(:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
        var referencia = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].registro.id;
        var dtPagamento = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].dtPagamento;
        var qtdMeses = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].qtdMeses;
        var status = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].status.id;
        editarFatura(id, referencia, dtPagamento, qtdMeses, status);
    });
}

function editarFatura(id, referencia, dtPagamento, qtdMeses, status){
    $("#idFatura").val(id);
    $("#referenciaFatura").val(referencia);
    $("#dtPagamentoFatura").val(dateFormatter(dtPagamento));
    $("#qtdMesesFatura").val(qtdMeses);
    $("#statusFatura").val(status);
    $(".selectpicker").selectpicker("refresh");
    $("#dialogEditarFatura").modal("show");
}

function salvarFatura(){
    var myform = $('#editarFaturaform');
    var serialized = myform.serialize();
    loading("open");
    $.post("SalvarFatura", serialized, function(data) {
        if(data != ""){
            alertar("Erro ao salvar o registro", "danger");
        }else{
            alertar("Registro salvo com sucesso", "success");
            $("#dialogEditarFatura").modal("hide");
            $("#tableFaturas").DataTable().ajax.reload();
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
    }).always(function() {
        loading("close");
    });
}

function buscarMesesFaltantes(idCliente){
    $.post("BuscarMesesFaltantes", {cliente: idCliente} ,function(data) {
        $("#qtdmesesfaltantes").html(data);
    }).fail(function(xhr, textStatus, errorThrown) {
        alertar("Ocorreu algum erro ao buscar os meses faltantes", "danger");
    }).always(function() {
        
    });
}

function salvar() {
    var myform = $('#form');
    var serialized = myform.serialize();
    loading("open");
    $.post("SalvarCobranca", serialized, function(data) {
        if(data != ""){
            alertar("Erro ao salvar o registro", "danger");
        }else{
            alertar("Registro salvo com sucesso", "success");
            $(".tablej").DataTable().ajax.reload();
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
    }).always(function() {
        loading("close");
    });
}

function criar() {
    var myform = $('#criarform');
    var serialized = myform.serialize();
    loading("open");
    $.post("CriarCobranca", serialized, function(data) {
        if(data != ""){
            alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
        }else{
            $("#dialogCriar").modal("hide");
            alertar("Registro criado com sucesso", "success");
            $("#tableAssinaturas").DataTable().ajax.reload();
        }
    }).fail(function(xhr, textStatus, errorThrown) {
            if (xhr.status == "417") {
                alertar("Cliente Não Tem Cadastro no Vindi!", "alert");
            } else{
                alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
            }    
    }).always(function() {
        loading("close");
    });
}

function preencheDescricao(){
    if($("#formcriarperiodicidade").val() === 1){
        
    }else if($("#formcriarperiodicidade").val() === "6"){
        $("#descricao").html("Semestralidade Tour Virtual Isratec");
    }else if($("#formcriarperiodicidade").val() === "3"){
        $("#descricao").html("Trimestralidade  Tour Virtual Isratec");
    }else if($("#formcriarperiodicidade").val() === "4"){
        $("#descricao").html("Quadrimestralidade Tour Virtual Isratec");
    }
}

