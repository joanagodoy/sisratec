var cnpj_cpf_valido = true;
var alterarCadastrao = false;

$(function() {
//    changeMask();
//    
    expandirMenu();
    
    listarClientes("adimplentes");
    buscarQtdTabsClientes();
    
});
var pessoaTipo;
function changeMask(){
    if($("#tipo").val() == 1){//juridica
        pessoaTipo = 1;
        $("#cnpj").mask("99.999.999/9999-99");
        $("#divInscricao").show();
        $("#divRg").hide();
        $("#labelCnpjCpf").html("CNPJ");
        $("#inscricao").prop('required',true);
        $("#rg").removeAttr('required');
    }else{
        pessoaTipo = 2;
        $("#cnpj").mask("999.999.999-99");
        $("#divInscricao").hide();
        $("#divRg").show();
        $("#labelCnpjCpf").html("CPF");
        $("#rg").prop('required',true);
        $("#inscricao").removeAttr('required');
        $("#rg").mask("99.999.999-9");
    }
    return true;
}

function buscarQtdTabsClientes(){
    $.post("buscarQtdTabsClientes", function(data) {
        $(".badge-success").html(data.split(";")[1]);
        $(".badge-warning").html(data.split(";")[2]);
        $(".badge-danger").html(data.split(";")[3]);
        $(".badge-light").html(data.split(";")[4]);
        
    });
}


function listarClientes(tab) {
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    $("#table"+tab).dataTable().fnDestroy();
    $("#table"+tab).html("<thead><tr><th>#</th><th>Nome Fantasia </th><th> Razão Social </th><th> Responsável </th><th> Periodicidade </th><th> Qtd Meses Faltantes </th></tr></thead>");
    var table = $("#table"+tab).DataTable({language: {url: "js/libs/dataTable.json"}, 
        "processing": false, 
        "dom": '<"toolbar">frtip', 
        "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarClientes", "dataSrc": ""
            , "data": function(d) {
                d.filtro = tab;
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "nomeFantasia"},
            {"data": "razSocial"},
            {"data": "responsavel"},
            {"data": "periodicidade"},
            {"data": "mesesFaltantes"}  
            
        ],
        "columnDefs": [
//            {
//                "render": function(data, type, row) {
//                    //<i class='fas fa-dot-circle'>
//                    if(data == true){
//                            return " <span style=\"padding-left:10px;color: green;\" data-toggle=\"tooltip\" title=\"Ativo\"> Ativo </span>";
//                    }else{
//                        return " <span style=\"padding-left:10px;color: gray;\" data-toggle=\"tooltip\" title=\"Inativo\"> Inativo </i></span>";
//                    }
//                },
//                "targets": 4
//            }
            {
                "render": function(data, type, row) {
                    if(data == 0){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"À Vista\"> À Vista </span>";
                    }else if(data == 1){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Anual\"> Anual </span>";
                    }else if(data == 12){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Mensal\"> Mensal </i></span>";
                    }else if(data == 4){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Quadrimestral\"> Quadrimestral </i></span>";
                    }else if(data == 3){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Trimestral\"> Trimestral </i></span>";
                    }else if(data == 6){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Semestral\"> Semestral </i></span>";
                    }else{
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Verificar\"> Verificar </i></span>";
                    }
                },
                "targets": 4
            }
        ],
        "fnDrawCallback": function(oSettings) {
        }
    });
//    setTimeout(function() {
//        $("div.toolbar").css("float", "left");
//        $("div.toolbar").css("padding-bottom", "5px");
////        var selectTorre = $("#divSelectTorre").html();
////        $("#divSelectTorre").remove();
//        $("div.toolbar").html('<button type="button" onclick="novo();" class="btn btn-success">Novo</button> ');
//    }, 500);
    $("#table"+tab +" tbody").on('click', 'tr td:not(:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
//        var data = table.row( this ).data();
        abrirCliente(id);
    });
//    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
//        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
//            return;
//        }
//        var rowIdx = table
//                .cell(this)
//                .index().row;
//        var id = table
//                .rows(rowIdx)
//                .nodes().data()[rowIdx].id;
//        var check = $("input[name=selected][value=" + id + "]");
//        if (check.prop("checked")) {
//            check.prop("checked", false);
//        } else {
//            check.prop("checked", true);
//        }
//    });
}

//function tipoChanged() {
//    var tipo = $("#form [name=tipo]").val();
//    if (tipo == 1 || tipo == 4 || tipo == 5 || tipo == 6) {
//        $(".formUnidade").hide(); 
//    } else {
//        $(".formUnidade").show();
//    }
//    
//}

//function iniciarExclusao() {
//    if ($("input[name=selected]:checked").length > 0) {
//        $("#dialogExcluir").modal("show");
//    } else {
//        alertar("Você deve selecionar pelo menos 1 item para excluir", "alert");
//    }
//}

//function novo() {
//    changeMask();
//    $("#divAtivo").css('display', 'none');
//    $("#divPossuiOutroAtivo").css('display', 'none');
////    $("#divVendedorCliente").css('display', 'none');
//    $("#form input[type=reset]").click();
//    $("#form input[name=id]").val("");
//    $("#form input[name=tipo]").val(1);
//    $("#cepAviso").css('display', 'none');
//    $("#cnpjAviso").css('display', 'none');
//    $("#btLimpar").show();
//    $("#dialogEditar").modal("show");
//}
//
//function editar(id) {
//    alert(2);
//    limparFormulario();
//    loading("open");
//    $("#divAtivo").css('display', 'block');
//    $("#divPossuiOutroAtivo").css('display', 'block');
//    $.post("BuscarCliente", {id: id}, function(data) {
//        cnpj_cpf_valido = true;
//        $("#form input[name=fantasia]").val(data.nomeFantasia);
//        $("#form input[name=razSocial]").val(data.razSocial);
//        $("#form input[name=responsavel]").val(data.responsavel);
//        $("#vendedorCliente").val(data.vendedor.id);
////        $("#divVendedorCliente").show();
//        
//        $("#form input[name=cnpj]").val(data.cnpj);
//        if(data.cnpj.length > 11){
////            pessoaTipo = 1;
//            $("#tipo").val(1);
////            $("#cnpj").mask("99.999.999/9999-99");
//            $("#inscricao").val(data.inscricao);
//        }else{
//            $("#tipo").val(2);
////            pessoaTipo = 2;
////            $("#cnpj").mask("999.999.999-99");
//            $("#rg").val(data.rg);            
//        }
//        
//        $("#ramo").val(data.ramo.id);            
//        $(".selectpicker").selectpicker("refresh");
//        changeMask();
//        
//        if(data.endereco != null){
//            $("#form input[name=cep]").val(data.endereco.cep);
//            $("#form input[name=cep]").addClass("cep");
//
//            $("#form input[name=uf]").val(data.endereco.uf);
//            $("#form input[name=cidade]").val(data.endereco.cidade.nome);
//            $("#form input[name=bairro]").val(data.endereco.bairro);
//            $("#form input[name=rua]").val(data.endereco.rua);
//            $("#form input[name=numero]").val(data.endereco.numero);
//            $("#form input[name=complemento]").val(data.endereco.complemento);
//        }
//        
//        $("#form input[name=telefone]").val(data.telefone1);
//        if(data.telefone1.length > 10) {  
//            $("#form input[name=telefone]").mask("(99) 99999-999?9");  
//        } else {  
//            $("#form input[name=telefone]").mask("(99) 9999-9999?9");  
//        }  
//        $("#form input[name=telefone2]").val(data.telefone2);
//        if(data.telefone2.length > 10) {  
//            $("#form input[name=telefone2]").mask("(99) 99999-999?9");  
//        } else {  
//            $("#form input[name=telefone2]").mask("(99) 9999-9999?9");  
//        }
//        $("#form input[name=telefone3]").val(data.telefone3);
//        if(data.telefone3.length > 10) {  
//            $("#form input[name=telefone3]").mask("(99) 99999-999?9");  
//        } else {  
//            $("#form input[name=telefone3]").mask("(99) 9999-9999?9");  
//        }
//        $("#form input[name=email]").val(data.email);
//        $("#form input[name=email2]").val(data.email2);
//
//        $("#form input[name=id]").val(id);
//        $("#form input[name=dataCobranca]").val(dateFormatter(data.dtCobranca));
//        $("#form input[name=ramo]").val(data.ramo);
//        if(data.ativo === true){
//            $("#form [name=ativo]").prop("checked", true);
//        } 
//        
//        if(data.possuiOutroAtivo === true){
//            $("#form [name=possuioutroativo]").prop("checked", true);
//        } 
//        
//        $("#btLimpar").hide();
//    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
//    }).always(function() {
//        loading("close");
//        $("#dialogEditar").modal("show");
//    });
//
//}
//
//
//function salvar() {
//    var erro = false;
//    
////    fixForm();
//    var myform = $('#form');
//    var disabled = myform.find(':input:disabled').removeAttr('disabled');
//    var serialized = myform.serialize();
//    disabled.attr('disabled','disabled');
//    if(cnpj_cpf_valido){
//        loading("open");
//        $.post("SalvarCliente", serialized, function(data) {
//            if(data != ""){
//                erro = true;
//                alertar("O cliente " + data + " já possui este CNPJ/CPF!", "danger");
//            }else{
//                alertar("Registro salvo com sucesso", "success");
//                $(".tablej").DataTable().ajax.reload();
//            }
//        }).fail(function(xhr, textStatus, errorThrown) {
//            erro = true;
//            if (xhr.status == "409") {
//                alertar("Você não tem mais permissão para alterar este cliente, entre em contato com a Administração!", "alert");
//            } else if (xhr.status == "414") {
//                alertar("Já existe cliente com este CNPJ/CPF!", "danger");
//            } else{
//                alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
//            }
//        }).always(function() {
//            $("#dialogEditar").modal("hide");
//            loading("close");
//            if(alterarCadastrao && !erro){
//                alterarCadastro("Contrato");
//            }
//        });
//    }else{
//        if(pessoaTipo == 1){
//             alertar("CNPJ Inválido", "alert");
//        }else{
//             alertar("CPF Inválido", "alert");
//        }
//       
//    }
//}
//
////function excluir() {
////    $("#dialogExcluir").modal("hide");
////    loading("open");
////    var ids = [];
////    $("input[name=selected]:checked").each(function(index) {
////        ids.push($(this).val());
////    });
////    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
////        alertar("Registros excluídos com sucesso", "success");
////        $(".tablej").DataTable().ajax.reload();
////    }).fail(function() {
////        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
////    }).always(function() {
////        loading("close");
////    });
////}
//
//function buscarCEP(){
//    $("#cepAviso").css('display', 'none');
//    var cep = $("#cep").val().replace(/\D/g, '');
//    if (cep !== "") {
//        //Expressão regular para validar o CEP.
//        var validacep = /^[0-9]{8}$/;
//            //Valida o formato do CEP.
//            if(validacep.test(cep)) {
//
//                //Preenche os campos com "..." enquanto consulta webservice.
//                $("#rua").val("...");
//                $("#bairro").val("...");
//                $("#cidade").val("...");
//                $("#uf").val("...");
//
//                //Consulta o webservice viacep.com.br/
//                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/unicode/?callback=?", function(dados) {
//
//                    if (!("erro" in dados)) {
//                        //Atualiza os campos com os valores da consulta.
//                        $("#rua").val(dados.logradouro);
//                        $("#bairro").val(dados.bairro);
//                        $("#cidade").val(dados.localidade);
//                        $("#uf").val(dados.uf);
////                        $("#codigoMunicipio").val(dados.ibge);
//                    } //end if.
//                    else {
//                        //CEP pesquisado não foi encontrado.
//                        limpa_formulário_cep();
////                        alert("CEP não encontrado.");
//                        $("#cepAviso").css('display', 'block');
//                    }
//                });
//            } //end if.
//            else {
//                //cep é inválido.
//                limpa_formulário_cep();
////                alert("Formato de CEP inválido.");
//                $("#cepAviso").css('display', 'block');
//            }
//    }else {
//            //cep sem valor, limpa formulário.
//            limpa_formulário_cep();
//    }    
//}
//
//function limpa_formulário_cep() {
//    // Limpa valores do formulário de cep.
//    $("#rua").val("");
//    $("#bairro").val("");
//    $("#cidade").val("");
//    $("#uf").val("");
////    $("#codigoMunicipio").val("");
//}
//
//
//
//function validarCNPJCPF() {
//    $("#cnpjAviso").css('display', 'none');
//    cnpj = $("#cnpj").val().toString().replace(/[^\d]+/g,'');
//    cnpj_cpf_valido = true;
//    if(pessoaTipo == 1){
//        $("#cnpjAviso").html("CNPJ INVÁLIDO!");
////        if(cnpj == ''){
////            $("#cnpjAviso").css('display', 'block');
////            return false;
////        }
//
//        if (cnpj.length != 14){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//        }
//
//        // Elimina CNPJs invalidos conhecidos
//        if (cnpj == "00000000000000" || 
//            cnpj == "11111111111111" || 
//            cnpj == "22222222222222" || 
//            cnpj == "33333333333333" || 
//            cnpj == "44444444444444" || 
//            cnpj == "55555555555555" || 
//            cnpj == "66666666666666" || 
//            cnpj == "77777777777777" || 
//            cnpj == "88888888888888" || 
//            cnpj == "99999999999999"){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//        }
//
//
//        // Valida DVs
//        tamanho = cnpj.length - 2
//        numeros = cnpj.substring(0,tamanho);
//        digitos = cnpj.substring(tamanho);
//        soma = 0;
//        pos = tamanho - 7;
//        for (i = tamanho; i >= 1; i--) {
//          soma += numeros.charAt(tamanho - i) * pos--;
//          if (pos < 2)
//                pos = 9;
//        }
//        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
//        if (resultado != digitos.charAt(0)){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//        }
//        tamanho = tamanho + 1;
//        numeros = cnpj.substring(0,tamanho);
//        soma = 0;
//        pos = tamanho - 7;
//        for (i = tamanho; i >= 1; i--) {
//          soma += numeros.charAt(tamanho - i) * pos--;
//          if (pos < 2)
//                pos = 9;
//        }
//        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
//        if (resultado != digitos.charAt(1)){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//        }         
//
//        $("#cnpjAviso").css('display', 'none');
//        return true;
//    }else{
//        $("#cnpjAviso").html("CPF INVÁLIDO!");
//        var Soma;
//        var Resto;
//        Soma = 0;
//        if (cnpj == "00000000000" || 
//            cnpj == "11111111111" || 
//            cnpj == "22222222222" || 
//            cnpj == "33333333333" || 
//            cnpj == "44444444444" || 
//            cnpj == "55555555555" || 
//            cnpj == "66666666666" || 
//            cnpj == "77777777777" || 
//            cnpj == "88888888888" || 
//            cnpj == "99999999999"){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//        }
//            
//        for (i=1; i<=9; i++) Soma = Soma + parseInt(cnpj.substring(i-1, i)) * (11 - i);
//        Resto = (Soma * 10) % 11;
//
//        if ((Resto == 10) || (Resto == 11))  Resto = 0;
//        if (Resto != parseInt(cnpj.substring(9, 10)) ){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//            
//        }
//
//            Soma = 0;
//        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cnpj.substring(i-1, i)) * (12 - i);
//        Resto = (Soma * 10) % 11;
//
//        if ((Resto == 10) || (Resto == 11))  Resto = 0;
//        if (Resto != parseInt(cnpj.substring(10, 11) ) ){
//            cnpj_cpf_valido = false;
//            $("#cnpjAviso").css('display', 'block');
//            return false;
//            
//        }
//        return true;
//    }
//    
//}
//
//function limparFormulario(){
//    $("#form input[type=reset]").click();
//    $("#form input[name=id]").val("");
//    $("#cepAviso").css('display', 'none');
//    $("#cnpjAviso").css('display', 'none');
//}

function abrirCliente(id){
    $(location).attr('href','Cliente?id='+id);
}