var alterarCadastrao = false;


$(function() {
    listarCobrancas();

});

//function reloadUsuarios() {
//    loading("open");
//    $(".tablej").DataTable().ajax.reload();
//}

function reloadCobrancas() {
    loading("open");
    $(".tablej").DataTable().ajax.reload();
}

function listarCobrancas() {
    var table = $(".tablej").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[2, "asc"]],
        "ajax": {url: "BuscarCobrancaBoletos", "dataSrc": ""
            , "data": function(d) {
                
                d.vencidos =  $('#vencidos').is(':checked');
            }
        },
        "columns": [
            {"data": "id", className: "never"},
            {"data": "dtPagamento"},
            {"data": "codigo"},
            {"data": "cliente.razSocial"},
            {"data": "dtEmissao"},
            {"data": "dtVencimento"}
//            {"data": "valor"}
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": "_all"},
            {
                "render": function(data, type, row) {
                    return dateFormatter(data);
                },
                "targets": [4, 5]
            },
            {
                "render": function(data, type, row) {
//                    return "<span data-toggle=\"tooltip\" title=\"Não Pago\" class=\"fa fa-clock-o\" ></span> ";
                    return "<span data-toggle=\"tooltip\" title=\"Vizualizar dados do cliente\" class=\"fa fa-address-card-o\" onclick=\"carregarCliente(" + row.cliente.id + ")\"></span> " ;
                },
                "targets": 6
            }
            ,
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "<span data-toggle=\"tooltip\" title=\"Não Pago\" class=\"fa fa-clock-o\" ></span> ";
                    }else{
                        return "<span data-toggle=\"tooltip\" title=\"Pago\" class=\"fa fa-check\" ></span> ";
                    }
                },
                "targets": 1
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
//    setTimeout(function() {
//        $("div.toolbar").css("float", "left");
//        $("div.toolbar").css("padding-bottom", "5px");
////        var selectTorre = $("#divSelectTorre").html();
////        $("#divSelectTorre").remove();
//        $("div.toolbar").html('<input type="checkbox" class="" name="vencido" id="vencido" checked onchange="">  Vencidos</input> ');
//    }, 500);
    $('.tablej tbody').on('click', 'tr td:not(:last-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
//        var data = table.row( this ).data();
        editar(id);
    });
//    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
//        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
//            return;
//        }
//        var rowIdx = table
//                .cell(this)
//                .index().row;
//        var id = table
//                .rows(rowIdx)
//                .nodes().data()[rowIdx].id;
//        var check = $("input[name=selected][value=" + id + "]");
//        if (check.prop("checked")) {
//            check.prop("checked", false);
//        } else {
//            check.prop("checked", true);
//        }
//    });
}

//function tipoChanged() {
//    var tipo = $("#form [name=tipo]").val();
//    if (tipo == 1 || tipo == 4 || tipo == 5 || tipo == 6) {
//        $(".formUnidade").hide(); 
//    } else {
//        $(".formUnidade").show();
//    }
//    
//}

//function iniciarExclusao() {
//    if ($("input[name=selected]:checked").length > 0) {
//        $("#dialogExcluir").modal("show");
//    } else {
//        alertar("Você deve selecionar pelo menos 1 item para excluir", "alert");
//    }
//}

function carregarCliente(id) {
//    limparFormulario();
    loading("open");
    $.post("BuscarCliente", {id: id}, function(data) {
        $("#form input[name=estabelecimento]").val(data.nomeFantasia);
        $("#form input[name=responsavel]").val(data.responsavel);
        
        $("#form input[name=cnpj]").val(data.cnpj);
        $("#form input[name=cnpj]").mask('99.999.999/9999-99');
//        $("#form input[name=cnpj]").addClass("cnpj");
        
        $("#form input[name=cep]").val(data.endereco.cep);
//        $("#form input[name=cep]").mask("99999-999");
        $("#form input[name=cep]").addClass("cep");
        
        $("#form input[name=uf]").val(data.endereco.uf);
        $("#form input[name=cidade]").val(data.endereco.cidade.nome);
        $("#form input[name=bairro]").val(data.endereco.bairro);
        $("#form input[name=rua]").val(data.endereco.rua);
        $("#form input[name=numero]").val(data.endereco.numero);
        $("#form input[name=complemento]").val(data.endereco.complemento);
        $("#form input[name=telefone]").val(data.telefone1);
        if(data.telefone1.length > 10) {  
            $("#form input[name=telefone]").mask("(99) 99999-999?9");  
        } else {  
            $("#form input[name=telefone]").mask("(99) 9999-9999?9");  
        }  
        $("#form input[name=telefone2]").val(data.telefone2);
        if(data.telefone2.length > 10) {  
            $("#form input[name=telefone2]").mask("(99) 99999-999?9");  
        } else {  
            $("#form input[name=telefone2]").mask("(99) 9999-9999?9");  
        }
        $("#form input[name=email]").val(data.email);
//        $("#form [name=tipo]").val(data.tipo.id);

//        if(data.excluido === true){
//            $("#form [name=excluido]").prop("checked", true);
//        }
//        if (typeof data.unidade != "undefined")
//            $("#form [name=unidade]").val(data.unidade.id);
//        tipoChanged();
        $("#form input[name=id]").val(id);
        $("#btLimpar").hide();
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading("close");
        $("#dialogCliente").modal("show");
    });

}

//
//function novo() {
//    $("#form input[type=reset]").click();
//    $("#form input[name=id]").val("");
////    tipoChanged();
//    $("#cepAviso").css('display', 'none');
//    $("#cnpjAviso").css('display', 'none');
//    $("#btLimpar").show();
//    $("#dialogEditar").modal("show");
//    
//    
//}
//
//function alterarMaskTelefone(id, tipo){
//    alert(id);
//    alert(tipo);
//    if(tipo == 1){//fixo
//        $("#"+id).mask("(99) 9999-9999");
//    }else{//celular
//        $("#"+id).mask("(99) 99999-9999");
//    }
//}
//
//function editar(id) {
//    limparFormulario();
//    loading("open");
//    $.post("BuscarCliente", {id: id}, function(data) {
//        $("#form input[name=estabelecimento]").val(data.nomeFantasia);
//        $("#form input[name=responsavel]").val(data.responsavel);
//        
//        $("#form input[name=cnpj]").val(data.cnpj);
//        $("#form input[name=cnpj]").mask('99.999.999/9999-99');
////        $("#form input[name=cnpj]").addClass("cnpj");
//        
//        $("#form input[name=cep]").val(data.endereco.cep);
////        $("#form input[name=cep]").mask("99999-999");
//        $("#form input[name=cep]").addClass("cep");
//        
//        $("#form input[name=uf]").val(data.endereco.uf);
//        $("#form input[name=cidade]").val(data.endereco.cidade.nome);
//        $("#form input[name=bairro]").val(data.endereco.bairro);
//        $("#form input[name=rua]").val(data.endereco.rua);
//        $("#form input[name=numero]").val(data.endereco.numero);
//        $("#form input[name=complemento]").val(data.endereco.complemento);
//        $("#form input[name=telefone]").val(data.telefone1);
//        if(data.telefone1.length > 10) {  
//            $("#form input[name=telefone]").mask("(99) 99999-999?9");  
//        } else {  
//            $("#form input[name=telefone]").mask("(99) 9999-9999?9");  
//        }  
//        $("#form input[name=telefone2]").val(data.telefone2);
//        if(data.telefone2.length > 10) {  
//            $("#form input[name=telefone2]").mask("(99) 99999-999?9");  
//        } else {  
//            $("#form input[name=telefone2]").mask("(99) 9999-9999?9");  
//        }
//        $("#form input[name=email]").val(data.email);
////        $("#form [name=tipo]").val(data.tipo.id);
//
////        if(data.excluido === true){
////            $("#form [name=excluido]").prop("checked", true);
////        }
////        if (typeof data.unidade != "undefined")
////            $("#form [name=unidade]").val(data.unidade.id);
////        tipoChanged();
//        $("#form input[name=id]").val(id);
//        $("#btLimpar").hide();
//    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
//    }).always(function() {
//        loading("close");
//        $("#dialogEditar").modal("show");
//    });
//
//}
//
//
//function salvar() {
//    var erro = false;
//    loading("open");
////    fixForm();
//    var myform = $('#form');
//    var disabled = myform.find(':input:disabled').removeAttr('disabled');
//    var serialized = myform.serialize();
//    disabled.attr('disabled','disabled');
//    
//    $.post("SalvarCliente", serialized, function(data) {
//        alertar("Registro salvo com sucesso", "success");
//        $(".tablej").DataTable().ajax.reload();
//    }).fail(function(xhr, textStatus, errorThrown) {
//        erro = true;
//        if (xhr.status == "409") {
//            alertar("Você não tem mais permissão para alterar este cliente, entre em contato com a Administração!", "alert");
//        } else if (xhr.status == "414") {
//            alertar("Já existe cliente com este CNPJ cadastrado!", "danger");
//        } else{
//            alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
//        }
//    }).always(function() {
//        $("#dialogEditar").modal("hide");
//        loading("close");
//        if(alterarCadastrao && !erro){
//            alterarCadastro("Contrato");
//        }
//    });
//}
//
////function excluir() {
////    $("#dialogExcluir").modal("hide");
////    loading("open");
////    var ids = [];
////    $("input[name=selected]:checked").each(function(index) {
////        ids.push($(this).val());
////    });
////    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
////        alertar("Registros excluídos com sucesso", "success");
////        $(".tablej").DataTable().ajax.reload();
////    }).fail(function() {
////        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
////    }).always(function() {
////        loading("close");
////    });
////}
//
//function buscarCEP(){
//    $("#cepAviso").css('display', 'none');
//    var cep = $("#cep").val().replace(/\D/g, '');
//    if (cep !== "") {
//        //Expressão regular para validar o CEP.
//        var validacep = /^[0-9]{8}$/;
//            //Valida o formato do CEP.
//            if(validacep.test(cep)) {
//
//                //Preenche os campos com "..." enquanto consulta webservice.
//                $("#rua").val("...");
//                $("#bairro").val("...");
//                $("#cidade").val("...");
//                $("#uf").val("...");
//
//                //Consulta o webservice viacep.com.br/
//                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/unicode/?callback=?", function(dados) {
//
//                    if (!("erro" in dados)) {
//                        //Atualiza os campos com os valores da consulta.
//                        $("#rua").val(dados.logradouro);
//                        $("#bairro").val(dados.bairro);
//                        $("#cidade").val(dados.localidade);
//                        $("#uf").val(dados.uf);
////                        $("#codigoMunicipio").val(dados.ibge);
//                    } //end if.
//                    else {
//                        //CEP pesquisado não foi encontrado.
//                        limpa_formulário_cep();
////                        alert("CEP não encontrado.");
//                        $("#cepAviso").css('display', 'block');
//                    }
//                });
//            } //end if.
//            else {
//                //cep é inválido.
//                limpa_formulário_cep();
////                alert("Formato de CEP inválido.");
//                $("#cepAviso").css('display', 'block');
//            }
//    }else {
//            //cep sem valor, limpa formulário.
//            limpa_formulário_cep();
//    }    
//}
//
//function limpa_formulário_cep() {
//    // Limpa valores do formulário de cep.
//    $("#rua").val("");
//    $("#bairro").val("");
//    $("#cidade").val("");
//    $("#uf").val("");
////    $("#codigoMunicipio").val("");
//}
//
//function validarCNPJ() {
////    alert("dada");
//    $("#cnpjAviso").css('display', 'none');
//    cnpj = $("#cnpj").val().toString().replace(/[^\d]+/g,'');
// 
//    if(cnpj == ''){
////        alert("porra");
//        $("#cnpjAviso").css('display', 'block');
//        return false;
//    }
//     
//    if (cnpj.length != 14){
////        alert("tamanho");
//        $("#cnpjAviso").css('display', 'block');
//        return false;
//    }
// 
//    // Elimina CNPJs invalidos conhecidos
//    if (cnpj == "00000000000000" || 
//        cnpj == "11111111111111" || 
//        cnpj == "22222222222222" || 
//        cnpj == "33333333333333" || 
//        cnpj == "44444444444444" || 
//        cnpj == "55555555555555" || 
//        cnpj == "66666666666666" || 
//        cnpj == "77777777777777" || 
//        cnpj == "88888888888888" || 
//        cnpj == "99999999999999"){
//        alert("estranho");
//        $("#cnpjAviso").css('display', 'block');
//        return false;
//    }
//        
//         
//    // Valida DVs
//    tamanho = cnpj.length - 2
//    numeros = cnpj.substring(0,tamanho);
//    digitos = cnpj.substring(tamanho);
//    soma = 0;
//    pos = tamanho - 7;
//    for (i = tamanho; i >= 1; i--) {
//      soma += numeros.charAt(tamanho - i) * pos--;
//      if (pos < 2)
//            pos = 9;
//    }
//    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
//    if (resultado != digitos.charAt(0)){
//        alert("aqui");
//        $("#cnpjAviso").css('display', 'block');
//        return false;
//    }
//        
//         
//    tamanho = tamanho + 1;
//    numeros = cnpj.substring(0,tamanho);
//    soma = 0;
//    pos = tamanho - 7;
//    for (i = tamanho; i >= 1; i--) {
//      soma += numeros.charAt(tamanho - i) * pos--;
//      if (pos < 2)
//            pos = 9;
//    }
//    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
//    if (resultado != digitos.charAt(1)){
//        alert("aqui2");
//        $("#cnpjAviso").css('display', 'block');
//        return false;
//    }         
//    
//    $("#cnpjAviso").css('display', 'none');
//    return true;
//    
//}
//
//function limparFormulario(){
//    $("#form input[type=reset]").click();
//    $("#form input[name=id]").val("");
//    $("#cepAviso").css('display', 'none');
//    $("#cnpjAviso").css('display', 'none');
//}