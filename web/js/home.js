$(function() {
    
    dashGeral($(".dashInformacoes[name=mes]"));
    if (tipoUsuarioSessao == 1 || tipoUsuarioSessao == 2) {
        $("#trVisitasPagas").show();
    }
    
    //se admin
//    if(tipoUsuarioSessao == 1){
        var day1 = new Date();
        var dd1 = ("0" + day1.getDate()).slice(-2);
        var mm1 = ("0" + (day1.getMonth() + 1)).slice(-2);
        var yyyy1 = day1.getFullYear();
        day1 = '01/' + mm1 + '/' + yyyy1;
        $("#dataInicial").val(day1);
        $("#dataInicialFotografo").val(day1);
        
        var lastDayMonth = new Date(yyyy1, mm1, 0);
        var dd2 = ("0" + lastDayMonth.getDate()).slice(-2);
        
//        var day2 = new Date(mm1 + '/' + dd1 + '/' + yyyy1);
//        day2.setDate(day2.getDate() + 7);
//        var dd2 = ("0" + lastDayMonth.getDate()).slice(-2);
//        var mm2 = ("0" + (day2.getMonth() + 1)).slice(-2);
//        var yyyy2 = day2.getFullYear();
//        var lastDayMonth = new Date(yyyy2, mm2, 0);
//        day2 = lastDayMonth + '/' + mm2 + '/' + yyyy2;
        var day2 = dd2 + '/' + mm1 + '/' + yyyy1;
        $("#dataFinal").val(day2);
        $("#dataFinalFotografo").val(day2);

        buscarValorComissao();
        buscarValorComissaoFotografo();
//    }
    
    //se fotografo
    if(tipoUsuarioSessao == 3){
        $.post("BuscarFotografias", {status: 1}).done(function(data) {
            if(data.length != 0){
                alertar("Ei, Você possui " + data.length +" clientes pendentes para enviar as fotos para os editores! =)", "alert");
            }
        });
        $.post("BuscarFotografias", {status: 4}).done(function(data) {
            if(data.length != 0){
                alertar("Ei, Você possui " + data.length +" clientes com falta de pontos de vista! =O", "alert");
            }
        });
    }
    
    //se editor
    if(tipoUsuarioSessao == 5){
        $.post("BuscarFotografias", {status: 8}).done(function(data) {
            if(data.length != 0){
                alertar("Ei, Você possui " + data.length +" clientes com problemas! =O", "alert");
            }
        });
    }
});


function dashGeral(periodo) {
    $(".dashInformacoes").removeClass("botaoDashboardAtivo");
    $(periodo).addClass("botaoDashboardAtivo");
    var img = "<img src='img/progress.gif' width=15 height = 15 style='width:15px;height:15px;'/>";
    $("#informacoesPendentes").html(img);
    $("#informacoesCancelados").html(img);
    $("#informacoesRealizados").html(img);
    $("#informacoesPagos").html(img);
    $.post("BuscarInformacoesVisita", {periodo: $(periodo).attr("name")}).done(function(data) {
        $("#informacoesPendentes").html('<span class="cursor" onclick="carregarVisitas(\''+data[0]+'\',\''+data[1]+'\',\'reservado\');">' + (data[2] != null ? data[2] : 0) + '</span>');
        $("#informacoesCancelados").html('<span class="cursor" onclick="carregarVisitas(\''+data[0]+'\',\''+data[1]+'\',\'cancelado\');">' + (data[3] != null ? data[3] : 0) + '</span>');
        $("#informacoesRealizados").html('<span class="cursor" onclick="carregarVisitas(\''+data[0]+'\',\''+data[1]+'\',\'realizado\');">' + (data[4] != null ? data[4] : 0) + '</span>');
        $("#informacoesPagos").html('<span class="cursor" onclick="carregarVisitas(\''+data[0]+'\',\''+data[1]+'\',\'pago\');">' + (data[5] != null ? data[5] : 0) + '</span>');
    });
}

function carregarVisitas(dataInicial,dataFinal,status){
    return false;
//    window.location = "VisitaLista?dataInicial="+dataInicial+"&dataFinal="+dataFinal+"&status="+status;
}

function comissaoExcel(){
    window.location = "BuscarComissaoVendedor?dataInicial="+ $("#dataInicial").val()+"&dataFinal="+ $("#dataFinal").val()+"&vendedor="+ $("#vendedorComissao").val()+ "&excel=1";
}

function comissaoExcelFotografo(){
    window.location = "BuscarComissaoFotografo?dataInicial="+ $("#dataInicialFotografo").val()+"&dataFinal="+ $("#dataFinalFotografo").val()+"&fotografo="+ $("#fotografoComissao").val()+ "&excel=1";
}

function buscarValorComissao(){
    $.post("BuscarComissaoVendedor", {dataInicial: $("#dataInicial").val(), dataFinal: $("#dataFinal").val(), vendedor: $("#vendedorComissao").val()}).done(function(data) {
        $("#divTotalComissao").html(currencyFormatter(data));
    });
}

function buscarValorComissaoFotografo(){
    $.post("BuscarComissaoFotografo", {dataInicial: $("#dataInicialFotografo").val(), dataFinal: $("#dataFinalFotografo").val(), fotografo: $("#fotografoComissao").val()}).done(function(data) {
        $("#divTotalComissaoFotografo").html(currencyFormatter(data));
    });
}

//function carregarPendencias() {
//    $.post("HME.BuscarFuncionariosPendencias", {}).done(function(data) {
//        var resultado = "";
//        for (var i = 0; i < data.length; i++) {
//            resultado += "<tr>";
//            resultado += "<td>" + data[i][0] + "</td>";
//            resultado += "<td>" + data[i][1] + "</td>";
//            resultado += "<td>" + data[i][2] + "</td>";
//            resultado += "</tr>";
//        }
//        $("#tablePendencias tbody").html(resultado);
//    });
//}

var cores = ["#CE4646", "#F68022", "#3498db", "#9D24EE", "#317928", "#778899", "#FFDAB9", "#FF6347", "#87CEEB", "#006400", "#DB7093", "#2E8B57"];

function dashboardVendasAdicionais(periodo) {
    $(".dashInformacoes").removeClass("botaoDashboardAtivo");
    $(periodo).addClass("botaoDashboardAtivo");
    $.post("SRV.RelatorioEvolucaoClientes", {periodo: $(periodo).attr("name")}).done(function(data) {
        $("#canvasVendasAdicionais").remove();
        $("#tableLegendaEvolucaoVolume tr div").each(function(index) {
            $(this).css("background-color", cores[index]);
        });
        $("#divCanvasVendasAdicionais").html("");
        $("#divCanvasVendasAdicionais").append("<canvas id='canvasVendasAdicionais' height='220' width='375'></canvas>");
        var dados = {
            labels: ["", "", "", "", ""],
            datasets: [
                {
                    label: "",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1,
                    hoverBorderColor: "rgba(255,99,132,1)",
                    data: [new Number(data.pararam), data.mantiveram, data.iniciaram, data.aumentaram, data.aumentaram50, data.diminuiram, data.diminuiram50]
                }
            ]
        };
        var ctx = document.getElementById("canvasVendasAdicionais").getContext("2d");
        window.barFill = new Chart(ctx).Bar(dados, {tooltipFontSize: 8});
        barFill.datasets[0].bars[0].fillColor = cores[0];
        barFill.datasets[0].bars[1].fillColor = cores[1];
        barFill.datasets[0].bars[2].fillColor = cores[2];
        barFill.datasets[0].bars[3].fillColor = cores[3];
        barFill.datasets[0].bars[4].fillColor = cores[4];
        barFill.datasets[0].bars[0].highlightFill = cores[0];
        barFill.datasets[0].bars[1].highlightFill = cores[1];
        barFill.datasets[0].bars[2].highlightFill = cores[2];
        barFill.datasets[0].bars[3].highlightFill = cores[3];
        barFill.datasets[0].bars[4].highlightFill = cores[4];
        barFill.update();
    });

}