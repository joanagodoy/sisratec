$(function() {

});


function salvarSenha() {
    var senhaAtual = $("#senhaAtual").val();
    var novaSenha = $("#senhaNova").val();
    var novaSenhaConfirma = $("#senhaNovaConf").val();
    if (segurancaSenha($("#senhaNova")) >= 40) {
        if (novaSenha == novaSenhaConfirma) {
            $.post("AlteraSenha", {novaSenha: novaSenha, senhaAtual: senhaAtual})
                    .done(function(data) {
                        if (data == 'ok') {
                            $("#senhaAtual").val('');
                            $("#senhaNova").val('');
                            $("#senhaNovaConf").val('');
                            $("#senhaNaoCoincide").hide();
                            $("#senhaFraca").hide();
                            $("#senhaMedia").hide();
                            $("#senhaBoa").hide();
                            alertar('Senha alterada com sucesso!.', 'success');
                        } else if (data == 'erro') {
                            alertar('Senha atual não confere.', 'danger');
                        } else {
                            alertar('Erro desconhecido.', 'danger');
                        }
                    });

        } else {
            alertar('Senhas não coincidem.', 'danger');
        }
    } else {
        alertar('A segurança de sua senha deve ser no mínimo média.', 'danger');
    }
    return false;
}

function segurancaSenha(campo) {
    var senha = $(campo).val();
    var seguranca = 0;
    if (senha.length != 0) {
        if (tem_numeros(senha) && tem_letras(senha)) {
            seguranca += 30;
        }
        if (tem_minusculas(senha) && tem_maiusculas(senha)) {
            seguranca += 30;
        }
        if (senha.length >= 4 && senha.length <= 5) {
            seguranca += 10;
        } else {
            if (senha.length >= 6 && senha.length <= 8) {
                seguranca += 30;
            } else {
                if (senha.length > 8) {
                    seguranca += 40;
                }
            }
        }
    }
    return seguranca;
}

function verificarSenhasIguais() {
    var novaSenha = $("#senhaNova").val();
    var novaSenhaConfirma = $("#senhaNovaConf").val();
    if (novaSenha != novaSenhaConfirma) {
        alertar("Senhas não coincidem", "danger");
    }
}

function tem_numeros(texto) {
    var numeros = "0123456789";
    for (i = 0; i < texto.length; i++) {
        if (numeros.indexOf(texto.charAt(i), 0) != -1) {
            return 1;
        }
    }
    return 0;
}

function tem_letras(texto) {
    var letras = "abcdefghyjklmnopqrstuvwxyz";
    texto = texto.toLowerCase();
    for (i = 0; i < texto.length; i++) {
        if (letras.indexOf(texto.charAt(i), 0) != -1) {
            return 1;
        }
    }
    return 0;
}

function tem_maiusculas(texto) {
    var letras_maiusculas = "ABCDEFGHYJKLMNOPQRSTUVWXYZ";
    for (i = 0; i < texto.length; i++) {
        if (letras_maiusculas.indexOf(texto.charAt(i), 0) != -1) {
            return 1;
        }
    }
    return 0;
}

function tem_minusculas(texto) {
    var letras = "abcdefghyjklmnopqrstuvwxyz";
    for (i = 0; i < texto.length; i++) {
        if (letras.indexOf(texto.charAt(i), 0) != -1) {
            return 1;
        }
    }
    return 0;
}
