$(document).ready(function() {
    
//    try {
//        $(".selectpicker").selectpicker();
//    } catch (ex) {
//
//    }
    $(".datepickerTotal").datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'dd/mm/yy',
//        todayBtn: "linked",
        language: "pt-BR",
        calendarWeeks: false,
        autoclose: true,
        todayHighlight: true    
    });
    $(".datepicker").datepicker({
        minDate: "+1D",
        format: "dd/mm/yyyy",
        dateFormat: 'dd/mm/yy',
        startDate: "+1d",
//        todayBtn: "linked",
        language: "pt-BR",
        calendarWeeks: false,
        autoclose: true,
        todayHighlight: true    
    });
    $(".datepickerBefore").datepicker({
        maxDate: "+0D",
        format: "dd/mm/yyyy",
        dateFormat: 'dd/mm/yy',
        startDate: "+1d",
//        todayBtn: "linked",
        language: "pt-BR",
        calendarWeeks: false,
        autoclose: true,
        todayHighlight: true    
    });
    $('div.ui-datepicker').css("font-size", "10px");
    
    if (!isMobile()) {
        $(".contaMobile").hide();
        $(".contaDesktop").show();
    }
    $('table.tablewfpd').DataTable({aoColumns: null, "bFilter": false, responsive: {
            details: {
                renderer: function(api, rowIdx) {
                    // Select hidden columns for the given row
                    var data = api.cells(rowIdx, ':hidden').eq(0).map(function(cell) {
                        var header = $(api.column(cell.column).header());

                        return '<tr>' +
                                '<td>' +
                                header.text() + ':' +
                                '</td> ' +
                                '<td>' +
                                api.cell(cell).data() +
                                '</td>' +
                                '</tr>';
                    }).toArray().join('');

                    return data ?
                            $('<table/>').append(data) :
                            false;
                }
            }
        }, "bPaginate": false, "bInfo": false});

    $(".tablewfpd thead").remove();

    $("input.telefone").mask("(99) 9999-9999?9").focusout(function (event) {  
        var target, phone, element;  
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
        phone = target.value.replace(/\D/g, '');
        element = $(target);  
        element.unmask();  
        if(phone.length > 10) {  
            element.mask("(99) 99999-999?9");  
        } else {  
            element.mask("(99) 9999-9999?9");  
        }  
    });
    
        
    $("[data-toggle='tooltip']").tooltip();
    $("input.numero").maskMoney({showSymbol: false, symbol: "", decimal: ",", thousands: "."});
    $(".cnpj").mask('99.999.999/9999-99');
    $(".cpf").mask('999.999.999-99');
    $(".cep").mask("99999-999");
//    $(".celular").mask("(99) 99999-9999");
//    $(".telefone").mask("(99) 9999-9999");
    $(".datepickerTotal").mask('99/99/9999');
    $(".datepicker").mask('99/99/9999');
    $(".datepickerBefore").mask('99/99/9999');
    if (!Modernizr.inputtypes.date) {
//        $("input[type=date]").datepicker({language: "pt-BR", format: 'yyyy-mm-dd', });
    }
    if (!Modernizr.inputtypes.number) {
        $("input[type=number]").keydown(function(event) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                    // Allow: Ctrl+A
                            (event.keyCode == 65 && event.ctrlKey === true) ||
                            // Allow: home, end, left, right
                                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    else {
                        // Ensure that it is a number and stop the keypress
                        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                            event.preventDefault();
                        }
                    }
                });
    }
});

function alterarCadastro(location){
    window.location = location+"?u=open";
}

function isMobile() {
    return $("#logoIsratec").css("display") != 'none';
}

function fixForm() {
    $(".numero").each(function(index) {
        if ($(this).val().indexOf(",") != -1) {
            $(this).toNumber({region: 'pt-BR'});
            $(this).val($(this).val().replace(",", "."));

        }
    });
}

function loading(v) {
    if (v === true) {
        $(".preloader").show();
    } else {
        $(".preloader").fadeOut();
    }
}

function alertar(mensagem, tipo) {
    var html = '';
    var id = "alert_" + (Math.random() * 10000).toFixed(0);
    if (typeof tipo == "undefined" || tipo == "alert") {
        html += '<div id="' + id + '" class="alert alert-warning alert-dismissible"  role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Atenção!</strong> ';
    } else if (tipo == "success") {
        html += '<div id="' + id + '" class="alert alert-success alert-dismissible" role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Muito bem!</strong> ';
    } else if (tipo == "danger") {
        html += '<div id="' + id + '" class="alert alert-danger alert-dismissible" role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Ocorreu um erro!</strong> ';
    } else if (tipo == "info") {
        html += '<div id="' + id + '" class="alert alert-info alert-dismissible" role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Aviso!</strong> ';
    }
    html += mensagem;
    html += '</div>';
    $("#alertContainer").append(html);
    var tempo = mensagem.length * 0.12 * 1000;
    if (tempo < 4000) {
        tempo = 4000;
    }
    setTimeout("$('#" + id + "').alert('close');", tempo);

}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}


function numberFormatter(valor) {
    return replaceAll("p", ",", replaceAll(",", ".", replaceAll(".", "p", new Number(valor).toFixed(2).replace(/./g, function(c, i, a) {
        return i && c !== "." && !((a.length - i) % 3) ? ',' + c : c;
    }))));

}
function currencyFormatter(valor) {
    return "R$ " + replaceAll("p", ",", replaceAll(",", ".", replaceAll(".", "p", new Number(valor).toFixed(2).replace(/./g, function(c, i, a) {
        return i && c !== "." && !((a.length - i) % 3) ? ',' + c : c;
    }))));

}
function dateFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null || typeof cellvalue == "undefined") {
        return "";
    }
    var today = new Date(cellvalue);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return today = dd + '/' + mm + '/' + yyyy;
}

function dateTimeFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null || typeof cellvalue == "undefined") {
        return "";
    }
    var today = new Date(cellvalue);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var m = today.getMinutes();
    if (hh < 10)
    {
        hh = '0' + hh;
    }
    if (m < 10)
    {
        m = '0' + m;
    }
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return today = dd + '/' + mm + '/' + yyyy + ' ' + hh + ":" + m;
}
function timeFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null || typeof cellvalue == "undefined") {
        return "";
    }
    var today = new Date(cellvalue);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var m = today.getMinutes();
    if (hh < 10)
    {
        hh = '0' + hh;
    }
    if (m < 10)
    {
        m = '0' + m;
    }
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return  hh + ":" + m;
}

function time24Formatter(time){
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if(AMPM === "PM" && hours<12) hours = hours+12;
    if(AMPM === "AM" && hours===12) hours = hours-12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if(hours<10) sHours = "0" + sHours;
    if(minutes<10) sMinutes = "0" + sMinutes;
    return  sHours + ":" + sMinutes;
}

function expandirMenu() {
//    var width = 300;
//    if ($("#divMenuEsquerdo").width() > 70) {
//        width = 70;
//        $("#logoMenu").fadeOut(250);
//        $(".divSubMenu").fadeOut(250);
//        $(".textoMenu").fadeOut(250);
//        $(".setasMenu").fadeOut(250);
//        $(".setasMenu").removeClass("glyphicon-menu-up");
//        $(".setasMenu").addClass("glyphicon-menu-down");
//    } else {
//        $(".setasMenu").fadeIn(750);
//        $(".textoMenu").fadeIn(750);
//        $("#logoMenu").fadeIn(750);
//    }
//    $("#divMenuEsquerdo").animate({
//        width: width
//    });
//    if (width == 300 && $("#paginaExpandida").val() != '') {
//        expandirSubMenu($("#paginaExpandida").val());
//        $("#paginaExpandida").val('');
//    }
    $("#expand-menu").click();
}