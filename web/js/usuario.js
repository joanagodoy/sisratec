$(function() {
    expandirMenu();
    listarUsuarios();
});

function reloadUsuarios() {
    loading("open");
    $(".tablej").DataTable().ajax.reload();
}

function listarUsuarios() {
    var table = $(".tablej").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarUsuario", "dataSrc": "", "data": function(d) { }},
        "columns": [
            {"data": "id", className: "never"},
            {"data": "nome"},
            {"data": "tipo.descricao"}
        ],
//        "columnDefs": [
//            {
//                "render": function(data, type, row) {
//                    return "<input type='checkbox' name='selected' value='" + row.id + "'>";
//                },
//                "targets": 0
//            },
//            {
//                "defaultContent": "",
//                "targets": [4, 3]
//            }
//        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    setTimeout(function() {
        $("div.toolbar").css("float", "left");
        $("div.toolbar").css("padding-bottom", "5px");
//        var selectTorre = $("#divSelectTorre").html();
        $("#divSelectTorre").remove();
        $("div.toolbar").html('<button type="button" onclick="novo();" class="btn btn-success">Novo</button>');
    }, 500);
    $('.tablej tbody').on('click', 'tr td', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
        editar(id);
    });
//    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
//        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
//            return;
//        }
//        var rowIdx = table
//                .cell(this)
//                .index().row;
//        var id = table
//                .rows(rowIdx)
//                .nodes().data()[rowIdx].id;
//        var check = $("input[name=selected][value=" + id + "]");
//        if (check.prop("checked")) {
//            check.prop("checked", false);
//        } else {
//            check.prop("checked", true);
//        }
//    });
}

function tipoChanged() {
    var tipo = $("#form [name=tipo]").val();
    if (tipo == 1 || tipo == 4 || tipo == 5 || tipo == 6) {
        $(".formUnidade").hide(); 
    } else {
        $(".formUnidade").show();
    }
}

function iniciarExclusao() {
    if ($("input[name=selected]:checked").length > 0) {
        $("#dialogExcluir").modal("show");
    } else {
        alertar("Você deve selecionar pelo menos 1 item para excluir", "alert");
    }
}

function novo() {
    $("#form input[type=reset]").click();
    $("#form input[name=id]").val("");
    $("#divAtivo").css('display', 'none');
    $("#form input[name=email]").attr('disabled',false);
    $("#dialogEditar").modal("show");
}

function editar(id) {
    $("#form input[type=reset]").click();
    $("#divAtivo").css('display', 'block');
    loading("open");
    $.post("BuscarUsuario", {id: id}, function(data) {
        $("#form input[name=nome]").val(data.nome);
        $("#form input[name=email]").val(data.email);
        $("#form input[name=email]").attr('disabled','disabled');
        
        $("#form input[name=senha]").val(data.senha);
        $('select[name=tipo]').val(data.tipo.id);
        $("select[name=regiao]").val(data.regiao.id);
//        $('.selectpicker').selectpicker('refresh');
        $(".select2").select2();
        if(data.ativo === true){
            $("#form [name=ativo]").prop("checked", true);
        }        
        $("#form input[name=id]").val(data.id);
        
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading("close");
        $("#dialogEditar").modal("show");
    });

}

function salvar() {    
    var myform = $('#form');
    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var serialized = myform.serialize();
    disabled.attr('disabled','disabled');
    
    loading("open");
    fixForm();
    $.post("SalvarUsuario", serialized, function(data) {
        alertar("Registro salvo com sucesso", "success");
        $(".tablej").DataTable().ajax.reload();
    }).fail(function(xhr, textStatus, errorThrown) {
        alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
    }).always(function() {
        $("#dialogEditar").modal("hide");
        loading("close");
    });
}

function excluir() {
    $("#dialogExcluir").modal("hide");
    loading("open");
    var ids = [];
    $("input[name=selected]:checked").each(function(index) {
        ids.push($(this).val());
    });
    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
        alertar("Registros excluídos com sucesso", "success");
        $(".tablej").DataTable().ajax.reload();
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
    }).always(function() {
        loading("close");
    });
}
