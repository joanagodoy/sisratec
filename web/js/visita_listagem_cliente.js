/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {   
//    var day1 = new Date();
////    var dd1 = ("0" + day1.getDate()).slice(-2);
//    var mm1 = ("0" + (day1.getMonth() + 1)).slice(-2);
//    var yyyy1 = day1.getFullYear();
//    day1 = '01/' + mm1 + '/' + yyyy1;
//    $("#dataInicial").val(day1);
//    var lastDayMonth = new Date(yyyy1, mm1, 0);
//    var dd2 = ("0" + lastDayMonth.getDate()).slice(-2);
////    var day2 = new Date(mm1 + '/' + dd1 + '/' + yyyy1);
////    day2.setDate(day2.getDate() + 7);
////    var dd2 = ("0" + day2.getDate()).slice(-2);
////    var mm2 = ("0" + (day2.getMonth() + 1)).slice(-2);
////    var yyyy2 = day2.getFullYear();
//    var day2 = dd2 + '/' + mm1 + '/' + yyyy1;
//    $("#dataFinal").val(day2);
//    
//    preencherRegioes();
//    gerarTable();

});

function listarVisitasCliente() {
    $("#tableVisitasCliente").dataTable().fnDestroy();
    $("#tableVisitasCliente").empty();
    $("#tableVisitasCliente").html("<thead><tr><th>#</th><th>Data</th><th>Status</th><th>hrI</th><th>hrF</th></tr></thead><tbody></tbody>");
    var table = $("#tableVisitasCliente").DataTable({language: {url: "js/libs/dataTable.json"}, 
        "processing": false, 
        dom: '<"toolbar">lfrtip',
        "bLengthChange": false,
        "responsive": true,
        "ajax": {url: "BuscarVisitas", "dataSrc": "", "data": {
//                "dataInicial": $("#dataInicial").val(), "dataFinal": $("#dataFinal").val(), "status": $("#status").val()
                "idCliente": idCliente
            }},
        "columns": [
            {"data": "id"},
            {"data": "dtVisita"},
            {"data": "status.descricao"},
            {"data": "hrVisitaInicial"},
            {"data": "hrVisitaFinal"}
        ],
        "columnDefs": [
            {
                "targets": [ 0,3,4 ],
                "visible": false,
                "searchable": false
            },
//            {
//                "render": function(data, type, row) {
//                    if(data == null){
//                        return "";
//                    }else{
//                        return data;
//                    }
//                },
//                "targets": 5
//            },
//            {
//                "render": function(data, type, row) {
//                    if(row.imovel == null){
//                        return data;
//                    }else{
//                        return row.imovel.descricao + " - " + data;
//                    }
//                },
//                "targets": 1
//            },
            {
                "render": function(data, type, row) {
                    var horarioFormatado = timeFormatter(row.hrVisitaInicial).split(':');
                    var dataFormatada = dateFormatter(data).split('/');
                    return '<span style="display:none" >'+(dataFormatada[2] + dataFormatada[1] + dataFormatada[0] + horarioFormatado[0]+horarioFormatado[1]) * 1+'</span>'+dateFormatter(data) +" - "+timeFormatter(row.hrVisitaInicial);
                },
                "targets": 1
            },
            {
                "render": function(data, type, row) {
                   if(data == 'Pagamento Efetuado' && tipoUsuarioSessao != 1 && tipoUsuarioSessao != 2){
                       return 'Realizada';
                   }else{
                       return data;
                   }
                },
                "targets": 2
            },
            {"width": "40%", "targets": 1}
        ]
    });
//    setTimeout(function() {
//        $("div.toolbar").css("float", "left");
////        $("div.toolbar").css("padding-bottom", "5px");
//        $("div.toolbar").css("margin-bottom", "10px");
//        var botoes = "";
//        if (tipoUser != 7) {
//            botoes += '<button style="margin-top: -5px;" type="button" onclick="novaReserva();" class="btn btn-success"><span class="descFiltro">Nova reserva</span><span title="Nova Reserva" style="display:none; margin-top: -5px;" class="descFiltroMob fa fa-plus"></span></button>';
//        }
//        if (tipoUser != 2 && tipoUser != 7) {
//            botoes += ' <button style="margin-top: -5px;" type="button" title="Checkin" onclick="novoCheckin();" class="btn btn-info"><span class="descFiltro">Check-in</span><span title="Checkin" style="display:none; margin-top: -5px;" class="descFiltroMob fa fa-check"></span></button>';
//        }
//        botoes += "<span data-toogle='tooltip' class='fa fa-print cursor' style='margin-top: -5px; margin-left: 7px; font-size: 20px; vertical-align: -3px;' title='Impressão simples'></span>";
//        $("div.toolbar").html(botoes);
//    }, 500);
    $('#tableVisitasCliente tbody').on('click', 'tr td:not(:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
        buscarVisita(id);
    });

}

function preencherRegioes(){
    $.post("BuscarRegiao").done(
        function(data) {
            var options = "";
            for (var i = 0; i < data.length; i++) {
                if(data[i].id == regiaoPadraoUsuario){
                    options = "<option value='" + data[i].id + "'>" + data[i].descricao + "</option>";
                }
            }

            for (var i = 0; i < data.length; i++) {
                if(data[i].id != regiaoPadraoUsuario){
                    options += "<option value='" + data[i].id + "'>" + data[i].descricao + "</option>";
                }
            }
            $("#regiao").html(options);
            $("#regiao").selectpicker("refresh");
        });
    
}

function confirmarFinalizar(tipo){
    if(tipo == 2){
        $("#formConfirmarPts [type=reset]").click();
        $("#dialogConfirmarPts").modal("show");
    }else{
        $("#dialogExcluir").modal("show");
    }
}

function finalizar(status){    
    var fst = $("#formConfirmarPts [name=ptsFeitos]").val();
    var sec = $("#formVisita [name=pts]").val();
    if(status == 2 && Number(sec)>Number(fst)){
        alertar("Não é possível salvar sem a quantidade de pontos mínimos!", "danger");
    }else{
        loading(true);
        $.post("FinalizarVisita", {cobranca: $('#cobranca').is(':checked'), obs: $("#formConfirmarPts [name=observacao]").val(), pts: fst, status: status, idVisita: $("#formVisita [name=id]").val(), idImovel: $("#formVisita [name=contrato]").val().split("-")[1]}, 
        function(data) {  
            alertar("Registro salvo com sucesso!", "success");
            $(".tablej").DataTable().ajax.reload();
        }).fail(function() {
            alertar("Ocorreu algum erro ao tentar alterar o registro", "danger");
        }).always(function() {
            $("#dialogVisita").modal("hide");
            $("#dialogConfirmarPts").modal("hide");
            $("#dialogExcluir").modal("hide");
            loading(false);        
        });    
    }
}

function buscarVisita(id) {
    $("#btPagamento").hide();
    $("#divPagamento").hide();
    
    $("#divVisitaRelizada").show();
//    $("#ptsRealizado").prop('required',false);
    
    $("#divLinkMaps").show();
    $("#divLinkMaps").html("");
    
    $("#divCliente").show();
    $("#divFormStatus").show();
    $("#divSelectCliente").hide();
//    if(usuarioVisita == usuarioSessao || tipoUsuarioSessao != 4){
        loading(true);
        $("#formVisita [name=contrato]").html("");
        $.post("buscarVisita", {id: id}, function(data) {
            
            if(data.imovel != null && typeof data.imovel !== "undefined"){
                $("#formVisita [name=contrato]").append("<option value='" + data.nrContrato.nrContrato + "-" + data.imovel.id + "'>" + data.nrContrato.nrContrato + "</option>");
                $("#formVisita [name=cliente]").html(data.imovel.descricao + " - " + data.nrContrato.cliente.nomeFantasia);
                $("#divLinkMaps").append("<a style='color: #337ab7' href='geo:0,0?q="+data.imovel.endereco.uf+"+"+data.imovel.endereco.cidade.nome+"+"+data.imovel.endereco.rua+"+"+data.imovel.endereco.numero+"'><i title='Abrir no Google Maps App' class='fas fa-map-marker-alt pointer'></i></a>");
                if(data.imovel.endereco != null){
                    $("#formVisita input[name=cep]").val(data.imovel.endereco.cep);
                    $("#formVisita input[name=cep]").addClass("cep");
                    $("#formVisita input[name=uf]").val(data.imovel.endereco.uf);
                    $("#formVisita input[name=cidade]").val(data.imovel.endereco.cidade.nome);
                    $("#formVisita input[name=bairro]").val(data.imovel.endereco.bairro);
                    $("#formVisita input[name=rua]").val(data.imovel.endereco.rua);
                    $("#formVisita input[name=numero]").val(data.imovel.endereco.numero);
                    $("#formVisita input[name=complemento]").val(data.imovel.endereco.complemento);
                }
            }else{
                $("#formVisita [name=contrato]").append("<option value='" + data.nrContrato.nrContrato + "-undefined" + "'>" + data.nrContrato.nrContrato + "</option>");
                $("#formVisita [name=cliente]").html(data.nrContrato.cliente.nomeFantasia);
                $("#divLinkMaps").append("<a style='color: #337ab7' href='geo:0,0?q="+data.nrContrato.cliente.endereco.uf+"+"+data.nrContrato.cliente.endereco.cidade.nome+"+"+data.nrContrato.cliente.endereco.rua+"+"+data.nrContrato.cliente.endereco.numero+"'><i title='Abrir no Google Maps App' class='fas fa-map-marker-alt pointer'></i></a>");
            }
                
                $("#contrato").selectpicker("refresh");
                $("#formVisita [name=id]").val(id);
                idCliente = data.nrContrato.cliente.id;
                $("#formVisita [name=dataVisita]").val(dateFormatter(data.dtVisita));
                buscarHorarios(data.hrVisitaInicial);
    //            $("#formVisita [name=horarioSelecionado]").val(timeFormatter(data.hrVisitaInicial));
                $("#formVisita [name=pts]").val(data.ptsVista);
                if(data.ptsVistaRealizado != null){
                    $("#formVisita [name=ptsRealizado]").val(data.ptsVistaRealizado);
                }else{
                    $("#formVisita [name=ptsRealizado]").val(0);
                }
                
                $("#formVisita [name=observacao]").val(data.obs);
                $("#formVisita [name=formStatus]").val(data.status.descricao);
                if (data.status.id == 1) {//Pendente
                    $("#btCancelada").show();
                    $("#btRealizada").show();
                    $("#btSalvar").show();
                } else if (data.status.id == 2) {//Realizado
                    $("#btCancelada").hide();
                    $("#btRealizada").hide();
                    $("#btSalvar").hide();
                    $("#divVisitaRelizada").show();
                    
                    if(tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1){
                        $("#btPagamento").show();
//                        $("#divPagamento").show();
//                        $("#pagamento").prop('checked', false);
                    }
                } else if (data.status.id == 3) {//Cancelado
                    $("#btCancelada").hide();
                    $("#btRealizada").hide();
                    $("#btSalvar").hide();
//                    if(tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1){
//                        $("#divPagamento").show();
//                        $("#pagamento").prop('checked', false);
//                    }
                }else if (data.status.id == 4) {//Pagamento Efetuado
                    $("#btCancelada").hide();
                    $("#btRealizada").hide();
                    $("#btSalvar").hide();
                    $("#divVisitaRelizada").show();
//                    if(tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1){
//                        $("#divPagamento").show();
//                        $("#pagamento").prop('checked', true);
//                    }
                }
                if(tipoUsuarioSessao == 3){
                    $("#formVisita [name=dataVisita]").prop( "disabled", true );
                    $("#formVisita [name=pts]").prop( "disabled", true );
                    $("#formVisita [name=observacao]").prop( "disabled", true );
                }
                
                if(tipoUsuarioSessao == 5){
                    $("#btCancelada").hide();
                    $("#btRealizada").hide();
                    $("#btSalvar").hide();
                    $("#btPagamento").hide();
                }                
                if(data.cliente.vendedor.id != usuarioSessao && tipoUsuarioSessao == 4){
                    $("#btCancelada").hide();
                    $("#btRealizada").hide();
                    $("#btSalvar").hide();
                    $("#btPagamento").hide();
                }
                onChangeCliente();
                $("#dialogVisita").modal("show");
        }).fail(function() {
            alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
        }).always(function() {
            loading(false);
        });
//    }else{
//        alertar("Você não tem permissão para acessar esta visita!", "alert");
//    }
}

//function cancelar() {
//    loading(true);
//    $.post("RSV.CancelarReserva", {id: $("#formVisualizacao [name=id]").val()}, function(data) {
//        $("#dialogVisualizar").modal("hide");
//        $("#calendar").fullCalendar('refetchEvents');
//    }).fail(function(xhr, textStatus, errorThrown) {
//        if (xhr.status == "400") {
//            alertar("Ocorreu algum erro ao tentar cancelar a reserva", "danger");
//        } else if (xhr.status == "410") {
//            alertar("Data da reserva anterior a data atual", "danger");
//        } else if (xhr.status == "403") {
//            alertar("Você não pode cancelar esta reserva", "danger");
//        } else if (xhr.status == "409") {
//            alertar("Prazo máximo para cancelamento atingido", "danger");
//        }
//    }).always(function() {
//        loading(false);
//    });
//}

//function aprovar() {
//    loading(true);
//    $.post("RSV.AprovarReserva", {id: $("#formVisualizacao [name=id]").val()}, function(data) {
//        $("#dialogVisualizar").modal("hide");
//        $("#calendar").fullCalendar('refetchEvents');
//    }).fail(function(xhr, textStatus, errorThrown) {
//        if (xhr.status == "400") {
//            alertar("Ocorreu algum erro ao tentar aprovar a reserva", "danger");
//        }
//    }).always(function() {
//        loading(false);
//    });
//}

function preencherContratos(visita){
//    if(!visita){//se visita false então preenche com todos os contratos para selecionar ao buscarVisita
//        visita = null;
//    }
    $.post("BuscarContratos", {visita: visita}, function(data) {
//        $('select[name=contrato]').html("");
//        for (var a = 0; a < data.length; a++) {
//                $("#contrato").append("<option value='" + data[a].nrContrato + "'>" + data[a].cliente.nomeFantasia + "</option>");
//        }
//        $("#contrato").selectpicker("refresh");

        $('select[name=contrato]').html("");
        for (var a = 0; a < data.length; a++) {
            if(data[a].imovel == null){
                $("#formVisita [name=contrato]").append("<option value='" + data[a].contrato.nrContrato + "-undefined" + "'>" + data[a].cliente.nomeFantasia + "</option>");
            }else{
                $("#formVisita [name=contrato]").append("<option value='" + data[a].contrato.nrContrato + "-" + data[a].imovel + "'>" + data[a].descricao + " - " + data[a].cliente.nomeFantasia + "</option>");
            }
        }
        $("#contrato").selectpicker("refresh");
        onChangeCliente();
    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading(false);
        $("#dialogVisita").modal("show");
    });
}

function onChangeCliente(){
//    $("#imovel").val($("#contrato").val().split("-")[1]);
	//alert($("#contrato").val().split("-")[1]);
    //if(typeof $("#contrato").val().split("-")[1] === "undefined"){
    if($("#contrato").val() != null){
        if($("#contrato").val().split("-")[1] == "undefined"){
            $("#cep").prop('required',false);
            $("#uf").prop('required',false);
            $("#cidade").prop('required',false);
            $("#bairro").prop('required',false);
            $("#rua").prop('required',false);
            $("#numero").prop('required',false);
            $("#divImovel").hide();
        }else{
            $("#cep").prop('required',true);
            $("#uf").prop('required',true);
            $("#cidade").prop('required',true);
            $("#bairro").prop('required',true);
            $("#rua").prop('required',true);
            $("#numero").prop('required',true);
            $("#divImovel").show();
        }
    }
}

function atualizaHorarioSelecionado(){
//    $("#horarioSelecionado").val($("#horario").val());
}

function novaVisita() {
//    if (podeReservar == true) {
        $("#formVisita [type=reset]").click();
        $("#divVisitaRelizada").hide();
        $("#formVisita input[name=id]").val("");
        $("#contrato").prop("disabled", false);
        $("#horario").html("");
        $("#btCancelada").hide();
        $("#btRealizada").hide();
        $("#btSalvar").show();
        $("#divCliente").hide();
        $("#divSelectCliente").show();
        $("#divLinkMaps").hide();
        $("#divFormStatus").hide();
        preencherContratos(true);
        $('.selectpicker').selectpicker('refresh');

}

function buscarHorarios(horarioSelecionado){
    if($("#dataVisita").val() !== ""){
        $.post("BuscarHorarios", {data: $("#dataVisita").val(), idVisita: $("#id").val(), contrato: $("#contrato").val()}, function(data) {
            $('select[name=horario]').html("");
            for (var a = 0; a < data.length; a++) {
                 $("#horario").append("<option value='" + time24Formatter(data[a]) + "'>" + time24Formatter(data[a]) + "</option>");
            }
            if(horarioSelecionado !== null){
                $('select[name=horario]').val(dateTimeFormatter(horarioSelecionado).substring(11));
            }
            $('.selectpicker').selectpicker('refresh');
        }).fail(function(xhr, textStatus, errorThrown) {
    //        if (xhr.status == "400") {
                alertar("Ocorreu algum erro ao buscar os horários disponíveis", "alert");
    //        } 
        }).always(function() {
            loading(false);
        });
    }
}

function salvar() {
    loading(true);
    
    var myform = $('#formVisita');
//    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var serialized = myform.serialize();
//    disabled.attr('disabled','disabled');
    
    $.post("SalvarVisita", serialized, function(data) {        
        alertar("Registro salvo com sucesso", "success");
        $(".tablej").DataTable().ajax.reload();
    }).fail(function(xhr, textStatus, errorThrown) {
        if (xhr.status == "400") {
            alertar("Ocorreu algum erro ao tentar efetuar a visita", "alert");
        }else if (xhr.status == "409") {
            alertar("Região do cliente selecionado não existe ou está inativa! Entrar em contato com a Administração.", "danger");
        } 
    }).always(function() {
        $("#dialogVisita").modal("hide");
        loading(false);
    });
}

//function dialogConfirmarFila(idReserva) {
//    $("#idFila").val(idReserva);
//    $("#dialogConfirmarFila").modal("show");
//}

//function converteDias(dias) {
//    dias = dias.substring(1, dias.length - 1);
//    dias = dias.split(",");
//    var resultado = "";
//    for (var i = 0; i < dias.length; i++) {
//        resultado += converteDia(dias[i].trim()) + ", ";
//    }
//    return resultado;
//}

//function converteDia(dia) {
//    switch (dia) {
//        case '1':
//            return "segunda";
//        case '2':
//            return "terça";
//        case '3':
//            return "quarta";
//        case '4':
//            return "quinta";
//        case '5':
//            return "sexta";
//        case '6':
//            return "sábado";
//        case '7':
//            return "domingo";
//    }
//}

//function buscarUsuarios() {
//    $.post("RSV.BuscarUsuariosAmbiente", {ambiente: $("#ambiente").val().split("-")[0]}).done(function(data) {
//        var html = "";
//        for (var i = 0; i < data.length; i++) {
//            var torre = "N/D";
//            if (data[i].unidade != null) {
//                torre = data[i].unidade.torre + " / " + data[i].unidade.descricao;
//            }
//            html += "<option value='" + data[i].id + "'>[ " + torre + " ] " + data[i].nome + "</option>";
//        }
//        $("#formCheckin [name=usuario]").html(html);
//        $("#formVisita [name=usuario]").html(html);
//    });
//}


//function salvarChecklist() {
//    loading(true);
//    fixForm();
//    $.post("RSV.SalvarChecklist", $("#form3").serialize(), function(data) {
//        alertar("Registros salvos com sucesso", "success");
//        $("#dialogEfetuarChecklist").modal("hide");
//        $("#dialogVisualizar").modal("hide");
//        $("#calendar").fullCalendar('refetchEvents');
//    }).fail(function() {
//        loading(false);
//        alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
//    }).always(function() {
//
//    });
//}

function expand(id, h3) {
    if ($(".grouped_" + id).hasClass("expanded")) {
        $(h3).children("span").removeClass("fa-minus-circle");
        $(h3).children("span").addClass("fa-plus-circle");
        $(".grouped_" + id).removeClass("expanded");
        $(".grouped_" + id).addClass("notExpanded");
    } else {
        $(h3).children("span").removeClass("fa-plus-circle");
        $(h3).children("span").addClass("fa-minus-circle");
        $(".grouped_" + id).removeClass("notExpanded");
        $(".grouped_" + id).addClass("expanded");
    }
}

//function minus(v) {
//    if (new Number($(v).next().val()) > 0) {
//        $(v).next().val(new Number($(v).next().val()) - 1);
//    }
//}
//
//function plus(v) {
//    $(v).prev().val(new Number($(v).prev().val()) + 1);
//}


function changeMask(){
    if($("#tipo").val() == 1){//juridica
        pessoaTipo = 1;
        $("#cnpj").mask("99.999.999/9999-99");
        $("#divInscricao").show();
        $("#divRg").hide();
        $("#labelCnpjCpf").html("CNPJ");
        $("#inscricao").prop('required',true);
        $("#rg").removeAttr('required');
    }else{
        pessoaTipo = 2;
        $("#cnpj").mask("999.999.999-99");
        $("#divInscricao").hide();
        $("#divRg").show();
        $("#labelCnpjCpf").html("CPF");
        $("#rg").prop('required',true);
        $("#inscricao").removeAttr('required');
        $("#rg").mask("99.999.999-9");
    }
    return true;
}
var idCliente;
function abrirCliente(id) {
//    limparFormulario();
    loading(true);
    $("#divAtivo").css('display', 'block');
    $.post("BuscarCliente", {id: idCliente}, function(data) {
        $("#form input[name=fantasia]").val(data.nomeFantasia);
        $("#form input[name=razSocial]").val(data.razSocial);
        $("#form input[name=responsavel]").val(data.responsavel);
        $("#form input[name=vendedorCliente]").val(data.vendedor.nome);
        $("#divVendedorCliente").show();
        
        $("#form input[name=cnpj]").val(data.cnpj);
        if(data.cnpj.length > 11){
//            pessoaTipo = 1;
            $("#tipo").val(1);
//            $("#cnpj").mask("99.999.999/9999-99");
            $("#inscricao").val(data.inscricao);
        }else{
            $("#tipo").val(2);
//            pessoaTipo = 2;
//            $("#cnpj").mask("999.999.999-99");
            $("#rg").val(data.rg);            
        }
        $("#tipo").selectpicker("refresh");
        changeMask();
        
        if(data.endereco != null){
            $("#form input[name=cep]").val(data.endereco.cep);
            $("#form input[name=cep]").addClass("cep");

            $("#form input[name=uf]").val(data.endereco.uf);
            $("#form input[name=cidade]").val(data.endereco.cidade.nome);
            $("#form input[name=bairro]").val(data.endereco.bairro);
            $("#form input[name=rua]").val(data.endereco.rua);
            $("#form input[name=numero]").val(data.endereco.numero);
            $("#form input[name=complemento]").val(data.endereco.complemento);
        }
        
        $("#form input[name=telefone]").val(data.telefone1);
        if(data.telefone1.length > 10) {  
            $("#form input[name=telefone]").mask("(99) 99999-999?9");  
        } else {  
            $("#form input[name=telefone]").mask("(99) 9999-9999?9");  
        }  
        $("#form input[name=telefone2]").val(data.telefone2);
        if(data.telefone2.length > 10) {  
            $("#form input[name=telefone2]").mask("(99) 99999-999?9");  
        } else {  
            $("#form input[name=telefone2]").mask("(99) 9999-9999?9");  
        }
        $("#form input[name=telefone3]").val(data.telefone3);
        if(data.telefone3.length > 10) {  
            $("#form input[name=telefone3]").mask("(99) 99999-999?9");  
        } else {  
            $("#form input[name=telefone3]").mask("(99) 9999-9999?9");  
        }
        $("#form input[name=email]").val(data.email);

        $("#form input[name=id]").val(id);
        
        if(data.ativo === true){
            $("#form [name=ativo]").prop("checked", true);
        }
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading(false);
        $("#dialogCliente").modal("show");
    });

}

function buscarCEP(){
    $("#cepAviso").css('display', 'none');
    var cep = $("#cep").val().replace(/\D/g, '');
    if (cep !== "") {
        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/unicode/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#uf").val(dados.uf);
//                        $("#codigoMunicipio").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulario_cep();
//                        alert("CEP não encontrado.");
                        $("#cepAviso").css('display', 'block');
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulario_cep();
//                alert("Formato de CEP inválido.");
                $("#cepAviso").css('display', 'block');
            }
    }else {
            //cep sem valor, limpa formulário.
            limpa_formulario_cep();
    }    
}

function limpa_formulario_cep() {
    // Limpa valores do formulário de cep.
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
//    $("#codigoMunicipio").val("");
}