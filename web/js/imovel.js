$(function() {
    listarImoveis();
    
    
});

function listarImoveis() {
    var table = $(".tablej").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarImoveis", "dataSrc": ""
//            , "data": function(d) {
//                
//            }
        },
        "columns": [
//            {"data": ""},
            {"data": "id"},
            {"data": "cliente.nomeFantasia"},
            {"data": "descricao"},
            {"data": "ativo"}
            
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    //<i class='fas fa-dot-circle'>
                    if(data == true){
                            return " <span style=\"padding-left:10px;color: green;\" data-toggle=\"tooltip\" title=\"Ativo\"> Ativo </span>";
                    }else{
                        return " <span style=\"padding-left:10px;color: gray;\" data-toggle=\"tooltip\" title=\"Inativo\"> Inativo </i></span>";
                    }
                },
                "targets": 3
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    setTimeout(function() {
        $("div.toolbar").css("float", "left");
        $("div.toolbar").css("padding-bottom", "5px");
//        var selectTorre = $("#divSelectTorre").html();
//        $("#divSelectTorre").remove();
//        $("div.toolbar").html('<button type="button" onclick="novo();" class="btn btn-success">Novo</button> ');
    }, 500);
    $('.tablej tbody').on('click', 'tr td:not(:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
//        var data = table.row( this ).data();
        editar(id);
    });
//    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
//        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
//            return;
//        }
//        var rowIdx = table
//                .cell(this)
//                .index().row;
//        var id = table
//                .rows(rowIdx)
//                .nodes().data()[rowIdx].id;
//        var check = $("input[name=selected][value=" + id + "]");
//        if (check.prop("checked")) {
//            check.prop("checked", false);
//        } else {
//            check.prop("checked", true);
//        }
//    });
}

//function tipoChanged() {
//    var tipo = $("#form [name=tipo]").val();
//    if (tipo == 1 || tipo == 4 || tipo == 5 || tipo == 6) {
//        $(".formUnidade").hide(); 
//    } else {
//        $(".formUnidade").show();
//    }
//    
//}

//function iniciarExclusao() {
//    if ($("input[name=selected]:checked").length > 0) {
//        $("#dialogExcluir").modal("show");
//    } else {
//        alertar("Você deve selecionar pelo menos 1 item para excluir", "alert");
//    }
//}

//function novo() {
//    changeMask();
//    $("#divAtivo").css('display', 'none');
////    $("#divVendedorCliente").css('display', 'none');
//    $("#form input[type=reset]").click();
//    $("#form input[name=id]").val("");
//    $("#form input[name=tipo]").val(1);
//    $("#cepAviso").css('display', 'none');
//    $("#cnpjAviso").css('display', 'none');
//    $("#btLimpar").show();
//    $("#dialogEditar").modal("show");
//}

function editar(id) {
    limparFormulario();
    loading("open");
    $("#divAtivo").css('display', 'block');
    $.post("BuscarImovel", {id: id}, function(data) {
        $("#form input[name=cliente]").val(data.cliente.nomeFantasia);
        $("#form input[name=descricao]").val(data.descricao);
        $("#form input[name=id]").val(data.id);
                
        if(data.endereco != null){
            $("#form input[name=cep]").val(data.endereco.cep);
            $("#form input[name=cep]").addClass("cep");

            $("#form input[name=uf]").val(data.endereco.uf);
            $("#form input[name=cidade]").val(data.endereco.cidade.nome);
            $("#form input[name=bairro]").val(data.endereco.bairro);
            $("#form input[name=rua]").val(data.endereco.rua);
            $("#form input[name=numero]").val(data.endereco.numero);
            $("#form input[name=complemento]").val(data.endereco.complemento);
        }
        
        if(data.ativo === true){
            $("#form [name=ativo]").prop("checked", true);
        } 
        
        $("#btLimpar").hide();
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading("close");
        $("#dialogEditar").modal("show");
    });

}

function salvar() {
    var erro = false;
    var myform = $('#form');
    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var serialized = myform.serialize();
    disabled.attr('disabled','disabled');
    loading("open");
    $.post("SalvarImovel", serialized, function(data) {
        if(data != ""){
            erro = true;
            alertar("O cliente " + data + " já possui este CNPJ/CPF!", "danger");
        }else{
            alertar("Registro salvo com sucesso", "success");
            $(".tablej").DataTable().ajax.reload();
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        erro = true;
            alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
    }).always(function() {
        $("#dialogEditar").modal("hide");
        loading("close");
        if(alterarCadastrao && !erro){
            alterarCadastro("Visita");
        }
    });    
}

//function excluir() {
//    $("#dialogExcluir").modal("hide");
//    loading("open");
//    var ids = [];
//    $("input[name=selected]:checked").each(function(index) {
//        ids.push($(this).val());
//    });
//    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
//        alertar("Registros excluídos com sucesso", "success");
//        $(".tablej").DataTable().ajax.reload();
//    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
//    }).always(function() {
//        loading("close");
//    });
//}

function buscarCEP(){
    $("#cepAviso").css('display', 'none');
    var cep = $("#cep").val().replace(/\D/g, '');
    if (cep !== "") {
        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/unicode/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#uf").val(dados.uf);
//                        $("#codigoMunicipio").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
//                        alert("CEP não encontrado.");
                        $("#cepAviso").css('display', 'block');
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
//                alert("Formato de CEP inválido.");
                $("#cepAviso").css('display', 'block');
            }
    }else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
    }    
}

function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
//    $("#codigoMunicipio").val("");
}

function limparFormulario(){
    $("#form input[type=reset]").click();
    $("#form input[name=id]").val("");
    $("#cepAviso").css('display', 'none');
}