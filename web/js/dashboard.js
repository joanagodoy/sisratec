$(function() {
    
    dashGeral($(".dashInformacoes[name=mes]"));
    if (tipoUsuarioSessao == 1 || tipoUsuarioSessao == 2) {
        $("#trVisitasPagas").show();
    } 
    
    var day1 = new Date();
    var dd1 = ("0" + day1.getDate()).slice(-2);
    var mm1 = ("0" + (day1.getMonth() + 1)).slice(-2);
    var yyyy1 = day1.getFullYear();
    day1 = dd1 + '/' + mm1 + '/' + yyyy1;
    $("#dataInicial").val(day1);
    
    var day2 = new Date(mm1 + '/' + dd1 + '/' + yyyy1);
    day2.setDate(day2.getDate() + 7);
    var dd2 = ("0" + day2.getDate()).slice(-2);
    var mm2 = ("0" + (day2.getMonth() + 1)).slice(-2);
    var yyyy2 = day2.getFullYear();
    day2 = dd2 + '/' + mm2 + '/' + yyyy2;
    $("#dataFinal").val(day2);
    
    buscarValorComissao();
});


function dashGeral(periodo) {
    $(".dashInformacoes").removeClass("botaoDashboardAtivo");
    $(periodo).addClass("botaoDashboardAtivo");
    var img = "<img src='img/progress.gif' width=15 height = 15 style='width:15px;height:15px;'/>";
    $("#informacoesPendentes").html(img);
    $("#informacoesCancelados").html(img);
    $("#informacoesRealizados").html(img);
    $("#informacoesPagos").html(img);
    $.post("BuscarInformacoesVisita", {periodo: $(periodo).attr("name")}).done(function(data) {
        $("#informacoesPendentes").html('<span class="cursor" onclick="carregarReservas(\''+data[0]+'\',\''+data[1]+'\',\'reservado\');">' + (data[2] != null ? data[2] : 0) + '</span>');
        $("#informacoesCancelados").html('<span class="cursor" onclick="carregarReservas(\''+data[0]+'\',\''+data[1]+'\',\'cancelado\');">' + (data[3] != null ? data[3] : 0) + '</span>');
        $("#informacoesRealizados").html('<span class="cursor" onclick="carregarReservas(\''+data[0]+'\',\''+data[1]+'\',\'realizado\');">' + (data[4] != null ? data[4] : 0) + '</span>');
        $("#informacoesPagos").html('<span class="cursor" onclick="carregarReservas(\''+data[0]+'\',\''+data[1]+'\',\'pago\');">' + (data[5] != null ? data[5] : 0) + '</span>');
    });
}

function carregarReservas(dataInicial,dataFinal,status){
    window.location = "VisitaLista?dataInicial="+dataInicial+"&dataFinal="+dataFinal+"&status="+status;
}

function comissaoExcel(){
    window.location = "BuscarComissao?dataInicial="+ $("#dataInicial").val()+"&dataFinal="+ $("#dataFinal").val()+"&vendedor="+ $("#vendedorComissao").val()+ "&excel=1";
}

function buscarValorComissao(){
    $.post("BuscarComissao", {dataInicial: $("#dataInicial").val(), dataFinal: $("#dataFinal").val(), vendedor: $("#vendedorComissao").val()}).done(function(data) {
        $("#divTotalComissao").html(currencyFormatter(data));
    });
}

//function carregarPendencias() {
//    $.post("HME.BuscarFuncionariosPendencias", {}).done(function(data) {
//        var resultado = "";
//        for (var i = 0; i < data.length; i++) {
//            resultado += "<tr>";
//            resultado += "<td>" + data[i][0] + "</td>";
//            resultado += "<td>" + data[i][1] + "</td>";
//            resultado += "<td>" + data[i][2] + "</td>";
//            resultado += "</tr>";
//        }
//        $("#tablePendencias tbody").html(resultado);
//    });
//}

