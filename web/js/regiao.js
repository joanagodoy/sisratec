$(function() {
    expandirMenu();
    listarRegioes();
    
    $('#dialogAssociacao').on('hidden.bs.modal', function () {
        $(".cidadesDisponiveis").dataTable().fnDestroy();
        $(".cidadesAssociadas").dataTable().fnDestroy();
    });
});



var cidadesAssociadas = [];

function reloadUsuarios() {
    loading("open");
    $(".tablej").DataTable().ajax.reload();
}

function listarRegioes() {
    var table = $(".tablej").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarRegiao", "dataSrc": "", "data": function(d) { }},
        "columns": [
            {"data": "id", className: "never"},
            {"data": "descricao"}
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    var p = " <span style=\"padding-left:10px;\" data-toggle=\"tooltip\" title=\"Cidades da Região\" onclick=\"openCidades(" + row.id + ",'" + row.descricao + "')\"><i title='Abrir Cidades da Região' class='fas fa-map-marker-alt pointer'></i></span>";
                    return p;
                },
                "targets": [2]
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    setTimeout(function() {
        $("div.toolbar").css("float", "left");
        $("div.toolbar").css("padding-bottom", "5px");
//        var selectTorre = $("#divSelectTorre").html();
        $("#divSelectTorre").remove();
        $("div.toolbar").html('<button type="button" onclick="novo();" class="btn btn-success">Novo</button>');
    }, 500);
    $('.tablej tbody').on('click', 'tr td:not(:last-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
        editar(id);
    });
//    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
//        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
//            return;
//        }
//        var rowIdx = table
//                .cell(this)
//                .index().row;
//        var id = table
//                .rows(rowIdx)
//                .nodes().data()[rowIdx].id;
//        var check = $("input[name=selected][value=" + id + "]");
//        if (check.prop("checked")) {
//            check.prop("checked", false);
//        } else {
//            check.prop("checked", true);
//        }
//    });
}
var tableCidadesAssociadas;
function listarCidadesAssociadas(){
//    alert($("#regiao").val());
//alert("listarCidadesAssociadas");
    tableCidadesAssociadas = $(".cidadesAssociadas").DataTable({
        language: {url: "js/libs/dataTable.json"}, 
        "processing": true, 
        "dom": '<"toolbar">frtip', 
        "responsive": true,
        "order": [[1, "asc"]],
//        "ajax": {url: "BuscarCidadesAssociadas", "dataSrc": "", "data": function(d) {
//            d.regiao = $("#regiao").val();
//        }},
        "data": montaArrayGeral(),
        "columns": [
            {"data": "id", className: "never"},
            {"data": "nome"}
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    
                    var p = " <span style=\"padding-left:10px;\" data-toggle=\"tooltip\"  title=\"Remover Cidade\" onclick=\"removerAssociacao(" + row.id + ")\"><i  class=\"fas fa-times pointer\"></i></span>";
                    return p;
                },
                "targets": [2]
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
//    $('.cidadesAssociadas tbody').on('click', 'tr td:last-child', function(event) {
//        alert($(event.target).closest('span'));
//        if ($(event.target).closest('span').length > 0) {
//            return;
//        }
//        $(".cidadesAssociadas").DataTable().row( $(this).parents('tr') ).remove().draw();
//    });
    $('.cidadesAssociadas').on('click', '.fa-times', function () {
       $(".cidadesAssociadas").DataTable().row( $(this).parents('tr') ).remove().draw();
    });
   
    
}

function listarCidadesDisponiveis(){
    var table = $(".cidadesDisponiveis").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarCidadesDisponiveis", "dataSrc": "", "data": function(d) {
            d.uf = $("#uf").val();
        }},
        "columns": [
            {"data": "id", className: "never"},
            {"data": "nome"}
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    var idCidade = jaEstaAdd(row.id);
                    if (row.id === idCidade){//cidade esta add, remover
                        return " <span style=\"padding-left:10px;\" data-toggle=\"tooltip\" title=\"Remover Cidade\"  onclick=\"removerAssociacao(" + row.id + "')\"><i class=\"fas fa-times pointer\"></i></span>";
                    }else{
                        return " <span style=\"padding-left:10px;\" data-toggle=\"tooltip\" title=\"Add Cidade\"  onclick=\"addAssociacao(" + row.id + ",'" + row.nome + "')\"><i class=\"fas fa-check pointer\"></i></span>";
                    }
                },
                "targets": [2]
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    
//    $('.cidadesDisponiveis').dataTable( {
//        paging: false,
//    } );
}
function jaEstaAdd(idCidade) {
    for (var i = 0; i < cidadesAssociadas.length; i++) {
        if (cidadesAssociadas[i].id === idCidade) {
            return cidadesAssociadas[i].id;
        }
    }
    return -1;
}

function reloadCidadesDisponiveis() {
//    loading("open");
    $(".cidadesDisponiveis").DataTable().ajax.reload();
}

function removerAssociacao(id){
    
    for (var i = 0; i < cidadesAssociadas.length; i++) {
        if (cidadesAssociadas[i].id === id) {
            cidadesAssociadas.splice(i, 1);
            break;
        }
    }
//    $(".cidadesAssociadas").DataTable().row( $(this).parents('tr') ).remove().draw();
//    $(".cidadesAssociadas").DataTable().row("#"+id).remove().draw();
}

function addAssociacao(id, nome){
    cidadesAssociadas.push({id: id, nome: nome});
//    tableCidadesAssociadas.clear();
    $(".cidadesAssociadas").DataTable().rows.add( [{"id": id,"nome": nome}] ).draw();
    reloadCidadesDisponiveis();
}

function montaArrayGeral() {
    var all = [];
    for (var i = 0; i < cidadesAssociadas.length; i++) {
        all.push(cidadesAssociadas[i]);
    }
    return all;
}

function carregarAssociacoes(id) {
    cidadesAssociadas = [];
    $.post("BuscarCidadesAssociadas", {regiao: id}).done(function (data) {
//        alert(data[0].id);
        for (var a = 0; a < data.length; a++) {
            cidadesAssociadas.push({id: data[a].id, nome: data[a].nome});
        }
        listarCidadesAssociadas();
    });
}
function openCidades(id, descricao){
    carregarAssociacoes(id);
    $("#descricao").html(descricao);
    $("#regiao").val(id);
    listarCidadesDisponiveis();
//    listarCidadesAssociadas();    
    $("#dialogAssociacao").modal("show");
}

function iniciarExclusao() {
    if ($("input[name=selected]:checked").length > 0) {
        $("#dialogExcluir").modal("show");
    } else {
        alertar("Você deve selecionar pelo menos 1 item para excluir", "alert");
    }
}

function novo() {
    $("#form input[type=reset]").click();
    $("#form input[name=id]").val("");
    $("#divAtivo").css('display', 'none');
    $("#dialogEditar").modal("show");
}

function editar(id) {
    $("#form input[type=reset]").click();
    $("#divAtivo").css('display', 'block');
    loading("open");
    $.post("BuscarRegiao", {id: id}, function(data) {
        $("#form input[name=descricao]").val(data.descricao);
        
        if(data.ativo === true){
            $("#form [name=ativo]").prop("checked", true);
        }        
        $("#form input[name=id]").val(data.id);
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading("close");
        $("#dialogEditar").modal("show");
    });

}

function salvarAssociacao(){
    var array = montaArrayGeral();
    var form = $("#formCadastrarAssociacao").serializeArray();
        form.push({name: 'associacao', value: JSON.stringify(array)});  
    $.post("SalvarAssociacaoRegiaoCidade", form, function() {
        alertar("Associação salva com sucesso", "success");
//        $(".tablej").DataTable().ajax.reload();
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
    }).always(function() {
        $("#dialogAssociacao").modal("hide");
        loading("close");
    });
}

function salvar() {    
//    var myform = $('#form');
//    var disabled = myform.find(':input:disabled').removeAttr('disabled');
//    var serialized = myform.serialize();
//    disabled.attr('disabled','disabled');
    
    loading("open");
    fixForm();
    $.post("SalvarRegiao", $('#form').serialize(), function(data) {
        alertar("Registro salvo com sucesso", "success");
        $(".tablej").DataTable().ajax.reload();
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
    }).always(function() {
        $("#dialogEditar").modal("hide");
        loading("close");
    });
}

function excluir() {
    $("#dialogExcluir").modal("hide");
    loading("open");
    var ids = [];
    $("input[name=selected]:checked").each(function(index) {
        ids.push($(this).val());
    });
    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
        alertar("Registros excluídos com sucesso", "success");
        $(".tablej").DataTable().ajax.reload();
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
    }).always(function() {
        loading("close");
    });
}
