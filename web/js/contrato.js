var alterarCadastrao = false;
$(function() {
//    alert(open);
    if(open){
        setTimeout('$("#btNovoContrato").click();', 600);
    }
//    listarContratos();

});

function changeTipoContrato(){
    if($("#tipoContrato").val() == "Estabelecimento"){
//        $("#divNrContrato").show();
        $("#divAdicionais").show();
        $("#divImovel").hide();
        $("#qtdImovel").prop('required',false);
    }else{
//        $("#divNrContrato").hide();
        $("#divAdicionais").hide();
        $("#divImovel").show();
        $("#qtdImovel").prop('required',true);
    }
}

function listarContratos() {
    var table = $(".tablej").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarContratos", "dataSrc": ""
//            , "data": function(d) {
//                
//            }
        },
        "columns": [
//            {"data": ""},
            {"data": "nrContrato"},
            {"data": "cliente.nomeFantasia"},
            {"data": "tipoCliente"},
            {"data": "pagamento"},
            {"data": "dtEntrega"}
            
        ],
        "columnDefs": [            
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "Pendente";
                    }else{
                        var dataFormatada = dateFormatter(data).split('/');
                        return '<span style="display:none" >'+(dataFormatada[2] + dataFormatada[1] + dataFormatada[0]) * 1+'</span>'+dateFormatter(data);
                    }
                },
                "targets": 4
            },
            {
                "render": function(data, type, row) {
                    if(data == 0){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"À Vista\"> À Vista </span>";
                    }else if(data == 1){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Anual\"> Anual </span>";
                    }else if(data == 12){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Mensal\"> Mensal </i></span>";
                    }else if(data == 4){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Quadrimestral\"> Quadrimestral </i></span>";
                    }else if(data == 3){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Trimestral\"> Trimestral </i></span>";
                    }else if(data == 6){
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Semestral\"> Semestral </i></span>";
                    }else{
                        return " <span style=\"padding-left:10px;color: black;\" data-toggle=\"tooltip\" title=\"Verificar\"> Verificar </i></span>";
                    }
                },
                "targets": 3
            },
            {
                "render": function(data, type, row) {
                    var p = " <span style=\"padding-left:10px;\" data-toggle=\"tooltip\" title=\"Fotos Contrato\" class=\"pointer\" onclick=\"carregarFotos(" + row.nrContrato + ")\"><i class='fas fa-camera'></i></span>";
//                    return "<span data-toggle=\"tooltip\" title=\"Moradores com permissão de acesso\" class=\"fa fa-users pointer\" onclick=\"carregarUsuarios(" + row.id + ")\"></span> " + p;
                    return p;
                },
                "targets": [5]
            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading(false);
        }
    });
    setTimeout(function() {
        $("div.toolbar").css("float", "left");
        $("div.toolbar").css("padding-bottom", "5px");
        $("div.toolbar").html('<button type="button" id="btNovoContrato" onclick="novo();" class="btn btn-success">Novo</button> ');
        
    }, 500);
    $('.tablej tbody').on('click', 'tr td:not(:last-child,:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].nrContrato;
//        var data = table.row( this ).data();
        editarContrato(id);
    });
//    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
//        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
//            return;
//        }
//        var rowIdx = table
//                .cell(this)
//                .index().row;
//        var id = table
//                .rows(rowIdx)
//                .nodes().data()[rowIdx].id;
//        var check = $("input[name=selected][value=" + id + "]");
//        if (check.prop("checked")) {
//            check.prop("checked", false);
//        } else {
//            check.prop("checked", true);
//        }
//    });
}

//function tipoChanged() {
//    var tipo = $("#form [name=tipo]").val();
//    if (tipo == 1 || tipo == 4 || tipo == 5 || tipo == 6) {
//        $(".formUnidade").hide(); 
//    } else {
//        $(".formUnidade").show();
//    }
//    
//}

//function iniciarExclusao() {
//    if ($("input[name=selected]:checked").length > 0) {
//        $("#dialogExcluir").modal("show");
//    } else {
//        alertar("Você deve selecionar pelo menos 1 item para excluir", "alert");
//    }
//}

function carregarFotos() {
    $("#idContrato").val($("#id").val());
    $("#dialogFotos").modal("show");
    buscarFotos();
}

function buscarFotos() {
    $("#divFotos").html("<center>Carregando...</center>");
    $.post("BuscarFotoContrato", {contrato: $("#idContrato").val()}).done(function(data) {
        if (data == null || data.length == 0) {
            $("#divFotos").html("<center>Nenhuma foto encontrada</center>");
        } else {
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(tipoUsuarioSessao == 1){
                    html += "<div class='thumbDiv'> <span onclick='excluirFoto(" + data[i] + ");' ><i class='fas fa-times'></i></span> <img src='BuscarFotoContrato?id=" + data[i] + "'/> </div>";
                }else{
                    html += "<div class='thumbDiv'> <img src='BuscarFotoContrato?id=" + data[i] + "'/> </div>";
                }
            }
            $("#divFotos").html(html);
        }
    });
}

function excluirFoto(foto) {
    loading(true);
    $.post("ExcluirFotoContrato", {foto: foto}).done(function(data) {
        loading("false");
        buscarFotos();
    });
}

function preencherClientes(){
    $.post("BuscarClientes", {contrato: "ok"}, function(data) {
        $('select[name=cliente]').html("");
        for (var a = 0; a < data.length; a++) {
                $("#cliente").append("<option value='" + data[a].id + "'>" + data[a].razSocial + "</option>");
        }
//        $("#cliente").selectpicker("refresh");
    $(".select2").select2();
    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading(false);
        $("#dialogEditar").modal("show");
    });
}

function novoContrato(cliente) {
//    $("#dataEntrega").prop('required', false);
    $("#formContrato input[type=reset]").click();
    $("#formContrato input[name=id]").val("");
    $("#formContrato input[name=cliente]").val(cliente);
    $("#divDtEntrega").hide();
    $("#divValorMensal").hide();
    $("#btLimpar").show();
    $("#divCliente").hide();
    $("#divSelectCliente").show();
    $("#formContrato input[name=nr]").prop( "disabled", false );
    $("#btAddImovel").hide();
    $("#visitaaberto").prop('checked', true);
    
//    tipoChanged();
//    preencherClientes();
//    $("#dialogEditar").modal("show");
    
}

function editarContrato() {
    limparFormulario();
//    $("#btLimpar").hide();
//    $("#divDtEntrega").show();
    $("#divValorMensal").show();
//    $("#dataEntrega").prop('required', false);
    loading(true);
    $.post("BuscarContrato", {idCliente: idCliente}, function(data) {
//        if(data.visitaAberto == true){
            $("#formContrato [name=visitaaberto]").prop("checked", data.visitaAberto);
//        }
        
        $("#formContrato input[name=nr]").val(data.nrContrato);
        $("#formContrato input[name=id]").val(data.nrContrato);
        $("#obs").val(data.obs);
//        if(data.dtEntrega != null){
        $("#formContrato input[name=dataEntrega]").val(dateFormatter(data.dtEntrega));
//        }else{
//            if(tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1){
//                $("#dataEntrega").prop('required', true);
//            }
//        }
        $('select[name=pagamento]').val(data.pagamento);
        $('select[name=edicao]').val(data.edicao);
        $("#formContrato input[name=valorMensal]").val(currencyFormatter(data.valorMensal));
        $("#formContrato input[name=desconto]").val(data.desconto);
        $("#formContrato input[name=qtdImovel]").val(data.qtdImovel);
        $("#formContrato input[name=qtdImovel]").prop( "disabled", true );
        $("#formContrato select[name=tipoContrato]").val(data.tipoCliente);
        $("#formContrato select[name=tipoContrato]").prop( "disabled", true );

        //        $('.selectpicker').selectpicker('refresh');
        $(".select2").select2();
        
        changeTipoContrato();
        
        if(data.tipoCliente == 'Imobiliaria' && (tipoUsuarioSessao == 1 || tipoUsuarioSessao == 2)){
            $("#btAddImovel").show();
        }else{
            $("#btAddImovel").hide();
        }
        $("#formContrato input[name=cliente]").val(data.cliente.nomeFantasia);
        $("#divCliente").show();
        $("#divSelectCliente").hide();
        $("#formContrato input[name=cliente]").prop( "disabled", true );
        
        $("#formContrato input[name=nr]").prop( "disabled", true );
//        $('select[name=cliente]').val(data.cliente.id);
        
        if(data.consultoria == 'true'){
            $("#formContrato [name=consultoria]").prop("checked", true);
        } 
        if(data.fichaGoogle == true){
            $("#formContrato [name=ficha]").prop("checked", true);
        } 
        if(data.analiseAnual == true){
            $("#formContrato [name=analiseanual]").prop("checked", true);
        } 
        if(data.analiseMensal == true){
            $("#formContrato [name=analisemensal]").prop("checked", true);
        } 
        if(data.site == true){
            $("#formContrato [name=site]").prop("checked", true);
        } 
        
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading(false);
        $("#dialogEditar").modal("show");
    });

}

function salvarContrato() {
    if(($("#tipoContrato").val() == "Imobiliaria" && $("#qtdImovel").val() >= 5) || ($("#tipoContrato").val() == "Estabelecimento")){
        var erro = false;
        $(".preloader").show();
//        $(".preloader").show();
    //    fixForm();
        var myform = $('#formContrato');
        var disabled = myform.find(':input:disabled').removeAttr('disabled');
        var serialized = myform.serialize();
        disabled.attr('disabled','disabled');

        $.post("SalvarContrato", serialized, function(data) {
            alertar("Registro salvo com sucesso", "success");
//            $(".tablej").DataTable().ajax.reload();
        }).fail(function(xhr, textStatus, errorThrown) {
            erro = true;
            if (xhr.status == "409") {
                alertar("Você não tem mais permissão para alterar este contrato, entre em contato com a Administração!", "alert");
            } else if (xhr.status == "302") {
                alertar("Já existe contrato com este número cadastrado!", "danger");
            } else{
                alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
            }
        }).always(function() {
//            $("#dialogEditar").modal("hide");
            $(".preloader").fadeOut();
            if(alterarCadastrao && !erro){
//                alterarCadastro("Visita");
                window.location = "Visita?u=open";
            }
        });
    }else{
        alertar("É necessário negociar pelo menos 5 imóveis!", "alert");
    }
}

//function excluir() {
//    $("#dialogExcluir").modal("hide");
//    loading(true);
//    var ids = [];
//    $("input[name=selected]:checked").each(function(index) {
//        ids.push($(this).val());
//    });
//    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
//        alertar("Registros excluídos com sucesso", "success");
//        $(".tablej").DataTable().ajax.reload();
//    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
//    }).always(function() {
//        loading(false);
//    });
//}

function limparFormulario(){
    $("#formContrato input[type=reset]").click();
    $("#formContrato input[name=id]").val("");
//    preencherClientes();
}

function openAddImovel(){
    $("#clienteAddImovel").val($("#formContrato input[name=cliente]").val());
    $("#qtdImovelAddImovel").val($("#formContrato input[name=qtdImovel]").val());
    $("#contratoAddImovel").val($("#formContrato input[name=nr]").val());
    $("#dialogAddImovel").modal("show");
}

function addImovel(){
    if($("#qtdImovelAdd").val() < 1){
        alertar("Qtd. Inválida!", "alert");
    }else{
        $.post("AddImovelContrato", $("#formAddImovel").serialize(), function(data) {
            alertar("Imóveis adicionados com sucesso!", "success");
//            $(".tablej").DataTable().ajax.reload();
        }).fail(function() {
            alertar("Ocorreu algum erro ao adicionar imóveis", "danger");
        }).always(function() {
            loading(false);
            $("#dialogAddImovel").modal("hide");
            $("#dialogEditar").modal("hide");
             
        });
    }
}