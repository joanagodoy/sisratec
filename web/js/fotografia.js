var today;
$(function() {
    expandirMenu();
    onChangeStatus();
    
    if(tipoUsuarioSessao == 5){
        $("#status").val(7);
    }
    if(tipoUsuarioSessao == 2){
        $("#status").val(3);
    }
    if(tipoUsuarioSessao == 3){
        $("#status").val(4);
    }
    
    today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
    
    
    $(".select2").select2();
    
});

//function reloadUsuarios() {
//    loading("open");
//    $(".tablej").DataTable().ajax.reload();
//}

function onChangeStatus(){
    $("#btAlterarDtEnvio").hide();
    $("#btEnviarEmail").hide();
    $("#btEnviarEmailImovel").hide();
    if($("#status").val() == 5 && (tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1)){
        $("#btEnviarEmail").show();
    }
    if($("#status").val() == 9 && (tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1)){
        $("#btEnviarEmailImovel").show();
    }
    if($("#status").val() == 1 && (tipoUsuarioSessao == 3 || tipoUsuarioSessao == 1)){
        $("#btAlterarDtEnvio").show();
    }
    listarFotografias();
//    $(".tablej").DataTable().ajax.reload();
}

function listarFotografias() {
    $(".tablej").dataTable().fnDestroy();
    $(".tablej").empty();
    $(".tablej").html("<thead><tr><th></th><th>#</th><th>Cliente</th><th>Status</th><th></th></tr></thead>");
    var table = $(".tablej").DataTable({language: {url: "js/libs/dataTable.json"}, "processing": true, "dom": '<"toolbar">frtip', "responsive": true,
        "order": [[1, "asc"]],
        "ajax": {url: "BuscarFotografias", "dataSrc": ""
            , "data": function(d) {
                d.status = $("#status").val();
            }
        },
        "columns": [
            {"data": ""},
            {"data": "id", className: "never"},
            {"data": "idCliente.nomeFantasia"},
            {"data": "status.descricao"},
            {"data": "imovel.descricao", className: "never"}
        ],
        "columnDefs": [
            {
                "render": function(data, type, row) {
                    return "<input type='checkbox' name='selected' value='" + row.id + "'>";
                },
                "targets": 0
            },
            {
                "render": function(data, type, row) {
                    if(data == null){
                        return "";
                    }else{
                        return data;
                    }
                },
                "targets": 4
            },
            {
                "render": function(data, type, row) {
                    if(row.imovel == null){
                        return data;
                    }else{
                        return row.imovel.descricao + " - " + data;
                    }
                },
                "targets": 2
            },
            {"width": "5%", "targets": 0}
//            ,
//            {
//                "defaultContent": "",
//                "targets": [4, 3]
//            }
//            {
//                "render": function(data, type, row) {
//                    var p = " <span style=\"padding-left:10px;\" data-toggle=\"tooltip\" title=\"Fotos Contrato\" class=\"fa fa-camera pointer\" onclick=\"carregarFotos(" + row.nrContrato + ")\"></span>";
////                    return "<span data-toggle=\"tooltip\" title=\"Moradores com permissão de acesso\" class=\"fa fa-users pointer\" onclick=\"carregarUsuarios(" + row.id + ")\"></span> " + p;
//                    return p;
//                },
//                "targets": [2]
//            }
        ],
        "fnDrawCallback": function(oSettings) {
            loading("close");
        }
    });
    setTimeout(function() {
        $("div.toolbar").css("float", "left");
        $("div.toolbar").css("padding-bottom", "5px");
//        var selectTorre = $("#divSelectTorre").html();
//        $("#divSelectTorre").remove();
        
//        if(tipoUsuarioSessao == 3){//fotografo
//            $("div.toolbar").html('');
//        }
//        if(tipoUsuarioSessao == 5){//editor
//            $("div.toolbar").html('<button type="button" onclick="alterarDataPostagem();" class="btn btn-success">Alterar Data Postagem</button> ');
//        }
//        if(tipoUsuarioSessao == 2){//secretaria
//            $("div.toolbar").html('');
//        }

    }, 500);
    $('.tablej tbody').on('click', 'tr td:not(:first-child)', function(event) {
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
//        var data = table.row( this ).data();
        editar(id);
    });
    $('.tablej tbody').on('click', 'tr td:first-child', function(event) {
        if ($(event.target).closest('input[type="checkbox"]').length > 0) {
            return;
        }
        var rowIdx = table
                .cell(this)
                .index().row;
        var id = table
                .rows(rowIdx)
                .nodes().data()[rowIdx].id;
        var check = $("input[name=selected][value=" + id + "]");
        if (check.prop("checked")) {
            check.prop("checked", false);
        } else {
            check.prop("checked", true);
        }
    });
}

//function tipoChanged() {
//    var tipo = $("#form [name=tipo]").val();
//    if (tipo == 1 || tipo == 4 || tipo == 5 || tipo == 6) {
//        $(".formUnidade").hide(); 
//    } else {
//        $(".formUnidade").show();
//    }
//    
//}

function changeFaltouPts(){
    if($('#faltouPts').is(":checked")){
        $("#novaobs").prop('required',true);
        $("#form input[name=dataConfirmacao]").val("");
        $("#form input[name=dataConfirmacao]").attr("disabled", true);
    }else{
        $("#novaobs").prop('required',false);
        $("#form input[name=dataConfirmacao]").val(today);
        $("#form input[name=dataConfirmacao]").attr("disabled", false);
    }
}

function changeProblema(){
    if($('#problema').is(":checked")){
        $("#novaobs").prop('required',true);
        $("#form input[name=dataPostagem]").val("");
        $("#form input[name=dataPostagem]").attr("disabled", true);
    }else{
        $("#novaobs").prop('required',false);
        $("#form input[name=dataPostagem]").val(today);
        $("#form input[name=dataPostagem]").attr("disabled", false);
    }
}

function changeEditorProblema(){
    if($('#editorProblema').is(":checked")){
        $("#novaobs").prop('required',true);
        $("#form input[name=link]").val("");
        $("#form input[name=link]").attr("disabled", true);
    }else{
        $("#novaobs").prop('required',false);
        $("#form input[name=link]").attr("disabled", false);
    }
}

function confirmarEntrega(){
    $.post("ConfirmarEntrega", {id: $("#id")}, function (data) {
            alertar("E-mail enviado com sucesso!", "success");
            $(".tablej").DataTable().ajax.reload();
        }).fail(function () {
            alertar("Ocorreu algum erro ao tentar enviar e-mail", "danger");
        }).always(function () {
            loading("close");
        });
}

function enviarEmail() {
    if($("#status").val() != 5){
        alertar("Só é possível enviar emails para Projetos Finalizados", "alert");
        return false;
    }
    if ($("input[name=selected]:checked").length > 0) {
        loading("open");
        var ids = [];
        $("input[name=selected]:checked").each(function (index) {
            ids.push($(this).val());
        });
        $.post("EnviarEmailLink", {id: JSON.stringify(ids), isCobranca: $("input[name=checkbox-cobranca]:checked").length}, function (data) {
            if(ids.length > 1){
                alertar("E-mails enviados com sucesso!", "success");
            }else{
                alertar("E-mail enviado com sucesso!", "success");
            }
            $(".tablej").DataTable().ajax.reload();
        }).fail(function () {
            alertar("Ocorreu algum erro ao tentar enviar e-mail", "danger");
        }).always(function () {
            loading("close");
        });
    }else {
        alertar("Você deve selecionar pelo menos 1 item para enviar e-mail", "alert");
    }
}

function enviarEmailImovel() {    
    if ($("input[name=selected]:checked").length > 0) {
        loading("open");
        var ids = [];
        $("input[name=selected]:checked").each(function (index) {
            ids.push($(this).val());
        });
        $.post("EnviarEmailLink", {id: JSON.stringify(ids)}, function (data) {
            if(ids.length > 1){
                alertar("E-mails enviados com sucesso!", "success");
            }else{
                alertar("E-mail enviado com sucesso!", "success");
            }
            $(".tablej").DataTable().ajax.reload();
        }).fail(function(xhr, textStatus, errorThrown) {
            if (xhr.status == "302") {
                alertar("Alguns e-mails não foram enviados!", "alert");
            }else{
                alertar("Ocorreu algum erro ao tentar enviar e-mail", "danger");
            }
            
        }).always(function () {
            loading("close");
        });
    }else {
        alertar("Você deve selecionar pelo menos 1 item para enviar e-mail", "alert");
    }
}

function reenviarEmail() {
    loading("open");
    $.post("ReenviarEmailLink", {id: $("#id").val()}, function (data) {
        alertar("E-mail enviado com sucesso!", "success");
        $(".tablej").DataTable().ajax.reload();
    }).fail(function () {
        alertar("Ocorreu algum erro ao tentar enviar e-mail", "danger");
    }).always(function () {
        $("#dialogEditar").modal("hide");
        loading("close");
    });
}

function alterarDataEnvio() {
    if ($("input[name=selected]:checked").length > 0) {
        loading("open");
        var ids = [];
        $("input[name=selected]:checked").each(function (index) {
            ids.push($(this).val());
        });
        $.post("AlterarDataEnvio", {id: JSON.stringify(ids)}, function (data) {
            alertar("Registros alterados com sucesso", "success");
            $(".tablej").DataTable().ajax.reload();
        }).fail(function () {
            alertar("Ocorreu algum erro ao tentar alterar os registros", "danger");
        }).always(function () {
            loading("close");
        });
    } else {
        alertar("Você deve selecionar pelo menos 1 item para alterar", "alert");
    }
}

function alterarDataPostagem() {
    if ($("input[name=selected]:checked").length > 0) {
        loading("open");
        var ids = [];
        $("input[name=selected]:checked").each(function (index) {
            ids.push($(this).val());
        });
        $.post("AlterarDataPostagem", {id: JSON.stringify(ids)}, function (data) {
            alertar("Registros alterados com sucesso", "success");
            $(".tablej").DataTable().ajax.reload();
        }).fail(function () {
            alertar("Ocorreu algum erro ao tentar alterar os registros", "danger");
        }).always(function () {
            loading("close");
        });
    } else {
        alertar("Você deve selecionar pelo menos 1 item para alterar", "alert");
    }
}

//function carregarFotos(id) {
//    $("#idContrato").val(id);
//    $("#dialogFotos").modal("show");
//    buscarFotos();
//}

//function buscarFotos() {
//    $("#divFotos").html("<center>Carregando...</center>");
//    $.post("BuscarFotoContrato", {contrato: $("#idContrato").val()}).done(function(data) {
//        if (data == null || data.length == 0) {
//            $("#divFotos").html("<center>Nenhuma foto encontrada</center>");
//        } else {
//            var html = "";
//            for (var i = 0; i < data.length; i++) {
//                html += "<div class='thumbDiv'> <span onclick='excluirFoto(" + data[i] + ");' class='fa fa-close'></span><img src='BuscarFotoContrato?id=" + data[i] + "'/></div>";
//            }
//            $("#divFotos").html(html);
//        }
//    });
//}

//function preencherClientes(){
//    $.post("BuscarClientes", {contrato: "ok"}, function(data) {
//        $('select[name=cliente]').html("");
//        for (var a = 0; a < data.length; a++) {
//                $("#cliente").append("<option value='" + data[a].id + "'>" + data[a].razSocial + "</option>");
//        }
//        $("#cliente").selectpicker("refresh");
//    }).fail(function() {
////        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
//    }).always(function() {
//        loading("close");
//        $("#dialogEditar").modal("show");
//    });
//}

//function novo(){
//    $("#form input[type=reset]").click();
//    $("#form input[name=id]").val("");
////    tipoChanged();
//    preencherClientes();
////    $("#dialogEditar").modal("show");    
//}

function editar(id) {
    limparFormulario();
//  alert(id);
    loading("open");
    $.post("BuscarFotografia", {id: id}, function(data) {    
        $("#form input[name=id]").val(data.id);
        if(data.imovel != null){
            $("#form input[name=cliente]").val(data.imovel.descricao + " - " + data.idCliente.nomeFantasia);
        }else{
            $("#form input[name=cliente]").val(data.idCliente.nomeFantasia);
        }
        $("#form input[name=link]").val(data.linkFotos);
        $("#form input[name=dataPostagem]").val(dateFormatter(data.dtPostagem));
        $("#form input[name=dataEnvio]").val(dateFormatter(data.dtEnvioEditor));
        $("#form input[name=dataConfirmacao]").val(dateFormatter(data.dtConfirmacaoEntrega));
        $("#form [name=statusFotografia]").val(data.status.descricao);
        $("#form [name=obs]").val(data.obs);
        
        if(data.faltouPts === true){
            $("#form [name=faltouPts]").prop("checked", true);
        }  
        if(data.problemaFotografo === true){
            $("#form [name=problema]").prop("checked", true);
        }  
        if(data.problemaEditor === true){
            $("#form [name=editorProblema]").prop("checked", true);
        } 
        
        if(data.status.id == 6 && (tipoUsuarioSessao == 2 || tipoUsuarioSessao == 1)){
            $("#btEnviarEmailNovamente").show();
        }else{
            $("#btEnviarEmailNovamente").hide();
        }
        
        $("#aviso").hide();
        $("#btConfirmar").hide();
//        if(data.status.id == 3){
            $("#form input[name=dataEnvio]").attr('disabled','disabled');
            $("#form input[name=dataPostagem]").attr('disabled','disabled');
            $("#form input[name=dataConfirmacao]").attr('disabled','disabled');
            $("#form [name=faltouPts]").attr('disabled','disabled');
            $("#form [name=problema]").attr('disabled','disabled');
//        }
        $("#divPostagem").hide();
        //se editor   
        if(tipoUsuarioSessao == 5 || (tipoUsuarioSessao == 1 && (data.status.id == 2 || data.status.id == 8 || data.status.id == 7))){
            $("#form input[name=dataEnvio]").attr('disabled','disabled');
            if(data.dtEnvioEditor != null){
                $("#btSalvar").show();
//                $("#aviso").hide();
            }else{
                $("#btSalvar").hide();
                $("#aviso").show();
            }
            
            if(data.status.id == 3){
                $("#btSalvar").hide();
            }
            if(data.status.id == 4){
                $("#form [name=faltouPts]").attr('disabled','disabled');
                $("#form [name=problema]").attr('disabled','disabled');
                $("#form [name=dataConfirmacao]").attr('disabled','disabled');
                $("#form [name=dataPostagem]").attr('disabled','disabled');
                $("#aviso").show();
                $("#btSalvar").hide();
            }
            
            $("#divPostagem").show();
            if(data.status.id == 7){
                $("#divPostagem").hide();
                $("#form input[name=dataConfirmacao]").val(today);
                $("#form [name=dataConfirmacao]").attr('disabled', false);
                $("#form [name=faltouPts]").attr('disabled', false);
            }
//            $("#divConfirmacao").show();
//            if(data.status.id == 7){
//                $("#divConfirmacao").hide();
//            }
            
//            changeFaltouPts();
//            changeProblema();
            
            if(data.status.id == 2 || data.status.id == 8){
//                $("#form [name=faltouPts]").prop("checked", false);
                $("#form [name=problema]").prop("checked", false);
                $("#form [name=faltouPts]").attr('disabled','disabled');
                $("#form [name=dataConfirmacao]").attr('disabled','disabled');
                $("#form [name=problema]").attr('disabled', false);
                $("#form [name=dataPostagem]").attr('disabled', false);
                $("#form input[name=dataPostagem]").val(today);
            }
        }else if(tipoUsuarioSessao == 3 || (tipoUsuarioSessao == 1 && (data.status.id == 1 || data.status.id == 4))){
            if(data.status.id == 4 || data.status.id == 1){
                $("#form input[name=dataEnvio]").val(today);
                $("#form input[name=dataEnvio]").attr('disabled', false);
                $("#btSalvar").show();
            }else{
                $("#btSalvar").hide();
            }
            
        }else if(tipoUsuarioSessao == 2 || (tipoUsuarioSessao == 1 && (data.status.id == 3 || data.status.id == 5 || data.status.id == 6))){
            $("#link").prop('required',true);
            if(data.status.id == 3 ){
                $("#form [name=editorProblema]").prop("checked", false);
                
//            }else{
//                $("#btSalvar").hide();
            }
            $("#btSalvar").show();
        }
        
    }).fail(function() {
        alertar("Ocorreu algum erro ao tentar buscar o registro", "danger");
    }).always(function() {
        loading("close");
        $("#dialogEditar").modal("show");
    });
}

function salvar() {
    loading("open");
    if($("#form input[name=dataEnvio]").val()){
        
    }
//    if(($("#form input[name=dataPostagem]").val() == "" && $("#form input[name=link]").val() != "") || ($("#form input[name=dataPostagem]").val() != "" && $("#form input[name=link]").val() == "")){
//        loading("close");
//        alertar("Você deve informar a Data de Postagem e o Link juntos!", "alert");
//    }else{
    $.post("SalvarFotografia", $('#form').serialize(), function(data) {
            alertar("Registro salvo com sucesso", "success");
            $(".tablej").DataTable().ajax.reload();
        }).fail(function(xhr, textStatus, errorThrown) {
            if (xhr.status === "409") {
                alertar("Você não tem mais permissão para alterar este contrato, entre em contato com a Administração!", "alert");
            } else{
                alertar("Ocorreu algum erro ao tentar salvar o registro", "danger");
            }
        }).always(function() {
            $("#dialogEditar").modal("hide");
            loading("close");
        });
//    }
}

//function excluir() {
//    $("#dialogExcluir").modal("hide");
//    loading("open");
//    var ids = [];
//    $("input[name=selected]:checked").each(function(index) {
//        ids.push($(this).val());
//    });
//    $.post("ADM.ExcluirUsuario", {id: JSON.stringify(ids)}, function(data) {
//        alertar("Registros excluídos com sucesso", "success");
//        $(".tablej").DataTable().ajax.reload();
//    }).fail(function() {
//        alertar("Ocorreu algum erro ao tentar excluir os registros", "danger");
//    }).always(function() {
//        loading("close");
//    });
//}

function limparFormulario(){
    $("#form input[type=reset]").click();
    $("#form input[name=id]").val("");
    $("#novaobs").prop('required',false);
    $("#link").prop('required',false);
//    preencherClientes();
}