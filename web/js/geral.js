$(function () {
    $("html").bind("click", function (e) {
        if ($("#accountBox").css('display') == 'block') {
            if (e.target.id != 'contaUsuario' && e.target.id != 'contaTopoUsuarioLogado' && e.target.id != 'setaConta' && e.target.id != 'accountBox' && !$(event.target).closest('#accountBox').length) {
                expandirConta();
            }
        }
    });
    $(".justNumbers").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
    $(".justNumbersNegative").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [188, 110, 46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39 || event.keyCode == 109 || event.keyCode == 189)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
    try {
        $("input.valor").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
        $(".data1d").mask('99/99/9999');
        $(".data").mask('99/99/9999');
        $(".dataTotal").mask('99/99/9999');
        $(".cnpj").mask('99.999.999/9999-99');
        $(".cpf").mask('999.999.999-99');
        $("#cep").mask("99999-999");
        $("#telefone").mask("(99) 9999-9999");
    } catch (ex) {

    }
    try {
        $(".data1d").datepicker({
            minDate: "+1D",
            startDate: "+1D",
            todayBtn: "linked",
            calendarWeeks: false,
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior'
        });
        $(".data").datepicker({
            startDate: "+0d",
            todayBtn: "linked",
            calendarWeeks: false,
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior'
        });
        $(".dataTotal").datepicker({
            todayBtn: "linked",
            calendarWeeks: false,
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior'
        });
    } catch (x) {

    }
});

function dateFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null) {
        return '-';
    }
    var today = new Date(cellvalue);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return today = dd + '/' + mm + '/' + yyyy;
}

function loading(v) {
    if (v || v == 'open') {
        $(".preloader").show();
//        $("#loadingContainer").show();
    } else {
        $(".preloader").fadeOut()();
//        $("#loadingContainer").hide();
    }
}

function selecionarMenu(id) {
    $(".divItemMenu").removeClass('menuSelecionado');
    $("#" + id).addClass('menuSelecionado');
}
function selecionarSubMenu(id) {
    var idPai = $("#" + id).parent().attr("id").toString().replace("subMenu", "menu");
    $(".divItemMenu").removeClass('menuSelecionado');
    $("#" + idPai).addClass('menuSelecionado');
    $(".subMenu").removeClass("subMenuSelecionado");
    $("#" + id).addClass("subMenuSelecionado");
}

function expandirConta() {
    if ($("#setaConta").hasClass("icon-arrow-down-7")) {
        $("#setaConta").removeClass("icon-arrow-down-7");
        $("#setaConta").addClass("icon-arrow-up-6");
        $("#accountBox").slideDown();
    } else {
        $("#setaConta").removeClass("icon-arrow-up-6");
        $("#setaConta").addClass("icon-arrow-down-7");
        $("#accountBox").slideUp();
    }
}

function expandirMenu() {
//    var width = 300;
//    if ($("#divMenuEsquerdo").width() > 70) {
//        width = 70;
//        $("#logoMenu").fadeOut(250);
//        $(".divSubMenu").fadeOut(250);
//        $(".textoMenu").fadeOut(250);
//        $(".setasMenu").fadeOut(250);
//        $(".setasMenu").removeClass("glyphicon-menu-up");
//        $(".setasMenu").addClass("glyphicon-menu-down");
//    } else {
//        $(".setasMenu").fadeIn(750);
//        $(".textoMenu").fadeIn(750);
//        $("#logoMenu").fadeIn(750);
//    }
//    $("#divMenuEsquerdo").animate({
//        width: width
//    });
//    if (width == 300 && $("#paginaExpandida").val() != '') {
//        expandirSubMenu($("#paginaExpandida").val());
//        $("#paginaExpandida").val('');
//    }

    $("#expand-menu").click();
}
function createPost(action, JSON, target) {

    var html = "<form action='" + action + "'";
    if (typeof target != 'undefined') {
        html += " target='" + target + "'";
    }
    html += " method='POST'>";
    html += "<input type='hidden' name='data'>";
    html += "</form>";
    $("#containerPost").html(html);
    $("#containerPost form [name=data]").val(JSON);
    $("#containerPost form").submit();

}

function expandirSubMenu(id) {
    if ($("#divMenuEsquerdo").width() == 70) {
        $(".textoMenu").fadeIn(750);
        $("#logoMenu").fadeIn(750);
        $("#divMenuEsquerdo").animate({
            width: 300
        });
    }
    $(".setasMenu").fadeIn(750);
    if ($("#" + id).css("display") == 'none') {
        $("#" + id).slideDown("slow");
        $("#" + id + "Seta").removeClass("glyphicon-menu-down");
        $("#" + id + "Seta").addClass("glyphicon-menu-up");
    } else {
        $("#" + id).slideUp("slow");
        $("#" + id + "Seta").removeClass("glyphicon-menu-up");
        $("#" + id + "Seta").addClass("glyphicon-menu-down");
    }
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

//function alertar(msg, tipo) {
//    alert(msg);
//    var id = (Math.random() * 10000).toFixed(0);
//    $("#alertContainer").append("<div id='alert_" + id + "' class='alert " + tipo + " ' onclick=\"$(this).alert('close');\"><button style='border: none; background-color: inherit; opacity: 0.85; position:absolute; right: 5px; top: 7px; font-size: 22px; color: white;' title='Fechar' type='button' class='icon icon-close' data-dismiss='alert'></button>" + msg + "</div>");
//    var tempo = msg.length * 200;
//    if (tempo < 4000) {
//        tempo = 4000;
//    }
//    setTimeout("$('#alert_" + id + "').alert('close');", tempo);
//}
