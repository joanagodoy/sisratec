<%-- 
    Document   : index
    Created on : 23/06/2015, 15:45:42
    Author     : Usuario
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>
<script src="js/clientes.js" type="text/javascript"></script>
<!DOCTYPE html>

<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Clientes</h4>
                        <div class="ml-auto text-right">
<!--                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>-->
                            <button onclick="window.location='Cliente'" type="button" class="btn btn-primary btn-lg">Novo Cliente</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"> <a class="nav-link" data-toggle="tab" href="#adimplentes" onclick="listarClientes('adimplentes');" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Adimplentes</span> <span class="badge badge-success"></span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#inadimplentes" onclick="listarClientes('inadimplentes');" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Inadimplentes</span> <span class="badge badge-warning"></span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#advogados" onclick="listarClientes('advogados');" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Advogado</span> <span class="badge badge-danger"></span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#inativos" onclick="listarClientes('inativos');" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Inativos</span> <span class="badge badge-light"></span></a> </li>
<!--                        <li class="active"> <a class="nav-link" data-toggle="tab" href="#adimplentes" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Ativos</span> <span class="badge badge-success">57</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#inadimplentes"  role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Inadimplentes</span> <span class="badge badge-warning">57</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#advogados"  role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Advogado</span> <span class="badge badge-danger">57</span></a> </li>
                        <li -->
                    </ul>
                </div>
               
                
                <div class="tab-content">
                    <div id="adimplentes" class="tab-pane fade">
                        <table id='tableadimplentes' class="table tablej table-striped" style="cursor:pointer" width="100%">
                            <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Nome Fantasia</th>
                                  <th>Razão Social</th>
                                  <th>Responsável</th>
                                  <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="inadimplentes" class="tab-pane fade">
                      <table id='tableinadimplentes' class="table tablej table-striped" style="cursor:pointer" width="100%">
                        <thead>
                            <tr>
                                <!--<th></th>-->
                                <th>#</th>
                                <th>Nome Fantasia</th>
                                <th>Razão Social</th>
                                <th>Responsável</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div id="advogados" class="tab-pane fade">
                      <table id='tableadvogados' class="table tablej table-striped" style="cursor:pointer" width="100%">
                        <thead>
                            <tr>
                                <!--<th></th>-->
                                <th>#</th>
                                <th>Nome Fantasia</th>
                                <th>Razão Social</th>
                                <th>Responsável</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                    </div>
                    <div id="inativos" class="tab-pane fade">
                      <table id='tableinativos' class="table table-striped tablej" style="cursor:pointer" width="100%">
                        <thead>
                            <tr>
                                <!--<th></th>-->
                                <th>#</th>
                                <th>Nome Fantasia</th>
                                <th>Razão Social</th>
                                <th>Responsável</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                    </div>
              </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            
<%@include file="footer.jsp" %>