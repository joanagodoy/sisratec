<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<script type="text/javascript" src="js/fotografia.js?${version}"></script>

<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h2 class="page-title">Fotografias</h2>
                        <div class="ml-auto text-left">
<!--                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <div class="container-fluid">
                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control select2" id="status" onchange='onChangeStatus();'>
                            <option value="todos">Todos</option>
                        <c:forEach var="status" items="${statusFotografia}">
                            <option value="${status.id}">${status.descricao}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group" id="btAlterarDtEnvio">
                    <button style="width: 220px"  type="button" onclick="alterarDataEnvio();" class="btn btn-success">Gravar Envio p/ Editor</button> 
                </div>
                <div class="form-group" id="btEnviarEmail">
                    <button style="width: 220px"  type="button" onclick="enviarEmail();" class="btn btn-success">Enviar E-mail p/ Selecionados</button> 
                    <input onchange="" type="checkbox" class="" name="checkbox-cobranca" id="checkbox-cobranca" > Enviar Cobrança
                </div>
                <div class="form-group" id="btEnviarEmailImovel">
                    <button style="width: 220px"  type="button" onclick="enviarEmailImovel();" class="btn btn-success">Enviar E-mail</button> 
                    <br>
                    <b style="color: red;font: bold 10px Arial; padding-right: 4px">Para o e-mail ser enviado é necessário que todos os imóveis ativos do cliente estejam finalizados!</b>
                </div>
                <%--<c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2 || usuario.tipo.id == 5}">--%>

                <%--</c:if>--%>
                <!--<br><br>-->
                <table class="table tablej display" style="width: 100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>#</th>
                            <th>Cliente</th>           
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

<div class="modal fade" id='dialogEditar'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fotografia</h4>
            </div>
            <div class="modal-body">
                <form id='form' name="form" onsubmit="try {
                            salvar();
                        } catch (e) {
                        }
                        return false;">
                    <div class="form-group">
                        <label for="nome">Cliente</label>
                        <input disabled="" type="text" class="form-control" name="cliente" id="cliente">
                    </div>
                    <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2}">
                        <div class="form-group">
                            <label >Problema Imagens Postadas</label>
                            <input onchange="changeEditorProblema()" type="checkbox" class="" name="editorProblema" id="editorProblema" >
                        </div>
                        <div class="form-group">                        
                            <label for="nome">Link Ficha Google</label>
                            <input type="text" class="form-control" name="link" id="link">
                        </div> 
                    </c:if>
                    <c:if test="${usuario.tipo.id == 1 || usuario.tipo.id == 2 || usuario.tipo.id == 5}">
                    <div id="divConfirmacao">
                        <div class="form-group">
                            <label >Faltou Pontos de Vista</label>
                            <input onchange="changeFaltouPts()" type="checkbox" class="" name="faltouPts" id="faltouPts" >
                        </div>
                        <div class="form-group" >
                            <label for="data">Data Confirmação Entrega</label>
                            <input  type="text" readonly="" class="form-control datepickerBefore" name="dataConfirmacao" id="dataConfirmacao">
                        </div>
                        </div>
                    <div id="divPostagem">
                        <div class="form-group">
                            <label >Problema Imagens</label>
                            <input onchange="changeProblema()" type="checkbox" class="" name="problema" id="problema" >
                        </div>
                        <div class="form-group">
                            <label for="data">Data Postagem</label>
                            <input  type="text" readonly="" class="form-control datepickerBefore" name="dataPostagem" id="dataPostagem">
                        </div>
                    </div>
                    
                    </c:if>
                    <%--<c:if test="${usuario.tipo.id == 1 ||  usuario.tipo.id == 2 || usuario.tipo.id == 3}">--%> 
                    <div class="form-group">
                        <label for="data">Data Envio para Editor</label>
                        <input  type="text" readonly="" class="form-control datepickerBefore" name="dataEnvio" id="dataEnvio">
                    </div>
                    <%--</c:if>--%>
                    <div class="form-group">                        
                        <label for="nome">Observação</label>
                        <textarea style="height: 100px" disabled="" id="obs" name="obs" class="form-control"></textarea>  
                    </div>       
                    <div class="form-group">                        
                        <label for="nome">Nova Observação</label>
                        <textarea id="novaobs" name="novaobs" class="form-control"></textarea>  
                    </div>       
                    <div class="form-group">
                        <label for="nome">Status</label>
                        <input disabled=""  type="text" class="form-control" name="statusFotografia" id="statusFotografia">
                    </div>
                    <input type="hidden" name="id" id="id">
                    <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                    <input type="reset" id="reset" style='display:none;'/>
                </form>
                <b id="aviso" style="color: red;font: bold 15px Arial; padding-right: 4px">Fotógrafo precisa atualizar Status!</b>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button id='btSalvar' type="button" class="btn btn-success" onclick='$("#form button[type=submit]").click();'>Alterar Status</button>
                <button style="display: none" id='btEnviarEmailNovamente' type="button" class="btn btn-primary" onclick='reenviarEmail()'>Enviar E-mail Novamente</button>
                <!--<button id='btConfirmar' type="button" class="btn btn-success" onclick='confirmarEntrega();'>Confirmar Entrega</button>-->
            </div>
        </div>
    </div>
</div>

<!--<div class="modal fade" id='dialogFotos'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fotos</h4>
            </div>
            <div class="modal-body">
                accept="image/*"
                <form action="EnviarFotoContrato" accept="image/*" class="form form-inline" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="loading('open');">
                    <input class="form-inline" style="display: inline-block;" name="file" required type="file">
                    <input type="hidden" class="form-inline" name="idContrato" id="idContrato" value="0">
                    <input type="submit" class=" form-inline btn btn-default" name="submitBtn" value="Enviar">
                </form>
                <iframe id="upload_target" name="upload_target" src="#" style="display:none;"></iframe> 
                <br>
                <div id="divFotos">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>-->

<%@include file="footer.jsp" %>