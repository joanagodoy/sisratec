<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<script type="text/javascript" src="js/regiao.js?${version}"></script>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h2 class="page-title">Região</h2>
                        <div class="ml-auto text-left">
<!--                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <div class="container-fluid">
                <table class="table tablej display" style="width:100%;">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Região</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

<div class="modal fade" id='dialogAssociacao'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cidades da Região : <span id="descricao"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="formCadastrarAssociacao">
                    <input type="hidden" id="regiao" name="regiao">
                </form>
                <div class="row">
                    <div class="col-md-6">
                        <div id="divSelectEstado" >
                            Estado  
                            <select onchange="reloadCidadesDisponiveis();" class="form-control selectpicker" name="uf" id="uf"  data-size="4" data-live-search="true" style="width: 205px;">
                                <c:forEach var="b" items="${estados}">
                                    <option value="${b.id}">${b.uf} - ${b.nome}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <br>
                        <table class="table display cidadesDisponiveis" style="cursor: default !important">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Cidades Disponíveis</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <br><br><br><br>
                        <table class="table display cidadesAssociadas" style="cursor: default !important" >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Cidades Associadas</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick='salvarAssociacao();'>Salvar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id='dialogEditar'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuário</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id='form' name="form" onsubmit="try {
                            salvar();
                        } catch (e) {
                        }
                        return false;">
<!--                    <div class="form-group">
                        <label for="nome">Usuário</label>
                        <input type="text" required="" class="form-control" name="usuario" id="usuario" placeholder="Usuário">
                    </div>-->
                    <div class="form-group">
                        <label for="nome">Descrição</label>
                        <input type="text" required="" class="form-control" name="descricao" id="descricao" placeholder="Descrição">
                    </div>                   
                    <div id="divAtivo" class="form-group">
                        <label for="telefone">Ativo</label>
                        <input type="checkbox" class="" name="ativo" id="ativo" >
                    </div>
                    <input type="hidden" name='id'>
                    <button type="submit"  style='display:none;' class="btn btn-default">Submit</button>
                    <input type="reset" id="reset" style='display:none;'/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick='$("#form button[type=submit]").click();'>Salvar</button>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>